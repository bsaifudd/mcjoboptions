evgenConfig.description = "EPOS inelastic minimum bias events for pileup, no filters. Broad decay widths for resonances."
evgenConfig.keywords = ["QCD", "minBias" , "SM"]
evgenConfig.contact  = [ "jeff.dandoy@cern.ch", "jan.kretzschmar@cern.ch" ]
evgenConfig.nEventsPerJob = 50000
evgenConfig.tune = "EPOS LHC"

include("Epos_i/Epos_Base_Fragment.py")

if hasattr(testSeq, "TestHepMC"):
      testSeq.TestHepMC.EnergyDifference = 5000.0

