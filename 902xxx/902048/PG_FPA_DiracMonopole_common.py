
if hasattr(runArgs, 'ecmEnergy'):
    ecm=runArgs.ecmEnergy*1000.
else:
  raise RuntimeError("No center-of-mass energy provided in the Gen_tf.py command, exiting now.")

MeVmass=float(monmass)*1000.
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = "Two magnetic monopoles generation for Mass=%s, Gcharge=%s in MC23" % (monmass,gcharge)
evgenConfig.keywords = ["exotic", "magneticMonopole", "singleParticle"]
evgenConfig.generators = ["ParticleGun"]
evgenConfig.contact = ["anlionti@cern.ch"]
evgenConfig.specialConfig = 'MASS=%s;GCHARGE=%s;preInclude=SimulationJobOptions/preInclude.Monopole.py' % (monmass,gcharge)



#--------------------------------------------------------------
# Configuration for ParticleGun
#--------------------------------------------------------------
include("ParticleGun/ParticleGun_Common.py")
import math, random
import ROOT
PI = math.pi
TWOPI = 2*math.pi
import ParticleGun as PG
PDG = 4110000
PG.MASSES[PDG] = MeVmass


class FPAMonopoleSampler(PG.ParticleSampler):
    "A special sampler with two _correlated_ particles."

    def __init__(self, monopoleMass):
        self.m = monopoleMass
        self.form = ROOT.TF1("fpa_mon_momentum", "exp((-4./[0])*( sqrt([1]*[1]+x*x) - [1]))", 0, 200*1000)
        omega = 73e3 * ecm / 5.02e6  # value used by MOEDAL for 5.02
        self.form.SetParameter(0, omega)
        self.form.SetParameter(1, self.m)
        self.rng = ROOT.TRandom3(int(runArgs.randomSeed))


    def shoot(self):
        "Return a vector of sampled particles"
        mom1 = self.form.GetRandom(0, 200*1000., self.rng)
        theta1 = self.rng.Uniform(0, PI)
        eta1= -math.log(math.tan(0.5*theta1))
        pz1 = math.cos(theta1)*mom1
        pt1 = math.sqrt(mom1*mom1 - pz1*pz1)
        phi1 = self.rng.Uniform(0, TWOPI)
        fourMom1 = ROOT.TLorentzVector()
        fourMom1.SetPtEtaPhiM(pt1, eta1, phi1, self.m)
        fourMom2 = ROOT.TLorentzVector()
        fourMom2.SetPtEtaPhiM(pt1, -eta1, phi1, self.m)
        fourMom2.RotateZ(PI)
        return [PG.SampledParticle(PDG, fourMom1), PG.SampledParticle(-PDG, fourMom2)]

genSeq.ParticleGun.sampler = FPAMonopoleSampler(monopoleMass=MeVmass)


#--------------------------------------------------------------
# Edit PDGTABLE.MeV with monopole mass
#--------------------------------------------------------------
ALINE1="M 4110000                          %s.E+03       +0.0E+00 -0.0E+00 Monopole        0" % (monmass)
ALINE2="W 4110000                          0.E+00         +0.0E+00 -0.0E+00 Monopole        0"

import os
import sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
    os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2

#--------------------------------------------------------------
# Edit G4particle_whitelist.txt with monopole
#--------------------------------------------------------------

ALINE1="4110000   mm  %s.E+03 (Mev/c) lepton %s" % (monmass,gcharge)
ALINE2="-4110000  mmbar  %s.E+03 (Mev/c) lepton -%s" % (monmass,gcharge)

import os
import sys

pdgmod = os.path.isfile('G4particle_whitelist.txt')
if pdgmod is True:
    os.remove('G4particle_whitelist.txt')
os.system('get_files -data G4particle_whitelist.txt')
f=open('G4particle_whitelist.txt','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2