from MadGraphControl.MadGraphUtils import *
from MCJobOptionUtils.JOsupport import get_physics_short


# general parameters
nevents     = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob
nevents    *= 1.2 # safety factor
lhe_version = 3

# process definition
process_definition = 'generate p p > t~ t~ NP=1 NPcpv=0, (t~ > b~ w- NP=0 NPcpv=0, w- > l- vl~ NP=0 NPcpv=0)\n'
model = "SMEFTsim_general_MwScheme_UFO-massless_sstt"
fixed_scale = 345. # ~ m(top)+m(top)

process = '''
import model ''' + model + '''
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define wdec = l+ l- vl vl~ j
''' + process_definition + '''
output -f
'''
process_dir = new_process(process)

# run card settings
settings = {
    'nevents'               : nevents,
    'maxjetflavor'          : 5,
    'pdlabel'               : 'lhapdf',
    'lhaid'                 : 262000,
    'use_syst'              : 'False',
    'ptj'                   : '0.0',
    'ptl'                   : '0.0',
    'etaj'                  : '-1.0',
    'etal'                  : '-1.0',
    'drjj'                  : '0.0',
    'drll'                  : '0.0',
    'drjl'                  : '0.0',
    'dynamical_scale_choice': '0',
    'fixed_ren_scale'       : 'True',
    'fixed_fac_scale'       : 'True',
    'scale'                 : fixed_scale,
    'dsqrt_q2fact1'         : fixed_scale,
    'dsqrt_q2fact2'         : fixed_scale,
}

modify_run_card(process_dir=process_dir,
                runArgs=runArgs,
                settings=settings)

# SM param card settings
params = dict()
params['mass'] = dict()
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['DECAY'] = dict()
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
   3.377000e-01   2   -1   2
   3.377000e-01   2   -3   4
   1.082000e-01   2  -11  12
   1.082000e-01   2  -13  14
   1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   6.382339e-03'

modify_param_card(process_dir=process_dir,params=params)

# EFT param card settings
params = dict() 
params['SMEFTcutoff'] = dict()
params['SMEFTcutoff']['1'] = '1.000000e+03'
base_cuu = 0.04
base_cqu1 = 0.1
base_cqu8 = 0.1
default_parameters={'cuuRe1313': base_cuu, 'cuuRe1323': base_cuu, 'cuuRe2323': base_cuu, 'cqu1Re1313': base_cqu1, 'cqu1Re1323': base_cqu1, 'cqu1Re2313': base_cqu1, 'cqu1Re2323': base_cqu1, 'cqu8Re1313': base_cqu8, 'cqu8Re1323': base_cqu8, 'cqu8Re2313': base_cqu8, 'cqu8Re2323': base_cqu8}
params['SMEFT']=default_parameters
modify_param_card(process_dir=process_dir,params=params)

all_operators = {
    'cuuRe1313':  ('SMEFT', 371),
    'cuuRe1323':  ('SMEFT', 374 ),
    'cuuRe2323':  ('SMEFT', 377),
    'cqu1Re1313': ('SMEFT', 783),
    'cqu1Re1323': ('SMEFT', 788),
    'cqu1Re2313': ('SMEFT', 798),
    'cqu1Re2323': ('SMEFT', 801),
    'cqu8Re1313': ('SMEFT', 828),
    'cqu8Re1323': ('SMEFT', 833),
    'cqu8Re2313': ('SMEFT', 843),
    'cqu8Re2323': ('SMEFT', 846),  
}

dict_op = {
    'cuu': ['cuuRe1313', 'cuuRe1323', 'cuuRe2323'],
    'cqu1': ['cqu1Re1313', 'cqu1Re1323', 'cqu1Re2313', 'cqu1Re2323'],
    'cqu8': ['cqu8Re1313', 'cqu8Re1323', 'cqu8Re2313', 'cqu8Re2323'],
}

wc_op = {
    'cuu': [0.02, 0.03, 0.04, 0.05],
    'cqu1': [0.05, 0.07, 0.1, 0.15],
    'cqu8': [0.1, 0.2, 0.25, 0.3],
}

wc_op_mixed = {
    'cuu': [0., 0.01, 0.02],
    'cqu1': [0., 0.05, 0.1],
    'cqu8': [0., 0.2, 0.3],
}


# convert operator values to text format
def value_to_string(value):
    string = "{:.2f}".format(abs(value)).replace(".","p")
    return string

def set_to_zero(name):
    out = ""
    for name_op in dict_op[name]:
        block = all_operators[name_op][0]
        idnumber = all_operators[name_op][1]
        out += "set "+ block + " " +str(idnumber) + " " + str(0.) + "\n"
    return out

# define reweighting points --> ON/OFF
reweight_commands = "change rwgt_dir rwgt\n"
reweight_commands += "change helicity False\n"
for operator in wc_op:
    for value in wc_op[operator]:
        reweight_commands += "launch --rwgt_info=" + operator + "_" + value_to_string(value) + "\n"
        if operator == 'cuu':
            reweight_commands += set_to_zero('cqu1') + set_to_zero('cqu8')
        elif operator == 'cqu1':
            reweight_commands += set_to_zero('cuu') + set_to_zero('cqu8')
        elif operator == 'cqu8':
            reweight_commands += set_to_zero('cuu') + set_to_zero('cqu1')
        else:
            print "ERROR operator not recognized"
        for name_op in dict_op[operator]:
            block = all_operators[name_op][0]
            idnumber = all_operators[name_op][1]
            reweight_commands += "set "+ block + " " +str(idnumber) + " " + str(value) + "\n"
#mixed rw
for uu_value in wc_op_mixed['cuu']:
    for qu1_value in wc_op_mixed['cqu1']:
        for qu8_value in wc_op_mixed['cqu8']:
            if uu_value == 0 and qu1_value == 0 and qu8_value == 0:
                continue
            if (uu_value == 0 and qu1_value == 0) or (uu_value == 0 and qu8_value == 0) or (qu1_value == 0 and qu8_value == 0):
                continue
            reweight_commands += "launch --rwgt_info=" + "cuu" + "_" + value_to_string(uu_value) + "_cqu1" + "_" + value_to_string(qu1_value) + "_cqu8" + "_" + value_to_string(qu8_value)+ "\n"
            for name_op in dict_op['cuu']:
                block = all_operators[name_op][0]
                idnumber = all_operators[name_op][1]
                reweight_commands += "set "+ block + " " + str(idnumber) + " " + str(uu_value) + "\n"
            for name_op in dict_op['cqu1']:
                block = all_operators[name_op][0]
                idnumber = all_operators[name_op][1]
                reweight_commands += "set "+ block + " " + str(idnumber) + " " + str(qu1_value) + "\n"
            for name_op in dict_op['cqu8']:
                block = all_operators[name_op][0]
                idnumber = all_operators[name_op][1]
                reweight_commands += "set "+ block + " " + str(idnumber) + " " + str(qu8_value) + "\n"

#an explicit mixed as an example
#reweight_commands += "launch --rwgt_info=cuu_0_cqu10p1_cqu80p1\n"
#for name_op in dict_op['cuu']:
#    block = all_operators[name_op][0]
#    idnumber = all_operators[name_op][1]
#    reweight_commands += "set "+ block + " " +str(idnumber) + " 0.\n"
#for name_op in dict_op['cqu1']:
#    block = all_operators[name_op][0]
#    idnumber = all_operators[name_op][1]
#    reweight_commands += "set "+ block + " " +str(idnumber) + " 0.1\n"
#for name_op in dict_op['cqu8']:
#    block = all_operators[name_op][0]
#    idnumber = all_operators[name_op][1]
#    reweight_commands += "set "+ block + " " +str(idnumber) + " 0.1\n"

            
# write to reweight card
reweight_card   = process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write(reweight_commands)
reweight_card_f.close()

print_cards()

generate(process_dir=process_dir,
         required_accuracy=0.001,
         runArgs=runArgs)

outputDS = arrange_output(process_dir=process_dir,
                          runArgs=runArgs,
                          saveProcDir=True,
                          lhe_version=lhe_version)

# Lepton filter
if not hasattr(filtSeq,"LeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()   
    filtSeq.LeptonFilter.Ptcut = 10000.0#MeV                         

evgenConfig.contact          = ['noemi.cavalli@cern.ch']
evgenConfig.generators       = ['MadGraph','EvtGen','Pythia8']
evgenConfig.description = 'MadGraph reweighted same-sign tbar-tbar EFT'
evgenConfig.keywords+=['Top']

check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#new lines to avoid crash when no pythia
#theApp.finalize()
#theApp.exit()
