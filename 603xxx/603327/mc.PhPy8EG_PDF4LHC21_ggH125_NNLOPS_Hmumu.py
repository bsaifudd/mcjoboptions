#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# uses LHE: mc23_13p6TeV.603288.Ph_PDF4LHC21_ggH125_NNLOPS_HZZ_batch2_LHE.evgen.TXT.e8557
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2muon", "mH125", "gluonFusionHiggs" ]
evgenConfig.process     = "ggH, H->mumu"
evgenConfig.description = "POWHEG+Pythia8 ggF NNLOPS production with A14 NNPDF2.3 tune, H->mumu"
evgenConfig.inputFilesPerJob = 100
evgenConfig.nEventsPerJob    = 50000
evgenConfig.generators  = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.contact          = [ 'sabidi@cern.ch', 'xinmeng.ye@cern.ch', 'michiel.jan.veen@cern.ch' ]

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2'  ]


#--------------------------------------------------------------
# H->mumu decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13']
