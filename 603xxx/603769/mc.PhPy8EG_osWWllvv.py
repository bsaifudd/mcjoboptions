#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 WW->llvv production with PDF4LHC21 PDF variations and A14 tune and mllmin4'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'WW' ]
evgenConfig.contact     = [ 'tairan.xu@cern.ch' ]
evgenConfig.generators       = [ 'Powheg', 'Pythia8', 'EvtGen' ]
#--------------------------------------------------------------
# Powheg WW setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_WW_Common.py')
PowhegConfig.decay_mode =  "w+ w- > l+ vl l'- vl'~"
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + list(range(260000, 260101)) + [27100] +  [14400] + [331700]
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands  += ['Powheg:NFinal = 2']

