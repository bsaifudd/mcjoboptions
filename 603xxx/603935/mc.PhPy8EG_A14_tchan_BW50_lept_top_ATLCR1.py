#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'POWHEG+Pythia8 single-top-quark t-channel (2->3) production (top),MadSpin, A14 tune + ATLAS CR1 settings.'
evgenConfig.keywords    = [ 'SM', 'top', 'lepton']
evgenConfig.contact     = [ 'lukas.kretschmann@cern.ch' ]
evgenConfig.inputFilesPerJob = 20
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------
# Powheg/Pythia matching
#--------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Colour reconnection model
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ["ColourReconnection:mode=1",
                            "MultipartonInteractions:pT0Ref = 1.89",
                            "MultipartonInteractions:expPow = 2.10",
                            "ColourReconnection:m0 = 2.17",
                            "ColourReconnection:junctionCorrection = 9.33",
                            "ColourReconnection:allowDoubleJunRem = off"
                           ]
