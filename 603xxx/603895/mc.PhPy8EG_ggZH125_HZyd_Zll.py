#---------------------------------------------------------------
# LHE files of ggH used as inputs 
# POWHEG+Pythia8 ggZH(Zinclusive), H-> Z+yd, Z-inclusive, mH=125GeV, m_yd=0
#---------------------------------------------------------------

# input LHE dataset:
# mc15_13TeV.345061.PowhegPythia8EvtGen_NNPDF3_AZNLO_ggZH125_HgamgamZinc.evgen.TXT.e5762
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 37

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py') 

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands  += ['Powheg:NFinal = 2']

#--------------------------------------------------------------
# H->Z+yd decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on', # BSM Higgs
                            '25:m0 = 125', # Higgs mass
                            '25:mWidth = 0.00407', # Higgs Width
                            '25:doForceWidth = on', # Higgs width
                            '25:onMode = off', # decay of Higgs
                            '25:addChannel = 1 1. 103 23 4900022', # H->Z+yd
                            '25:onIfMatch = 23 4900022',
                            '4900022:m0 = 0.0', # yd mass
                            '4900022:onMode = off', # yd decay off
                            '4900022:tau0 = off', # yd no decay to tau
                            '4900022:onIfAny = 12 14 16', # only neurinos ifdecay
                             ]
#--------------------------------------------------------------
# Lepon filter for Z->ll (for ll+yd final state)
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py') 
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 ggZH production: H->Z+yd"
evgenConfig.keywords    = [ 'BSM' , 'Higgs' , 'BSMHiggs' , 'ZHiggs' , 'Z' , 'darkPhoton' ]
evgenConfig.contact     = [ 'rachid.mazini@cern.ch' ]
evgenConfig.process = "gg->ZH, H->Z+yd"
