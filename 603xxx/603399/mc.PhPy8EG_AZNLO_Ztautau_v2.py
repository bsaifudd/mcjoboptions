# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 Ztautau production, AZNLO, new PDF selection"
evgenConfig.keywords = ["SM", "Z", "tau"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg","Pythia8"]
evgenConfig.nEventsPerJob = 10000
filterMultiplier = 1.1

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg Z_EW process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_Z_EW_Common.py")

PowhegConfig.decay_mode = "z > tau+ tau-"

include("PowhegControl_WorZ_AZNLO_LOEW.py")

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------    
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')
# this is needed to approximately reproduce low pT(V) in Pythia8.245 and Pythia8.3
genSeq.Pythia8.Commands += ["BeamRemnants:primordialKThard = 1.4"]

# next level of Photos
genSeq.Photospp.CreateHistory = True # restore Photospp_i default that changed in 22.6
genSeq.Photospp.ZMECorrection = True
genSeq.Photospp.WMECorrection = False
genSeq.Photospp.PhotonSplitting = True
genSeq.Photospp.WtInterference = 4.0 # increase Photos upper limit for ME corrections
