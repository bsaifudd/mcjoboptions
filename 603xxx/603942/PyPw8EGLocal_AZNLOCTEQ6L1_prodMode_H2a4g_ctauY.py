#--------------------------------------------------------------
# from #id   : 602106
# author     : E. Karentzos
# description: 
#   Production mode: ZH
#   Decay: g and light-quarks (d,u,s) = (1,2,3) => https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
#   generators: Pythia (POWHEG, https://www.pythia.org/latest-manual/Index.html)
#  
#   # Low Mass, m<5GeV and with a set of values [1,2,2.5,3,4,5]
#   DID: MGPy8EGLocal_AZNLOCTEQ6L1_H2a4g_ctauY
#   LOCALLY: 
#           Gen_tf.py --ecmEnergy=13600. --maxEvents=1200 --randomSeed=123456 --outputEVNTFile=evgen.root --jobConfig=100001 
#                     --inputGeneratorFile=mc23_13p6TeV.601395.Ph_PDF4LHC21_WpH125J_Wincl_MINLO_LHE.evgen.TXT.e8537/TXT.33691048._000924.tar.gz.1,mc23_13p6TeV.601395.Ph_PDF4LHC21_WpH125J_Wincl_MINLO_LHE.evgen.TXT.e8537/TXT.33691048._003232.tar.gz.1  
#   
#--------------------------------------------------------------


import os, sys, glob

for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

###Run Number Encoding and decoding
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short  # check get_physics_short : https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/MadGraphControl/python/MadGraphUtilsHelpers.py
print(get_physics_short())

## decode dataset short name, should be of the form MadGraphPythia8EvtGen_AU14NNPDF23LO_HVT_Agv1_VzWW_qqqq_m1000 (split by '_')
tokens = get_physics_short().replace(".py","").split('_')  # splits the filename of the *.py

#ctau = float(tokens[-1].split('ctau')[-1])  # tau0:lifetime
ctau = tokens[-1].split('ctau')[-1]  # tau0:lifetime
ma   = float(tokens[-3].split('a')[-1])

decayProcess = str(tokens[-2])
prodMode = str(tokens[-5])

# unify all ctau cases:
if ctau == "001" or ctau == "005"  : ctau = float(ctau)/100. 
elif ctau == "01" or ctau == "05"  : ctau = float(ctau)/10.
else: ctau = float(ctau)


print('#############################################################')
print('ma ='+str(ma))
print('ctau ='+str(ctau))
print('decayProcess ='+str(decayProcess))
print('tokens ='+str(tokens))
print('#############################################################')

if decayProcess == "4b":
    adecay= 5
elif decayProcess == "4d": 
    adecay= 1
elif decayProcess == "4u":
    adecay= 2
elif decayProcess == "4s":
    adecay= 3
elif decayProcess == "4c":
    adecay= 4
elif decayProcess == "4tau":
    adecay= 15
elif decayProcess == "4g":
    adecay= 21
else:
	print("Decay process not availabe. Recheck Job specific JO name or rewrite general JO code")

print('decayProcess ='+str(decayProcess))

if adecay==21:
	adecay1 = adecay
	adecay2 = adecay
else:
	adecay1 = adecay
	adecay2 = -adecay

print('decayProducts ='+str(adecay1)+', '+str(adecay2))

m_ma=ma-0.5                 # mass of a (a_mass)
p_ma=ma+0.5                 # 
width=(1.9732699E-13)/ctau  # Gamma width of the Breit-Wigner distribution, decay width of the particle in GeV
                            # here: hbar * c = 197.3 [eV][nm] ==> 1.9732699E-13 [GeV][mm] and thus divided over the tau0 (ctau) [1 mm]
                            #       Gamma [GeV] = hbar/tau [eV*s/s] = hbar * c / ctau [eV nm / mm] 
nProcess=1                  # initialisation
#safety = 1.1               # safety factor to account for filter efficiency
                            # The safety factor makes sure the generation step produces enough events to account for any losses due to filters and otherwise.
#nevents = runArgs.maxEvents * safety

if prodMode == "WpH":      
    nProcess = 0 
if prodMode == "WmH":
    nProcess = 1
if prodMode == "ZllH":      
    nProcess = 2 
if prodMode == "ggZllH":
    nProcess = 3
if prodMode == "VBF":
    nProcess = 4
print(str(prodMode), str(nProcess))


include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py') # Powheg+Pythia 8 - DY - AZNLO, CT10: Z-dilepton (e, mu, tau) range: 120-5000GeV
include('Pythia8_i/Pythia8_Powheg_Main31.py')

if nProcess == 2: #ZH
    genSeq.Pythia8.Commands += ['Powheg:NFinal = 3']                # Number of outgoing particles of POWHEG Born level process, i.e. not counting additional POWHEG radiation { (default = -1; minimum = -1) }
    genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']    # Option to switch on the dipole-recoil scheme
                                                                    # By default the recoil of an ISR emission is taken by the whole final state. The option gives an alternative approach with local recoils, where only one final-state parton takes the recoil of an emission
                                                                    # CAUTION: in other scripts: elif process=="ZH": genSeq.Pythia8.Commands += [ 'POWHEG:nFinal = 3'], Here?
else:
    genSeq.Pythia8.Commands += ['Powheg:NFinal = 2']
    genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# Higgs->aa at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',    # produces BSM Higgs
                            '35:m0 = 125',          # 
                            '35:mWidth = 0.00407',  # SM Higgs width (GeV) => From the relativistic Breit-Wigner distribution of the Higgs boson resonance with a width (gamma_H) of 4.1 MeV. 
                            '35:doForceWidth = on', # a flag applicable only for resonances, whereby it is possible to force resonances to retain their assigned width! 
                                                    # All particles with m0 above 20 GeV are by default initialized to be considered as resonances.
                            '35:onMode = off',      # decay of Higgs
                            '35:onIfMatch = 36 36', # h->aa (A0/H3, pseudo-scalar)
                            #'36:onMode = off',      # decay of aa, do I need also that one ?
                            '36:oneChannel = 1 1.0 101 {0} {1}'.format(adecay1,adecay2),    # the 1st oneChannel command could be followed by several subsequent addChannel ones, to build up a completely new decay table for an existing particle.
                                                                                            # id:oneChannel removes all decay channels of id and thus all previous changes in this decay table
                                                                                            # onMode bRatio meMode product1 product2
                                                                                            #   onMode: integer code for use or not of channel,
                                                                                            #   bRatio: the branching ratio of the channel
                                                                                            #   meMode: the mode of processing this channel, possibly with matrix elements
                                                                                            #   product(i): the identity code of the decay products, where i runs between 0 and multiplicity - 1. Trailing positions are filled with 0 ==> example a->mumu, a-> gg, etc.
                                                                                            #   multiplicity: the number of decay products of the channel. Can be at most 8
                            '36:m0=%.1f' % ma,              # nominal mass
                            '36:mMin=%.1f' %m_ma,           # the lower limit of the allowed mass range generated by the Breit-Wigner (in GeV). Has no meaning for particles without width, and would typically be 0 there
                            '36:mMax = %.1f' %p_ma,         # the upper limit of the allowed mass range generated by the Breit-Wigner (in GeV). If mMax < mMin then no upper limit is imposed. Has no meaning for particles without width, and would typically be 0 there.
                            '36:mWidth= %.7g' % width,      # the width Gamma of the Breit-Wigner distribution (in GeV)
                            '36:tau0 %.1f' % ctau,          # the nominal proper lifetime tau_0 (in [mm/c]). --> ctau is in [mm] though.
                            ]


genSeq.Pythia8.Commands = [i for i in genSeq.Pythia8.Commands if (("limitTau0" not in i) and ("tau0Max" not in i))] # ??
genSeq.Pythia8.Commands += [
                            'ParticleDecays:tau0Max = 100000.0',    # parm  ParticleDecays:tauMax   (default = 10.; minimum = 0.). The above tauMax, expressed in mm/c.
                                                                    # In order for this and the subsequent tests to work, a tau is selected and stored for each particle, whether in the end it decays or not.
                            'ParticleDecays:limitTau0 = off'        # When on, only particles with tau < tauMax are decayed.
                           ]

testSeq.TestHepMC.MaxTransVtxDisp = 100000000   #in mm  ==> vertex display
testSeq.TestHepMC.MaxVtxDisp = 100000000        #in mm


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
print('EVGEN configuration')
print('\t Selected nProcess: '+str(nProcess))

if nProcess==0:         
    if adecay==5:   #b
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-bbbarbbbar production"
        evgenConfig.process = "WpH, H->2a->4b, W->lv"
    elif adecay==15: #tau
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-tau+tau-tau+tau- production"
        evgenConfig.process = "WpH, H->2a->4tau, W->lv"
    elif adecay==1:  #d
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ddbarddbar production"
        evgenConfig.process = "WpH, H->2a->4d, W->lv"
    elif adecay==2:  #u
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-uubaruubar production"
        evgenConfig.process = "WpH, H->2a->4u, W->lv"
    elif adecay==3:  #s, ssbar = Phi meson
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ssbarssbar production"
        evgenConfig.process = "WpH, H->2a->4s, W->lv"
    elif adecay==4: #c, ccbar = J/psi meson
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ccbarccbar production"
        evgenConfig.process = "WpH, H->2a->4c, W->lv"
    elif adecay==21: #g
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-gggg production"
        evgenConfig.process = "WpH, H->2a->4g, W->lv"
elif nProcess==1:
    if adecay==5:   #b
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-bbbarbbbar production"
        evgenConfig.process = "WmH, H->2a->4b, W->lv"
    elif adecay==15: #tau
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-tau+tau-tau+tau- production"
        evgenConfig.process = "WmH, H->2a->4tau, W->lv"
    elif adecay==1:  #d
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ddbarddbar production"
        evgenConfig.process = "WmH, H->2a->4d, W->lv"
    elif adecay==2:  #u
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-uubaruubar production"
        evgenConfig.process = "WmH, H->2a->4u, W->lv"
    elif adecay==3:  #s, ssbar = Phi meson
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ssbarssbar production"
        evgenConfig.process = "WmH, H->2a->4s, W->lv"
    elif adecay==4: #c, ccbar = J/psi meson
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ccbarccbar production"
        evgenConfig.process = "WmH, H->2a->4c, W->lv"
    elif adecay==21: #g
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-gggg production"
        evgenConfig.process = "WmH, H->2a->4g, W->lv"
elif nProcess==2:   
    if adecay==5:   #b
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-bbbarbbbar production"
        evgenConfig.process = "ZH, H->2a->4b, Z->ll"
    elif adecay==15: #tau
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-tau+tau-tau+tau- production"
        evgenConfig.process = "ZH, H->2a->4tau, Z->ll"
    elif adecay==1:  #d
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ddbarddbar production"
        evgenConfig.process = "ZH, H->2a->4d, Z->ll"
    elif adecay==2:  #u
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-uubaruubar production"
        evgenConfig.process = "ZH, H->2a->4u, Z->ll"
    elif adecay==3:  #s, ssbar = Phi meson
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ssbarssbar production"
        evgenConfig.process = "ZH, H->2a->4s, Z->ll"        
    elif adecay==4: #c, ccbar = J/psi meson
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ccbarccbar production"
        evgenConfig.process = "ZH, H->2a->4c, Z->ll"
    elif adecay==21: #g
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-gggg production"
        evgenConfig.process     = "ZH, H->2a->4g, Z->ll"
        #evgenConfig.saveJets    = True
elif nProcess==3:
    if adecay==5:   #b
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-bbbarbbbar production"
        evgenConfig.process = "ggZH, H->2a->4b, Z->ll"
    elif adecay==15: #tau
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-tau+tau-tau+tau- production"
        evgenConfig.process = "ggZH, H->2a->4tau, Z->ll"
    elif adecay==1:  #d
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ddbarddbar production"
        evgenConfig.process = "ggZH, H->2a->4d, Z->ll"
    elif adecay==2:  #u
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-uubaruubar production"
        evgenConfig.process = "ggZH, H->2a->4u, Z->ll"
    elif adecay==3:  #s, ssbar = Phi meson
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ssbarssbar production"
        evgenConfig.process = "ggZH, H->2a->4s, Z->ll"
    elif adecay==4: #c, ccbar = J/psi meson
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ccbarccbar production"
        evgenConfig.process = "ggZH, H->2a->4c, Z->ll"
    elif adecay==21: #g
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-gggg production"
        evgenConfig.process = "ggZH, H->2a->4g, Z->ll"
elif nProcess==4:
    if adecay==5:   #b
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-bbbarbbbar production"
        evgenConfig.process = "VBF, H->2a->4b"
    elif adecay==15: #tau
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-tau+tau-tau+tau- production"
        evgenConfig.process = "VBF, H->2a->4tau"
    elif adecay==1:  #d
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ddbarddbar production"
        evgenConfig.process = "VBF, H->2a->4d"
    elif adecay==2:  #u
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-uubaruubar production"
        evgenConfig.process = "VBF, H->2a->4u"
    elif adecay==3:  #s, ssbar = Phi meson
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ssbarssbar production"
        evgenConfig.process = "VBF, H->2a->4s"
    elif adecay==4: #c, ccbar = J/psi meson
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-ccbarccbar production"
        evgenConfig.process = "VBF, H->2a->4c"
    elif adecay==21: #g
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-gggg production"
        evgenConfig.process = "VBF, H->2a->4g"

evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125"]
evgenConfig.contact     = [ 'efstathios.karentzos@cern.ch' ]

