#--------------------------------------------------------------
# Pythia A14 tune
#-------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

# Decay to diphoton
genSeq.Pythia8.Commands  += [ '25:onMode = off', 
                              '25:onIfMatch = 22 22', 
                              'TimeShower:mMaxGamma = 90',         #Increase maximum allowed gamma* mass -n.b no interferece between gamma * and Z)
                              'TimeShower:nGammaToQuark = 0',     #Turn off gamma* to quarks
                              'TimeShower:nGammaToLepton = 2']    #Turn off gamma* to taus



# Repeat time showers in Pythia8 to improve efficiency
from Pythia8_i.Pythia8_iConf import HllgamRepeatTimeShower
hllgamRepeatTimeShower = HllgamRepeatTimeShower( name = "HllgamRepeatTimeShower" ) 
ToolSvc += hllgamRepeatTimeShower
genSeq.Pythia8.CustomInterface = hllgamRepeatTimeShower


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description     = 'POWHEG+Pythia8 ttH production with A14 NNPDF2.3 tune, H->yystar'
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "diphoton" ]
evgenConfig.contact     = [ 'tong.qiu@cern.ch' ]
evgenConfig.generators  = [ 'Powheg','Pythia8' ]
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob    = 20000

