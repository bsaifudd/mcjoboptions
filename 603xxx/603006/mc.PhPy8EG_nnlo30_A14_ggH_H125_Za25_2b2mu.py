#--------------------------------------------------------------
# Use LHE files as input
#--------------------------------------------------------------

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )
    
#--------------------------------------------------------------
# Defining the function to extract parameters
#--------------------------------------------------------------
dict_pdgIds = {}
dict_pdgIds["b"]   = 5
dict_pdgIds["mu"]  = 13
dict_pdgIds["tau"] = 15
dict_pdgIds["g"]   = 21
dict_pdgIds["y"]   = 22

def getParameters():
    import re

    #--- Read parts of the job option
    jonamelist = jofile.rstrip(".py").split("_")
    tune = jonamelist[2]
    process = jonamelist[3]
    ma = float(jonamelist[5].split("a")[-1].replace("p", "."))
    decayChan = str(jonamelist[6])
    partFilter = None
    if len(jonamelist)>7:
        partFilter = str(jonamelist[7])

    #--- list of decays, e.g. [mu, tau] for 2mu2tau
    decayProducts = []
    for part in dict_pdgIds.keys():
        decay = re.findall("[1-4]%s" % part, decayChan)
        if len(decay)>0:
            decayProducts.append(decay[0][1:]) # remove the number in front of the letter
    process = re.sub(r'\d+', '', process)

    return tune, process, ma, decayChan, decayProducts, partFilter

#    MC15.999999.PowhegPy8EG_ggH_H125_a60a60_2mu2tau_[filtXXX].py
tune, process, ma, decayChan, decayProducts, partFilter = getParameters()
print("Parameters: ")
print(tune, process, ma, decayChan, decayProducts, partFilter)

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
tunelongname = ""
if tune == "A14":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
    tunelongname = "A14NNPDF23LO"
elif tune == "A14v1d":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Down_EvtGen_Common.py')
    tunelongname = "A14v1dNNPDF23LO"
elif tune == "A14v1u":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Up_EvtGen_Common.py')
    tunelongname = "A14v1uNNPDF23LO"
elif tune == "A14v2d":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Down_EvtGen_Common.py')
    tunelongname = "A14v2dNNPDF23LO"
elif tune == "A14v2u":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var2Up_EvtGen_Common.py')
    tunelongname = "A14v2uNNPDF23LO"
elif tune == "A14v3ad":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aDown_EvtGen_Common.py')
    tunelongname = "A14v3adNNPDF23LO"
elif tune == "A14v3au":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3aUp_EvtGen_Common.py')
    tunelongname = "A14v3auNNPDF23LO"
elif tune == "A14v3bd":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bDown_EvtGen_Common.py')
    tunelongname = "A14v3bdNNPDF23LO"
elif tune == "A14v3bu":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3bUp_EvtGen_Common.py')
    tunelongname = "A14v3buNNPDF23LO"
elif tune == "A14v3cd":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py')
    tunelongname = "A14v3cdNNPDF23LO"
elif tune == "A14v3cu":
    include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py')
    tunelongname = "A14v3cuNNPDF23LO"
else:
    print('ERROR: tune ' + tune + ' not found')

# Settings that match those used in Higgs Group
genSeq.Pythia8.Commands += [ 'POWHEG:pThard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg_Main31.py")
if process=="ggH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']
elif process=="VBF":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WpH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WmH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ggZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']


genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',
                            '35:m0 = 125',
                            '35:mWidth = 0.00407',
                            '35:doForceWidth = on',
                            'HiggsH2:coup2d = 0',
                            'HiggsH2:coup2u = 0',
                            'HiggsH2:coup2l = 0',
                            'HiggsH2:coup2Z = 0',
                            'HiggsH2:coup2W = 0',
                            'HiggsH2:coup2A3A3 = 0',
                            'HiggsH2:coup2H1H1 = 0',
                            'HiggsH2:coup2Hchg = 0',
                            'HiggsH2:coup2HchgW = 0',
                            'HiggsH2:coup2A3H1 = 0',
                            'HiggsH2:coup2H1Z = 0',
                            'HiggsA3:coup2H2Z = 1',
                            '23:onMode = off',
                            '23:onIfAny = 5',
                            '36:oneChannel = 1 1.0 0 13 -13',
                            '36:m0 = %.1f' % ma,
                            '36:mWidth = 0.00407', # narrow width
                            '36:doForceWidth = on',
                            '36:mMin %.1f' % (ma-0.5), #scalar mass
                            '36:mMax %.1f' % (ma+0.5), #scalar mass
                            '36:tau0 = 0', #scalarlife time
                            ]



#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "Z", "mH125"]

if process=="ggH" and (decayChan=="2b2mu" or decayChan=="2mu2b"):
    evgenConfig.description = "POWHEG+Pythia8 H+jet production with MiNLO and PDF4LHC21 variations, H->Za->bbmumu (mH=125 GeV) with Z->bb and a->mumu"
    evgenConfig.process     = "ggH H->Za->bbmumu"
    evgenConfig.contact     = [ 'yesenia@cern.ch' ]
    evgenConfig.tune        = tunelongname

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 6 # Multiple external LHE files as inputs

