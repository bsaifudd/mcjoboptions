
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 recoil-to-colour bblvlv_beta dilepton with all dilep flav combinations, hdamp 1.5 mtop"
evgenConfig.keywords = [ 'SM', 'top', 'WWbb', 'lepton']
evgenConfig.contact = ["katharin@cern.ch"]
# starting from nominal bb4l-dl LHE files (200 events per file, DSID: 602496)
evgenConfig.inputFilesPerJob = 50
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, BB4L UserHook and Py8 Splitting Kernel Var. Weights
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

# Py8 default recoil setting is recoil-to-colour, which is TimeShower:recoilStrategyRF = 1
# recoil-to-top (nominal setting in ATLAS bb4l sample) corresponds to TimeShower:recoilStrategyRF = 3
#genSeq.Pythia8.Commands += [ "TimeShower:recoilStrategyRF = 3" ]

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    print ('UserHook present')
    genSeq.Pythia8.UserHooks += ['PowhegBB4Ldlsl']

genSeq.Pythia8.Commands += ["POWHEG:veto=1"]
genSeq.Pythia8.Commands += ["POWHEG:vetoCount = 3"]
genSeq.Pythia8.Commands += ["POWHEG:pThard = 0"]
genSeq.Pythia8.Commands += ["POWHEG:pTemt = 0"]
genSeq.Pythia8.Commands += ["POWHEG:emitted = 0"]
genSeq.Pythia8.Commands += ["POWHEG:pTdef = 1"] #default 0
genSeq.Pythia8.Commands += ["POWHEG:nFinal = -1"]
genSeq.Pythia8.Commands += ["POWHEG:MPIveto = 1"] #default 0
genSeq.Pythia8.Commands += ["POWHEG:QEDveto = 1"] #default 0

genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:veto = 1"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:vetoQED = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:vetoDipoleFrame = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTpythiaVeto = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:ScaleResonance:veto = 0"]                                                                                                                           
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTminVeto = 1.2"] # change from default 0.8 to 1.2 to have same as in HERWIG setting


# Pythia8 shower weights are only available in release 2.26 and later.  The
# test below checks the Pythia8 release and also verifies that the Pythia_i
# tag is recent enought to store the shower weights.

if "ShowerWeightNames" in genSeq.Pythia8.__slots__.keys():
    print ("Initalizing Custom Shower Weights from job options following the example from Pythia8_ShowerWeights.py")
    # New Shower Weights which need to overwrite the existing ones so duplicate for now
    # Var3c - A14 tune variation
    # Red - reduced sqrt(2)
    # Def - default 2
    # Con - conservative 4
    # Splitting with default 2
    # \- g2gg, g2qq, q2qg, x2xg; where x is b,t if nFlavQ = 4
    # cNS - non-singular terms
    genSeq.Pythia8.Commands += ['UncertaintyBands:doVariations = on',
    "UncertaintyBands:List = {\
    Var3cUp isr:muRfac=0.549241,\
    Var3cDown isr:muRfac=1.960832,\
    isr:PDF:plus=1,\
    isr:PDF:minus=2,\
    isrRedHi isr:muRfac=0.707,\
    fsrRedHi fsr:muRfac=0.707,\
    isrRedLo isr:muRfac=1.414,\
    fsrRedLo fsr:muRfac=1.414,\
    isrDefHi isr:muRfac=0.5,\
    fsrDefHi fsr:muRfac=0.5,\
    isrDefLo isr:muRfac=2.0,\
    fsrDefLo fsr:muRfac=2.0,\
    isrConHi isr:muRfac=0.25,\
    fsrConHi fsr:muRfac=0.25,\
    isrConLo isr:muRfac=4.0,\
    fsrConLo fsr:muRfac=4.0,\
    isr_cNS_dn isr:cNS=-2.0,\
    isr_cNS_up isr:cNS=2.0,\
    fsr_cNS_dn fsr:cNS=-2.0,\
    fsr_cNS_up fsr:cNS=2.0,\
    fsr_G2GG_muR_dn fsr:G2GG:muRfac=0.5,\
    fsr_G2GG_muR_up fsr:G2GG:muRfac=2.0,\
    fsr_G2QQ_muR_dn fsr:G2QQ:muRfac=0.5,\
    fsr_G2QQ_muR_up fsr:G2QQ:muRfac=2.0,\
    fsr_Q2QG_muR_dn fsr:Q2QG:muRfac=0.5,\
    fsr_Q2QG_muR_up fsr:Q2QG:muRfac=2.0,\
    fsr_X2XG_muR_dn fsr:X2XG:muRfac=0.5,\
    fsr_X2XG_muR_up fsr:X2XG:muRfac=2.0,\
    fsr_G2GG_cNS_dn fsr:G2GG:cNS=-2.0,\
    fsr_G2GG_cNS_up fsr:G2GG:cNS=2.0,\
    fsr_G2QQ_cNS_dn fsr:G2QQ:cNS=-2.0,\
    fsr_G2QQ_cNS_up fsr:G2QQ:cNS=2.0,\
    fsr_Q2QG_cNS_dn fsr:Q2QG:cNS=-2.0,\
    fsr_Q2QG_cNS_up fsr:Q2QG:cNS=2.0,\
    fsr_X2XG_cNS_dn fsr:X2XG:cNS=-2.0,\
    fsr_X2XG_cNS_up fsr:X2XG:cNS=2.0,\
    isr_G2GG_muR_dn isr:G2GG:muRfac=0.5,\
    isr_G2GG_muR_up isr:G2GG:muRfac=2.0,\
    isr_G2QQ_muR_dn isr:G2QQ:muRfac=0.5,\
    isr_G2QQ_muR_up isr:G2QQ:muRfac=2.0,\
    isr_Q2QG_muR_dn isr:Q2QG:muRfac=0.5,\
    isr_Q2QG_muR_up isr:Q2QG:muRfac=2.0,\
    isr_X2XG_muR_dn isr:X2XG:muRfac=0.5,\
    isr_X2XG_muR_up isr:X2XG:muRfac=2.0,\
    isr_G2GG_cNS_dn isr:G2GG:cNS=-2.0,\
    isr_G2GG_cNS_up isr:G2GG:cNS=2.0,\
    isr_G2QQ_cNS_dn isr:G2QQ:cNS=-2.0,\
    isr_G2QQ_cNS_up isr:G2QQ:cNS=2.0,\
    isr_Q2QG_cNS_dn isr:Q2QG:cNS=-2.0,\
    isr_Q2QG_cNS_up isr:Q2QG:cNS=2.0,\
    isr_X2XG_cNS_dn isr:X2XG:cNS=-2.0,\
    isr_X2XG_cNS_up isr:X2XG:cNS=2.0\
    }"]

    genSeq.Pythia8.ShowerWeightNames = ["Var3cUp",
                                        "Var3cDown",
                                        "isr:PDF:plus",
                                        "isr:PDF:minus",
                                        "isrRedHi", 
                                        "fsrRedHi", 
                                        "isrRedLo", 
                                        "fsrRedLo", 
                                        "isrDefHi", 
                                        "fsrDefHi", 
                                        "isrDefLo", 
                                        "fsrDefLo", 
                                        "isrConHi", 
                                        "fsrConHi", 
                                        "isrConLo", 
                                        "fsrConLo", 
                                        "isr_cNS_dn",                                    
                                        "isr_cNS_up",
                                        "fsr_cNS_dn",
                                        "fsr_cNS_up",
                                        "fsr_G2GG_muR_dn", 
                                        "fsr_G2GG_muR_up", 
                                        "fsr_G2QQ_muR_dn", 
                                        "fsr_G2QQ_muR_up", 
                                        "fsr_Q2QG_muR_dn", 
                                        "fsr_Q2QG_muR_up", 
                                        "fsr_X2XG_muR_dn", 
                                        "fsr_X2XG_muR_up", 
                                        "fsr_G2GG_cNS_dn", 
                                        "fsr_G2GG_cNS_up", 
                                        "fsr_G2QQ_cNS_dn", 
                                        "fsr_G2QQ_cNS_up", 
                                        "fsr_Q2QG_cNS_dn", 
                                        "fsr_Q2QG_cNS_up", 
                                        "fsr_X2XG_cNS_dn", 
                                        "fsr_X2XG_cNS_up", 
                                        "isr_G2GG_muR_dn", 
                                        "isr_G2GG_muR_up", 
                                        "isr_G2QQ_muR_dn", 
                                        "isr_G2QQ_muR_up", 
                                        "isr_Q2QG_muR_dn", 
                                        "isr_Q2QG_muR_up", 
                                        "isr_X2XG_muR_dn", 
                                        "isr_X2XG_muR_up", 
                                        "isr_G2GG_cNS_dn", 
                                        "isr_G2GG_cNS_up", 
                                        "isr_G2QQ_cNS_dn", 
                                        "isr_G2QQ_cNS_up", 
                                        "isr_Q2QG_cNS_dn", 
                                        "isr_Q2QG_cNS_up", 
                                        "isr_X2XG_cNS_dn", 
                                        "isr_X2XG_cNS_up"]

    # New settings
    genSeq.Pythia8.Commands += ['UncertaintyBands:nFlavQ = 4', # define X=bottom/top in X2XG variations
                                'UncertaintyBands:MPIshowers = on',
                                'UncertaintyBands:overSampleFSR = 10.0',
                                'UncertaintyBands:overSampleISR = 10.0',
                                'UncertaintyBands:FSRpTmin2Fac = 20',
                                'UncertaintyBands:ISRpTmin2Fac = 1']

