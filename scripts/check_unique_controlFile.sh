#!/bin/bash

# For coloured output
. scripts/helpers.sh

# Make sure that if a command fails the script will continue
set +e

unique=true

# Get the files modified in the last commit
modified=($(git diff-tree --name-only -r origin/master..HEAD --diff-filter=AM | grep -E ".*\.py" | grep -vE "mc\..*\.py"))

if [[ "${#modified[@]}" > 0 ]] ; then
  printInfo "Control files modified in the latest commit: ${#modified[@]}"
else
  printGood "No control files files modified since last commit"
  exit 0
fi

# Loop over modified files
for file in "${modified[@]}" ; do
  printInfo "Checking $file"
  filename=$(basename $file)
  # Find how many files have same name
  nfiles=$(find . -name $filename -type f | wc -l)
  if (( $nfiles > 1 )) ; then
    unique=false
    printError "ERROR: Duplicate file(s) found:"
    find . -name $filename -type f
    printInfo "If the files have exactly the same content, please only keep one physical file replacing the rest with symbolic links."
    printInfo "If the files have differences consider renaming the files that you added."
    printInfo 'You can check for differences with diff -w file1 file2'
  fi
done

if [ "$unique" = true ] ; then
  printGood "Result: SUCCESS"
  exit 0
else
  printError "Result: FAILURE"
  exit 1
fi
