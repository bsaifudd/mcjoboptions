from MadGraphControl.MadGraphUtils import *
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')

#Read filename of jobOptions to obtain: productionmode, ewkino mass.
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
tokens = get_physics_short().split('_')
try:
    _  = float(tokens[-1])
    event_filter = ''
except ValueError:
    event_filter = tokens[-1]
    tokens = tokens[:-1]
    
productionmode = str(tokens[2])
decaymode = None

gluinoMass = 1.e10
neutralinoMass = 1.e10
squarkMass = 1.e10
stopMass = 1.e10

if productionmode == 'GG':
    decaymode = str(tokens[3])
    gluinoMass = float(tokens[5])
    ktdurham = gluinoMass / 4.
elif productionmode == 'SqSq':
    decaymode = str(tokens[3])
    squarkMass = float(tokens[5])
    ktdurham = squarkMass / 4.
elif productionmode == 'TT':
    decaymode = str(tokens[3])
    stopMass = float(tokens[5])
    ktdurham = stopMass / 4.

branchingRatiosN1 = '''
#          BR          NDA       ID1       ID2       ID3
         0.25            3         1         2         3 #BR(N1 to d u s)
         0.25            3        -1        -2        -3 #BR(N1 to d~ u~ s~)
         0.25            3         1         4         3 #BR(N1 to d c s)
         0.25            3        -1        -4        -3 #BR(N1 to d~ c~ s~)
#'''
branchingRatiosFullHadW = '''
#          BR          NDA       ID1       ID2
          0.5            2        -1         2 #BR(W to d~ u)
          0.5            2        -3         4 #BR(W to s~ c)
'''
branchingRatiosFullHadZ = '''
#          BR          NDA       ID1       ID2
         0.20            2        -1         1 #BR(z to u~ u)
         0.20            2        -2         2 #BR(z to d~ d)
         0.20            2        -3         3 #BR(z to s~ s)
         0.20            2        -4         4 #BR(z to c~ c)
         0.20            2        -5         5 #BR(z to b~ b)
'''

if productionmode == 'GG':
    if decaymode == 'qqq':
        branchingRatiosG = '''
                #          BR          NDA       ID1       ID2       ID3
                          0.5            3         1         2         3 #BR(G to d u s)
                          0.5            3         1         4         3 #BR(G to d c s)
        '''
    elif decaymode == 'qqN1':
        neutralinoMass = float(tokens[6])
        branchingRatiosG = '''
                #          BR          NDA       ID1       ID2       ID3
                         0.25            3         1        -1   1000022
                         0.25            3         2        -2   1000022
                         0.25            3         3        -3   1000022
                         0.25            3         4        -4   1000022
        '''
    elif decaymode == 'qSq':
        squarkMass = float(tokens[6])
        branchingRatiosG = '''
                #          BR          NDA       ID1       ID2
                          0.0            2         1  -1000001 #BR(G to d sdl~)
                          1.0            2         1  -2000001 #BR(G to d sdr~)
        '''
        branchingRatiosSq = '''
                #          BR          NDA       ID1       ID2
                          1.0            2        -2        -3 #BR(sd to u~ s~)
        '''
    elif decaymode == 'tT':
        stopMass = float(tokens[6])
        branchingRatiosG = '''
                #          BR          NDA       ID1       ID2
                          0.5            2         6  -1000006 #BR(G to t t1~)
                          0.5            2        -6   1000006 #BR(G to t~ t1)
        '''
        branchingRatiosT = '''
                #          BR          NDA       ID1       ID2
                          1.0            2        -1        -3 #BR(T to d~ s~)
        '''
elif productionmode == 'SqSq':
    if decaymode == 'qN1':
        neutralinoMass = float(tokens[6])
        branchingRatiosSq = '''
                #          BR          NDA       ID1       ID2
                          1.0            2         1   1000022
        '''
        # default branchingRatiosN1
    
    if decaymode == 'TV':
        stopMass = float(tokens[6])
        branchingRatiosSqU = '''
                #          BR          NDA       ID1       ID2
                          1.0            2   1000006        23 #BR(t2 to t1 Z)
        '''
        branchingRatiosSqD = '''
                #          BR          NDA       ID1       ID2
                          1.0            2   1000006       -24 #BR(b1 to t1 W-)
        '''
        branchingRatiosT = '''
                #          BR          NDA       ID1       ID2
                          1.0            2        -1        -3 #BR(t1 to d~ s~)
        '''
        # default branchingRatiosFullHadW
        # default branchingRatiosFullHadZ
elif productionmode == 'TT':
    if decaymode == 'tN1':
        neutralinoMass = float(tokens[6])
        branchingRatiosT = '''
                #          BR          NDA       ID1       ID2
                          1.0            2         6   1000022
        '''
        # default branchingRatiosN1
        # default branchingRatiosFullHadW

if productionmode == 'GG':
    masses['1000021'] = gluinoMass
    decays['1000021'] = 'DECAY   1000021  1.0' + branchingRatiosG

    if decaymode in ['qqN1']:
        masses['1000022'] = neutralinoMass #N1
        decays['1000022'] = 'DECAY   1000022  1.0' + branchingRatiosN1
    if decaymode in ['qSq']:
        #masses['1000001'] = squarkMass #sdl as Sq
        masses['2000001'] = squarkMass #sdr as Sq
        #decays['1000001'] = 'DECAY   1000001  1.0' + branchingRatiosSq
        decays['2000001'] = 'DECAY   2000001  1.0' + branchingRatiosSq
    if decaymode in ['tT']:
        masses['1000006'] = stopMass
        decays['1000006'] = 'DECAY   1000006  1.0' + branchingRatiosT

elif productionmode == 'SqSq':
    if decaymode in ['TV']:
        masses['1000005'] = squarkMass #sb (b1)
        masses['2000006'] = squarkMass #st (t2)
        decays['1000005'] = 'DECAY   1000005  1.0' + branchingRatiosSqD # b1 > t1 W-
        decays['2000006'] = 'DECAY   2000006  1.0' + branchingRatiosSqU # t2 > t1 Z
    else:
        masses['2000001'] = squarkMass #sd (dr)
        decays['2000001'] = 'DECAY   2000001  1.0' + branchingRatiosSq

    if decaymode in ['qN1']:
        masses['1000022'] = neutralinoMass #N1
        decays['1000022'] = 'DECAY   1000022  1.0' + branchingRatiosN1
    if decaymode in ['TV']:
        masses['1000006'] = stopMass #st (t1)
        decays['1000006'] = 'DECAY   1000006  1.0' + branchingRatiosT

elif productionmode == 'TT':
    masses['1000006'] = stopMass
    decays['1000006'] = 'DECAY   1000006  1.0' + branchingRatiosT
    
    if decaymode in ['tN1']:
        masses['1000022'] = neutralinoMass #N1
        decays['1000022'] = 'DECAY   1000022  1.0' + branchingRatiosN1

if (productionmode, decaymode) in [('GG','tT'), ('SqSq','TV'), ('TT','tN1')]:
    #decays['23'] = 'DECAY   23  1.0' + branchingRatiosFullHadZ
    #decays['24'] = 'DECAY   24  1.0' + branchingRatiosFullHadW
    pass

njets = 2
# __And now production. Taking cues from higgsinoRPV.py generator__
process = '''
import model RPVMSSM_UFO
define susyall = go ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~ h01 h2 h3 h+ sve svm svt el- mul- ta1- er- mur- ta2- h- sve~ svm~ svt~ el+ mul+ ta1+ er+ mur+ ta2+ n1 n2 n3 n4 x1+ x2+ x1- x2-
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define squarks = ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define susyew = sve svm svt el- mul- ta1- er- mur- ta2- w- h- sve~ svm~ svt~ el+ mul+ ta1+ er+ mur+ ta2+ n1 n2 n3 n4 x1+ x2+ ta- x1- x2- ta+
define c1 = x1+ x1-
define q = j / g
'''

if decaymode in ['qSq']:
    process += '''
    define squarks = squarks / dr dr~
    '''
if decaymode in ['tT','TV']:
    process += '''
    define squarks = squarks / t1 t1~
    '''

if productionmode == 'GG':
    if decaymode == 'qSq':
        process += '''
        generate    p p > go go / squarks      RPV<=2 QED<=2  @1 #,  (go > d dr~, dr~ > u s)
        add process p p > go go j / squarks    RPV<=2 QED<=2  @2 #,  (go > d dr~, dr~ > u s)
        add process p p > go go j j / squarks  RPV<=2 QED<=2  @3 #,  (go > d dr~, dr~ > u s)
        '''
    elif decaymode == 'tT':
        process += '''
        generate    p p > go go / squarks     RPV<=2 QED<=2 @1
        add process p p > go go j / squarks   RPV<=2 QED<=2 @2
        add process p p > go go j j / squarks RPV<=2 QED<=2 @3
        '''
    else:
        process += '''
        generate    p p > go go / squarks     RPV=0 QED<=2 @1
        add process p p > go go j / squarks   RPV=0 QED<=2 @2
        add process p p > go go j j / squarks RPV=0 QED<=2 @3
        '''
elif productionmode == 'SqSq':
    if decaymode in ['TV']:
        process += '''
        define susy_notBT = susyall / b1 b1~ t1 t1~
        define susy_notT = susyall / t1 t1~ t2 t2~

        generate    p p > b1 b1~     / susy_notBT RPV=0 @1 #QED<=2
        add process p p > b1 b1~ j   / susy_notBT RPV=0 @2 #QED<=2
        add process p p > b1 b1~ j j / susy_notBT RPV=0 @3 #QED<=2
        
        add process p p > t2 t2~     / susy_notT RPV=0 @1 #QED<=2
        add process p p > t2 t2~ j   / susy_notT RPV=0 @2 #QED<=2
        add process p p > t2 t2~ j j / susy_notT RPV=0 @3 #QED<=2
        '''
        #
        #define notBTV = all / b1 b1~ t1 t1~ w+ w- z
        #define notTV = all / t1 t1~ t2 t2~ w+ w- z
        #
        #generate    p p > b1 b1~     / go sq_notBT QED<=2 RPV<=2,  (b1 > t1 q q  / notBTV QED=2, t1 > d~ s~), (b1~ > t1~ q q / notBTV QED=2, t1~ > d s) @1
        #add process p p > b1 b1~ j   / go sq_notBT QED<=2 RPV<=2,  (b1 > t1 q q  / notBTV QED=2, t1 > d~ s~), (b1~ > t1~ q q / notBTV QED=2, t1~ > d s) @2
        #add process p p > b1 b1~ j j / go sq_notBT QED<=2 RPV<=2,  (b1 > t1 q q  / notBTV QED=2, t1 > d~ s~), (b1~ > t1~ q q / notBTV QED=2, t1~ > d s) @3
        #
        #add process p p > t2 t2~     / go sq_notT QED<=2 RPV<=2,  (t2 > t1 q q  / notTV QED=2, t1 > d~ s~), (t2~ > t1~ q q / notTV QED=2, t1~ > d s) @1
        #add process p p > t2 t2~ j   / go sq_notT QED<=2 RPV<=2,  (t2 > t1 q q  / notTV QED=2, t1 > d~ s~), (t2~ > t1~ q q / notTV QED=2, t1~ > d s) @2
        #add process p p > t2 t2~ j j / go sq_notT QED<=2 RPV<=2,  (t2 > t1 q q  / notTV QED=2, t1 > d~ s~), (t2~ > t1~ q q / notTV QED=2, t1~ > d s) @3
    elif decaymode == 'qN1':
        process += '''
        define susy_notDN = susyall / dr dr~ n1
        generate    p p > dr dr~     / susy_notDN RPV=0 @1 #QED<=2
        add process p p > dr dr~ j   / susy_notDN RPV=0 @2 #QED<=2
        add process p p > dr dr~ j j / susy_notDN RPV=0 @3 #QED<=2
        '''
    else:
        process += '''
        define squarks = squarks / dr dr~
        generate    p p > dr dr~     / go squarks RPV=0 @1 #QED<=2
        add process p p > dr dr~ j   / go squarks RPV=0 @2 #QED<=2
        add process p p > dr dr~ j j / go squarks RPV=0 @3 #QED<=2
        '''
elif productionmode == 'TT':
    process += '''
    define susystrong = susystrong / t1 t1~
    generate    p p > t1 t1~     / susystrong RPV=0 QED=0 @1 # QED<=2
    add process p p > t1 t1~ j   / susystrong RPV=0 QED=0 @2 # QED<=2
    add process p p > t1 t1~ j j / susystrong RPV=0 QED=0 @3 # QED<=2
    '''
elif productionmode == 'C1N2N1' :
    process += '''
    define X = n1 c1
    define Y = n2 c1
    generate    p p > X Y     RPV=0 QED<=2 / susystrong @1
    add process p p > X Y j   RPV=0 QED<=2 / susystrong @2
    add process p p > X Y j j RPV=0 QED<=2 / susystrong @3
    '''
elif productionmode == 'C1N1' :
    process += '''
    generate    p p > n1 c1     RPV=0 QED<=2 / susystrong @1
    add process p p > n1 c1 j   RPV=0 QED<=2 / susystrong @2
    add process p p > n1 c1 j j RPV=0 QED<=2 / susystrong @3
    '''
elif productionmode == 'N1N2' :
    process += '''
    generate    p p > n1 n2     RPV=0 QED<=2 / susystrong @1
    add process p p > n1 n2 j   RPV=0 QED<=2 / susystrong @2
    add process p p > n1 n2 j j RPV=0 QED<=2 / susystrong @3
    '''
elif productionmode == 'C1C1' :
    process += '''
    generate    p p > x1+ x1-     RPV=0 QED<=2 / susystrong @1
    add process p p > x1+ x1- j   RPV=0 QED<=2 / susystrong @2
    add process p p > x1+ x1- j j RPV=0 QED<=2 / susystrong @3
    '''
elif productionmode == 'SmuonSmuon' :
    process += '''
    generate    p p > mur+ mur-     RPV=0 QED<=2 / susystrong @1
    add process p p > mur+ mur- j   RPV=0 QED<=2 / susystrong @2
    add process p p > mur+ mur- j j RPV=0 QED<=2 / susystrong @3
    '''
else:
    raise RunTimeError("ERROR: did not recognize productionmode arg")

if njets==0:
    process = '\n'.join([x for x in process.split('\n') if not "j" in x])
elif njets==1:
    process = '\n'.join([x for x in process.split('\n') if not "j j" in x])


if '1L20' in event_filter:
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()
    filtSeq.LeptonFilter.Ptcut  = 20*GeV
    evt_multiplier = 3.

evgenConfig.contact = ["jmontejo@cern.ch"]
evgenConfig.keywords +=['SUSY', 'RPV']

if productionmode == 'GG' :
    evgenConfig.keywords += ['gluino']
elif productionmode == 'SqSq' :
    evgenConfig.keywords += ['squark']
elif productionmode == 'TT' :
    evgenConfig.keywords += ['stop']
else:
    evgenConfig.keywords += ['neutralino']

if productionmode == 'C1N2N1' :
    evgenConfig.description = 'C1N1, C1N2, N1N2, C1C1 ewk production'
elif productionmode == 'C1N1' :
    evgenConfig.description = 'chargino - neutralino1'
elif productionmode == 'N1N2' :
    evgenConfig.description = 'neutralino1 - neutralino2'
elif productionmode == 'C1C1' :
    evgenConfig.description = 'chargino+ - chargino-'
elif productionmode == 'GG' :
    evgenConfig.description = 'gluino - gluino'
elif productionmode == 'SqSq' :
    evgenConfig.description = 'squark - squark~'
elif productionmode == 'TT' :
    evgenConfig.description = 'stop - stop~'
#evgenConfig.description += 'Electroweak production, and decays via RPV LQD i33 coupling (i=2,3), m_N1 = m_C1 = m_N2 = %.1f GeV'%(masses['1000022'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]

if (productionmode, decaymode) in [('GG','tT'), ('SqSq','TV'), ('TT','tN1')]:
    genSeq.Pythia8.Commands += [
        "23:onMode = off", #switch off all Z decays
        "24:onMode = off", #switch off all W decays
        "23:onIfAny = 1 2 3 4 5", # switch on Z->qqbar
        "24:onIfAny = 1 2 3 4 5" # switch on W->qqbar
    ]
