#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = "MG5_aMC@NLO+Herwig7 tHjb, H->inv production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"  ]
evgenConfig.contact     = [ 'philipp.mogg@cern.ch']
evgenConfig.process = "tH125+jb, H->inv, t->all"
evgenConfig.nEventsPerJob = 5000
evgenConfig.inputFilesPerJob = 2
evgenConfig.generators       = [ "aMcAtNlo", "Herwig7", "EvtGen" ]
evgenConfig.tune = "H7-UE-MMHT2014"

# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.tune_commands()

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# only consider H->inv
Herwig7Config.add_commands("""
do /Herwig/Particles/h0:SelectDecayModes h0->Z0,Z0;
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/Z0:SelectDecayModes Z0->nu_e,nu_ebar; Z0->nu_mu,nu_mubar; Z0->nu_tau,nu_taubar;
do /Herwig/Particles/Z0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# Missing Et filter
#--------------------------------------------------------------
include('GeneratorFilters/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV
filtSeq.Expression = "MissingEtFilter"
