from MadGraphControl.MadGraphUtils import *

name = "Wc"
gridpack_mode=False

# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for filter efficiency
multiplier=18
nevents=multiplier*runArgs.maxEvents if runArgs.maxEvents>0 else 5500

if isPythia:
   parton_shower = 'PYTHIA8'
elif isHerwig7:
   parton_shower = 'HERWIGPP'
else:
   print "parton shower not specified"

print "parton shower ",parton_shower
cmd = "\n"
if useCKM:
    cmd+="import model loop_sm-ckm_c_mass\n"
else:
    cmd+="import model loop_sm-c_mass\n"

cmd += "define p = g u d s u~ d~ s~\n"

if We:
    cmd += "generate p p > e+ ve c~ [QCD]\n"
    cmd += "add process p p > e- ve~ c [QCD]\n"
elif Wmu:
    cmd += "generate p p > mu+ vm c~ [QCD]\n"
    cmd += "add process p p > mu- vm~ c [QCD]\n"
else:
    print "Lepton specied not specified"

cmd += "output -f"

process_dir = new_process(cmd)

import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
     'central_pdf':303200,
#    NNPDF30_nlo_as_0118, NNPDF30_nnlo_as_0118,NNPDF31_nnlo_as_0118, ATLAS-epWZ16-EIG, CT18NLO, CT18ANLO, MMHT2014nlo68clas118
     'pdf_variations':[303200,91400], 
# CT10
     'alternative_pdfs':[269000,270000,274000,14068,304400], 
     'scale_variations':[0.5,1,2],
     'alternative_dynamic_scales' : [1,2,4]  # 3 is the same as -1
} 
 
settings = {
           'parton_shower' : parton_shower,
           'maxjetflavor'  : 4,
           'ickkw'         : 0,
           'ptj'           : 0,
           'nevents':int(nevents)
           }


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)

evgenConfig.generators = ["aMcAtNlo"]
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)  

if isPythia:
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
    include("Pythia8_i/Pythia8_aMcAtNlo.py")
elif isHerwig7:
    evgenConfig.generators    += ["Herwig7"]
    include("Herwig7_i/Herwig72_LHEF.py")
    Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
    Herwig7Config.tune_commands()
    Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
    include("Herwig7_i/Herwig71_EvtGen.py")
    Herwig7Config.run()

