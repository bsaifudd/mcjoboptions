evgenConfig.description = "Sherpa Z/gamma* -> tau tau + 0,1,2j@NLO + 3,4,5j@LO with light-jet filter taking input from existing unfiltered input file."
evgenConfig.keywords = ["SM", "Z", "2tau", "jets", "NLO" ]
evgenConfig.contact  = [ "oldrich.kepka@cern.ch" ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 28

if runArgs.trfSubstepName == 'generate' :
   print ("ERROR: These JO require an input file.  Please use the --afterburn option")
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())


   #--------------------------------------------------------------
   # FILTERS
   #--------------------------------------------------------------
   include('GeneratorFilters/MultiLeptonFilter.py')
   ## Default cut params
   filtSeq.MultiLeptonFilter.Ptcut = 3500.
   filtSeq.MultiLeptonFilter.Etacut = 2.7
   filtSeq.MultiLeptonFilter.NLeptons = 2

   from GeneratorFilters.GeneratorFiltersConf import TauFilter
   tauLepFilter = TauFilter("tauLepFilter")

   filtSeq += tauLepFilter
   filtSeq.tauLepFilter.Ntaus = 2
   filtSeq.tauLepFilter.EtaMaxe = 2.7
   filtSeq.tauLepFilter.EtaMaxmu = 2.7
   filtSeq.tauLepFilter.EtaMaxhad = 2.7 # no hadronic tau decays
   filtSeq.tauLepFilter.Ptcute = 12000.0
   filtSeq.tauLepFilter.Ptcutmu = 12000.0

   include('GeneratorFilters/ChargedTrackFilter.py')
   filtSeq.ChargedTracksFilter.NTracks = 21
   filtSeq.ChargedTracksFilter.NTracksMax = -1
   filtSeq.ChargedTracksFilter.Ptcut = 500
   filtSeq.ChargedTracksFilter.Etacut= 2.5

   postSeq.CountHepMC.CorrectRunNumber = True

