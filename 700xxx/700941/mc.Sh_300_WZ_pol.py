include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/PDF4LHC21.py")

evgenConfig.description = "WZ polarization with 0,1j@NLO"
evgenConfig.keywords = ["SM", "diboson", "3lepton", "jets", "NLO" ]
evgenConfig.contact  = ["zhzhao@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

genSeq.Sherpa_i.RunCard="""

# collider setup

# settings matrix-element generation
ME_GENERATORS:
  - Comix
  - Amegic
  - OpenLoops
COMIX_DEFAULT_GAUGE: 0

# scale setting
SCALES: METS{0.25*sqr(91.19+80.38)}

# width 0 for the stable vector bosons in the hard matrix element
PARTICLE_DATA:
  24:
    Width: 0
  23:
    Width: 0

WIDTH_SCHEME: Fixed

# speed and neg weight fraction improvements
NLO_CSS_PSMODE: 2

# vector boson decays
HARD_DECAYS:
  Enabled: true
  Channels:
    23,11,-11: {Status: 2}
    23,13,-13: {Status: 2}
    24,12,-11: {Status: 2}
    24,14,-13: {Status: 2}
    -24,-12,11: {Status: 2}
    -24,-14,13: {Status: 2}
# settings for polarized cross sections 
  Pol_Cross_Section:
    Enabled: true
    Reference_System: [Lab, COM]

# PDG code:
#   23: Z, 24: W+, -24: W-
#   11: e, 13: mu, 15: tau; -11: e+, -13: mu+, -15: tau+
#   12: ve, 14: vmu, 16: vtau
# sherpa internal:
#   90:lepton, 91:neutrino, 92:fermion, 93:jet, 94:quark

# vector boson production part pp -> WZ
PROCESSES:
# leading order
- 93 93 -> 23 24 93{1}:
    Order: {QCD: 0, EW: 2}
    # NLO QCD corrections
    NLO_Mode: MC@NLO
    NLO_Order: {QCD: 1, EW: 0}
    ME_Generator: Amegic
    RS_ME_Generator: Comix
    Loop_Generator: OpenLoops


- 93 93 -> 23 -24 93{1}:
    Order: {QCD: 0, EW: 2}
    # NLO QCD corrections
    NLO_Mode: MC@NLO
    NLO_Order: {QCD: 1, EW: 0}
    ME_Generator: Amegic
    RS_ME_Generator: Comix
    Loop_Generator: OpenLoops

"""

