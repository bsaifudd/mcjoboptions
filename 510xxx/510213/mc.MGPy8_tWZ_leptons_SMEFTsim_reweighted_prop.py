selected_operators = ['cHQ1','cHQ3','cHl311','cll1221']

process_definition = 'set group_subprocesses False\ndefine wdec = u u~ c c~ d d~ s s~ l- l- vl vl~\n' # define inclusive W decay
process_definition = 'generate p p > t w- l+ l- QCD=1 QED=4 NPprop=2, (t > w+ b,  w+ > wdec wdec), (w- > wdec wdec) @0 NP=0 SMHLOOP=0\n'
process_definition+= 'add process p p > t~ w+ l+ l- QCD=1 QED=4 NPprop=2, (t~ > w- b~, w- > wdec wdec), (w+ > wdec wdec) @1 NP=0 SMHLOOP=0'

fixed_scale = 333.5 # ~ m(top)+m(W)+m(Z)

gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tW+ll, top model, inclusive, reweighted, no EFT vertices, propagator correction'
evgenConfig.nEventsPerJob=10000

include("Common_SMEFTsim_topmW_topX_tWZ_prop_reweighted.py")
