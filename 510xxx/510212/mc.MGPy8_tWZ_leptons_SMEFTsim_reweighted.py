selected_operators = ['ctGRe','ctWRe','ctBRe','cHQ1','cHQ3','cHt','cHl311','cll1221','cQl111','cQl122','cQl133','cQl311','cQl322','cQl333','cte11','cte22','cte33','cQe11','cQe22','cQe33','ctl11','ctl22','ctl33','ctGIm','ctWIm','ctBIm','cleQt1Re11','cleQt1Re22','cleQt3Re11','cleQt3Re22']

process_definition = 'set group_subprocesses False\ndefine wdec = u u~ c c~ d d~ s s~ l- l- vl vl~\n' # define inclusive W decay
process_definition = 'generate p p > t w- l+ l- QCD=1 QED=4 NP=1, (t > w+ b NP=0,  w+ > wdec wdec NP=0), (w- > wdec wdec NP=0) @0 NPprop=0 SMHLOOP=0\n'
process_definition+= 'add process p p > t~ w+ l+ l- QCD=1 QED=4 NP=1, (t~ > w- b~ NP=0, w- > wdec wdec NP=0), (w+ > wdec wdec NP=0) @1 NPprop=0 SMHLOOP=0'

fixed_scale = 333.5 # ~ m(top)+m(W)+m(Z)

gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tW+ll, top model, inclusive, reweighted, EFT vertices, no propagator correction'
evgenConfig.nEventsPerJob=5000

include("Common_SMEFTsim_topmW_topX_tWZ_reweighted.py")
