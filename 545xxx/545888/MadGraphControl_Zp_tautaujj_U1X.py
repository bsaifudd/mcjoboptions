# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
import re

# Some includes that are necessary to interface MadGraph with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Extract the mass of the Z' particle from the name of the file
joName = get_physics_short()
z_prime_mass = int(re.findall(r'\d+',re.findall(r'Zp\d+',joName)[0])[0])

# Number of events to produce
safety = 1.1 # safety factor to account for filter efficiency
nevents = runArgs.maxEvents * safety

# Here is where we define the commands that will be passed to MadGraph
# Import the physics model
process = """
import model U1XGeneric_UFO
"""

# Define the physics process to be simulated
process += """
generate p p > zp > ta+ ta- j j QCD=0 weighted<=8 $$ w+ w- h1 h2 / ta+ ta- l+ l- j 
"""

# This defines the MadGraph outputs
process += """
output -f
"""

# Define the process and create the run card from a template
process_dir = new_process(process)
settings = {'ickkw': 0, 
        'nevents':nevents,
        'mmll': 140,
        'mmjj': 500}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Set some values in the param card
# BSM particle masses
masses={'mzp':z_prime_mass,
        'mh2': 10000,
        'mn1': 10000,
        'mn2': 10000,
        'mn3': 10000}


# Create the param card and modify some parameters from their default values
modify_param_card(process_dir=process_dir,params={'MASS':masses})

# Do the event generation
generate(process_dir=process_dir,runArgs=runArgs)

# These details are important information about the JOs
evgenConfig.description = 'Generic U1X Z prime . m_Zprime = %s GeV' % (z_prime_mass)
evgenConfig.contact = [ "Diego Baron <diego.baron@cern.ch>" ]
evgenConfig.keywords += ['bsm', 'zprime', '2tau', 'vbf']

arrange_output(process_dir=process_dir, runArgs=runArgs)