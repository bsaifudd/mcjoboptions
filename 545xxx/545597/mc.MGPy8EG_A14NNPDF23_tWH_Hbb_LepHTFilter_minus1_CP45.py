parameters={                                                                                                              
    'yukawa':{
        'ymt':   '-1.725000e+02'},
    'frblock':{
        'cosa':  '0.7071068',
        'kSM':   '1.41421356'},
}

evgenConfig.nEventsPerJob=1000
include('MadGraphControl_tWH_CP_NLO_MEC.py')
evgenConfig.inputconfcheck = "tWH"
