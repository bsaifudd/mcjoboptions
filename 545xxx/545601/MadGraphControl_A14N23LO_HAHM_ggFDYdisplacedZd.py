from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
import subprocess
import os
import math

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
if hasattr(runArgs,'maxEvents'):
    nEvents = int(runArgs.maxEvents)
else:
    beamEnergy = 6800.
    nEvents = 10000

# Read filename of jobOptions to obtain two free parameters of the model: dark photon mass and kinetic mixing coupling epsilon
tokens = get_physics_short().split('_')

productionmode = str(tokens[3])
decaymode = str(tokens[4])
mZp = float(tokens[5])                    # MeV
ctau = float(tokens[6].replace("p", ".")) # m

# Mapping to the corresponding Zp width for epsilon=1
Zp_mass_vs_width = { 250: 1.0491e-03,
                     300: 1.3886e-03,
                     400: 2.1099e-03, 
                     500: 3.0310e-03,
                     600: 4.8631e-03,
                     800: 1.6899e-02,
                     1000: 7.7453e-03,
                     1200: 8.8523e-03,
                     1600: 1.7068e-02,
                     2000: 2.0336e-02,
                     3000: 3.0504e-02,
                     4000: 5.7895e-02,
                     6000: 9.3789e-02,
                     8000: 1.2624e-01,
                     10000: 1.6150e-01
                   }

# Constants.
c    = 2.99792458e8     # Speed of light (m/s).
hbar = 6.58211951e-25   # Reduced Planck's constant (GeV s).

# Get width from ctau
wZp = hbar*c/ctau

# Get epsilon 
epsilon = math.sqrt(wZp/Zp_mass_vs_width[mZp])

# Set model parameters
# kappa = 1e-3 and mhs = 1 TeV gives Br(h->ZdZd) ~ 1e-4 - 1e-5.

pcard_params = { "HIDDEN": { 'epsilon': str(epsilon), # kinetic mixing
                             'kap': '1e-3', # higgs mixing
                             'mzdinput': str(mZp*0.001), # dark photon mass
                             'mhsinput': '200.0' # dark higgs mass
                           },
                "HIGGS": { 'mhinput':'125.0'}, #higgs mass
                "DECAY": { 'wzp': str(wZp), 'wh':'Auto', 'whs':'Auto' } #auto-calculate decay widths and BR of H, t
                }

rcard_params = {'ebeam2': beamEnergy, 'ebeam1': beamEnergy, "nevents" : 1.1*nEvents,
                   'cut_decays':'F',
                   'ptj':'0',
                   'ptb':'0',
                   'pta':'0',
                   'ptl':'0',
                   'etaj':'-1',
                   'etab':'-1',
                   'etaa':'-1',
                   'etal':'-1',
                   'drjj':'0',
                   'drbb':'0',
                   'drll':'0',
                   'draa':'0',
                   'drbj':'0',
                   'draj':'0',
                   'drjl':'0',
                   'drab':'0',
                   'drbl':'0',
                   'dral':'0',
		   'time_of_flight':'1E-25'}


# Production.
process = '''
import model HAHM_variableMW_v3_UFO
define q = u d s
define q~ = u~ d~ s~
'''

if productionmode == 'ggFHZdZd' and decaymode == 'mumu':
    process += '''
    generate g g > h, (h > Zp Zp, Zp > mu+ mu-)
    '''
elif productionmode == 'ggFHZdZd' and decaymode == '2mu2e':
    process += '''
    generate g g > h, (h > Zp Zp, Zp > mu+ mu-, Zp > e+ e-)
    '''
elif productionmode == 'ggFHZdZd' and decaymode == '2mu2q':
    process += '''
    generate g g > h, (h > Zp Zp, Zp > mu+ mu-, Zp > q q~)
    '''
elif productionmode == 'DYppZd' and decaymode == 'mumu':
    process += '''
    generate p p > Zp j, (Zp > mu+ mu-)
    '''
else:
    raise RunTimeError("ERROR: did not recognize productionmode arg")

process += '''
output -f
'''


# goenerating events in MG
process_dir = new_process(process)
   
modify_run_card(process_dir=process_dir,settings=rcard_params) 
modify_param_card(process_dir=process_dir,params=pcard_params)
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)  

# Helper for resetting process number
check_reset_proc_number(opts)


# Shower PDF
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
# LO shower
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]
#relax the cuts on displaced vertices and non G4 particles
testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV


# Config
evgenConfig.keywords += ['exotic', 'BSMhiggs', 'longlived']
if productionmode == 'ggFHZdZd' :
    evgenConfig.description = 'displaced HAHM gg -> H -> 2Zp, Zp->2mu, mZp=%s, eps=%s, mH=125' % (mZp, epsilon)
    evgenConfig.process="dimuonDV_HAHMdisplaced_ggF_Run3"
elif productionmode == 'DYppZd' :
    evgenConfig.description = 'displaced minimal pp -> Zp + j, Zp->2mu, mZp=%s, eps=%s' % (mZp, epsilon)
    evgenConfig.process="dimuonDV_minimalDarkPhotondisplaced_ggF_Run3"
evgenConfig.contact  = ['kehang.bai@cern.ch']
evgenConfig.nEventsPerJob = nEvents



