from MadGraphControl.MadGraphUtils import * 
from MadGraphControl.MadGraphParamHelpers import set_top_params
from MadGraphControl.DiagramRemoval import do_DRX
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

evgenConfig.nEventsPerJob = 500
nevents = int(50.*runArgs.maxEvents) if runArgs.maxEvents > 0 else int(50.*evgenConfig.nEventsPerJob)


DR_mode=1
mode=0
parton_shower='PYTHIA8'
decay_mode='inclusive'
zdecay="decay z > l+ l-"
if parton_shower=='PYTHIA8':
  parton_shower_name = "Pythia8"
elif parton_shower=='HERWIGPP':
  parton_shower_name = "Herwig7"


if decay_mode=='dilepton':
  tandw_decays = """decay t > w+ b, w+ > l+ v
 decay t~ > w- b~, w- > l- v~
 decay w+ > l+ v
 decay w- > l- v~"""

elif decay_mode=='inclusive':
  tandw_decays = """decay t > w+ b, w+ > all all
 decay t~ > w- b~, w- > all all
 decay w+ > all all
 decay w- > all all"""


gen_process = """
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define w = w+ w-
define ttbar = t t~
generate p p > ttbar w Z [QCD]
output -f
"""
process_dir = new_process(gen_process)

set_top_params(process_dir,mTop=172.5,FourFS=False)



#Call DR code
do_DRX(DR_mode,process_dir)
    
run_settings = { 
    'nevents'    : nevents,
    'lhe_version':'3.0',
    'maxjetflavor'  : 5,
    'parton_shower' : parton_shower,
    'fixed_ren_scale' : "False",
    'fixed_fac_scale' : "False",
    'fixed_QES_scale' : "False",
    'mll_sf'        : -1,
    'req_acc' :0.001,
}

# Set up the process

# Set up the run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

madspin_card_loc=process_dir+'/Cards/madspin_card.dat'                                                                                                                                   
mscard = open(madspin_card_loc,'w')  
mscard.write("""
  set max_weight_ps_point 500
  set BW_cut 50
 set seed {}
 # specify the decay for the final state particles
 define q = u d s c b
 define q~ = u~ d~ s~ c~ b~
 define l+ = e+ mu+ ta+
 define l- = e- mu- ta-
 define v = ve vm vt
 define v~ = ve~ vm~ vt~
 {}
 {}
 # running the actual code
 launch""".format(runArgs.randomSeed,tandw_decays,zdecay))
mscard.close()


# Generate the events
generate(process_dir=process_dir,runArgs=runArgs)

# Remember to set saveProcDir to FALSE before sending for production!!
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)


#### Shower
## run Pythia8 on-the-fly -----------------------------------------------------
## Provide config information
evgenConfig.generators += ["aMcAtNlo", parton_shower_name]
evgenConfig.description = "MG5aMCatNLO/MadSpin/"+parton_shower_name+" tWZ_Ztoll "+decay_mode+" DR"+str(DR_mode)+", DR applied only to MG code"
evgenConfig.keywords    = ["SM","top"]
evgenConfig.contact     = ["andrej.saibel@cern.ch", "sabidi@cern.ch" ,"chiara.arcangeletti@cern.ch"]

if parton_shower=='PYTHIA8':
  include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
  ## Enable MG5_aMC@NLO LHEF reading in Pythia8
  include("Pythia8_i/Pythia8_LHEF.py")
  #evgenConfig.generators += ["aMcAtNlo"]

  #aMC@NLO default Pythia8 settings from http://amcatnlo.web.cern.ch/amcatnlo/list_detailed2.htm#showersettings
  #plus MEC fix see https://arxiv.org/pdf/2308.06389.pdf
  genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",
                              "SpaceShower:pTmaxFudge = 1",
                              "SpaceShower:MEcorrections = off",
                              "TimeShower:pTmaxMatch = 1",
                              "TimeShower:pTmaxFudge = 1",
                              "TimeShower:MEcorrections = on",
                              "TimeShower:MEextended    = off",
                              "TimeShower:globalRecoil = on",
                              "TimeShower:limitPTmaxGlobal = on",
                              "TimeShower:nMaxGlobalRecoil = 1",
                              "TimeShower:globalRecoilMode = 2",
                              "TimeShower:nMaxGlobalBranch = 1.",
                              "TimeShower:weightGluonToQuark=1.",
                              "Check:epTolErr = 1e-2" ]

  include("GeneratorFilters/FourLeptonInvMassFilter.py")
  filtSeq.FourLeptonInvMassFilter.MinPt   = 4.*GeV
  filtSeq.FourLeptonInvMassFilter.MaxEta  = 3.
  filtSeq.FourLeptonInvMassFilter.MinMass = 100.*GeV 
  filtSeq.FourLeptonInvMassFilter.MaxMass = 170.*GeV

  include("GeneratorFilters/FourLeptonMassFilter.py")
  filtSeq.FourLeptonMassFilter.MinPt           = 4000.
  filtSeq.FourLeptonMassFilter.MaxEta          = 4.
  filtSeq.FourLeptonMassFilter.MinMass1        = 40000.
  filtSeq.FourLeptonMassFilter.MaxMass1        = 14000000.
  filtSeq.FourLeptonMassFilter.MinMass2        = 8000.
  filtSeq.FourLeptonMassFilter.MaxMass2        = 14000000.
  filtSeq.FourLeptonMassFilter.AllowElecMu     = False
  filtSeq.FourLeptonMassFilter.AllowSameCharge = False

  include("GeneratorFilters/MultiLeptonFilter.py")
  filtSeq.MultiLeptonFilter.Ptcut    = 8000.
  filtSeq.MultiLeptonFilter.Etacut   = 3.
  filtSeq.MultiLeptonFilter.NLeptons = 3

elif parton_shower=='HERWIGPP':
  evgenConfig.tune = "H7.2-Default"
  # initialize Herwig7 generator configuration for showering of LHE files                                                                                               
  include("Herwig7_i/Herwig72_LHEF.py")

  # configure Herwig7 
  Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
  Herwig7Config.tune_commands()
  Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile+".events", me_pdf_order="NLO")


  # add EvtGen                                                                                                                                                                                                                
  include("Herwig7_i/Herwig7_EvtGen.py")

  # run Herwig7                                                                                                                                                                                                          
  Herwig7Config.run()



