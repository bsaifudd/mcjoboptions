from os import environ
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import *
import fileinput

include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')

#Read filename of jobOptions to obtain: productionmode, ewkino mass.
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
tokens = get_physics_short().split('_')

LepBR = False

if 'LepBR' in tokens:   LepBR = True;           tokens.remove('LepBR')

if ('maxmix' in tokens or "maxm" in tokens): 
    evgenLog.info('Running with (50%/50%) stopL/R mixing')
    tokens.remove('maxmix')
    tokens.remove('maxm')
    common_mixing_matrix("stop_maxmix")
elif ('pureL'  in tokens or "pL" in tokens):  
    evgenLog.info('Running with pure stopL mixing')
    tokens.remove('pureL')
    tokens.remove('pL')
    common_mixing_matrix("stop_pureL")
elif ('pureR'  in tokens or "pR" in tokens): 
    evgenLog.info('Running with pure stopR mixing')
    tokens.remove('pureR')
    tokens.remove('pR')
    common_mixing_matrix("stop_pureR")
else:
    evgenLog.info('Running default setting (30%/70%) stopL/R mixing')

try:
    _  = float(tokens[-1])
    event_filter = ''
except ValueError:
    event_filter = tokens[-1]
    tokens = tokens[:-1]
    
productionmode = str(tokens[2])
if productionmode != 'TT':
    raise RunTimeError("ERROR: did not recognize productionmode {productionmode}")

decaymode       = str(tokens[3])
stopMass        = float(tokens[4])
neutralinoMass  = float(tokens[5])

if decaymode == 'bC1' or decaymode == 'tN1bC1':
    stopMass        = float(tokens[4])
    charginomass    = float(tokens[5])
    neutralinoMass  = float(tokens[6])

# Option
randomSeed = runArgs.randomSeed

masses['1000006'] = stopMass
masses['1000022'] = neutralinoMass #N1

njets = 2
# __And now production. Taking cues from higgsinoRPV.py generator__
process = '''
define susyall = go ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~ h01 h2 h3 h+ sve svm svt el- mul- ta1- er- mur- ta2- h- sve~ svm~ svt~ el+ mul+ ta1+ er+ mur+ ta2+ n1 n2 n3 n4 x1+ x2+ x1- x2-
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define c1 = x1+ x1-
define ll- = e- mu- ta-
define ll+ = e+ mu+ ta+
'''

process += '''
define exclude = susyall / t1 t1~ n1
generate    p p > t1 t1~     / exclude QED=0 @1
add process p p > t1 t1~ j   / exclude QED=0 @2
add process p p > t1 t1~ j j / exclude QED=0 @3
'''

# decay chains
if decaymode == 'tN1':
    randomSeed += 0
    if LepBR:
        decay_chains = [    "t1 > n1 t   , (t  > b w+  , w+ > ll+ vl)" ,
                            "t1~ > n1 t~ , (t~ > b~ w- , w- > ll- vl~)",
                            "w+ > ll+ vl",
                            "w- > ll- vl~"]
    else:
        decay_chains = [    "t1 > n1 t   , (t  > b w+  , w+ > fu  fd~)",
                            "t1~ > n1 t~ , (t~ > b~ w- , w- > fu~ fd)",
                            "w+ > fu  fd~",
                            "w- > fu~ fd"]
elif decaymode == 'bC1':
    masses['1000024'] = charginomass
    randomSeed += 3
    if LepBR:
        decay_chains = [    "t1 > x1+ b ,  (x1+ > w+ n1, w+ > ll+ vl)" , 
                            "t1~ > x1- b~, (x1- > w- n1, w- > ll- vl~)",
                            "x1+ > w+ n1",
                            "x1- > w- n1",
                            "w+ > ll+ vl",
                            "w- > ll- vl~"]
    else:
        decay_chains = [    "t1 > x1+ b  , (x1+ > w+ n1, w+ > fu  fd~)" , 
                            "t1~ > x1- b~, (x1- > w- n1, w- > fu~ fd)",
                            "x1+ > w+ n1",
                            "x1- > w- n1",
                            "w+ > fu  fd~",
                            "w- > fu~ fd"]
elif decaymode == 'tN1bC1':
    masses['1000024'] = charginomass
    randomSeed += 4
    if LepBR:
        decay_chains = [    "t1 > x1+ b ,  (x1+ > w+ n1, w+ > ll+ vl)" , 
                            "t1~ > x1- b~, (x1- > w- n1, w- > ll- vl~)",
                            "t1 > n1 t   , (t   > b w+ , w+ > ll+ vl)" ,
                            "t1~ > n1 t~ , (t~  > b~ w-, w- > ll- vl~)",
                            "x1+ > w+ n1",
                            "x1- > w- n1",
                            "w+ > ll+ vl",
                            "w- > ll- vl~"]
    else:
        decay_chains = [    "t1 > x1+ b  , (x1+ > w+ n1, w+ > fu  fd~)", 
                            "t1~ > x1- b~, (x1- > w- n1, w- > fu~ fd)" ,
                            "t1 > n1 t   , (t   > b w+ , w+ > fu  fd~)",
                            "t1~ > n1 t~ , (t~  > b~ w-, w- > fu~ fd)" ,
                            "x1+ > w+ n1",
                            "x1- > w- n1",
                            "w+ > fu  fd~",
                            "w- > fu~ fd"]
else:
    raise RunTimeError("ERROR: did not recognize decaymode {decaymode}")

evgenLog.info('Running w/ MadSpin option')
madspin_card='madspin_card_test.dat'
# fixEventWeightsForBridgeMode = True
mscard = open(madspin_card,'w')
mscard_to_write = '''
#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
set BW_cut 40                 # cut on how far the particle can be off-shell

#'''

if randomSeed:
    mscard_to_write += '''
set seed %i
    '''%(randomSeed)

mscard_to_write += '''
set spinmode full
# specify the decay for the final state particles
'''

for decay_chain in decay_chains:
    mscard_to_write += '''
    decay %s
    '''%(decay_chain)

mscard_to_write += '''
# running the actual code
launch
'''
mscard.write(mscard_to_write)
mscard.close()

if njets==1:
    process = '\n'.join([x for x in process.split('\n') if not "j j" in x])

if event_filter == '1L20orMET100' or event_filter == '1L20M100':
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()
    filtSeq.LeptonFilter.Ptcut  = 20*GeV
    if 'HEPMCVER' in environ and int(environ['HEPMCVER'])>=3:
        include ("GeneratorFilters/CreatexAODSlimContainers.py")
        createxAODSlimmedContainer("TruthMET",prefiltSeq)
        prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'
        from GeneratorFilters.GeneratorFiltersConf import xAODMETFilter
        xAODMissingEtFilter = xAODMETFilter("xAODMissingEtFilter")  
        filtSeq += xAODMissingEtFilter
        filtSeq.xAODMissingEtFilter.METCut = 100*GeV
        filtSeq.Expression = "LeptonFilter or xAODMissingEtFilter"
    elif 'HEPMCVER' in environ and int(environ['HEPMCVER']) < 3:
        from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
        filtSeq += MissingEtFilter()
        filtSeq.MissingEtFilter.METCut = 100*GeV
        filtSeq.Expression = "LeptonFilter or MissingEtFilter"
    masssplitting = stopMass - neutralinoMass
    evt_multiplier = 5. if masssplitting <= 10 else 3.
elif event_filter == '1L20':
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()
    filtSeq.LeptonFilter.Ptcut  = 20*GeV
    masssplitting = stopMass - neutralinoMass
    evt_multiplier = 5. if masssplitting <= 10 else 3.
elif event_filter == 'MET100':
    if 'HEPMCVER' in environ and int(environ['HEPMCVER'])>=3:
        include ("GeneratorFilters/CreatexAODSlimContainers.py")
        createxAODSlimmedContainer("TruthMET",prefiltSeq)
        prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'
        from GeneratorFilters.GeneratorFiltersConf import xAODMETFilter
        xAODMissingEtFilter = xAODMETFilter("xAODMissingEtFilter")  
        filtSeq += xAODMissingEtFilter
        filtSeq.xAODMissingEtFilter.METCut = 100*GeV
    elif 'HEPMCVER' in environ and int(environ['HEPMCVER']) < 3:
        from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
        filtSeq += MissingEtFilter()
        filtSeq.MissingEtFilter.METCut = 100*GeV
    evt_multiplier = 13.
elif event_filter == '2L15':
    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
    filtSeq += MultiElecMuTauFilter()
    filtSeq.MultiElecMuTauFilter.MinPt  = 15000.
    filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
    filtSeq.MultiElecMuTauFilter.NLeptons = 2
    filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0
    evt_multiplier = 50.


evgenConfig.contact = ["mrimoldi@cern.ch"]
evgenConfig.keywords +=['SUSY', 'stop', 'neutralino']
if productionmode == 'TT' :
    evgenConfig.description = 'TT pair production '

if productionmode == 'TT' and decaymode == 'tN1':
    evgenConfig.description += 'Stop pair production decay to qN1, m_T = %.1f GeV, m_N1 = %.1f GeV'%(masses['1000006'], masses['1000022'])
elif productionmode == 'TT' and (decaymode == 'bC1' or decaymode == 'tN1bC1'):
    evgenConfig.description += 'SUSY strong pair production, m_T = %.1f GeV, m_C1 = %.1f GeV , m_N1 = %.1f GeV'%(masses['1000006'], masses['1000024'],masses['1000022'])
    evgenConfig.keywords +=['chargino']
else:
    evgenConfig.description += 'SUSY strong pair production, m_parent = %.1f GeV, m_child = %.1f GeV'%(masses['1000006'], masses['1000022'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
