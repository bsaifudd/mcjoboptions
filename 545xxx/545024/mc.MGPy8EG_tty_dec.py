import os, shutil
from MadGraphControl.MadGraphParamHelpers import set_top_params
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *


# General settings
nevents=int(1.1*runArgs.maxEvents)

defs = """
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define uc = u c
define uc~ = u~ c~
define ds = d s
define ds~ = d~ s~
"""

mcdec = defs+"""
generate p p > t t~ QCD=2 QED=0, (t > l+ vl b a), (t~ > ds uc~ b~)\n
add process p p > t t~ QCD=2 QED=0, (t > l+ vl b), (t~ > ds uc~ b~ a)\n
add process p p > t t~ QCD=2 QED=0, (t > uc ds~ b a), (t~ > l- vl~ b~)\n
add process p p > t t~ QCD=2 QED=0, (t > uc ds~ b), (t~ > l- vl~ b~ a)\n
add process p p > t t~ QCD=2 QED=0, (t > l+ vl b a), (t~ > l- vl~ b~)\n
add process p p > t t~ QCD=2 QED=0, (t > l+ vl b), (t~ > l- vl~ b~ a)
"""

process = """
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
"""+mcdec+"""
output -f
"""

settings = {'lhe_version'   :'3.0',
           'maxjetflavor'  :5,
           'cut_decays'    :'T',
           'ptgmin'        :15.,
           'R0gamma'       :0.1,
           'xn'            :2,
           'epsgamma'      :0.1,
           'ptj'           :0.,
           'xptl'          :0.,#was: 20.  minimum pt for at least one charged lepton  (inclusive cut)
           'etal'          :-1.0,
           'etaa'          :5.0,
           'mll_sf'        :0.,
           'dynamical_scale_choice':'3',
           'nevents'    :nevents}

process_dir = new_process(process)
# Run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

set_top_params(process_dir,mTop=172.5,FourFS=False)

# Print cards
print_cards()
# set up
generate(process_dir=process_dir,runArgs=runArgs)
# run
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

# Go to serial mode for Pythia8
check_reset_proc_number(opts)

## pythia shower
keyword=['SM','top',  'photon']#'ttgamma',
evgenConfig.keywords += keyword
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_ttgamma_nonallhad_GamFromDecay'
evgenConfig.contact = ["jan.joachim.hahn@cern.ch", "arpan.ghosal@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
#include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

