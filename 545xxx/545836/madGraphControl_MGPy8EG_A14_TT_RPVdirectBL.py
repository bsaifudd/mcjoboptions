from MadGraphControl.MadGraphUtils import *

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

JOname = get_physics_short()
infoStrings = JOname.split("_")

evgenLog.info( "  jobConfig: %s  ", runArgs.jobConfig[0] )
evgenLog.info( "  stop mass: %s  ", infoStrings[4] )
evgenLog.info( "  stop ctau: %s  ", infoStrings[6].replace('p','.') )

gentype   = str(infoStrings[2])
decaytype = str(infoStrings[3])

## Setting masses from filename parsing
masses['1000006'] = float(infoStrings[4])
masses['1000005'] = 3.00000000E+05
masses['1000022'] = 100000.

## Converting lifetime from filename to width

lifetimeString = str(infoStrings[6])
stopLifetime = lifetimeString.replace("ns","").replace(".py","").replace("p",".")
hbar = 6.582119514e-16
stopWidth = hbar/float(stopLifetime)

evgenLog.info( "  stop lifetime, width: %f, %f  ", float(stopLifetime), stopWidth )

## Optional custom gluinoball probability in the filename

if len(infoStrings)>7:
	gluinoBallProbabilityString = str(infoStrings[7])
else:
	gluinoBallProbabilityString = "gball10"

gluinoBallProbability = float(gluinoBallProbabilityString.replace("gball",""))/100.
evgenLog.info( "  gluino-ball probability: %f  ", gluinoBallProbability )

## Defining the narrow width stop and its decay modes to b+mu
## ... Also need to define widths for SM particles for MG's add_lifetimes to work properly

decays['1000006']:"""
DECAY   1000006   %s  #stop1 decay
    1.0000000000    2          -13         5   # BR(~t_1 -> b mu )
"""%(stopWidth)

process = '''
import model MSSM_SLHA2-full
define susylq = ul ur dl dr cl cr sl sr
define susylq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~
generate p p > t1 t1~ $ go susylq susylq~ b2 t1 t2 b2~ t1~ t2~ @1
add process p p > t1 t1~ j $ go susylq susylq~ b2 t1 t2 b2~ t1~ t2~ @2
add process p p > t1 t1~ j j $ go susylq susylq~ b2 t1 t2 b2~ t1~ t2~ @3
'''

# Merging configuration in case of extra jets in ME

njets = 2

evgenConfig.contact  = [ "lucas.bezio@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel']
evgenConfig.description = 'stop direct pair production, st->b+mu in simplified model, m_stop = %s GeV, Long-lived with R-Hadron treatment.'%(masses['1000006'])
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,-1000006}"]

genSeq.Pythia8.Commands += ["Init:showChangedSettings = on"]
genSeq.Pythia8.Commands += ["Rhadrons:allow = on"]
genSeq.Pythia8.Commands += ["RHadrons:allowDecay = off"]
# Check if we are explicitly setting the gluinoball fraction; if not use the default value
if gluinoBallProbability!=-1:
    genSeq.Pythia8.Commands += ["RHadrons:probGluinoball = %f"%gluinoBallProbability]
else:
    genSeq.Pythia8.Commands += ["RHadrons:probGluinoball = 0.1"]
genSeq.Pythia8.Commands += ["Next:showScaleAndVertex = on"]
genSeq.Pythia8.Commands += ["Check:nErrList = 2"]

# Make sure that the RHadrons are allowed by TestHepMC
if 'testSeq' in dir():
    extra_pdgids_f = open('extra_pdgids.txt','w')
    from RHadrons.RHadronMasses import offset_options
    # Just allow all of them to have an anti-particle.  It's more future-proof.
    for a in offset_options:
        extra_pdgids_f.write(str(a)+'\n')
        extra_pdgids_f.write('-'+str(a)+'\n')
    extra_pdgids_f.close()
    Extras = [1009002]
    with open('extra_pdgids.txt','a') as f:
        for a in Extras:
            f.write(str(a)+'\n')
            f.write('-'+str(a)+'\n')
    
    testSeq.TestHepMC.G4ExtraWhiteFile='extra_pdgids.txt'
    testSeq.TestHepMC.UnknownPDGIDFile = 'extra_pdgids.txt'
evgenConfig.specialConfig = "LIFETIME={lifetime};preInclude=SimulationJobOptions/preInclude.RHadronsPythia8.py".format(lifetime=stopLifetime)

import os
if os.access('param_card.dat',os.R_OK):
    from RHadrons.RHadronMasses import get_Pythia8_commands
    genSeq.Pythia8.Commands += get_Pythia8_commands('param_card.dat',spectrum)
# Otherwise this is going to be done a different way by the simulation pre-include
