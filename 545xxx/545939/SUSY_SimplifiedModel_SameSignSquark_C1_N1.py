include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )
from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

# setup mass information
Msquark=float(JOName.split('_')[3])
masses['1000001'] = Msquark #sdown L
masses['1000002'] = Msquark #sup L
masses['1000003'] = Msquark #sstrange L
masses['1000004'] = Msquark #scharm L

masses['2000001'] = Msquark #sdown R
masses['2000002'] = Msquark #sup R
masses['2000003'] = Msquark #sstrange R
masses['2000004'] = Msquark #scharm R

masses['1000024'] = float(JOName.split('_')[4]) #chi1+
masses['1000022'] = float(JOName.split('_')[5]) #chi10

masses['1000021'] = float(JOName.split('_')[6]) #gluino
# setup gen & decay type

# setup process
process = ''' 
generate p p > susylq susylq  @1
add process p p > susylq~ susylq~  @2
add process p p > susylq susylq j  @3
add process p p > susylq~ susylq~ j  @4
''' 

# setup decay
decays["1000001"]="""DECAY   1000001     1.31836627E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2    -1000024      2   # BR(~d_L > ~chi_1- u)"""
decays["2000001"]="""DECAY   2000001     3.95203891E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2    -1000024      2   # BR(~d_R > ~chi_1- u)"""

decays["1000002"]="""DECAY   1000002     1.31301150E+01   # sup_L decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2     1000024      1   # BR(~u_L > ~chi_1+ d)"""
decays["2000002"]="""DECAY   2000002     5.45626401E+00   # sup_R decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2     1000024      1   # BR(~u_R > ~chi_1+ d)"""

decays["1000003"]="""DECAY   1000003     1.31836627E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2    -1000024      4   # BR(~s_L > ~chi_1- c)"""
decays["2000003"]="""DECAY   2000003     3.95203891E+00   # sstrange_R decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2    -1000024      4   # BR(~s_R > ~chi_1- c)"""

decays["1000004"]="""DECAY   1000004     1.31301150E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2     1000024      3   # BR(~c_L > ~chi_1+ s)"""
decays["2000004"]="""DECAY   2000004     5.45626401E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
1.00000000E-00         2     1000024      3   # BR(~c_R > ~chi_1+ s)"""
decays["1000024"]="""DECAY   1000024     7.00367294E-03   # chargino1+ decays                                                                                            
#          BR         NDA      ID1       ID2
1.00000000E+00         2     1000022      24  # BR(~chi_1+ -> ~chi_20  W+)
0.0000000              3     1000022      -11       12 # BR(~chi_1+ -> ~chi_20 e+ v)"""


# setup basic information
evgenConfig.contact  = [ "shiyi.liang@cern.ch" ]
evgenConfig.description = 'SUSY Simplified Model with squark production and decays via C1 with MadGraph/Pythia8, m_sq = %s GeV, m_C1 = %s GeV, m_N1 = %s GeV'%(masses['1000001'],masses['1000024'],masses['1000022'])
evgenConfig.keywords += ['simplifiedModel','squark']
run_settings['zerowidth_tchannel']=False

evt_multiplier=20
if Msquark<1000:
    evt_multiplier*=2
    if Msquark<600:
        evt_multiplier*=4

njets = 1
include( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

pythia = genSeq.Pythia8

pythia.Commands += ["24:mMin = 0.2"]
pythia.Commands += ["23:mMin = 0.2"]

evgenLog.info('Using LeptonPairFilter accepting SS lepton pairs with massive parents')
include("GeneratorFilters/xAODLeptonPairFilter_Common.py")
filtSeq.xAODLeptonPairFilter.NLeptons_Min = 0
filtSeq.xAODLeptonPairFilter.NLeptons_Max = 2 # NB: filter is always passed if nLeptons > nLeptons_Max
filtSeq.xAODLeptonPairFilter.Ptcut = 2000
filtSeq.xAODLeptonPairFilter.Etacut = 2.8
filtSeq.xAODLeptonPairFilter.NSFOS_Min = -1
filtSeq.xAODLeptonPairFilter.NSFOS_Max = -1
filtSeq.xAODLeptonPairFilter.NOFOS_Min = -1
filtSeq.xAODLeptonPairFilter.NOFOS_Max = -1
filtSeq.xAODLeptonPairFilter.NSFSS_Min = -1
filtSeq.xAODLeptonPairFilter.NSFSS_Max = -1
filtSeq.xAODLeptonPairFilter.NOFSS_Min = -1
filtSeq.xAODLeptonPairFilter.NOFSS_Max = -1
filtSeq.xAODLeptonPairFilter.NPairSum_Max = -1
filtSeq.xAODLeptonPairFilter.NPairSum_Min = 1
filtSeq.xAODLeptonPairFilter.UseSFOSInSum = False
filtSeq.xAODLeptonPairFilter.UseSFSSInSum = True
filtSeq.xAODLeptonPairFilter.UseOFOSInSum = False
filtSeq.xAODLeptonPairFilter.UseOFSSInSum = True

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
    else:
        genSeq.Pythia8.UserHook = "JetMergingaMCatNLO"
