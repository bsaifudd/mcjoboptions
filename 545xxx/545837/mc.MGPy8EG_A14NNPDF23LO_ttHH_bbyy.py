import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment

# Some includes that are necessary to interface MadGraph with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_EvtGen.py")    

evgenConfig.description     = "ttHH"
evgenConfig.keywords        = ['SM', 'top', 'ttVV', 'HH']
evgenConfig.contact         = ["arthur.lafarge@cern.ch"]
evgenConfig.generators      = ['aMcAtNlo', 'Pythia8']
evgenConfig.nEventsPerJob   = 50000

# Number of events to produce
safety = 3 # safety factor to account for filter efficiency
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*safety)
else: nevents = int(evgenConfig.nEventsPerJob*safety)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define w = w+ w-
generate p p > t t~ h h 
output -f"""

extras = {
           'nevents':int(nevents),
	}

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,
               settings=extras, runArgs=runArgs)

generate(process_dir=process_dir, runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs,lhe_version=3)

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.5 100 5 -5 ",  # bb decay
                             "25:addChannel = on 0.5 100 22 22 " ] # gammagamma decay

#--------------------------------------------------------------
# disable Dalitz decays (HH->yy*->yll)
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"] 

#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#--------------------------------------------------------------
# Generator Filters
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq.Expression = "HbbFilter and HyyFilter"