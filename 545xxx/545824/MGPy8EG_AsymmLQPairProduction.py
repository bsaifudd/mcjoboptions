from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

# grab the mass and yukawa coupling from the physics short
phys_short = get_physics_short().split('_')
mass = float(phys_short[-3][1:])
y = float(phys_short[-2][1:])/10.0



# LQ Mass
lq_mass = mass

# define the fixed renormalization/factorization scales to be mLQ/2
fact_ren_scale = lq_mass/2.

safety = 2.4 # !!!CHANGE WHEN IMPLEMENTING A FILTER!!!
nevents = runArgs.maxEvents * safety

# Make sure our LQ PDG IDs are known to TestHepMC since it doesn't like unknown pdgid's
pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""
-9000007
-9000006
-9000005
9000005
9000006
9000007
""")
pdgfile.close()


# Generate process of asymmetric pair of LQ's
# From a qq initial state
# They decay into a tau and top pair
process = """
import model LO_LQ_S1_R2_Bmass
define q = u d c s b
define top = t t~
define tau = ta+ ta-
generate q q > s1m13 r2p53, (s1m13 > top tau), (r2p53 > top tau)
output -f
"""

# create the process
process_dir = new_process(process)



# modify our run card
settings = {
        'nevents':nevents,
        'fixed_ren_scale': True, #fix factorization and renormalization scales to mLQ/2
        'fixed_fac_scale': True,
        'scale': fact_ren_scale,
        'dsqrt_q2fact1': fact_ren_scale,
        'dsqrt_q2fact2': fact_ren_scale,}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)



# Set masses in param card
masses={'9000005':lq_mass, #S1
        '9000006':lq_mass, #R2 (+5/3)
        '9000007':lq_mass #R2 (+2/3)
        }
lq_param_s1 = {'1': lq_mass}
lq_param_r2 = {'1': lq_mass}


# Leptoquark width calculations for param card
# Probably unnecessary as I believe madgraph does this automatically from the UFO, but it can't hurt
s1_ll_couplings = [0.] * 9
s1_rr_couplings = [0., 0., y, 0., 0., y, 0., 0., y]
ylls11x1, ylls11x2, ylls11x3, ylls12x1, ylls12x2, ylls12x3, ylls13x1, ylls13x2, ylls13x3 = s1_ll_couplings
yrrs11x1, yrrs11x2, yrrs11x3, yrrs12x1, yrrs12x2, yrrs12x3, yrrs13x1, yrrs13x2, yrrs13x3 = s1_rr_couplings

r2_rl_couplings = [0.] * 9
r2_lr_couplings = [0., 0., 0., 0., 0., 0., y, y, y]
yrlr21x1, yrlr21x2, yrlr21x3, yrlr22x1, yrlr22x2, yrlr22x3, yrlr23x1, yrlr23x2, yrlr23x3 = r2_rl_couplings
ylrr21x1, ylrr21x2, ylrr21x3, ylrr22x1, ylrr22x2, ylrr22x3, ylrr23x1, ylrr23x2, ylrr23x3 = r2_lr_couplings

ymt = 172.

import math
s1_width = (lq_mass*(2*abs(ylls11x1)**2 + 2*abs(ylls11x2)**2 + 2*abs(ylls11x3)**2 + 2*abs(ylls12x1)**2 + 2*abs(ylls12x2)**2 + 2*abs(ylls12x3)**2 + abs(ylls13x1)**2 + abs(ylls13x2)**2 + abs(ylls13x3)**2 + abs(yrrs11x1)**2 + abs(yrrs11x2)**2 + abs(yrrs11x3)**2 + abs(yrrs12x1)**2 + abs(yrrs12x2)**2 + abs(yrrs12x3)**2) + ((lq_mass**2 - ymt**2)**2*(abs(ylls13x1)**2 + abs(ylls13x2)**2 + abs(ylls13x3)**2 + abs(yrrs13x1)**2 + abs(yrrs13x2)**2 + abs(yrrs13x3)**2))/lq_mass**3)/(16.*math.pi)
r2p53_width = (lq_mass*(abs(ylrr21x1)**2 + abs(ylrr21x2)**2 + abs(ylrr22x1)**2 + abs(ylrr22x2)**2 + abs(ylrr23x1)**2 + abs(ylrr23x2)**2 + abs(yrlr21x1)**2 + abs(yrlr21x2)**2 + abs(yrlr21x3)**2 + abs(yrlr22x1)**2 + abs(yrlr22x2)**2 + abs(yrlr22x3)**2) + ((lq_mass**2 - ymt**2)**2*(abs(ylrr21x3)**2 + abs(ylrr22x3)**2 + abs(ylrr23x3)**2 + abs(yrlr23x1)**2 + abs(yrlr23x2)**2 + abs(yrlr23x3)**2))/lq_mass**3)/(16.*math.pi)
r2p23_width = (lq_mass*(abs(ylrr21x1)**2 + abs(ylrr21x2)**2 + abs(ylrr21x3)**2 + abs(ylrr22x1)**2 + abs(ylrr22x2)**2 + abs(ylrr22x3)**2 + abs(ylrr23x1)**2 + abs(ylrr23x2)**2 + abs(ylrr23x3)**2 + abs(yrlr21x1)**2 + abs(yrlr21x2)**2 + abs(yrlr21x3)**2 + abs(yrlr22x1)**2 + abs(yrlr22x2)**2 + abs(yrlr22x3)**2))/(16.*math.pi) + ((lq_mass**2 - ymt**2)**2*(abs(yrlr23x1)**2 + abs(yrlr23x2)**2 + abs(yrlr23x3)**2))/(16.*math.pi*lq_mass**3)

decays={'9000005':"""DECAY 9000005  %g #S1 leptoquark decay""" % s1_width,
        '9000006':"""DECAY 9000006  %g #R2 (5/3) leptoquark decay""" % r2p53_width,
        '9000007':"""DECAY 9000007  %g #R2 (2/3) leptoquark decay""" % r2p23_width}



# we want coupling with all quark generations (to avoid PDF suppressions for larger quarks)
# and third generation leptons (taus only, really, since we specify only right-chiral states)
# all other couplings are by default 0, no need to change them
yukr2lr={'3   1':ylrr23x1,
         '3   2':ylrr23x2,
         '3   3':ylrr23x3}
yuks1rr={'1   3':yrrs11x3,
         '2   3':yrrs12x3,
         '3   3':yrrs13x3}


# place these settings in the param card
modify_param_card(
        process_dir=process_dir,
        params={
                'LQPARAMR2':lq_param_r2,
                'LQPARAMS1':lq_param_s1,
                'MASS':masses,
                'DECAY':decays,
                'YUKS1RR':yuks1rr, 'YUKR2LR': yukr2lr
                }
        )


# with modified cards, we generate the process
generate(process_dir=process_dir,runArgs=runArgs)


# misc JO info/identifiers
evgenConfig.description = 'Leptoquark pair production. m_S1 = m_R2 = %s GeV' % (lq_mass)
evgenConfig.contact = [ "Casey Hampson <casey.richard.hampson@cern.ch>" ]
evgenConfig.keywords += ['BSM','exotic', 'scalar', 'leptoquark']


# output (not entirely sure what this does, really)
arrange_output(process_dir=process_dir, runArgs=runArgs)



# shower with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")



# lepton filter
include ("GeneratorFilters/CreatexAODSlimContainers.py")
createxAODSlimmedContainer("TruthElectrons",prefiltSeq)
prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'

createxAODSlimmedContainer("TruthMuons",prefiltSeq)
prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'

from GeneratorFilters.GeneratorFiltersConf import xAODMultiLeptonFilter

# we want the first particle to have pT of 20.0 GeV and eta of 2.7
filtSeq += xAODMultiLeptonFilter("LepFilter1")
filtSeq.LepFilter1.Ptcut = 20000.
filtSeq.LepFilter1.Etacut = 2.7
filtSeq.LepFilter1.NLeptons = 1

# we want the second particle to have a pT of 8.0 GeV and an eta of 2.7
filtSeq += xAODMultiLeptonFilter("LepFilter2")
filtSeq.LepFilter2.Ptcut = 8000.
filtSeq.LepFilter2.Etacut = 2.7
filtSeq.LepFilter2.NLeptons = 2

filtSeq.Expression = "(LepFilter1) and (LepFilter2)"
