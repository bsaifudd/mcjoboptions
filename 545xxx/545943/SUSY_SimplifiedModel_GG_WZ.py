# Generator transform pre-include
#  Gets us ready for on-the-fly SUSY SM generation
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()
mass_string = JOName.replace('.py','').split('WZ_')[1]

#--------------------------------------------------------------
# MadGrpah configuration
#--------------------------------------------------------------
masses['1000021'] = float( mass_string.split('_')[0] )  #gluino
masses['1000024'] = float( mass_string.split('_')[1] )  #chi1
masses['1000023'] = float( mass_string.split('_')[2] )  #chi20
masses['1000022'] = float( mass_string.split('_')[3] )  #chi10

process = '''
generate p p > go go $ susysq susysq~ @1
add process p p > go go j $ susysq susysq~ @2
add process p p > go go j j $ susysq susysq~ @3
'''
decays['1000021']="""DECAY 1000021 7.40992706E-02 # Wgo
##  BR         NDA      ID1       ID2       ID3
2.5000000e-01  3        1         -2        1000024
2.5000000e-01  3        2         -1        -1000024
2.5000000e-01  3        3         -4        1000024
2.5000000e-01  3        4         -3        -1000024"""

decays["1000024"]="""DECAY   1000024     7.00367294E-03   # chargino1+ decays 
#          BR         NDA      ID1       ID2
1.00000000E+00         2     1000023      24  # BR(~chi_1+ -> ~chi_20  W+)
0.0000000              3     1000023      -11       12 # BR(~chi_1+ -> ~chi_20 e+ v)"""
decays["1000023"] = """DECAY   1000023     9.37327589E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
1.00000000E+00         2     1000022      23  # BR(~chi_20 -> ~chi_10   Z )
0.0000000              3     1000022       11      -11 # BR(~chi_20 -> ~chi_10 e- e+) """

evgenConfig.contact  = [ "liuya@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','gluino']
evgenConfig.description = 'SUSY Simplified Model with gluino production and decays via sleptons with MadGraph/Pythia8, m_glu = %s GeV, m_C1 = %s GeV, m_N2 = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000024'],masses['1000023'],masses['1000022'])

njets=2

#--------------------------------------------------------------
# Pythia configuration
#--------------------------------------------------------------
# This comes after all Simplified Model setup files
evgenLog.info('Will use Pythia8...')

pythia = genSeq.Pythia8
pythia.Commands += ["24:mMin = 0.2"]
pythia.Commands += ["23:mMin = 0.2"]

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------

include( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

    
if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = guess" ]
    
    ### needed to use "guess" option of Pythia.
    if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
    else:
        genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'
