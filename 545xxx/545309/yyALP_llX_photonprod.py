import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={     
'central_pdf':13300,     
'pdf_variations': [13300,26400,26500,26100,26200],#None, #[26400,26500,26100,26200],     
'alternative_pdfs':[13300,26500,26400,26100,26200],#  None,# [26400,26500,26100,26200],     
'scale_variations':[0.5,1.,2.]
}

# Retrieve mass that we want both ALPs to have from the JO name
name_jobfile = jofiles
config_names = jofile.split('.')[1].split('_')
ms1 = float(config_names[2][3:].replace('p','.'))
ms2 = float(config_names[3][3:].replace('p','.'))
gcl = float(config_names[4][3:].replace('p','.'))
gca = float(config_names[5][3:].replace('p','.'))



# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
nevents=10*runArgs.maxEvents if runArgs.maxEvents>0 else 5500

process = """
    import model ALP/yyALP_UFO
    define p = a g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate a a > s1 s2, s1 > l+ l-
    output -f"""


process_dir = new_process(process) 
# Fetch default LO run_card.dat and set parameters 
settings = {'lhe_version':'3.0', 
            'cut_decays' :'F', 
            'lpp1'      : '2',
            'lpp2'      : '2',
            'fixed_fac_scale1' : 'F',
            'fixed_fac_scale2' : 'F',
            'nevents' : int(nevents) } 

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings) 

# Fetch default param_card.dat and set a different Higgs mass


#Parameter card
masses = {'901': ms1, #s1 mass
          '902' : ms2} #s2 mass

decays = {'901': 'auto'}

couplings = {'2': gcl,
             '3': gca}

params={}
params['MASS']=masses
params['DECAY']=decays
params['FRBLOCK']=couplings

modify_param_card(param_card_input=None,process_dir=process_dir,params=params)


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=False)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

evgenConfig.generators = ["MadGraph"]

runName='PROC_sm_0/run_01'  

#Add the pdgids to a .txt file
bonus_file = open('pdg_extras.dat','w')
bonus_file.write('901\n')
bonus_file.write('902\n')
bonus_file.close()
testSeq.TestHepMC.UnknownPDGIDFile='pdg_extras.dat'


include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_EE_Common.py")     
include("Pythia8_i/Pythia8_MadGraph.py")

from GeneratorFilters.GeneratorFiltersConf import ForwardProtonFilter
from AthenaCommon.SystemOfUnits import GeV

######################
# Proton filter side A
######################

filtSeq += ForwardProtonFilter("ProtonSideA")

filtSeq.ProtonSideA.xi_min = 0.00 
filtSeq.ProtonSideA.xi_max = 0.15 
filtSeq.ProtonSideA.beam_energy = 6500.*GeV 
filtSeq.ProtonSideA.pt_min = 0.0*GeV 
filtSeq.ProtonSideA.pt_max = 1.5*GeV
filtSeq.ProtonSideA.single_tagA = True

######################
# Proton filter side C
######################

filtSeq += ForwardProtonFilter("ProtonSideC")

filtSeq.ProtonSideC.xi_min = 0.00 
filtSeq.ProtonSideC.xi_max = 0.15 
filtSeq.ProtonSideC.beam_energy = 6500.*GeV 
filtSeq.ProtonSideC.pt_min = 0.0*GeV 
filtSeq.ProtonSideC.pt_max = 1.5*GeV
filtSeq.ProtonSideC.single_tagC = True

filtSeq.Expression = "ProtonSideA and ProtonSideC"

#-------------------------------------------------------------- 
# Evgen 
#-------------------------------------------------------------- 
evgenConfig.description = 'MadGraphPy8EG_yyALP'
evgenConfig.contact = ["maura.barros@cern.ch"]

