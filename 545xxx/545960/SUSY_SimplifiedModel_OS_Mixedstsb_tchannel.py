include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

# setup mass information
masses['1000005'] = float(JOName.split('_')[3]) #sbottom L
masses['1000006'] = float(JOName.split('_')[3]) #stop L
masses['1000023'] = float(JOName.split('_')[4]) #chi20
masses['1000024'] = float(JOName.split('_')[4]) #chi1+
masses['1000022'] = float(JOName.split('_')[5]) #chi10

# setup process
process = '''
import model MSSM_SLHA2-full
generate p p > t1 b1~
add process p p > t1 b1~ j
add process p p > t1~ b1
add process p p > t1~ b1 j
'''

decays["1000005"]="""DECAY   1000005     3.82023386E+00   # sbottom_L decays
#          BR         NDA      ID1       ID2      ID3    ID4
1.11111111E-01          4    -1000024     5      -11     12     # BR(~b_1 -- chi^-_1 b e+ nu_e)
1.11111111E-01          4    -1000024     5      -13     14     # BR(~b_1 -- chi^-_1 b mu+ nu_mu)
1.11111111E-01          4    -1000024     5      -15     16     # BR(~b_1 -- chi^-_1 b tau+ nu_tau)
3.33333333E-01          2     1000023     5                     # BR(~b_1 -- chi^0_2 b)
3.33333333E-01          2     1000022     5                     # BR(~b_1 -- chi^0_1 b)"""

decays["1000006"]="""DECAY   1000006     2.06904927E+00   # stop_L decays
#          BR         NDA      ID1       ID2     ID3     ID4
1.00000000E-01          4     1000023     5      -11     12     # BR(~t_1 -- ~chi_20 b e+ nu_e)
1.00000000E-01          4     1000023     5      -13     14     # BR(~t_1 -- ~chi_20 b mu+ nu_mu)
1.00000000E-01          4     1000023     5      -15     16     # BR(~t_1 -- ~chi_20 b tau+ nu_tau)
1.00000000E-01          4     1000022     5      -15     16     # BR(~t_1 -- ~chi_10 b e+ nu_e)
1.00000000E-01          4     1000022     5      -15     16     # BR(~t_1 -- ~chi_10 b mu+ nu_mu)
1.00000000E-01          4     1000022     5      -15     16     # BR(~t_1 -- ~chi_10 b tau+ nu_tau)
4.00000000E-01          2     1000024     5                     # BR(~t_1 -- ~chi_1+ b)"""

decays["1000024"]="""DECAY   1000024     1.48971412E-02   # chargino1+ decays
#          BR         NDA      ID1       ID2     ID3
3.33333333E-01          3     1000022     12     -11     # BR(~chi_1+ -- ~chi_10 nu_e  e+  )
3.33333333E-01          3     1000022     14     -13     # BR(~chi_1+ -- ~chi_10 nu_mu mu+ )
3.33333334E-01          3     1000022     16     -15     # BR(~chi_1+ -- ~chi_10 nu_tau tau+)"""

decays["1000023"] = """DECAY   1000023   2.09542688E-02   # neutralino2 decays
#          BR         NDA      ID1       ID2     ID3    
1.66666666E-01          3     1000022     11     -11     # BR(~chi_20 -- ~chi_10 e- e+)
1.66666666E-01          3     1000022     13     -13     # BR(~chi_20 -- ~chi_10 mu- mu+)
1.66666666E-01          3     1000022     15     -15     # BR(~chi_20 -- ~chi_10 tau- tau+)
1.66666666E-01          3     1000022     12     -12     # BR(~chi_20 -- ~chi_10 nu_e nu_e*)
1.66666666E-01          3     1000022     14     -14     # BR(~chi_20 -- ~chi_10 nu_mu nu_mu*)
1.66666666E-01          3     1000022     16     -16     # BR(~chi_20 -- ~chi_10 nu_tau nu_tau*)"""

tL = '0.999999'
tR = '0.001414'
bL = '0.999999'
bR = '0.001414'

param_blocks['usqmix']={}
param_blocks['usqmix']['1 1']='1.00000000E+00'
param_blocks['usqmix']['2 2']='1.00000000E+00'
param_blocks['usqmix']['3 3']=tL
param_blocks['usqmix']['3 6']=tR
param_blocks['usqmix']['4 4']='1.00000000E+00'
param_blocks['usqmix']['5 5']='1.00000000E+00'
param_blocks['usqmix']['6 3']='-'+tR
param_blocks['usqmix']['6 6']=tL

param_blocks['dsqmix']={}
param_blocks['dsqmix']['1 1']='1.00000000E+00'
param_blocks['dsqmix']['2 2']='1.00000000E+00'
param_blocks['dsqmix']['3 3']=bL
param_blocks['dsqmix']['3 6']=bR
param_blocks['dsqmix']['4 4']='1.00000000E+00'
param_blocks['dsqmix']['5 5']='1.00000000E+00'
param_blocks['dsqmix']['6 3']='-'+bR
param_blocks['dsqmix']['6 6']=bL

# Off-diagonal chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='0.00E+00'
param_blocks['VMIX']['1 2']='1.00E+00'
param_blocks['VMIX']['2 1']='1.00E+00'
param_blocks['VMIX']['2 2']='0.00E+00'
# Off-diagonal chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='0.00E+00'
param_blocks['UMIX']['1 2']='1.00E+00'
param_blocks['UMIX']['2 1']='1.00E+00'
param_blocks['UMIX']['2 2']='0.00E+00'
# Neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
param_blocks['NMIX']={}
param_blocks['NMIX']['1 1']=' 0.00E+00'   # N_11 bino
param_blocks['NMIX']['1 2']=' 0.00E+00'   # N_12
param_blocks['NMIX']['1 3']=' 7.07E-01'   # N_13
param_blocks['NMIX']['1 4']='-7.07E-01'   # N_14
param_blocks['NMIX']['2 1']=' 0.00E+00'   # N_21
param_blocks['NMIX']['2 2']=' 0.00E+00'   # N_22
param_blocks['NMIX']['2 3']='-7.07E-01'   # N_23 higgsino
param_blocks['NMIX']['2 4']='-7.07E-01'   # N_24 higgsino
param_blocks['NMIX']['3 1']=' 1.00E+00'   # N_31
param_blocks['NMIX']['3 2']=' 0.00E+00'   # N_32
param_blocks['NMIX']['3 3']=' 0.00E+00'   # N_33 higgsino
param_blocks['NMIX']['3 4']=' 0.00E+00'   # N_34 higgsino
param_blocks['NMIX']['4 1']=' 0.00E+00'   # N_41
param_blocks['NMIX']['4 2']='-1.00E+00'   # N_42 wino
param_blocks['NMIX']['4 3']=' 0.00E+00'   # N_43
param_blocks['NMIX']['4 4']=' 0.00E+00'   # N_44

flavourScheme = 5

# setup basic information
evgenConfig.contact  = [ "minlin.wu@cern.ch" ]
evgenConfig.description = 'Mixed stop-sbottom t-channel'
evgenConfig.keywords += ['simplifiedModel','stop','sbottom']

evt_multiplier = 2
njets = 1

include( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

pythia = genSeq.Pythia8
pythia.Commands += ["23:mMin = 0.2"]
pythia.Commands += ["24:mMin = 0.2"]

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
