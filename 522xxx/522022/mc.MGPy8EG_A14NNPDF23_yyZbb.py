import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

process = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > a a Z , Z > b b~ @0
add process p p > a a Z j , Z > b b~ @1 
output -f"""

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters

settings = { 'lhe_version':'3.0', 
           'cut_decays':'F', 
           'pta':'15',
           'etaa':'2.7',
           'mmaa':'90',
           'mmaamax':'175',
           'draa':'0.4',
           'drbj':'0.4',
           'draj':'0.4',
           'drab':'0.4',
           'nevents':int(nevents)}



modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

#### Shower 
evgenConfig.description = 'MadGraph_yyZbb'
evgenConfig.keywords+=["Z","jets","dijet","2jet","diphoton","heavyFlavour","bottom"]
evgenConfig.contact = ['Alexis Vallier <avallier@cern.ch>']
evgenConfig.nEventsPerJob = 10000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
