# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 12.05.2022,  15:47
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    7.74776562E+00  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.69549551E+03  # scale for input parameters
    1   -9.88352892E+01  # M_1
    2   -5.80147066E+02  # M_2
    3    4.53120877E+03  # M_3
   11   -1.26497838E+03  # A_t
   12    1.37554559E+03  # A_b
   13    6.49099593E+02  # A_tau
   23   -1.42659582E+03  # mu
   25    7.39001315E+00  # tan(beta)
   26    4.84536577E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.02202004E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.42752224E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.42436791E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.69549551E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.69549551E+03  # (SUSY scale)
  1  1     8.46078660E-06   # Y_u(Q)^DRbar
  2  2     4.29807959E-03   # Y_c(Q)^DRbar
  3  3     1.02213098E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.69549551E+03  # (SUSY scale)
  1  1     1.25642393E-04   # Y_d(Q)^DRbar
  2  2     2.38720547E-03   # Y_s(Q)^DRbar
  3  3     1.24597883E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.69549551E+03  # (SUSY scale)
  1  1     2.19255164E-05   # Y_e(Q)^DRbar
  2  2     4.53350140E-03   # Y_mu(Q)^DRbar
  3  3     7.62460355E-02   # Y_tau(Q)^DRbar
Block Au Q=  2.69549551E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.26497853E+03   # A_t(Q)^DRbar
Block Ad Q=  2.69549551E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.37554563E+03   # A_b(Q)^DRbar
Block Ae Q=  2.69549551E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     6.49099567E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.69549551E+03  # soft SUSY breaking masses at Q
   1   -9.88352892E+01  # M_1
   2   -5.80147066E+02  # M_2
   3    4.53120877E+03  # M_3
  21    2.09522571E+07  # M^2_(H,d)
  22   -1.52036331E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.02202004E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.42752224E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.42436791E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.18165044E+02  # h0
        35     4.84502693E+03  # H0
        36     4.84536577E+03  # A0
        37     4.84672891E+03  # H+
   1000001     1.00682488E+04  # ~d_L
   2000001     1.00452181E+04  # ~d_R
   1000002     1.00680336E+04  # ~u_L
   2000002     1.00488418E+04  # ~u_R
   1000003     1.00682502E+04  # ~s_L
   2000003     1.00452187E+04  # ~s_R
   1000004     1.00680350E+04  # ~c_L
   2000004     1.00488438E+04  # ~c_R
   1000005     2.08222045E+03  # ~b_1
   2000005     3.50624057E+03  # ~b_2
   1000006     2.08524020E+03  # ~t_1
   2000006     3.48434490E+03  # ~t_2
   1000011     1.00213862E+04  # ~e_L-
   2000011     1.00082744E+04  # ~e_R-
   1000012     1.00206286E+04  # ~nu_eL
   1000013     1.00213880E+04  # ~mu_L-
   2000013     1.00082773E+04  # ~mu_R-
   1000014     1.00206302E+04  # ~nu_muL
   1000015     1.00090736E+04  # ~tau_1-
   2000015     1.00219120E+04  # ~tau_2-
   1000016     1.00210772E+04  # ~nu_tauL
   1000021     4.91202431E+03  # ~g
   1000022     9.89113686E+01  # ~chi_10
   1000023     6.25763485E+02  # ~chi_20
   1000025     1.44264332E+03  # ~chi_30
   1000035     1.44710908E+03  # ~chi_40
   1000024     6.25820893E+02  # ~chi_1+
   1000037     1.44766171E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.28486192E-01   # alpha
Block Hmix Q=  2.69549551E+03  # Higgs mixing parameters
   1   -1.42659582E+03  # mu
   2    7.39001315E+00  # tan[beta](Q)
   3    2.43388782E+02  # v(Q)
   4    2.34775694E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99813437E-01   # Re[R_st(1,1)]
   1  2     1.93155843E-02   # Re[R_st(1,2)]
   2  1    -1.93155843E-02   # Re[R_st(2,1)]
   2  2     9.99813437E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99993552E-01   # Re[R_sb(1,1)]
   1  2     3.59106279E-03   # Re[R_sb(1,2)]
   2  1    -3.59106279E-03   # Re[R_sb(2,1)]
   2  2    -9.99993552E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -7.82310148E-02   # Re[R_sta(1,1)]
   1  2     9.96935258E-01   # Re[R_sta(1,2)]
   2  1    -9.96935258E-01   # Re[R_sta(2,1)]
   2  2    -7.82310148E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99498451E-01   # Re[N(1,1)]
   1  2    -1.51368093E-03   # Re[N(1,2)]
   1  3    -3.10020621E-02   # Re[N(1,3)]
   1  4     6.27917023E-03   # Re[N(1,4)]
   2  1     3.94057957E-03   # Re[N(2,1)]
   2  2     9.96779421E-01   # Re[N(2,2)]
   2  3     7.07836238E-02   # Re[N(2,3)]
   2  4    -3.74824801E-02   # Re[N(2,4)]
   3  1     1.74283209E-02   # Re[N(3,1)]
   3  2    -2.36273676E-02   # Re[N(3,2)]
   3  3     7.06296431E-01   # Re[N(3,3)]
   3  4     7.07307114E-01   # Re[N(3,4)]
   4  1     2.61452199E-02   # Re[N(4,1)]
   4  2    -7.66175055E-02   # Re[N(4,2)]
   4  3     7.03685940E-01   # Re[N(4,3)]
   4  4    -7.05884043E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.94945264E-01   # Re[U(1,1)]
   1  2    -1.00418727E-01   # Re[U(1,2)]
   2  1    -1.00418727E-01   # Re[U(2,1)]
   2  2     9.94945264E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.98570119E-01   # Re[V(1,1)]
   1  2     5.34576288E-02   # Re[V(1,2)]
   2  1     5.34576288E-02   # Re[V(2,1)]
   2  2    -9.98570119E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02760535E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99037818E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.91324813E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     6.55444512E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.43847555E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.69127769E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.03574814E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.15390507E-03    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.02369379E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.92885022E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02839162E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.98881970E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.30205988E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     6.93971147E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
DECAY   1000013     1.43851511E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.69104297E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.03566663E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.16739524E-03    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.02352924E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.92868834E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.30736291E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.42469457E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     6.61837758E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.02402595E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     9.70711744E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.31386385E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.78261499E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.44392684E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.80706617E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     3.00094308E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.07277756E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     5.42667475E-03    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.95371947E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.96363123E-03    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43851355E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.78949601E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.00990018E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.18995725E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.41206471E-03    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.06703945E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     1.68001630E-03    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43855308E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.78925591E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.00981797E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.18987015E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.41199886E-03    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.06687657E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     1.70700105E-03    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.44969404E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.72209934E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.98682271E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.16550903E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.39357841E-03    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     6.02131876E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     9.25472934E-03    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.64657630E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.20550305E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.87933343E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.94712045E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.40040965E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     7.33626555E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     4.72081214E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.46391667E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.44133285E-03    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.75880770E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.64659871E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.20549755E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.87928711E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.94716849E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.40039244E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     7.33620829E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     4.72986220E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.46390516E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.44727920E-03    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.75874745E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.40307334E+01   # ~b_1
#    BR                NDA      ID1      ID2
     8.66502025E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.21233252E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.59365014E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.95574788E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.32890516E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     3.31661813E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     3.67045233E+00   # ~b_2
#    BR                NDA      ID1      ID2
     5.32078275E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     9.15761131E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.01795925E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.01149738E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.82219793E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.01074102E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.76658629E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.38837762E-02    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.96143619E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.81860036E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.65154439E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.53439742E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.94710402E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.32007997E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     7.35681365E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     3.70095013E-04    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.47457506E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     4.08461144E-04    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.75845609E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.81867299E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.65147506E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.53425735E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.94715173E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.32006183E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     7.35675634E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     3.73062718E-04    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.47456363E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     4.10275448E-04    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.75839575E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     3.41007652E+01   # ~t_1
#    BR                NDA      ID1      ID2
     8.25033797E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.20698434E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.63480405E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.57310320E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.42976650E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     6.03700561E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.24451880E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.13232411E+02   # ~t_2
#    BR                NDA      ID1      ID2
     6.82441032E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.27918096E-03    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.18879569E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.16344845E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.60918287E-03    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.35824349E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.50657969E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.24488134E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.93041602E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.63950338E-03   # chi^+_1
#    BR                NDA      ID1      ID2
     9.95484427E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     4.51317357E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.18136059E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     8.41319591E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.08281954E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.97447397E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.96709276E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.90970083E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.21255155E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     4.30539791E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.35859593E-03   # chi^0_2
#    BR                NDA      ID1      ID2
     1.28150810E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     8.70710344E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     9.98644341E-04    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
     1.19506106E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
DECAY   1000025     1.34697634E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.60475999E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.60475999E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     5.19487084E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.41197323E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     2.83665546E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.43769394E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.15804595E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.46765363E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     5.56596876E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     5.56596876E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.17102431E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     3.03102793E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     3.03102793E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.62742485E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.61939518E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.43857473E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.73094845E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.42376300E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.57875362E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.91975896E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.91975896E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.66068074E+02   # ~g
#    BR                NDA      ID1      ID2
     1.82891889E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.82891889E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     6.74642776E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     6.74642776E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.83860337E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.83860337E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     6.57290682E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     6.57290682E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     2.67866394E-03   # Gamma(h0)
     2.70268353E-03   2        22        22   # BR(h0 -> photon photon)
     1.10686866E-03   2        22        23   # BR(h0 -> photon Z)
     1.60954694E-02   2        23        23   # BR(h0 -> Z Z)
     1.52284304E-01   2       -24        24   # BR(h0 -> W W)
     8.94743772E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.64758904E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.51210582E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.24223984E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.62428935E-07   2        -2         2   # BR(h0 -> Up up)
     3.15215324E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.60299794E-07   2        -1         1   # BR(h0 -> Down down)
     2.38813648E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.33901514E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     5.93033728E+01   # Gamma(HH)
     1.08662504E-07   2        22        22   # BR(HH -> photon photon)
     1.27818798E-07   2        22        23   # BR(HH -> photon Z)
     3.77108332E-06   2        23        23   # BR(HH -> Z Z)
     6.44484206E-07   2       -24        24   # BR(HH -> W W)
     4.99395105E-06   2        21        21   # BR(HH -> gluon gluon)
     6.05020548E-10   2       -11        11   # BR(HH -> Electron electron)
     2.69426717E-05   2       -13        13   # BR(HH -> Muon muon)
     7.78195364E-03   2       -15        15   # BR(HH -> Tau tau)
     2.75260612E-12   2        -2         2   # BR(HH -> Up up)
     5.34043636E-07   2        -4         4   # BR(HH -> Charm charm)
     4.45455089E-02   2        -6         6   # BR(HH -> Top top)
     4.26689409E-08   2        -1         1   # BR(HH -> Down down)
     1.54333780E-05   2        -3         3   # BR(HH -> Strange strange)
     4.34872049E-02   2        -5         5   # BR(HH -> Bottom bottom)
     5.13897605E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.71908052E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.71908052E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     4.76227018E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.66255686E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.32502018E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     5.06661483E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.87544701E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     2.54564663E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.78828262E-01   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     8.88861418E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     2.50210005E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     2.19811368E-03   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.00779758E-03   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     2.88416340E-05   2        25        25   # BR(HH -> h0 h0)
     3.85097983E-05   2  -1000006   1000006   # BR(HH -> Stop1 stop1)
     6.00877349E-06   2  -1000005   1000005   # BR(HH -> Sbottom1 sbottom1)
DECAY        36     5.90049785E+01   # Gamma(A0)
     1.35145142E-07   2        22        22   # BR(A0 -> photon photon)
     1.45979524E-07   2        22        23   # BR(A0 -> photon Z)
     1.29025052E-05   2        21        21   # BR(A0 -> gluon gluon)
     5.85828739E-10   2       -11        11   # BR(A0 -> Electron electron)
     2.60880402E-05   2       -13        13   # BR(A0 -> Muon muon)
     7.53511173E-03   2       -15        15   # BR(A0 -> Tau tau)
     2.55241538E-12   2        -2         2   # BR(A0 -> Up up)
     4.95192112E-07   2        -4         4   # BR(A0 -> Charm charm)
     4.15872665E-02   2        -6         6   # BR(A0 -> Top top)
     4.13143160E-08   2        -1         1   # BR(A0 -> Down down)
     1.49435048E-05   2        -3         3   # BR(A0 -> Strange strange)
     4.21029744E-02   2        -5         5   # BR(A0 -> Bottom bottom)
     7.11149178E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.71532066E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.71532066E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.09418217E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.85257616E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.62700204E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.94382311E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     5.01187745E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.52549149E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     9.11349577E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.76417506E-01   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.29152280E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.14124364E-03   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     2.62239928E-03   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.00732723E-05   2        23        25   # BR(A0 -> Z h0)
     2.82721462E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     6.04941764E+01   # Gamma(Hp)
     7.95332582E-10   2       -11        12   # BR(Hp -> Electron nu_e)
     3.40029520E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     9.61797252E-03   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.87251313E-08   2        -1         2   # BR(Hp -> Down up)
     8.26741459E-07   2        -3         2   # BR(Hp -> Strange up)
     5.70746770E-07   2        -5         2   # BR(Hp -> Bottom up)
     3.21099076E-08   2        -1         4   # BR(Hp -> Down charm)
     1.81928557E-05   2        -3         4   # BR(Hp -> Strange charm)
     7.99259750E-05   2        -5         4   # BR(Hp -> Bottom charm)
     3.27617033E-06   2        -1         6   # BR(Hp -> Down top)
     7.14576113E-05   2        -3         6   # BR(Hp -> Strange top)
     1.03899774E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.86988627E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     7.59941161E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     5.86464357E-08   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.71181532E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.66988833E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.58343941E-03   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.67317541E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     3.03604528E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     9.83593968E-06   2        24        25   # BR(Hp -> W h0)
     3.21459684E-13   2        24        35   # BR(Hp -> W HH)
     1.05940081E-13   2        24        36   # BR(Hp -> W A0)
     2.50710549E-05   2  -1000005   1000006   # BR(Hp -> Sbottom1 stop1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.13045737E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.46992486E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.46122944E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00159221E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.67186848E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.83108952E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.13045737E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.46992486E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.46122944E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99963825E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.61748788E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99963825E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.61748788E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27051724E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.24539633E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.50179890E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.61748788E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99963825E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.24168111E-04   # BR(b -> s gamma)
    2    1.58958619E-06   # BR(b -> s mu+ mu-)
    3    3.52486362E-05   # BR(b -> s nu nu)
    4    2.51821363E-15   # BR(Bd -> e+ e-)
    5    1.07575230E-10   # BR(Bd -> mu+ mu-)
    6    2.25199978E-08   # BR(Bd -> tau+ tau-)
    7    8.49615100E-14   # BR(Bs -> e+ e-)
    8    3.62955310E-09   # BR(Bs -> mu+ mu-)
    9    7.69865712E-07   # BR(Bs -> tau+ tau-)
   10    9.67933094E-05   # BR(B_u -> tau nu)
   11    9.99837829E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42081727E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93689609E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15772929E-03   # epsilon_K
   17    2.28166228E-15   # Delta(M_K)
   18    2.47969255E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28975472E-11   # BR(K^+ -> pi^+ nu nu)
   20    5.06110985E-17   # Delta(g-2)_electron/2
   21    2.16378334E-12   # Delta(g-2)_muon/2
   22    6.12119451E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -1.34312636E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.93177152E-01   # C7
     0305 4322   00   2    -6.50087930E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.98198905E-02   # C8
     0305 6321   00   2    -7.96210786E-05   # C8'
 03051111 4133   00   0     1.61241052E+00   # C9 e+e-
 03051111 4133   00   2     1.61268759E+00   # C9 e+e-
 03051111 4233   00   2     1.38660674E-05   # C9' e+e-
 03051111 4137   00   0    -4.43510262E+00   # C10 e+e-
 03051111 4137   00   2    -4.43304246E+00   # C10 e+e-
 03051111 4237   00   2    -1.04369729E-04   # C10' e+e-
 03051313 4133   00   0     1.61241052E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61268758E+00   # C9 mu+mu-
 03051313 4233   00   2     1.38660671E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43510262E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43304246E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.04369729E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50490480E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.26406323E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50490480E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.26406324E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50490480E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.26406369E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85819135E-07   # C7
     0305 4422   00   2     1.91502755E-06   # C7
     0305 4322   00   2    -1.07960585E-07   # C7'
     0305 6421   00   0     3.30479605E-07   # C8
     0305 6421   00   2     1.69919298E-06   # C8
     0305 6321   00   2    -2.35044165E-08   # C8'
 03051111 4133   00   2     5.57061826E-07   # C9 e+e-
 03051111 4233   00   2     2.59865404E-07   # C9' e+e-
 03051111 4137   00   2     9.37119579E-07   # C10 e+e-
 03051111 4237   00   2    -1.95641444E-06   # C10' e+e-
 03051313 4133   00   2     5.57061693E-07   # C9 mu+mu-
 03051313 4233   00   2     2.59865402E-07   # C9' mu+mu-
 03051313 4137   00   2     9.37119720E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.95641445E-06   # C10' mu+mu-
 03051212 4137   00   2    -1.74714223E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     4.24400115E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.74714216E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     4.24400115E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.74712297E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     4.24400114E-07   # C11' nu_3 nu_3
