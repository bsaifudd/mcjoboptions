evgenConfig.description = "ParticleGun, two very-collimated photons with pT > 5 GeV, pT_yy > 130 GeV and 0.05 < deltaR_yy < 0.25."
evgenConfig.keywords = ["diphoton"]
evgenConfig.contact = ["romain.van.den.broucke@cern.ch"]
evgenConfig.generators += ["ParticleGun"]
evgenConfig.nEventsPerJob = 1e5

import ParticleGun as PG
import math, random

class ExpSampler(PG.ContinuousSampler):
    "Randomly sample from an exponential distribution with a coefficient, in a range."

    def __init__(self, low, high, coef):
        self.low = float(low)
        self.high = float(high)
        self.coef = float(coef)

    def shoot(self):
        rand = random.random()
        expval = rand*math.exp(-self.high / self.coef) + (1 - rand)*math.exp(-self.low / self.coef)
        val = -self.coef*math.log(expval)
        return val

class MyTwoPhotons(PG.ParticleSampler):
    "A special sampler to generate two photons close to each other."

    def __init__(self):
        self.photon1 = PG.PtEtaMPhiSampler(pt=ExpSampler(5e3, 10000e3, 70e3), eta=[-2.5, 2.5], phi=[-math.pi, math.pi])
        self.eta = 2.5
        self.phi = math.pi

    def shoot(self):
        "Return a vector of sampled particles"
        p1 = PG.SampledParticle(22, self.photon1.shoot())
        eta1 = p1.mom.Eta()
        phi1 = p1.mom.Phi()
        pt1 = p1.mom.Pt()
        if pt1 > 130e3:
            photon2 = PG.PtEtaMPhiSampler(pt=ExpSampler(5e3, 10000e3, 70e3),
                                    eta=PG.GaussianSampler(eta1, .125),
                                    phi=PG.GaussianSampler(phi1, .125))
        else:
            photon2 = PG.PtEtaMPhiSampler(pt=ExpSampler(130e3 - pt1, 10000e3, 70e3),
                                    eta=PG.GaussianSampler(eta1, .125),
                                    phi=PG.GaussianSampler(phi1, .125))
        p2 = PG.SampledParticle(22, photon2.shoot())
        pt2 = p2.mom.Pt()
        if pt1 > pt2:
            return [p1, p2]
        else:
            return [p2, p1]

genSeq += PG.ParticleGun()
genSeq.ParticleGun.sampler = MyTwoPhotons()

from GeneratorFilters.GeneratorFiltersConf import DiPhotonFilter
filtSeq += DiPhotonFilter()
filtSeq.DiPhotonFilter.PtCut1st = 5000. # 5 GeV
filtSeq.DiPhotonFilter.PtCut2nd = 5000. # 5 GeV
filtSeq.DiPhotonFilter.DeltaRCutFrom = 0.05
filtSeq.DiPhotonFilter.DeltaRCutTo = 0.25
filtSeq.DiPhotonFilter.Use1st2ndPhotons = True
