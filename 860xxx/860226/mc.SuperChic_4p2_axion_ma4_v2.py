
from Superchic_i.SuperChicUtils import SuperChicConfig, SuperChicRun
import Superchic_i.EventFiller as EF
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

JOName = get_physics_short()

massstr = JOName.split("_")[3]
mass = float(massstr.strip('ma').replace('p', '.'))

evgenConfig.description = 'Superchic 4.2 ALP  process in UPC collisions at 5360 GeV, pT(gamma) > 1 GeV, |eta(gamma)| < 5.0'
evgenConfig.keywords = ['2photon', '2photon']
evgenConfig.contact = ['malak.ait.tamlihat@cern.ch']
evgenConfig.generators = ['Superchic']
evgenConfig.nEventsPerJob = 10000

scConfig = SuperChicConfig(runArgs)
scConfig.isurv = 4
scConfig.intag = 'in5'
scConfig.PDFname = 'MMHT2015qed_nnlo'
scConfig.PDFmember = 0
scConfig.proc = 68
scConfig.beam = 'ion'
scConfig.outtg = 'out'
scConfig.sfaci = True
scConfig.ncall = 10000 
scConfig.itmx = 10
scConfig.prec = 10000
scConfig.ymin = -5.0
scConfig.ymax = 5.0
scConfig.mmin = 2
scConfig.mmax = -1
scConfig.gencuts = True
scConfig.ptamin = 1
scConfig.ptbmin = 1
scConfig.etaamin = -5
scConfig.etaamax = 5
scConfig.etabmin = -5
scConfig.etabmax = 5
#scConfig.acoabmax = 100
scConfig.malp = mass
scConfig.gax = 0.001
scConfig.alpt = 'ps'
scConfig.ionbreakup = False

SuperChicRun(scConfig, genSeq)

# Transform modified LHE to EVNT
include('Superchic_i/LheEventFiller_Common.py')


