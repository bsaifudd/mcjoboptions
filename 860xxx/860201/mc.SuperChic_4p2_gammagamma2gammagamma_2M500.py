from Superchic_i.SuperChicUtils import SuperChicConfig, SuperChicRun
import Superchic_i.EventFiller as EF


evgenConfig.description = 'Superchic 4.2  gamma + gamma -> gamma + gamma process in UPC collisions, 2 GeV < M_gg < 500 GeV, pT(gamma) > 1 GeV, |eta(gamma)| < 5.0'
evgenConfig.keywords = ['2photon']
evgenConfig.contact = ['malak.ait.tamlihat@cern.ch']
evgenConfig.generators = ['Superchic']
evgenConfig.nEventsPerJob = 10000
scConfig = SuperChicConfig(runArgs)

ecmEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    ecmEnergy = runArgs.ecmEnergy
else: 
    raise RuntimeError("No center of mass energy found.")

scConfig.rts = ecmEnergy
scConfig.isurv = 4
scConfig.intag = 'in5'
scConfig.PDFname = 'MMHT2015qed_nnlo'
scConfig.PDFmember = 0
scConfig.proc = 59
scConfig.beam = 'ion'
scConfig.outtg = 'out'
scConfig.sfaci = True
scConfig.ncall = 10000 
scConfig.itmx = 10
scConfig.prec = 1
scConfig.ymin = -5.0
scConfig.ymax = 5.0
scConfig.mmin = 2
scConfig.mmax = 500
scConfig.gencuts = True
scConfig.ptamin = 1
scConfig.ptbmin = 1
scConfig.etaamin = -5
scConfig.etaamax = 5
scConfig.etabmin = -5
scConfig.etabmax = 5
scConfig.acoabmax = 100

SuperChicRun(scConfig, genSeq)

# Transform modified LHE to EVNT
include('Superchic_i/LheEventFiller_Common.py')

