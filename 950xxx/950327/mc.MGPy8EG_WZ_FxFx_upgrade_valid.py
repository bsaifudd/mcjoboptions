#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'aMcAtNloPythia8EvtGen_WZ_FxFx_lvll'
evgenConfig.keywords=['SM','WZ']
evgenConfig.contact = ['Louis DEramo <lderamo@cern.ch>','Corinne Goy <Corinne.Goy@cern.ch>']
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.inputconfcheck = "MGPy8EG_WZ_FxFx"
evgenConfig.nEventsPerJob = 10000
nevents = runArgs.maxEvents*5.0 if runArgs.maxEvents>0 else 5.0*evgenConfig.nEventsPerJob

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

gridpack_mode=True

process = """
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define wpm = w+ w-
generate p p > wpm z [QCD] @0
add process p p > wpm  z j  [QCD] @1
add process p p > w+ z j j  [QCD] @2
add process p p > w- z j j  [QCD] @2
output -f
"""
process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters
extras = {'lhe_version'     :'3.0',
          'maxjetflavor'    : 5,
          'ickkw'           : 3,
          'nevents'         : int(nevents),
          'parton_shower'   : 'PYTHIA8',
          'PDF_set_min'     : 260001,
          'PDF_set_max'     : 260100,
          'jetradius'       : 1.0,
          'mll_sf'          : 70.,
          'mll'             : -1,
          'ptl'             : 0.0,
          'etal'            : -1.0,
          'drll'            : 0.0,
          'ptj'             : 10.0,
          'etaj'            : 5.
          }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)



if not is_gen_from_gridpack(): # When generating the gridpack
  param_card_loc = process_dir+"/Cards/param_card.dat"
  madspin_card = process_dir+"/Cards/madspin_card.dat"
else:
  gridpack_dir = "madevent/"
  param_card_loc = gridpack_dir+"Cards/param_card.dat"
  madspin_card = gridpack_dir+"Cards/madspin_card.dat"

paramcard = subprocess.Popen(['get_files','-data',param_card_loc])
paramcard.wait()
if not os.access(param_card_loc,os.R_OK):
    raise RuntimeError("ERROR: Could not get %s"%param_card_loc)


if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
# set seed %i
# specify the decay for the final state particles
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > mu+ vm
decay w- > mu- vm~
decay z > e+ e-
# running the actual code
launch"""%runArgs.randomSeed)
mscard.close()

modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=extras)

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,saveProcDir=True)


if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

############################

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("GeneratorFilters/MultiLeptonFilter.py")
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.Etacut = 4.2
MultiLeptonFilter.NLeptons = 3
include("Pythia8_i/Pythia8_aMcAtNlo.py")

PYTHIA8_nJetMax=2
PYTHIA8_qCut=20.
PYTHIA8_etaCut = 5.
# Matching settings
genSeq.Pythia8.Commands += ["JetMatching:merge            = on",
                            "JetMatching:scheme           = 1",
                            "JetMatching:setMad           = off",
                            "JetMatching:qCut             = %f"%PYTHIA8_qCut,
                            "JetMatching:coneRadius       = 1.0",
                            "JetMatching:etaJetMax        = %f"%PYTHIA8_etaCut,
                            "JetMatching:doFxFx           = on",
                            "JetMatching:qCutME           = 10.0",
                            "JetMatching:nJetMax          = %i"%PYTHIA8_nJetMax ]
genSeq.Pythia8.UserHooks = ['JetMatchingMadgraph']
genSeq.Pythia8.FxFxXS = True
