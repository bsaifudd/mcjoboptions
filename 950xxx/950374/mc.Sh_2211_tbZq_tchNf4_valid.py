include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/Fusing_Fragmentation.py")

evgenConfig.description = "Sherpa tbZq tchNf4 Production"
evgenConfig.keywords = ["SM", "top" ]
evgenConfig.contact = [ "atlas-generators-sherpa@cern.ch" , "shuhui.huang@cern.ch" ]
evgenConfig.nEventsPerJob = 10000
genSeq.Sherpa_i.RunCard="""
(run){

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN
  EVENT_GENERATION_MODE Weighted
  LOOPGEN:=OpenLoops
  HARD_DECAYS On

  # scales, tags for scale variations
  #   muR = transverse momentum of the bottom
  #   muF = muQ = transverse momentum of the top
  SCF:=1.; FSF:=SCF; RSF:=SCF; QSF:=SCF
  rscl2:=MPerp2(p[3])
  fscl2:=MPerp2(p[2])
  SCALES VAR{FSF*fscl2}{RSF*rscl2}{QSF*fscl2}

  # disable hadronic W decays
  HDH_STATUS[24,2,-1] 0
  HDH_STATUS[24,4,-3] 0
  HDH_STATUS[-24,-2,1] 0
  HDH_STATUS[-24,-4,3] 0

  # choose EW Gmu input scheme
  EW_SCHEME 3

  # required for using top-quark in ME
  WIDTH[6]  0.
  WIDTH[23] 0. 

  # configure for N_f = 4
  PDF_LIBRARY LHAPDFSherpa
  PDF_SET NNPDF30_nlo_as_0118_nf_4
  USE_PDF_ALPHAS 1
  MASS[5] 4.18  # as in NNPDF30_nlo_as_0118_nf_4
  MASSIVE[5] 1
  
}(run)

(processes){
  Process 93 93 -> 6 -5 93 23
  NLO_QCD_Mode MC@NLO
  Order (*,3)
  ME_Generator Amegic
  RS_ME_Generator Comix
  Loop_Generator LOOPGEN
  Min_N_TChannels 1  # require t-channel W
  End process
}(processes)
"""
genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [  "pptzjj" ]
genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.", "WIDTH[23]=0.","EW_SCHEME=3" ]
