include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/PDF4LHC21.py")

evgenConfig.description = "Sherpa eegamma + 0,1j@NLO + 2,3j@LO with 35<pT_y<70."
evgenConfig.keywords = ["SM", "2electron", "photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]

genSeq.Sherpa_i.RunCard="""

ME_SIGNAL_GENERATOR:
  - Comix
  - Amegic
  - OpenLoops

OL_PARAMETERS:
  redlib1: 5
  redlib2: 5
  write_parameters: 1

EW_SCHEME: Gmu
GF: 1.166397e-5

PROCESSES:
- 93 93 -> 22 11 -11 93{3}:
    Order: {QCD: 0, EW: 3}
    CKKW: 20
    2->3-4:
      NLO_QCD_Mode: MC@NLO
      ME_Generator: Amegic
      RS_ME_Generator: Comix
      Loop_Generator: OpenLoops
    2->4:
      PSI_ItMin: 20000
      Integration_Error: 0.99
    2->5-6:
      PSI_ItMin 50000:
      Integration_Error: 0.99

SELECTORS:
  - [PT, 22, 35, 70]
  - [IsolationCut, 22, 0.1, 2, 0.10]
  - [DR, 22, 90, 0.1, 1000.0]
  - [Mass, 11,  -11,  10.0,  E_CMS]
"""


genSeq.Sherpa_i.NCores = 24
evgenConfig.nEventsPerJob = 5000
