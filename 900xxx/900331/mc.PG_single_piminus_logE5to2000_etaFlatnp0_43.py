evgenConfig.description = "Single pi- with log(E) in [5-2000] GeV, flat eta (between -4.3 and 4.3), and flat phi"
evgenConfig.keywords = ["singleParticle", "pi-"]
evgenConfig.contact = ["chris.malena.delitzsch@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG 
genSeq += PG.ParticleGun() 
evgenConfig.generators += ["ParticleGun"]
       
genSeq.ParticleGun.sampler.pid = -211
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(5000,2000000), eta=[-4.3, 4.3])
