evgenConfig.description = "Noncommutative black holes decaying to all particles."
evgenConfig.process = "Noncommutative black holes"
evgenConfig.keywords = ["BSM","exotic", "blackhole", "extraDimensions", "ADD"]
evgenConfig.generators += ["Charybdis2"]
evgenConfig.contact = ["Elena Villhauer <elena.michelle.villhauer@cern.ch>"]
evgenConfig.inputfilecheck = "TXT"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")

genSeq.Pythia8.Commands += [
                            #"1000022:m0 = 1000",
                            "1000022:spinType = 0",
                            "1000022:mWidth = 0",
                            "1000022:onMode = off",
                            "1000022:mMax = 0",
                            "1000022:colType = 0",
                            "1000022:chargeType = 0",
                            "1000022:mMin = 0",
                            "1000022:mayDecay = off",
                            "1000022:isResonance = false",
                            "1000015:m0 = 1000",
                            "1000015:spinType = 0",
                            "1000015:mWidth = 0",
                            "1000015:onMode = off",
                            "1000015:mMax = 0",
                            "1000015:colType = 0",
                            "1000015:chargeType = 3",
                            "1000015:mMin = 0",
                            "1000015:mayDecay = off"
                            ]

genSeq.Pythia8.Commands += [#"Check:event = off",  ## disable energy cons check
                            "TimeShower:allowBeamRecoil = off",
                            "Init:showAllParticleData = on", ## show more info in log.generate
                            "Next:numberShowLHA = 10", # prinoutt to log
                            "Next:numberShowEvent = 10" ]

