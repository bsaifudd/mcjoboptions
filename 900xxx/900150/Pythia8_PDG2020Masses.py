genSeq.Pythia8.Commands += ['15:m0  = 1.7768600e+00']# PDG 2020 tau

genSeq.Pythia8.Commands += ['511:m0 = 5.2796500e+00']# PDG 2020
genSeq.Pythia8.Commands += ['521:m0 = 5.2793400e+00']# PDG 2020
genSeq.Pythia8.Commands += ['531:m0 = 5.3668800e+00']# PDG 2020
genSeq.Pythia8.Commands += ['541:m0 = 6.2749000E+00']# PDG 2020

genSeq.Pythia8.Commands += ['513:m0 = 5.3247000e+00']# PDG 2020
genSeq.Pythia8.Commands += ['523:m0 = 5.3247000e+00']# PDG 2020
genSeq.Pythia8.Commands += ['533:m0 = 5.4154000e+00']# PDG 2020

genSeq.Pythia8.Commands += ['411:m0 = 1.8696500e+00']# PDG 2020
genSeq.Pythia8.Commands += ['413:m0 = 2.0102600e+00']# PDG 2020
genSeq.Pythia8.Commands += ['421:m0 = 1.8648300e+00']# PDG 2020
genSeq.Pythia8.Commands += ['423:m0 = 2.0068500e+00']# PDG 2020
genSeq.Pythia8.Commands += ['431:m0 = 1.9683400e+00']# PDG 2020
genSeq.Pythia8.Commands += ['433:m0 = 2.1122000e+00']# PDG 2020

genSeq.Pythia8.Commands += ['443:m0    = 3.0969000e+00']# PDG 2020 J/psi
genSeq.Pythia8.Commands += ['100443:m0 = 3.6861000e+00']# PDG 2020 psi(2S)
genSeq.Pythia8.Commands += ['10441:m0  = 3.4147100e+00']# PDG 2020 chi_c0
genSeq.Pythia8.Commands += ['20443:m0  = 3.5106700e+00']# PDG 2020 chi_c1
genSeq.Pythia8.Commands += ['445:m0    = 3.5561700e+00']# PDG 2020 chi_c2
genSeq.Pythia8.Commands += ['10443:m0  = 3.5253800e+00']# PDG 2020 h_c
genSeq.Pythia8.Commands += ['30443:m0  = 3.7737000e+00']# PDG 2020 psi(3770)

