
#--------------------------------------------------------------
# Standard pre-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

#--------------------------------------------------------------
# Some options for local testing.  Shouldn't hurt anything in production.
#
# Nominal configuration for production: MadSpin decays, with 0,1,2+ partom emissions in the matrix element
#
#--------------------------------------------------------------
# Interpret the name of the JO file to figure out the mass spectrum
#
def MassToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)

#--------------------------------------------------------------
# split up the JO file input name to interpret it
# e.g. jofile: mc.MGPy8EG_A14N_N2N3_ZZ_200_50_MadSpin.py
splitConfig = jofile.rstrip('.py').split('_')

#C1/N2/N3 degenerate
masses['1000025'] = -MassToFloat(splitConfig[4])
masses['1000023'] = MassToFloat(splitConfig[4])
masses['1000024'] = MassToFloat(splitConfig[4])
#N1
masses['1000022'] = MassToFloat(splitConfig[5])
if masses['1000022']<0.5: masses['1000022']=0.5

# interpret the generation type.
gentype = splitConfig[2] # should be always N2N3

############################
# Updating the parameters
############################

#
# N2,N3 branching ratio
#
decaytype = splitConfig[3] #e.g. ZZ,Zh,hh,Zh50

if decaytype == 'ZZ':
  evgenLog.info('Force Br(N2->ZN1)=Br(N3->ZN1)=100%')
  decays ={'1000023':'''DECAY   1000023     2.05122242E-01   # neutralino2 decays
#       BR              NDA     ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
       ''',
           '1000025':'''DECAY   1000025     2.10722495E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
       '''}
elif decaytype == 'Zh':
  evgenLog.info('Force Br(N2->ZN1)=Br(N3->hN1)=100%')
  decays ={'1000023':'''DECAY   1000023     2.05122242E-01   # neutralino2 decays
#       BR              NDA     ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
       ''',
           '1000025':'''DECAY   1000025     2.10722495E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.00000000E+00    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
       '''}
elif decaytype == 'Zh50': 
  evgenLog.info('Force Br(N2->ZN1)=Br(N3->hN1)=50%')
  decays ={'1000023':'''DECAY   1000023     2.05122242E-01   # neutralino2 decays
#       BR              NDA     ID1       ID2
     0.50000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     0.50000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
       ''',
           '1000025':'''DECAY   1000025     2.10722495E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     0.50000000E+00    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     0.50000000E+00    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
       '''}
elif decaytype == 'hh':
  evgenLog.info('Force Br(N2->hN1)=Br(N3->hN1)=100%')
  decays ={'1000023':'''DECAY   1000023     2.05122242E-01   # neutralino2 decays
#       BR              NDA     ID1       ID2
     0.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.00000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
       ''',
           '1000025':'''DECAY   1000025     2.10722495E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.00000000E+00    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
       '''}
#
# Mixing matrix for the Higgsino NLSP / Bino LSP / decoupled Wino
# Derived from SoftSUSY 4.1.7 (M1,M2,mu)=(100, 10000, 800)GeV, tan_beta=10 and all the others decoupled
#
# Off-diagonal chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='0.00E+00'
param_blocks['VMIX']['1 2']='1.00E+00'
param_blocks['VMIX']['2 1']='-1.00E+00'
param_blocks['VMIX']['2 2']='0.00E+00'
# Off-diagonal chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='0.00E+00'
param_blocks['UMIX']['1 2']='1.00E+00'
param_blocks['UMIX']['2 1']='-1.00E+00'
param_blocks['UMIX']['2 2']='0.00E+00'
# Neutralino mixing matrix chi_i0 = N_ij i(B,W,H_d,H_u)
param_blocks['NMIX']={}
param_blocks['NMIX']['1 1']=' 9.98314580e-01'  # N_11 bino (N1)
param_blocks['NMIX']['1 2']=' -4.91972985e-04' # N_12
param_blocks['NMIX']['1 3']=' 5.66046345e-02'  # N_13
param_blocks['NMIX']['1 4']=' -1.27934907e-02' # N_14
param_blocks['NMIX']['2 1']=' -4.90895344e-02' # N_21
param_blocks['NMIX']['2 2']=' -2.70112948e-02' # N_22 
param_blocks['NMIX']['2 3']=' 7.05966582e-01'  # N_23 higgsino (N2)
param_blocks['NMIX']['2 4']=' -7.06025349e-01' # N_24 higgsino (N2)
param_blocks['NMIX']['3 1']=' -3.09519330e-02' # N_31
param_blocks['NMIX']['3 2']=' 1.26912209e-02'  # N_32 
param_blocks['NMIX']['3 3']=' 7.05906671e-01'  # N_33 higgsino (N3)
param_blocks['NMIX']['3 4']=' 7.07514440e-01'  # N_34 higgsino (N3)
param_blocks['NMIX']['4 1']=' -4.42207291e-04' # N_41
param_blocks['NMIX']['4 2']=' 9.99554441e-01'  # N_42 wino (N4)
param_blocks['NMIX']['4 3']=' 1.01426210e-02'  # N_43
param_blocks['NMIX']['4 4']=' -2.80686812e-02' # N_44

#
# Debug the SM higgs mass/branching ratio in the default param_card (which uses the values of 110.8GeV higgs)
#
masses['25'] = 125.00
higgs_decay = {'25':'''DECAY   25     4.06911399E-03   # higgs decays
#          BR         NDA      ID1       ID2
        5.82000000E-01    2           5        -5   # BR(H1 -> b bbar)
        2.13700000E-01    2          24       -24   # BR(H1 -> W+ W-)
        8.18700000E-02    2          21        21   # BR(H1 -> g g)
        6.27200000E-02    2          15       -15   # BR(H1 -> tau- tau+)
        2.89100000E-02    2           4        -4   # BR(H1 -> c cbar)   
        2.61900000E-02    2          23        23   # BR(H1 -> Z Z)
        2.27000000E-03    2          22        22   # BR(H1 -> gamma gamma)   
        2.17600000E-04    2          13       -13   # BR(H1 -> mu mu)         
        1.53300000E-03    2          23        22   # BR(H1 -> Z gamma)  
        2.46000000E-04    2           3        -3   # BR(H1 -> s sbar)     
#
       '''}
decays.update(higgs_decay)

madspindecays=False
if (decaytype == 'ZZ' or decaytype == 'Zh' or decaytype == 'Zh50' or decaytype == 'hh') and ('MadSpin' in jofile) :
  madspindecays = True;

print "gentype", gentype
print "decaytype", decaytype
print "decays", decays
print "masses", masses
print "madspindecays", madspindecays

# max number of jets will be two, unless otherwise specified.
njets = 2

#--------------------------------------------------------------
# MadGraph options
#
process = '''
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define lv = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~
generate p p > n2 n3 $ susystrong @1
add process p p > n2 n3 j $ susystrong @2
add process p p > n2 n3 j j $ susystrong @3
'''

mergeproc="{n2,1000023}{n3,1000025}"

msdecaystring=""
if madspindecays == True:
  if decaytype == 'ZZ':
    msdecaystring="decay n2 > z n1, z > f f \ndecay n3 > z n1, z > f f\n"
  elif decaytype == 'Zh':
    msdecaystring="decay n2 > z n1, z > f f \ndecay n3 > h01 n1\n"
  elif decaytype == 'hh':
    msdecaystring="decay n2 > h01 n1, \ndecay n3 > h01 n1\n"
  elif decaytype == 'Zh50':
    msdecaystring="decay n2 > z n1, z > f f \ndecay n2 > h01 n1\n decay n3 > z n1, z > f f \ndecay n3 > h01 n1\n"


# print the process, just to confirm we got everything right
print "Final process card:"
print process


#--------------------------------------------------------------
# Madspin configuration
#
if madspindecays==True:
  if msdecaystring=="":
    raise RuntimeError("Asking for MadSpin decays, but no decay string provided!")
  madspin_card='madspin_card.dat'

  mscard = open(madspin_card,'w')

  mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
set BW_cut 15                # default for onshell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
set spinmode none
# specify the decay for the final state particles

%s


# running the actual code
launch"""%(runArgs.randomSeed,msdecaystring))
  mscard.close()
  mergeproc+="LEPTONS,NEUTRINOS"

#--------------------------------------------------------------
# Pythia options
#
pythia = genSeq.Pythia8
pythia.Commands += ["23:mMin = 0.2"]
pythia.Commands += ["24:mMin = 0.2"]

# information about this generation
evgenLog.info('Registered generation of ~chi20 ~chi30 production, decaying into ~chi10 via ZZ/Zh/hh; grid point decoded into mass point ' + str(masses['1000025']) + ' ' + str(masses['1000022']))
evgenConfig.contact  = [ "shion.chen@cern.ch" ]
evgenConfig.keywords += ['gaugino', 'neutralino']
evgenConfig.description = '~chi20 ~chi30 production, decaying into ~chi10 via ZZ/Zh/hh in simplified model. m_N2N3 = %s GeV, m_N1 = %s GeV'%(masses['1000025'],masses['1000022'])

#--------------------------------------------------------------
# No filter at the moment
evt_multiplier=2
evgenLog.info('inclusive processes will be generated')

#--------------------------------------------------------------
# Standard post-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

#--------------------------------------------------------------
# Merging options
#
if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = pp>{n2,1000023}{n3,1000025}",
                                 "1000025:spinType = 1",
                                 "1000023:spinType = 1" ]
