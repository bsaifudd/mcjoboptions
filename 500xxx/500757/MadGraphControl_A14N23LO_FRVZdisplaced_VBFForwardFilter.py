from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
import os

gridpack_mode=True

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
minevents= 10000
safe_factor=7
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:
  minevents = int(runArgs.maxEvents) 
nevents = int(minevents*safe_factor)

#---------------------------------------------------------------------------
# MG5 process 
#---------------------------------------------------------------------------
# defaut is including all leptons and quarks
fminus = 'define f- = e- m- tt- d s b u~ c~'
fplus = 'define f+ = e+ m+ tt+ d~ s~ b~ u c'

if decayMode == "quarks":
    fminus = 'define f- = d s b u~ c~'
    fplus = 'define f+ = d~ s~ b~ u c'
elif decayMode == "electrons":
    fminus = 'define f- = e-'
    fplus = 'define f+ = e+'
elif decayMode == "muons":
    fminus = 'define f- = m-'
    fplus = 'define f+ = m+'
elif decayMode == "emu":
    fminus = 'define f- = e- m-' 
    fplus = 'define f+ = e+ m+'


process="""
import model --modelname iDM 
define v = ve vm vt
define v~ = ve~ vm~ vt~
define j = g u c d s u~ c~ d~ s~
"""+fminus+"""
"""+fplus+"""
generate p p > j j h $$ w+ w- z QCD=0, ((h > chi2 chi2), (chi2 > chi1 zp, zp > f- f+), (chi2 > chi1 zp, zp > f- f+))
output -f
"""

if not is_gen_from_gridpack():
  process_dir = new_process(process)
else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
settings = { 'lhe_version':'3.0',
    'cut_decays':'F',
    'ptj':"10",
    'nevents'      : int(nevents),
}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------
parameters={
  'hidden': {
    '1': '%s' % (float(mZd)*0.001),
    '2': '%s' % (float(mfd1)), # mHS
    '3': '1.000000e-03', # epsilon
    '4': '1.000000e-09', # kap
    '5': '1.279000e+02', # aXM1 
    '6': '1.000000e-01', # ghchi2
    '7': '1.000000e-01', # ghdchi2chi2
    '8': '1.200000e-03', # gXmu 
    '9': '1.200000e-03', # gXe
    '10': '1.000000e-00' # gXpi
  }, 
  'decay': {
    '3000001':  'DECAY 3000001 1.00000e-03', # WZd
    '3000005':  'DECAY 3000005 1.000000e-08', # WHS
    '30000016': 'DECAY 30000016 1.000000e-03', # Wchi2
    '30000015': 'DECAY  30000015 0.000000', # Wchi1
  }, 
  'mass': {
    '25':       '%s' % (float(mH)),
    '3000001':  '%s' % (float(mZd)*0.001), # zp 
    '3000005':  '%s' % (float(mfd1)), # hslp 
    '30000015': '%s' % (float(mfd1)), # chi
    '30000016': '%s' % (float(mfd2)), #chi1
  }
}
modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
try:
    generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
except RuntimeError as rte:
    for an_arg in rte.args:
        if 'Gridpack sucessfully created' in an_arg:
            print 'Handling exception and exiting'
            theApp.finalize()
            theApp.exit()
    print 'Unhandled exception - re-raising'
    raise rte

#---------------------------------------------------------------------------
# hacking LHE file BEFORE arrange_output
#---------------------------------------------------------------------------
rname = 'run_01'
unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
unzip1.wait()
oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')

# initialise random number generator/sequence for the lifetime of dark photon
import random
random.seed(runArgs.randomSeed)

init = True
# lifetime function
def lifetime(avgtau = 21):
    import math
    t = random.random()
    return -1.0 * avgtau * math.log(t)

for line in oldlhe:
    if init==True:
        if '30000016' in line:
            line = line.replace('30000016','3000016')
        elif '30000015' in line:
            line = line.replace('30000015','1000022')
        newlhe.write(line)
        if '</init>' in line:
            init = False
    else:
        newline=line.rstrip('\n')
        columns=newline.split()
        pdgid=columns[0]

        if pdgid == '3000001' and avgtau>0:
            part1 = line[:-22]
            part2 = "%.5e" % (lifetime(avgtau))
            part3 = line[-12:]
            newlhe.write(part1+part2+part3)
        elif (pdgid == '-30000016') :
            part1 = ' -3000016'
            part2 = line[10:]
            newlhe.write(part1+part2)
        elif (pdgid == '30000016') :
            part1 = '  3000016'
            part2 = line[10:]
            newlhe.write(part1+part2)
        elif (pdgid == '-30000015') :
            part1 = ' -1000022'
            part2 = line[10:]
            newlhe.write(part1+part2)
        elif (pdgid == '30000015') :
            part1 = '  1000022'
            part2 = line[10:]
            newlhe.write(part1+part2)
        else:
            newlhe.write(line)
oldlhe.close()
newlhe.close()

# re-zipping hacked LHE
zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
zip1.wait()
shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')

arrange_output(runArgs=runArgs, process_dir=process_dir, saveProcDir=False) 

from MadGraphControl.MadGraphUtils import check_reset_proc_number
check_reset_proc_number(opts)

#--------------------------------------------------------------
# Shower
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]

#relax the cuts on displaced vertices and non G4 particles
testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = 'displaced FRVZ VBF + Higgs, Higgs -> 2 gamma_d + X, mZd=%s, mH=%s, mfd2=%s, mfd1=%s' % (mZd, mH, mfd2, mfd1)
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]
evgenConfig.contact  = ['yanyan.gao@cern.ch']
evgenConfig.process="LJ_FRVZdisplaced_vbfh"
evgenConfig.inputconfcheck=""
evgenConfig.nEventsPerJob = 10000
#--------------------------------------------------------------
# VBFForwardJetsFilter
#--------------------------------------------------------------
# https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/GeneratorFilters/share/common/VBFForwardJetsFilter.py
# hard copied for 21.2.23
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)

if not hasattr( filtSeq, "VBFForwardJetsFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
    filtSeq += VBFForwardJetsFilter()
    pass
VBFForwardJetsFilter = filtSeq.VBFForwardJetsFilter
VBFForwardJetsFilter.JetMinPt = 30.*GeV
VBFForwardJetsFilter.JetMaxEta = 5.
VBFForwardJetsFilter.NJets = 2
VBFForwardJetsFilter.Jet1MinPt = 30.*GeV
VBFForwardJetsFilter.Jet1MaxEta = 5.
VBFForwardJetsFilter.Jet2MinPt = 30.*GeV
VBFForwardJetsFilter.Jet2MaxEta = 5.
VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2 = False
VBFForwardJetsFilter.MassJJ = 750.*GeV
VBFForwardJetsFilter.DeltaEtaJJ = 2.0
VBFForwardJetsFilter.UseLeadingJJ = True 
VBFForwardJetsFilter.TruthJetContainer = "AntiKt4TruthJets"
VBFForwardJetsFilter.LGMinPt = 30.*GeV
VBFForwardJetsFilter.LGMaxEta = 2.5
VBFForwardJetsFilter.DeltaRJLG = 0.05
VBFForwardJetsFilter.RatioPtJLG = 0.3
