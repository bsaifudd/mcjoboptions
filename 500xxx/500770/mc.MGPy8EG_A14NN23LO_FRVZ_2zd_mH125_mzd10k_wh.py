mH = 125
mfd2 = 35
mfd1 = 6
mZd = 10000
nGamma = 2
avgtau = 900
decayMode = 'normal'
include("MadGraphControl_A14N23LO_FRVZdisplaced_wh.py")
evgenConfig.nEventsPerJob=2000
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]
