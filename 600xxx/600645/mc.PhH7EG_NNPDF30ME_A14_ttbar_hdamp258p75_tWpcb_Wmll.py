process="tWpcb"
include("PowhegControl_ttWcbWlep_NLO.py")

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.description = 'Powheg+MadSpin+Herwig7 LHE shower for tt->WbWb,W->ll,W->cb, H7.1 default tune'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar']
evgenConfig.tune = "H7.1-Default"
evgenConfig.contact = ['anna.ivina@cern.ch']
evgenConfig.nEventsPerJob = 2000


# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename="PowhegOTF._1.events", me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.run()
#--------------------------------------------------------------
