from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment
from MadGraphControl.MadGraphUtils import check_reset_proc_number

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={     
'central_pdf': 260400, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets     
'pdf_variations':[260400,93700,320500,334700], # pdfs for which all variations (error sets) will be included as weights    
'alternative_pdfs':[266400,265400,261400,13191,25510,28300,27700,320900,334300,92000], # pdfs for which only the central set will be included as weights     
'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated     
'alternative_dynamic_scales' : [1,2,3,4], 
}

gridpack_mode=False

mode=0

#20000 events as the production with 10000 events is inefficient for W0Z0jj
evgenConfig.nEventsPerJob = 10000
nevents = 1.4*(runArgs.maxEvents if runArgs.maxEvents > 0 else evgenConfig.nEventsPerJob)
maxjetflavor = 4


name = pol_state+"_lvlljj_LO_"

proc_str = ""

if(pol_state=="W0Z0"):
    proc_str = "wpm{0} z{0}"
elif(pol_state=="W0ZT"):
    proc_str = "wpm{0} z{T}"
elif(pol_state=="WTZ0"):
    proc_str = "wpm{T} z{0}"
elif(pol_state=="WTZT"):
    proc_str = "wpm{T} z{T}"
elif(pol_state=="WZ"):
    proc_str = "wpm z"
else:
    print("ERROR: pol_state has to be one of W0Z0, W0ZT, WTZ0, WTZT or WZ (incl.). ")


process  = """
import model sm
define wp = w+
define wm = w-
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm
define vl~ = ve~ vm~
define wpm = w+ w-
generate p p > %s j j QCD=0, w+ > l+ vl, w- > l- vl~, z > l+ l- @0
output -f
""" % (proc_str)


process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters                                                                                                

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------   
#produce 40% more due to compensate MG inefficiencies                                                                   


extras = { 'me_frame' : '3,4,5,6',
           'lhe_version' : '3.0',
           'cut_decays' : 'True', 
           'clusinfo' : 'T',
	   'event_norm' : 'average',
           'auto_ptj_mjj' : 'False',
           'maxjetflavor' : 4,
           'asrwgtflavor' : 4,
           'ickkw' : 0,
           'xqcut' : 0.0,
           'drjl' : 0.2,
           'drll' : 0.2,
	   'mmll': 0, 
	   'drjj': 0.4,
           'etal' : 5.0,
           'etaj' : 5.0,
           'ptl' : 4.0, 
           'ptj' : 15.0,
	   'ptheavy': 2.0,
	   'mmjj' : 110.0,
	   'sde_strategy' : 2,
           'systematics_program' : 'systematics',
           'sys_matchscale' : 25.0,
           'sys_alpsfact' : '1 0.5 2',
           'nevents'      : nevents,
	   'dynamical_scale_choice' : '3',
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)


print_cards()

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)



############################                                                                                                
# Shower JOs will go here                                                                                                             
check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil=on"]
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.contact = [ 'Angela Burger <angela.maria.burger@cern.ch>' ]
evgenConfig.keywords+=["VBS","WZ","LO","SM","diboson","3lepton"]



