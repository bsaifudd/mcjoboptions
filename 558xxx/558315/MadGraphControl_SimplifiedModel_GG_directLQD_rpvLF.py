include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
jobConfig = get_physics_short() 

masses['1000021'] = float(jobConfig.split('_')[4]) 
masses['1000022'] = float(jobConfig.split('_')[5])
neutralino_frac = float(phys_short.split('_')[6].replace("p","."))
direct_frac=1-neutralino_frac
process = '''
import model RPVMSSM_UFO
define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate p p > go go $ susysq susysq~ @1
add process p p > go go j $ susysq susysq~ @2
add process p p > go go j j $ susysq susysq~ @3
'''
#gluino->uubar/ddbar/ssbar/ccbar
decays['1000021']="""DECAY 1000021 7.40992706E-02 # Wgo
##  BR         NDA      ID1       ID2       ID3
{0:.5f}    3        1         -1        1000022
{0:.5f}    3        2         -2        1000022
{0:.5f}    3        3         -3        1000022
{0:.5f}    3        4         -4        1000022
{1:.5f}    3         -11        -2         1
{1:.5f}    3          11         2        -1
{1:.5f}    3         -13        -2         1
{1:.5f}    3          13         2        -1
{1:.5f}    3         -12        -1         1
{1:.5f}    3          12         1        -1
{1:.5f}    3         -14        -1         1
{1:.5f}    3          14         1        -1
""".format(neutralino_frac/4., direct_frac/8.)
decays['1000022']="""DECAY   1000022     0.1   # neutralino1 decays
##          BR         NDA      ID1       ID2
1.25000000E-01    3         -11        -2         1
1.25000000E-01    3          11         2        -1
1.25000000E-01    3         -13        -2         1
1.25000000E-01    3          13         2        -1
1.25000000E-01    3         -12        -1         1
1.25000000E-01    3          12         1        -1
1.25000000E-01    3         -14        -1         1
1.25000000E-01    3          14         1        -1   """
#LSP->e+ u- d, e- u d-, mu+ u- d, mu- u d-, vebar d- d, ve d d-, vmubar d- d, vmu d d- #Lifetime=0.1
njets = 2
evt_multiplier = 4
#evgenLog.info('Registered generation of gluino grid '+str(runArgs.runNumber))

evgenConfig.contact  = [ "sliang@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','gluino']
evgenConfig.description = 'gluino production, glu->qq->LQD) in simplified model, m_glu = %s GeV'%(masses['1000021'])

testSeq.TestHepMC.MaxVtxDisp = 1e8
testSeq.TestHepMC.MaxTransVtxDisp = 1e8

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
        genSeq.Pythia8.Commands += ["Merging:Process = guess"]
        genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
