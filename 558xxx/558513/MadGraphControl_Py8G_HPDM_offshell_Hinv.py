import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import re

# to check lepton filter
import os
### get MC job-options filename
FIRST_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
jofiles = [f for f in os.listdir(FIRST_DIR) if (f.startswith('mc') and f.endswith('.py'))]
filter_string=jofiles[0].split('_')[-1].replace(".py","")

#----------------------------------------------------------------------------
# Process definition
#----------------------------------------------------------------------------
gridpack_mode=False
#
process="""
import model HiggsPortal_UFO -modelname
define p = g d u s c b d~ u~ s~ c~ b~
define j = g d u s c b d~ u~ s~ c~ b~
generate p p > h > j j ~chi ~chi QCD=0
output -f
"""

process_dir = new_process(process)

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of events to generate
#---------------------------------------------------------------------------
nevents=evgenConfig.nEventsPerJob
if LHE_EventMultiplier>0:
  nevents=runArgs.maxEvents*LHE_EventMultiplier

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {}
extras = {
      'maxjetflavor'  : 5,
      'asrwgtflavor'  : 5,
      'lhe_version'   : '3.0',
      'cut_decays'    : 'F',
      'nevents'       : nevents,
  }

# Build run_card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------

# Build param_card.dat

params={}

## blocks might be modified
dict_blocks={
"FRBLOCK": ["fcomp", "mdm", "cd", "lamhs"],
}

for bl in dict_blocks.keys():
  for pa in dict_blocks[bl]:
    if pa in USERparams.keys():
      if bl not in params: params[bl]={}
      params[bl][pa]=USERparams[pa]

## auto calculation of decay width
USERparams_decay={
"25": "Auto",
}

params["decay"]=USERparams_decay
## Dakr matter mass
mdm = USERparams["mdm"]
params["mass"]={"51":f"{mdm}"}
print("Updating parameters:")
print(params)

modify_param_card(process_dir=process_dir,params=params)
# Build reweight_card.dat
if reweight:
  print("reweight starts")
  # Create reweighting card
  reweight_card_loc=process_dir+'/Cards/reweight_card.dat'
  rwcard = open(reweight_card_loc,'w')

  for rw_name in reweights:
    params_rwt = params.copy()
    for param in rw_name.split('-'):
      param_name, value = param.split('_')
      params_rwt["FRBLOCK"][f"{param_name}"] = value
    param_card_reweight = process_dir+'/Cards/param_card_reweight.dat'
    shutil.copy(process_dir+'/Cards/param_card.dat', param_card_reweight)
    param_card_rwt_new=process_dir+'/Cards/param_card_rwt_%s.dat' % rw_name
    modify_param_card(param_card_input=param_card_reweight, process_dir=process_dir,params=params_rwt, output_location=param_card_rwt_new)
    rwcard.write("launch --rwgt_name=%s\n" % rw_name)
    rwcard.write("%s\n" % param_card_rwt_new)
    
  rwcard.close()

print("Print param_card_rwt_new card: ")

#---------------------------------------------------------------------------
# Generate the events    
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False) 

#---------------------------------------------------------------------------                                                                                                                                                                       
# Metadata                                                                                                                                                          
#---------------------------------------------------------------------------
evgenConfig.description = "Offshell Higgs portal for Higgs invisible decays"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Lailin Xu <lailin.xu@cern.ch>, Zhiliang Chen <zhiliang@cern.ch>"]



#---------------------------------------------------------------------------
# Filters
#---------------------------------------------------------------------------
include('GeneratorFilters/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV
filtSeq.Expression = "MissingEtFilter"


#---------------------------------------------------------------------------
# Shower
#---------------------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Teach pythia about the dark matter particle
genSeq.Pythia8.Commands += ["SLHA:allowUserOverride = on",
                            "51:all = ~chi ~chi 1 0 0 %d 0.0 0.0 0.0 0.0" % (int(USERparams['mdm'])),
                            "51:isVisible = false",
                            "51:mayDecay = off"
                            ]



