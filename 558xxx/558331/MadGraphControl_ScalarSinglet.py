from MadGraphControl.MadGraphParamHelpers import set_top_params
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import os
# No filter - efficiency is 100%. Adding extra 10% as recommended by the generator experts
nevents = runArgs.maxEvents*1.4 if runArgs.maxEvents>0 else 1.4*evgenConfig.nEventsPerJob
name='Top-Philic Scalar Singlet\np p > t t~ t t~ QED<=0 QCD<=2 NP<=2\n'
process=""" 
import model Top_Philic_Scalar_Singlet_UFO
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define wdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ g u c d s b u~ c~ d~ s~ b~
generate p p > t t~ t t~ QED<=0 QCD<=2 NP<=2
output -f"""

process_dir = new_process(process)
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

model_pars_str = str(jofile)
msig1=int(1500)
ynpS1=int(2)
singletWidth = 'Auto'

joFileName = str(jofile)
ms=None
ys=None
for joPart in joFileName.split("_"):
    # print('  jobConfig fragment :'+joPart)
    parStr = 'ms'
    if parStr in joPart:
        modJoPart=joPart.replace(parStr,"").replace("p",".")
        try:
            ms = float(modJoPart)*1000
        except ValueError:
            pass
        else:
            pass
    
    parStr = 'ys'
    if parStr in joPart:
        modJoPart=joPart.replace(parStr,"").replace("p",".")
        try:
            ys = float(modJoPart)
        except ValueError:
            pass
        else:
            pass

print('Mass:', ms, 'Coupling:', ys)

if ms is None:
    raise RuntimeError("No ms parameter found in the job options file name")
if ys is  None:
    raise RuntimeError("No ys parameter found in the job options file name")

# Mass of signlet in GeV is integer type
msig1=int(ms)
# Coupling of signlet is float type
ynpS1=float(ys)

# Parameters must have the format: Dict[Block : Dict[Variable : Value]]
parameters = {
    'MASS':{
        'msig1': msig1,
    },
    'DECAY':{
        'wsig1': singletWidth,
    },
    'NPyS1': {
      'ynpS1': ynpS1,
    },
}
# Modify the run card [https://gitlab.cern.ch/atlas/athena/-/blob/main/Generators/MadGraphControl/python/MadGraphUtils.py#L2019]
modify_param_card(process_dir=process_dir, params=parameters)
set_top_params(process_dir, mTop=172.5, FourFS=False)
# Local definintion of the run card
extras = { 'lhe_version':'3.0',
        # 'pdlabel'       : "'lhapdf'",
        # 'lhaid'       : lhaid,
        'nevents':int(nevents),
        'event_norm':'sum',
        'dynamical_scale_choice' : 3,
        # 'parton_shower':'PYTHIA8'
}
# [https://gitlab.cern.ch/atlas/athena/-/blob/main/Generators/MadGraphControl/python/MadGraphUtils.py#L2167]
modify_run_card(runArgs=runArgs,
                process_dir=process_dir,
                settings=extras)

doReweight=True
if doReweight:
    reweightCommand='''launch --rwgt_name=rwgt_y_2p1
set msig1 {msig1}
set ynpS1 2.1
set wsig1 Auto
launch --rwgt_name=rwgt_y_2p0
set msig1 {msig1}
set ynpS1 2.0
set wsig1 Auto
launch --rwgt_name=rwgt_y_1p9
set msig1 {msig1}
set ynpS1 1.9
set wsig1 Auto
launch --rwgt_name=rwgt_y_1p8
set msig1 {msig1}
set ynpS1 1.8
set wsig1 Auto
launch --rwgt_name=rwgt_y_1p7
set msig1 {msig1}
set ynpS1 1.7
set wsig1 Auto
launch --rwgt_name=rwgt_y_1p6
set msig1 {msig1}
set ynpS1 1.6
set wsig1 Auto
launch --rwgt_name=rwgt_y_1p5
set msig1 {msig1}
set ynpS1 1.5
set wsig1 Auto
launch --rwgt_name=rwgt_y_1p4
set msig1 {msig1}
set ynpS1 1.4
set wsig1 Auto
launch --rwgt_name=rwgt_y_1p3
set msig1 {msig1}
set ynpS1 1.3
set wsig1 Auto
launch --rwgt_name=rwgt_y_1p2
set msig1 {msig1}
set ynpS1 1.2
set wsig1 Auto
launch --rwgt_name=rwgt_y_1p1
set msig1 {msig1}
set ynpS1 1.1
set wsig1 Auto
launch --rwgt_name=rwgt_y_1p0
set msig1 {msig1}
set ynpS1 1.0
set wsig1 Auto
launch --rwgt_name=rwgt_y_0p9
set msig1 {msig1}
set ynpS1 0.9
set wsig1 Auto
launch --rwgt_name=rwgt_y_0p8
set msig1 {msig1}
set ynpS1 0.8
set wsig1 Auto
launch --rwgt_name=rwgt_y_0p7
set msig1 {msig1}
set ynpS1 0.7
set wsig1 Auto
launch --rwgt_name=rwgt_y_0p6
set msig1 {msig1}
set ynpS1 0.6
set wsig1 Auto
launch --rwgt_name=rwgt_y_0p5
set msig1 {msig1}
set ynpS1 0.5
set wsig1 Auto
launch --rwgt_name=rwgt_y_0p4
set msig1 {msig1}
set ynpS1 0.4
set wsig1 Auto
launch --rwgt_name=rwgt_y_0p3
set msig1 {msig1}
set ynpS1 0.3
set wsig1 Auto
launch --rwgt_name=rwgt_y_0p2
set msig1 {msig1}
set ynpS1 0.2
set wsig1 Auto
launch --rwgt_name=rwgt_y_0p1
set msig1 {msig1}
set ynpS1 0.1
set wsig1 Auto
'''.format(msig1=msig1)

    rcard = open(os.path.join(process_dir,'Cards', 'reweight_card.dat'), 'w')
    rcard.write(reweightCommand)
    rcard.close()

#---------------------------------------------------------------------------------------------------                                               
# Check cards and proceed with event generation                                                                                                                             
#---------------------------------------------------------------------------------------------------   
print_cards()

run_card=process_dir+'/Cards/run_card.dat'

# # Decay with MadSpin
madspin_card=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card,'w') 
# set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
set BW_cut 15                # cut on how far the particle can be off-shell
set seed 10
define wdec = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ g u c d s b u~ c~ d~ s~ b~
decay t > w+ b, w+ > wdec wdec
decay t~ > w- b~, w- > wdec wdec
launch
""")
mscard.close()

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)
evgenConfig.description = 'MadGraph_tttt_ScalarSinglet'
evgenConfig.generators += ["aMcAtNlo","Pythia8","EvtGen"]

evgenConfig.process= "p p -> t+t~+t+t~"
# Get from https://gitlab.cern.ch/atlas/athena/-/blob/release/22.2.100/Generators/EvgenJobTransforms/share/file/evgenkeywords.txt
# evgenConfig.keywords += ["top", "4top", "BSMtop", "scalar","SUSY","jets"]
evgenConfig.keywords += ["top", "BSMtop", "jets"]

evgenConfig.contact = ["dtimoshy@cern.ch"]
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
# [!] USING FILTER TO REMOVE ALL-HADRONIC EVENTS ~ 21%[!]
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #no-allhad
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
