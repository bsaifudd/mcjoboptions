include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# Set masses based on physics short
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

# phys_short: [event generation]_[pdf]_[channel]_[decay flavor]_[stop mass]_[neutralino mass]_[neutralino_fraction]_[prompt_fraction]
# ex: MGPy8EG_A14NNPDF23LO_TT_rpvHF_400_200_0p25_0p75
phys_short = get_physics_short()

decayflavor = phys_short.split('_')[3]

masses['1000006'] = float(phys_short.split('_')[4]) #stop 
masses['1000022'] = float(phys_short.split('_')[5].split('.')[0]) #N1

neutralino_frac = float(phys_short.split('_')[6].replace("p","."))
direct_frac = float(phys_short.split('_')[7].replace("p","."))

process = '''
import model RPVMSSM_UFO
define susylq = ul ur dl dr cl cr sl sr
define susylq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~
generate    p p > t1 t1~ QED=0 RPV=0    / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @1
add process p p > t1 t1~ j QED=0 RPV=0  / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2
add process p p > t1 t1~ j j QED=0 RPV=0 / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @3
'''

decays['1000006'] = """DECAY   1000006     5.69449678E+00   # stop decays
  #          BR          NDA       ID1       ID2       ID3
        {0:.4f}  2    1000022  6
        {1:.4f}  2      -5    -3 
 
  """.format(neutralino_frac, direct_frac)

decays['1000022'] = """DECAY   1000022     1.0  # neutralino decays
  #          BR          NDA       ID1       ID2       ID3
        0.5000  3    6   3   5
        0.5000  3   -6  -3  -5
  #"""   

# Set up a default event multiplier
evt_multiplier = 2

njets=2

evgenConfig.contact  = ["zubair.bhatti@cern.ch"]
evgenConfig.keywords += [ 'SUSY', 'RPV', 'stop', 'simplifiedModel', 'neutralino']
evgenConfig.description = 'stop pair production and decay via RPV lamba323, or decay to top quarks and neutralino, which then decays via RPV lambda323, m_stop = %s GeV, m_N1 = %s GeV'%(masses['1000006'],masses['1000022'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

genSeq.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,1000006}"]

