# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 10.04.2021,  16:15
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.62333030E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.21448132E+03  # scale for input parameters
    1    1.57200745E+03  # M_1
    2   -1.70507969E+03  # M_2
    3    3.00690598E+03  # M_3
   11   -2.91419373E+03  # A_t
   12   -1.18521223E+03  # A_b
   13    3.21758289E+02  # A_tau
   23   -4.09579224E+02  # mu
   25    3.54415626E+01  # tan(beta)
   26    1.79562479E+03  # m_A, pole mass
   31    6.77122654E+02  # M_L11
   32    6.77122654E+02  # M_L22
   33    9.05375354E+02  # M_L33
   34    1.68437939E+03  # M_E11
   35    1.68437939E+03  # M_E22
   36    1.81613282E+03  # M_E33
   41    4.87240310E+03  # M_Q11
   42    4.87240310E+03  # M_Q22
   43    1.92902240E+03  # M_Q33
   44    2.49773343E+03  # M_U11
   45    2.49773343E+03  # M_U22
   46    9.21005619E+02  # M_U33
   47    2.82748361E+03  # M_D11
   48    2.82748361E+03  # M_D22
   49    1.00210885E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.21448132E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.21448132E+03  # (SUSY scale)
  1  1     8.38770892E-06   # Y_u(Q)^DRbar
  2  2     4.26095613E-03   # Y_c(Q)^DRbar
  3  3     1.01330261E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.21448132E+03  # (SUSY scale)
  1  1     5.97360442E-04   # Y_d(Q)^DRbar
  2  2     1.13498484E-02   # Y_s(Q)^DRbar
  3  3     5.92394373E-01   # Y_b(Q)^DRbar
Block Ye Q=  1.21448132E+03  # (SUSY scale)
  1  1     1.04243765E-04   # Y_e(Q)^DRbar
  2  2     2.15543045E-02   # Y_mu(Q)^DRbar
  3  3     3.62507943E-01   # Y_tau(Q)^DRbar
Block Au Q=  1.21448132E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -2.91419377E+03   # A_t(Q)^DRbar
Block Ad Q=  1.21448132E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.18521254E+03   # A_b(Q)^DRbar
Block Ae Q=  1.21448132E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     3.21758291E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  1.21448132E+03  # soft SUSY breaking masses at Q
   1    1.57200745E+03  # M_1
   2   -1.70507969E+03  # M_2
   3    3.00690598E+03  # M_3
  21    3.09032756E+06  # M^2_(H,d)
  22   -2.13003431E+05  # M^2_(H,u)
  31    6.77122654E+02  # M_(L,11)
  32    6.77122654E+02  # M_(L,22)
  33    9.05375354E+02  # M_(L,33)
  34    1.68437939E+03  # M_(E,11)
  35    1.68437939E+03  # M_(E,22)
  36    1.81613282E+03  # M_(E,33)
  41    4.87240310E+03  # M_(Q,11)
  42    4.87240310E+03  # M_(Q,22)
  43    1.92902240E+03  # M_(Q,33)
  44    2.49773343E+03  # M_(U,11)
  45    2.49773343E+03  # M_(U,22)
  46    9.21005619E+02  # M_(U,33)
  47    2.82748361E+03  # M_(D,11)
  48    2.82748361E+03  # M_(D,22)
  49    1.00210885E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23017181E+02  # h0
        35     1.79552163E+03  # H0
        36     1.79562479E+03  # A0
        37     1.79841054E+03  # H+
   1000001     4.88851835E+03  # ~d_L
   2000001     2.85695458E+03  # ~d_R
   1000002     4.88806400E+03  # ~u_L
   2000002     2.51057573E+03  # ~u_R
   1000003     4.88851967E+03  # ~s_L
   2000003     2.85696218E+03  # ~s_R
   1000004     4.88806535E+03  # ~c_L
   2000004     2.51057702E+03  # ~c_R
   1000005     9.43237395E+02  # ~b_1
   2000005     1.91457916E+03  # ~b_2
   1000006     7.62947094E+02  # ~t_1
   2000006     1.93324661E+03  # ~t_2
   1000011     6.69641801E+02  # ~e_L-
   2000011     1.70811685E+03  # ~e_R-
   1000012     6.64722576E+02  # ~nu_eL
   1000013     6.69632949E+02  # ~mu_L-
   2000013     1.70810408E+03  # ~mu_R-
   1000014     6.64715764E+02  # ~nu_muL
   1000015     8.99908428E+02  # ~tau_1-
   2000015     1.83442015E+03  # ~tau_2-
   1000016     8.96275769E+02  # ~nu_tauL
   1000021     3.08862657E+03  # ~g
   1000022     4.15862878E+02  # ~chi_10
   1000023     4.17408284E+02  # ~chi_20
   1000025     1.56548820E+03  # ~chi_30
   1000035     1.74856121E+03  # ~chi_40
   1000024     4.16456519E+02  # ~chi_1+
   1000037     1.74872024E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.78087244E-02   # alpha
Block Hmix Q=  1.21448132E+03  # Higgs mixing parameters
   1   -4.09579224E+02  # mu
   2    3.54415626E+01  # tan[beta](Q)
   3    2.44180643E+02  # v(Q)
   4    3.22426839E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     1.34998253E-01   # Re[R_st(1,1)]
   1  2     9.90845837E-01   # Re[R_st(1,2)]
   2  1    -9.90845837E-01   # Re[R_st(2,1)]
   2  2     1.34998253E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.14347240E-02   # Re[R_sb(1,1)]
   1  2     9.99934621E-01   # Re[R_sb(1,2)]
   2  1    -9.99934621E-01   # Re[R_sb(2,1)]
   2  2    -1.14347240E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.99943850E-01   # Re[R_sta(1,1)]
   1  2     1.05970057E-02   # Re[R_sta(1,2)]
   2  1    -1.05970057E-02   # Re[R_sta(2,1)]
   2  2    -9.99943850E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     1.65470344E-02   # Re[N(1,1)]
   1  2     4.24029564E-02   # Re[N(1,2)]
   1  3     7.07526116E-01   # Re[N(1,3)]
   1  4    -7.05219810E-01   # Re[N(1,4)]
   2  1     2.71761585E-02   # Re[N(2,1)]
   2  2     2.49338049E-02   # Re[N(2,2)]
   2  3    -7.06538813E-01   # Re[N(2,3)]
   2  4    -7.06712578E-01   # Re[N(2,4)]
   3  1    -9.99493466E-01   # Re[N(3,1)]
   3  2     6.99368149E-04   # Re[N(3,2)]
   3  3    -7.48890389E-03   # Re[N(3,3)]
   3  4    -3.09231140E-02   # Re[N(3,4)]
   4  1     6.81058965E-04   # Re[N(4,1)]
   4  2    -9.98789170E-01   # Re[N(4,2)]
   4  3     1.23942680E-02   # Re[N(4,3)]
   4  4    -4.76037051E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.75252425E-02   # Re[U(1,1)]
   1  2    -9.99846421E-01   # Re[U(1,2)]
   2  1    -9.99846421E-01   # Re[U(2,1)]
   2  2     1.75252425E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     6.73168118E-02   # Re[V(1,1)]
   1  2     9.97731651E-01   # Re[V(1,2)]
   2  1     9.97731651E-01   # Re[V(2,1)]
   2  2    -6.73168118E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     5.31699188E-03   # ~e^-_L
#    BR                NDA      ID1      ID2
     5.49354240E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.22593950E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.28050937E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     2.27256966E-01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.15177948E-03    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.46610861E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.66185658E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000013     7.64133847E-03   # ~mu^-_L
#    BR                NDA      ID1      ID2
     5.35401509E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.75501703E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     8.90961821E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     2.55524619E-01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     3.55086449E-02    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     4.92026795E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     8.59158594E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     5.46405834E-02    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     1.48949775E-03    2     1000013        25   # BR(~mu^-_R -> ~mu^-_L h^0)
DECAY   1000015     1.46556428E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.02303212E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     4.95884854E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.81193348E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     9.70966052E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     2.22428227E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.22422855E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     7.00120022E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     4.43833423E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.11685311E-02    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     1.04641328E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     9.66239185E-03    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     1.11229058E-02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.09753258E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.04689193E-02    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     8.79777822E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     1.33858639E-02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.11954935E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.69876640E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     9.00105740E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     1.46429830E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.86880432E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.79242684E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     9.97951953E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
DECAY   2000001     7.81779679E-01   # ~d_R
#    BR                NDA      ID1      ID2
     5.47644392E-04    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.45557108E-03    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.97971565E-01    2     1000025         1   # BR(~d_R -> chi^0_3 d)
DECAY   1000001     1.88939511E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.74826976E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.92847210E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     8.64178033E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.73150408E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.37214140E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.95771825E-01   # ~s_R
#    BR                NDA      ID1      ID2
     4.93847420E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     5.81694287E-03    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.80431315E-01    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     8.81124673E-03    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
DECAY   1000003     1.88953752E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.07436536E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.92825531E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     8.64113324E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.73137462E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.37159298E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.06722560E+00   # ~b_1
#    BR                NDA      ID1      ID2
     2.65446439E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.63524371E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     4.71024210E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     9.87561505E+01   # ~b_2
#    BR                NDA      ID1      ID2
     6.15842657E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     6.13390809E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.00750825E-04    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.34204831E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.51187522E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.19938007E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.35249766E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.95582787E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     2.09749418E+00   # ~u_R
#    BR                NDA      ID1      ID2
     6.92261020E-04    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.86647804E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.97440931E-01    2     1000025         2   # BR(~u_R -> chi^0_3 u)
DECAY   1000002     1.88930046E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.30820885E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.88255713E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     8.63674276E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.01733823E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.72401929E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.37000448E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     2.09920602E+00   # ~c_R
#    BR                NDA      ID1      ID2
     8.94762674E-04    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.06870406E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.96628745E-01    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     4.06681844E-04    2     1000024         3   # BR(~c_R -> chi^+_1 s)
DECAY   1000004     1.88944193E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.35387050E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.04072933E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     2.88234718E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     8.63609782E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.08239822E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.72389114E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.36945672E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.28570021E+01   # ~t_1
#    BR                NDA      ID1      ID2
     2.09785972E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.08883397E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     5.81327073E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
DECAY   2000006     1.10747766E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.61650084E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.61363633E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.40681699E-04    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     6.51461414E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.19030550E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.82823302E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.49295287E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.36396186E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.13346218E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.13755306E-13   # chi^+_1
#    BR                NDA      ID1      ID2
     6.97602937E-01    2     1000022       211   # BR(chi^+_1 -> chi^0_1 pi^+)
#    BR                NDA      ID1      ID2       ID3
     1.62756941E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.39640122E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
DECAY   1000037     4.69923854E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.19242182E-01    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     1.19243266E-01    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     8.84727503E-02    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     1.19336385E-01    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     1.19337230E-01    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     8.86422638E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     1.79853913E-02    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     7.86236819E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     7.85354532E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     7.81096391E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     8.23093427E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.86408695E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.45791003E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     2.87700294E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     3.04124932E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     7.41704336E-10   # chi^0_2
#    BR                NDA      ID1      ID2
     9.70265181E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     4.85984801E-03    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     6.23074816E-03    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     5.96401559E-03    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.40612527E-03    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.31266180E-03    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     8.33343507E-03    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     4.94156004E-04    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     4.94156004E-04    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     1.64747080E-04    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     1.64747080E-04    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     1.55089498E-04    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     1.55089498E-04    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
DECAY   1000025     1.82546449E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     3.58249752E-02    2     1000011       -11   # BR(chi^0_3 -> ~e^-_L e^+)
     3.58249752E-02    2    -1000011        11   # BR(chi^0_3 -> ~e^+_L e^-)
     3.58254065E-02    2     1000013       -13   # BR(chi^0_3 -> ~mu^-_L mu^+)
     3.58254065E-02    2    -1000013        13   # BR(chi^0_3 -> ~mu^+_L mu^-)
     2.40665947E-02    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     2.40665947E-02    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     3.62496655E-02    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     3.62496655E-02    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     3.62499923E-02    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     3.62499923E-02    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     2.43786357E-02    2     1000016       -16   # BR(chi^0_3 -> ~nu_tau nu_bar_tau)
     2.43786357E-02    2    -1000016        16   # BR(chi^0_3 -> ~nu^*_tau nu_tau)
     1.48937589E-01    2     1000006        -6   # BR(chi^0_3 -> ~t_1 t_bar)
     1.48937589E-01    2    -1000006         6   # BR(chi^0_3 -> ~t^*_1 t)
     2.91098759E-02    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     2.91098759E-02    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     5.79179261E-02    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     5.79179261E-02    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.24407784E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.58189333E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     4.19121851E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     3.75534788E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     9.37152309E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     8.88232698E-04    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     1.59799824E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     1.59799824E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     4.85801122E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     5.74998674E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     5.74998674E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     5.75004177E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     5.75004177E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     4.26656887E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     4.26656887E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     5.78738235E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     5.78738235E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     5.78742242E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     5.78742242E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     4.29837287E-02    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     4.29837287E-02    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     8.94042796E-03    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     8.94042796E-03    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     7.60891133E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     7.60891133E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.94221199E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     5.66854377E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.92130872E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     5.34040997E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.43195641E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.75519356E-04    3     1000022         5        -5   # BR(chi^0_4 -> chi^0_1 b b_bar)
     1.37903375E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.80622408E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     2.60419860E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.60419860E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.48954704E+02   # ~g
#    BR                NDA      ID1      ID2
     2.11363290E-02    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     2.11363290E-02    2    -2000002         2   # BR(~g -> ~u^*_R u)
     2.11362034E-02    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     2.11362034E-02    2    -2000004         4   # BR(~g -> ~c^*_R c)
     1.54644646E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.54644646E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.56664315E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.56664315E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     7.07749129E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     7.07749129E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     3.82807863E-03    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     3.82807863E-03    2    -2000001         1   # BR(~g -> ~d^*_R d)
     3.82783696E-03    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     3.82783696E-03    2    -2000003         3   # BR(~g -> ~s^*_R s)
     1.50972743E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.50972743E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     6.96093999E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     6.96093999E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     4.38056805E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     4.38056805E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.59269919E-03   # Gamma(h0)
     2.37485216E-03   2        22        22   # BR(h0 -> photon photon)
     1.34743721E-03   2        22        23   # BR(h0 -> photon Z)
     2.32007207E-02   2        23        23   # BR(h0 -> Z Z)
     1.99446639E-01   2       -24        24   # BR(h0 -> W W)
     7.37955734E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.38643082E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.39596903E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.90704074E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.51541361E-07   2        -2         2   # BR(h0 -> Up up)
     2.94069452E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.24958194E-07   2        -1         1   # BR(h0 -> Down down)
     2.26031895E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.00891014E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     3.58920963E+01   # Gamma(HH)
     1.25079359E-08   2        22        22   # BR(HH -> photon photon)
     5.48247259E-08   2        22        23   # BR(HH -> photon Z)
     1.83305284E-06   2        23        23   # BR(HH -> Z Z)
     1.13458978E-06   2       -24        24   # BR(HH -> W W)
     1.68521056E-05   2        21        21   # BR(HH -> gluon gluon)
     1.14886041E-08   2       -11        11   # BR(HH -> Electron electron)
     5.11446735E-04   2       -13        13   # BR(HH -> Muon muon)
     1.44379073E-01   2       -15        15   # BR(HH -> Tau tau)
     8.77000052E-14   2        -2         2   # BR(HH -> Up up)
     1.70108448E-08   2        -4         4   # BR(HH -> Charm charm)
     1.53833007E-03   2        -6         6   # BR(HH -> Top top)
     8.51436524E-07   2        -1         1   # BR(HH -> Down down)
     3.07903153E-04   2        -3         3   # BR(HH -> Strange strange)
     8.48241703E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.43436410E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.81863890E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.78847137E-05   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.36183714E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     7.99703527E-06   2        25        25   # BR(HH -> h0 h0)
     2.09378754E-07   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     2.07959479E-07   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     3.29325606E-03   2  -1000006   1000006   # BR(HH -> Stop1 stop1)
DECAY        36     3.45422158E+01   # Gamma(A0)
     1.05449377E-07   2        22        22   # BR(A0 -> photon photon)
     5.33464982E-08   2        22        23   # BR(A0 -> photon Z)
     3.75604733E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.15178430E-08   2       -11        11   # BR(A0 -> Electron electron)
     5.12747271E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.44747054E-01   2       -15        15   # BR(A0 -> Tau tau)
     9.27799649E-14   2        -2         2   # BR(A0 -> Up up)
     1.79955402E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.68372927E-03   2        -6         6   # BR(A0 -> Top top)
     8.53599140E-07   2        -1         1   # BR(A0 -> Down down)
     3.08685564E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.50439668E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.93545854E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.65775404E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     3.79885802E-05   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.77617134E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     2.52927399E-06   2        23        25   # BR(A0 -> Z h0)
     1.77572707E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     3.37048794E+01   # Gamma(Hp)
     1.15843742E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.95268176E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.40089829E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.53405289E-07   2        -1         2   # BR(Hp -> Down up)
     1.43138383E-05   2        -3         2   # BR(Hp -> Strange up)
     9.11284443E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.06242959E-08   2        -1         4   # BR(Hp -> Down charm)
     3.07496030E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.27612348E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.09434859E-07   2        -1         6   # BR(Hp -> Down top)
     2.83313433E-06   2        -3         6   # BR(Hp -> Strange top)
     8.56991825E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.05718301E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     5.99719421E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.60892599E-06   2        24        25   # BR(Hp -> W h0)
     8.11771230E-12   2        24        35   # BR(Hp -> W HH)
     6.76847154E-12   2        24        36   # BR(Hp -> W A0)
     8.74417428E-07   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     8.72294186E-07   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     2.39081063E-06   2  -1000005   1000006   # BR(Hp -> Sbottom1 stop1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.71899851E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.25613246E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.25610436E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00002237E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.73741324E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.96112196E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.71899851E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.25613246E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.25610436E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999841E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.59402028E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999841E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.59402028E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.21605132E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    8.97954976E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.25612961E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.59402028E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999841E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.84582722E-04   # BR(b -> s gamma)
    2    1.58680519E-06   # BR(b -> s mu+ mu-)
    3    3.52253604E-05   # BR(b -> s nu nu)
    4    1.59810640E-15   # BR(Bd -> e+ e-)
    5    6.82636338E-11   # BR(Bd -> mu+ mu-)
    6    1.39580927E-08   # BR(Bd -> tau+ tau-)
    7    5.39896867E-14   # BR(Bs -> e+ e-)
    8    2.30625309E-09   # BR(Bs -> mu+ mu-)
    9    4.78190270E-07   # BR(Bs -> tau+ tau-)
   10    9.39657043E-05   # BR(B_u -> tau nu)
   11    9.70629751E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43829477E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94145756E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16757429E-03   # epsilon_K
   17    2.28176558E-15   # Delta(M_K)
   18    2.47950957E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28847587E-11   # BR(K^+ -> pi^+ nu nu)
   20    4.90464728E-15   # Delta(g-2)_electron/2
   21    2.09681643E-10   # Delta(g-2)_muon/2
   22    5.77015437E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.64590873E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.60222304E-01   # C7
     0305 4322   00   2    -6.66752405E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.39852317E-01   # C8
     0305 6321   00   2    -4.96940530E-04   # C8'
 03051111 4133   00   0     1.58463062E+00   # C9 e+e-
 03051111 4133   00   2     1.58609302E+00   # C9 e+e-
 03051111 4233   00   2     1.00837323E-04   # C9' e+e-
 03051111 4137   00   0    -4.40732272E+00   # C10 e+e-
 03051111 4137   00   2    -4.40301897E+00   # C10 e+e-
 03051111 4237   00   2    -7.92129755E-04   # C10' e+e-
 03051313 4133   00   0     1.58463062E+00   # C9 mu+mu-
 03051313 4133   00   2     1.58608785E+00   # C9 mu+mu-
 03051313 4233   00   2     1.00835710E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.40732272E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.40302415E+00   # C10 mu+mu-
 03051313 4237   00   2    -7.92130210E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50441382E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.72924481E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50441382E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.72924742E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50441391E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.72998129E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85751768E-07   # C7
     0305 4422   00   2     8.75801261E-06   # C7
     0305 4322   00   2    -7.04526876E-06   # C7'
     0305 6421   00   0     3.30421901E-07   # C8
     0305 6421   00   2     1.22949423E-05   # C8
     0305 6321   00   2    -2.78542429E-06   # C8'
 03051111 4133   00   2    -2.18335415E-06   # C9 e+e-
 03051111 4233   00   2     3.02427235E-06   # C9' e+e-
 03051111 4137   00   2     1.75783078E-05   # C10 e+e-
 03051111 4237   00   2    -2.37941134E-05   # C10' e+e-
 03051313 4133   00   2    -2.18329849E-06   # C9 mu+mu-
 03051313 4233   00   2     3.02426529E-06   # C9' mu+mu-
 03051313 4137   00   2     1.75783638E-05   # C10 mu+mu-
 03051313 4237   00   2    -2.37941433E-05   # C10' mu+mu-
 03051212 4137   00   2    -3.77976760E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     5.19431200E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -3.77976809E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     5.19431200E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -3.77806609E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     5.19431047E-06   # C11' nu_3 nu_3
