from MadGraphControl.MadGraphUtils import *
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')

#Read filename of jobOptions to obtain: productionmode, ewkino mass.
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

#mc.MGPy8EG_A14NNPDF23LO_C1N2N1_LQD_HF_500_1L20.py 
tokens = get_physics_short().split('_')
try:
    _  = float(tokens[-1])
    event_filter = ''
except ValueError:
    event_filter = tokens[-1]
    tokens = tokens[:-1]
    
productionmode = str(tokens[2])
decaymode      = str(tokens[4])
neutralinoMass = float(tokens[5])

if decaymode == 'HF':
    #LQD decays only to leptons
    branchingRatiosN1 = '''
            # BR          NDA       ID1       ID2       ID3
            0.0833      3    11   2  -5
            0.0833      3    11   4  -5
            0.0833      3   -11  -2   5
            0.0833      3   -11  -4   5
            0.0833      3    13   2  -5
            0.0833      3    13   4  -5
            0.0833      3   -13  -2   5
            0.0833      3   -13  -4   5
            0.0833      3    15   2  -5
            0.0833      3    15   4  -5
            0.0833      3   -15  -2   5
            0.0833      3   -15  -4   5
            #'''
    #LQD decays only to leptons, lep up up-bar not allowed due lqD in 3rd position
    branchingRatiosC1 = '''
            #          BR          NDA       ID1       ID2       ID3
            0.1667      3    -11   1  -5
            0.1667      3    -11   3  -5
            0.1667      3    -13   1  -5
            0.1667      3    -13   3  -5
            0.1667      3    -15   1  -5
            0.1667      3    -15   3  -5
            '''
elif decaymode == 'LF':
    #LQD decays only to leptons
    branchingRatiosN1 = '''
            # BR          NDA       ID1       ID2       ID3
            0.0416      3    11   2  -1
            0.0416      3    11   2  -3
            0.0416      3    11   4  -1
            0.0416      3    11   4  -3
            0.0416      3   -11  -2   1
            0.0416      3   -11  -2   3
            0.0416      3   -11  -4   1
            0.0416      3   -11  -4   3
            0.0416      3    13   2  -1
            0.0416      3    13   2  -3
            0.0416      3    13   4  -1
            0.0416      3    13   4  -3
            0.0416      3   -13  -2   1
            0.0416      3   -13  -2   3
            0.0416      3   -13  -4   1
            0.0416      3   -13  -4   3
            0.0416      3    15   2  -1
            0.0416      3    15   2  -3
            0.0416      3    15   4  -1
            0.0416      3    15   4  -3
            0.0416      3   -15  -2   1
            0.0416      3   -15  -2   3
            0.0416      3   -15  -4   1
            0.0416      3   -15  -4   3
            #'''
    #LQD decays only to leptons, lep up up-bar not allowed due lqD in 3rd position
    branchingRatiosC1 = '''
            #          BR          NDA       ID1       ID2       ID3
            0.0833      3    -11   1  -1
            0.0833      3    -11   3  -3
            0.0833      3    -11   3  -1
            0.0833      3    -11   1  -3
            0.0833      3    -13   1  -1
            0.0833      3    -13   3  -3
            0.0833      3    -13   3  -1
            0.0833      3    -13   1  -3
            0.0833      3    -15   1  -1
            0.0833      3    -15   3  -3
            0.0833      3    -15   3  -1
            0.0833      3    -15   1  -3
            '''
else:
    raise RunTimeError(f"ERROR: did not recognize decaymode arg {decaymode}")

masses['1000022'] = neutralinoMass #N1
masses['1000023'] = -neutralinoMass #N2 needs negative sign to be higgsino-like
masses['1000024'] = neutralinoMass #C1
decays['1000022'] = 'DECAY   1000022  1.0' + branchingRatiosN1
decays['1000023'] = 'DECAY   1000023  1.0' + branchingRatiosN1 #N2 has same decay as N1
decays['1000024'] = 'DECAY   1000024  1.0' + branchingRatiosC1
param_blocks.update(common_mixing_matrix("higgsino"))

njets = 2
# __And now production. Taking cues from higgsinoRPV.py generator__
process = '''
import model RPVMSSM_UFO
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define c1 = x1+ x1-
'''

if productionmode == 'C1N2N1' : #production modes cover all of C1N1, N2N1, C1+C1-, and C1N2
    process += '''
    define X = n1 c1
    define Y = n2 c1
    generate    p p > X Y     RPV=0 QED<=2 / susystrong @1
    add process p p > X Y j   RPV=0 QED<=2 / susystrong @2
    add process p p > X Y j j RPV=0 QED<=2 / susystrong @3
    '''
elif productionmode == 'C1N1' :                                                                                           
    process += '''
    generate    p p > n1 c1     RPV=0 QED<=2 / susystrong @1
    add process p p > n1 c1 j   RPV=0 QED<=2 / susystrong @2
    add process p p > n1 c1 j j RPV=0 QED<=2 / susystrong @3
    '''
elif productionmode == 'N1N2' :
    process += '''
    generate    p p > n1 n2     RPV=0 QED<=2 / susystrong @1
    add process p p > n1 n2 j   RPV=0 QED<=2 / susystrong @2
    add process p p > n1 n2 j j RPV=0 QED<=2 / susystrong @3
    '''
elif productionmode == 'C1C1' :
    process += '''
    generate    p p > x1+ x1-     RPV=0 QED<=2 / susystrong @1
    add process p p > x1+ x1- j   RPV=0 QED<=2 / susystrong @2
    add process p p > x1+ x1- j j RPV=0 QED<=2 / susystrong @3
    '''
else:
    raise RunTimeError(f"ERROR: did not recognize productionmode arg {productionmode}")

if njets==1:
    process = '\n'.join([x for x in process.split('\n') if not "j j" in x])

if '1L20' in event_filter:
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()
    filtSeq.LeptonFilter.Ptcut  = 20*GeV
    evt_multiplier = 3.

evgenConfig.contact = ["jmontejo@cern.ch"]
evgenConfig.keywords +=['SUSY', 'RPV', 'neutralino']
if productionmode == 'C1N2N1' :
    evgenConfig.description = 'C1N1, C1N2, N1N2, C1C1 ewk production'
elif productionmode == 'C1N1' :
    evgenConfig.description = 'chargino - neutralino1'
elif productionmode == 'N1N2' :
    evgenConfig.description = 'neutralino1 - neutralino2'
elif productionmode == 'C1C1' :
    evgenConfig.description = 'chargino+ - chargino-'
if decaymode == 'HF':
    evgenConfig.description += 'Electroweak production, and decays via RPV LQD ijk coupling (i,j=1,2 and k=3), m_N1 = m_C1 = m_N2 = %.1f GeV'%(masses['1000022'])
if decaymode == 'LF':
    evgenConfig.description += 'Electroweak production, and decays via RPV LQD ijk coupling (i,j,k=1,2), m_N1 = m_C1 = m_N2 = %.1f GeV'%(masses['1000022'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
