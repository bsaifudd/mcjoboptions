from MadGraphControl.MadGraphUtils import modify_param_card, new_process, modify_run_card, generate, print_cards, arrange_output
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
from MadGraphControl import MadGraph_NNPDF30NLO_Base_Fragment

evgenConfig.description = 'aMcAtNlo High mass charged Higgs NLO4FS'
evgenConfig.keywords+=['2photon','chargedHiggs', 'BSMHiggs']
evgenConfig.contact = ['walter.hopkins@cern.ch', 'kevin.sedlaczek@cern.ch']
evgenConfig.nEventsPerJob = 50000

# maximum number of events
nevents = runArgs.maxEvents*1.2 if runArgs.maxEvents>0 else 1.2*evgenConfig.nEventsPerJob

# Decay widths for mass points: mhc|mh3: [width_A, width_Hpm]
# taken from https://arxiv.org/abs/2103.07484
width_dict = {
    '80.0': {'260.0': [0.0002432614054551, 3.4577278747719498],
            '240.0': [0.0002432614054551, 2.3309364156248407],
            '220.0': [0.0002432614054551, 1.4038726765679916],
            '200.0': [0.0002432614054551, 0.6803883702392283],
            '180.0': [0.0002432614054551, 0.1896026332095376]},
    '100.0': {'260.0': [0.0003094590615153, 2.781557074542913],
            '240.0': [0.0003094590615153, 1.7521652916679018],
            '220.0': [0.0003094590615153, 0.9309303976990704],
            '200.0': [0.0003094590615153, 0.3289444851304176]},
    '120.0': {'260.0': [0.000385082759502, 2.0859180362855536]},

}

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
  
physics_short = str(phys_short.split('_')[2][0:])
print("Physics short is {}".format(physics_short))

process="""
    set group_subprocesses Auto
    set ignore_six_quark_processes False   
    set loop_optimized_output True
    set complex_mass_scheme False
    import model 2HDM_NLO_CPV
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define wpm = w+ w-
    define hpm = h+ h-
    define ts = t t~
    define bs = b b~
    generate p p > ts hpm bs [QCD]
    output -f
    """
#------------------------------------------------------------
# Set masses and decays
#------------------------------------------------------------

# SM Higgs
mh1 = 125.0
# H^+-
mhc = float(phys_short.split('_')[3][1:])
# h_BSM
mh2 = mhc
# A
mh3 = float(phys_short.split('_')[4][1:])

print('''
-------  MASSES  -------
SM Higgs:   {} GeV
H^+-:       {} GeV
h_BSM:      {} GeV
A:          {} GeV
'''.format(mh1, mhc, mh2, mh3)
)


# We are considering the H+->WA benchmark from https://arxiv.org/abs/2103.07484
masses = {'25':str(mh1),
          '35':str(mh2),
          '36':str(mh3),
          '37':str(mhc)
          }

params = {}
params['mass'] = masses
print(masses)

# Set decay channels
# Here it's h+ -> W A and A -> yy
decays = {}

decays['36'] = '''DECAY   36     {}   # A
# BR             NDA      ID1    ID2
1.00000000E+00    2       22     22     # Set BR(A -> yy)
'''.format(width_dict[str(mh3)][str(mhc)][0])

decays['37'] = '''DECAY   37     {}   # h+
# BR              NDA      ID1     ID2
1.00000000E+00    2        24      36   # Set BR(h+ -> W A)
'''.format(width_dict[str(mh3)][str(mhc)][1])

decays['24'] = '''DECAY   24     2.085000   # w+'''
decays['25'] = '''DECAY   25    4.1e-03   # SM h'''

params['DECAY'] = decays


#----------------------------------------------------------------


extras = { 'lhe_version':'3.0',
           'parton_shower':'PYTHIA8',
        #    'PDF_set_min'    :'260401',
        #    'PDF_set_max'    :'260500',
           }

extras['nevents'] = nevents

# set up process
process_dir = new_process(process)
print("process_dir + {}".format(process_dir))

#------------------------------------------------
#Run Card
#------------------------------------------------
modify_run_card(process_dir = process_dir,
                runArgs = runArgs,
                settings = extras
                )

#------------------------------------------------
#Param Card
#------------------------------------------------
modify_param_card(process_dir = process_dir,
                params = params
                )

################################################################
# MADSPIN
################################################################

madspin_card = process_dir + '/Cards/madspin_card.dat'
if os.access(madspin_card, os.R_OK):
    os.unlink(madspin_card)

mscard = open(madspin_card, 'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
# set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
# set BW_cut 400                # cut on how far the particle can be off-shell
# set max_weight_ps_point 400  # number of PS to estimate the maximum for each event

set seed %i
# set spinmode none

decay h+ > w+ h3
decay h3 > a a
decay w+ > all all
decay h- > w- h3
decay w- > all all

launch"""%runArgs.randomSeed)
mscard.close()

print_cards()

generate(process_dir = process_dir, runArgs = runArgs)

outputDS = arrange_output(process_dir = process_dir,
                    runArgs = runArgs,
                    lhe_version = 3,
                    saveProcDir = False
                    )


# Showering via Pythia8
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")


genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
        "37:oneChannel = 1 1.0 0 24 36",      # switch on H+ to WA
        ]

genSeq.Pythia8.Commands += ["Higgs:useBSM = on",
        "36:oneChannel = 1 1.0 0 22 22",      # switch on A -> yy
        ]
