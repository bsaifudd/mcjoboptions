### includes
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

### helpers
def StringToFloat(s):
    if "p" in s:
        return float(s.replace("p", "."))
    return float(s)

### parse job options name 
# N.B. 60 chars limit on PhysicsShort name...
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
evgenLog.info("Physics short name: " + str(phys_short))
additional_options = phys_short.split("_")[4:]
evgenLog.info("DirectStau: Additional options: " + str(additional_options))

if not 'ISR' in phys_short :
  raise RuntimeError("This control file is only for StauStauISR production")

# Extract job settings/masses etc.
mslep      = StringToFloat(phys_short.split('_')[2]) 
mn1        = StringToFloat(phys_short.split('_')[3])
masses['1000011'] = mslep
masses['1000013'] = mslep
masses['1000015'] = mslep
masses['2000011'] = mslep
masses['2000013'] = mslep
masses['2000015'] = mslep
masses['1000022'] = mn1
m_dM=mslep-mn1


process = '''
generate    p p > ta1- ta1+ j , (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @1 
add process p p > ta2- ta2+ j , (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @2 
add process p p > ta1- ta1+ j j , (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @3
add process p p > ta2- ta2+ j j, (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @4
'''
useguess = True

evgenLog.info('generation of stau-pair production with ISR; grid point decoded into m(stau) = %f GeV, m(N1) = %f GeV' % (mslep, mn1))

evgenConfig.contact = [ "mann@cern.ch" ]
evgenConfig.keywords += ['SUSY', 'stau']
evgenConfig.description = 'Direct stau-pair production with ISR in simplified model, m_stauLR = %s GeV, m_N1 = %s GeV' % (mslep, mn1)

# Filter and event multiplier 
evt_multiplier = 2.2

# if m_dM >= 10 :
#     evt_multiplier *= 3
# elif m_dM >= 5 :
#     evt_multiplier *= 4
# elif m_dM >= 3 :
#     evt_multiplier *= 5
# else:
#     raise RuntimeError("Couldn't properly set evt_multiplier (mass splitting below 2 GeV not currently allowed), exiting")

#//////////////////////////////////////////////////////////////////////////////
# MadGraph5 Options
#--------------------------------------------------------------
run_settings['ptj1min']=50 # boosted jet 
run_settings['ptj'] = min(mslep*0.25, 500) / 2 // 10 *10  

#for opt in additional_options:
#    if 'MET' in opt:
#        lower_cut = int(opt.split('MET')[1]) * 1000
#        include ("GeneratorFilters/xAODMETFilter_Common.py")
#        filtSeq.xAODMissingEtFilter.METCut = lower_cut
#        filtSeq.xAODMissingEtFilter.UseNeutrinosFromHadrons = False
#        filtSeq.Expression = "xAODMissingEtFilter"
#        if lower_cut <= 50e3:
#            evt_multiplier *= 1.2
#        elif lower_cut < 80e3:
#            evt_multiplier *= 2
#        else:
#            evt_multiplier *= 3

if '2TFilt' in additional_options:
  
    evgenLog.info('DirectStau: 2 Tau filter is applied.')
                  
    include ("GeneratorFilters/xAODTauFilter_Common.py")
    filtSeq.xAODTauFilter.EtaMaxe = 2.6
    filtSeq.xAODTauFilter.EtaMaxmu = 2.8
    filtSeq.xAODTauFilter.EtaMaxhad = 2.6

    filtSeq.xAODTauFilter.Ptcute = 4e3
    filtSeq.xAODTauFilter.Ptcutmu = 3e3
    filtSeq.xAODTauFilter.Ptcuthad = 12.e3
    filtSeq.xAODTauFilter.Ntaus = 2
    
    filtSeq.xAODTauFilter.UseNewOptions = False

    filtSeq.Expression = "xAODTauFilter"    

    # set higher evt_multiplier when using filter
    evt_multiplier *= 4

elif 'METor2TFilt' in additional_options:
  
    evgenLog.info('DirectStau: 2 Tau filter is applied.')
                  
    include ("GeneratorFilters/xAODTauFilter_Common.py")
    filtSeq.xAODTauFilter.EtaMaxe = 2.6
    filtSeq.xAODTauFilter.EtaMaxmu = 2.8
    filtSeq.xAODTauFilter.EtaMaxhad = 2.6

    filtSeq.xAODTauFilter.Ptcute = 4e3
    filtSeq.xAODTauFilter.Ptcutmu = 3e3
    filtSeq.xAODTauFilter.Ptcuthad = 12.e3
    filtSeq.xAODTauFilter.Ntaus = 2
    
    filtSeq.xAODTauFilter.UseNewOptions = False

    include ("GeneratorFilters/xAODMETFilter_Common.py")
    filtSeq.xAODMissingEtFilter.METCut = 175000
    filtSeq.xAODMissingEtFilter.UseNeutrinosFromHadrons = False

    filtSeq.Expression = "xAODTauFilter or xAODMissingEtFilter"

    # set higher evt_multiplier when using filter
    evt_multiplier *= 5
elif 'METorTFilt' in additional_options:
  
    evgenLog.info('DirectStau: Tau filter is applied.')
                  
    include ("GeneratorFilters/xAODTauFilter_Common.py")
    filtSeq.xAODTauFilter.EtaMaxe = 2.6
    filtSeq.xAODTauFilter.EtaMaxmu = 2.8
    filtSeq.xAODTauFilter.EtaMaxhad = 2.6

    filtSeq.xAODTauFilter.Ptcute = 4e3
    filtSeq.xAODTauFilter.Ptcutmu = 3e3
    filtSeq.xAODTauFilter.Ptcuthad = 12.e3
    filtSeq.xAODTauFilter.Ntaus = 1
 
    filtSeq.xAODTauFilter.UseNewOptions = False

    include ("GeneratorFilters/xAODMETFilter_Common.py")
    filtSeq.xAODMissingEtFilter.METCut = 125000
    filtSeq.xAODMissingEtFilter.UseNeutrinosFromHadrons = False

    filtSeq.Expression = "xAODTauFilter or xAODMissingEtFilter"

    evt_multiplier *= 3
    if m_dM <=10:
        evt_multiplier *= 1.3
elif 'TFilt' in additional_options:
  
    evgenLog.info('DirectStau: Tau filter is applied.')
                  
    include ("GeneratorFilters/xAODTauFilter_Common.py")
    filtSeq.xAODTauFilter.EtaMaxe = 2.6
    filtSeq.xAODTauFilter.EtaMaxmu = 2.8
    filtSeq.xAODTauFilter.EtaMaxhad = 2.6

    filtSeq.xAODTauFilter.Ptcute = 4e3
    filtSeq.xAODTauFilter.Ptcutmu = 3e3
    filtSeq.xAODTauFilter.Ptcuthad = 12.e3
    filtSeq.xAODTauFilter.Ntaus = 1
 
    filtSeq.xAODTauFilter.UseNewOptions = False

    filtSeq.Expression = "xAODTauFilter"    
    evt_multiplier *= 3
    if m_dM <=10:
        evt_multiplier *= 1.3

elif 'testFilt' in additional_options:
  
    evgenLog.info('DirectStau: test Tau filter is applied.')
                  
    include ("GeneratorFilters/xAODTauFilter_Common.py")
    filtSeq.xAODTauFilter.Ntaus = 0
    filtSeq.xAODTauFilter.UseNewOptions = False
    filtSeq.Expression = "xAODTauFilter"    
else:
  
    evgenLog.info('DirectStau: No filter is applied')

# need more at low stau masses
# if mslep < 130: 
#evt_multiplier *= 4
#if m_dM <=10:
#    evt_multiplier *= 4
#evt_multiplier *= 2

# Configure our decays
decays['1000015'] = """DECAY   1000015     auto   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     0.00000000E+00    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     0.00000000E+00    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     0.00000000E+00    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     0.00000000E+00    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     0.00000000E+00    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     0.00000000E+00    2     1000016       -37   # BR(~tau_1 -> ~nu_tauL H-)
     0.00000000E+00    2     1000016       -24   # BR(~tau_1 -> ~nu_tauL W-)
"""
decays['2000015'] = """DECAY   2000015     auto   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     0.00000000E+00    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     0.00000000E+00    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     0.00000000E+00    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     0.00000000E+00    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     0.00000000E+00    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
     0.00000000E+00    2     1000016       -37   # BR(~tau_2 -> ~nu_tauL H-)
     0.00000000E+00    2     1000016       -24   # BR(~tau_2 -> ~nu_tauL W-)
     0.00000000E+00    2     1000015        25   # BR(~tau_2 -> ~tau_1 h)
     0.00000000E+00    2     1000015        35   # BR(~tau_2 -> ~tau_1 H)
     0.00000000E+00    2     1000015        36   # BR(~tau_2 -> ~tau_1 A)
     0.00000000E+00    2     1000015        23   # BR(~tau_2 -> ~tau_1 Z)
"""

# Configure stau mixing
param_blocks['selmix'] = {}
if 'mmix' in additional_options:
    # https://arxiv.org/pdf/0801.0045.pdf
    # use maximally mixed staus
    evgenLog.info("DirectStau: Maxmix scenario selected.")
    param_blocks['selmix'][ '3   3' ] = '0.70710678118' # # RRl3x3
    param_blocks['selmix'][ '3   6' ] = '0.70710678118' # # RRl3x6
    param_blocks['selmix'][ '6   3' ] = '-0.70710678118' # # RRl6x3
    param_blocks['selmix'][ '6   6' ] = '0.70710678118' # # RRl6x6

else:
    # https://arxiv.org/pdf/0801.0045.pdf
    # No mixing stau1=stauL stau2=stauR
    param_blocks['selmix'][ '3   3' ] = '1.0' # # RRl3x3
    param_blocks['selmix'][ '3   6' ] = '0.0' # # RRl3x6
    param_blocks['selmix'][ '6   3' ] = '0.0' # # RRl6x3
    param_blocks['selmix'][ '6   6' ] = '1.0' # # RRl6x6

if 's11' in additional_options:
  
    # only do stau1 modes
    evgenLog.info("DirectStau: Only do stau1-stau1 production modes.")
    process = '''
    generate    p p > ta1- ta1+ j  , (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @1
    add process p p > ta1- ta1+ j j, (ta1- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @2
    '''
    useguess = False
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{ta1-,1000015}{ta1+,-1000015}"]

if 's22' in additional_options:
  
    # only do stau2 modes
    evgenLog.info("DirectStau: Only do stau2-stau2 production modes.")
    process = '''
    generate    p p > ta2- ta2+ j  , (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @1
    add process p p > ta2- ta2+ j j, (ta2- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @2
    '''
    useguess = False
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{ta2-,2000015}{ta2+,-2000015}"]

if 's12' in additional_options:

    # include mixed production modes
    evgenLog.info("DirectStau: Include mixed production modes.")
    process += '''
    add process p p > ta1- ta2+ j  , (ta1- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @3
    add process p p > ta1- ta2+ j j, (ta1- > ta- n1), (ta2+ > ta+ n1)   $ susystrong @4
    add process p p > ta2- ta1+ j  , (ta2- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @5
    add process p p > ta2- ta1+ j j, (ta2- > ta- n1), (ta1+ > ta+ n1)   $ susystrong @6
    '''
if "Dm" in additional_options:
    Dm = filter(lambda x: x.startswith("Dm"), additional_options)
    if Dm:
    
        # introduce mass splitting
        deltaM = int(Dm[0][2:])
        masses['2000011'] = mslep + deltaM
        masses['2000013'] = mslep + deltaM
        masses['2000015'] = mslep + deltaM
        evgenLog.info('DirectStau: Mass of stau2 (etc.) set to %d GeV' % (masses['2000015']))

# post include
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

# AGENE-1511: new CKKW-L "guess" merging features
if useguess:
    evgenLog.info('Using Merging:Process = guess')
    genSeq.Pythia8.Commands += ["Merging:Process = guess"] 
    genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO'] 
else:
    evgenLog.info('Using standard merging syntax: ' + str(genSeq.Pythia8.Commands))
