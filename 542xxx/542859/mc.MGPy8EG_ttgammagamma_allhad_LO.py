from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[260000,93300], # list of pdfs ids for which all variations (error sets) will be included as weights
    'alternative_pdfs':[260800, 266000, 265000, 14400, 27100], # list of pdfs ids for which only the central set will be included as weights
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

evgenConfig.description = 'MG5_aMcAtNlo ttyy 2->8 allhadronic @LO'
evgenConfig.keywords+=['SM', 'ttgammagamma', 'allHadronic']
evgenConfig.contact = ['brendon.aurele.bullard@cern.ch','mvassil@stanford.edu' ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.generators += ['MadGraph', 'Pythia8']

######
nevents = runArgs.maxEvents * 1.2
gridpack_mode=False

check_reset_proc_number(opts)
    
if not is_gen_from_gridpack():    
    process ="""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define uc = u c
    define uc~ = u~ c~
    define ds = d s
    define ds~ = d~ s~
    set acknowledged_v3.1_syntax True --no_save
    generate p p > t t~ > uc ds~ b ds uc~ b~ a a QCD=2 QED=6
    output -f
    """
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

extras = {
    'nevents'        : int(nevents),
    'bwcutoff'       : 50.,
    'dynamical_scale_choice' : 3,
    'lhe_version'   :'3.0',
    'maxjetflavor'  : 5,
    'pta'           : 20,
    'ptl'           : 0,
    'xptl'          : 5,
    'ptj'           : 0,
    'etal'          : 5.0,
    'etaj'          : 5.0,
    'etaa'          : 5.0,
    'drjj'          : 0.0,
    'drjl'          : 0.0,
    'drll'          : 0.0,
    'draa'          : 0.0,
    'draj'          : 0.2,
    'dral'          : 0.2,
    'mmaa'          : 90,
    'mmaamax'       : 175
}

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

### common to all job option
arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3,saveProcDir=False)
check_reset_proc_number(opts)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
