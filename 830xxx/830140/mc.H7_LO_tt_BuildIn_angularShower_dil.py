include("Herwig7_i/Herwig7_BuiltinME.py")
include("Herwig7_i/Herwig71_EvtGen.py")

evgenConfig.generators  += ["Herwig7"]
evgenConfig.description = "Herwig7 LO ttbar sample with H7 Default tune, build-in ME, MMHT2014lo68cl PDF, angular-ordered shower, dilepton filter"
evgenConfig.keywords    = ["SM", "ttbar", "top", "lepton"]
evgenConfig.contact     = ["Andrea Knue (aknue@cern.ch)"]
evgenConfig.tune = "H7.1-Default"
evgenConfig.nEventsPerJob=5000

Herwig7Config.me_pdf_commands(order="LO", name="MMHT2014lo68cl")

Herwig7Config.add_commands("""

insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEHeavyQuark
set /Herwig/MatrixElements/SubProcess:MatrixElements[0]:QuarkType Top
set /Herwig/MatrixElements/SubProcess:MatrixElements[0]:TopMassOption OffShell

""")

Herwig7Config.run()

include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
