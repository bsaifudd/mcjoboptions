import MadGraphControl.MadGraphUtils
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*1.5 if runArgs.maxEvents>0 else 1.5*evgenConfig.nEventsPerJob

maxjetflavor=5
parton_shower='PYTHIA8'
nJetMax=0
qCut=20.

gridpack_mode=True

if not is_gen_from_gridpack():
  process="""
  import model QAll_5_Aug21v2 
  define l = e+ mu+ e- mu-
  define v = ve ve~ vm vm~
  define p = g u c d s u~ c~ d~ s~ b b~
  define j = g u c d s u~ c~ d~ s~ b b~
  generate p p > l v a a QCD=0 M0^2==1 M2^2==1
  output -f"""
  process_dir = new_process(process)
else:
  process_dir = str(MADGRAPH_GRIDPACK_LOCATION)

settings={'nevents':int(nevents),
            'dynamical_scale_choice': 3,
            'asrwgtflavor': 5,
            'auto_ptj_mjj': False,
            'cut_decays'    : True,
            'ptl'           : 22,
            'ptgmin'        : 17, 
            'etal'          : 3.0,
            'etaa'          : 3.0,
            'epsgamma'      : 0.1,
            'R0gamma'       : 0.1,
            'xn'            : 2,
            'isoEM'         : True,
            'maxjetflavor'  :int(maxjetflavor),
            'dral'          : 0.1,
            'draa'          :0.1, 
}


param_card_settings={'anoinputs':{
            'FS0'   : 0.0,
            'FS1'   : 0.0,
            'FS2'   : 0.0,
            'FM0'   : 9e-11,
            'FM1'   : 0.0,
            'FM2'   : 9e-11,
            'FM3'   : 0.0,
            'FM4'   : 0.0,
            'FM5'   : 0.0,
            'FM6'  : 0.0,
            'FM7'  : 0.0,
            'FT0'  : 0.0,
            'FT1'  : 0.0,
            'FT2'  : 0.0,
            'FT3'  : 0.0,
            'FT4'  : 0.0,
            'FT5'  : 0.0,
            'FT6'  : 0.0,
            'FT7'  : 0.0,
            'FT8'  : 0.0,
            'FT9'  : 0.0}
}

sminputs={'aEWM1':"1.323489e+02"}
param_card_settings['sminputs'] = sminputs
massinputs={'MMU':"0.0","MT":"1.725000e+02",'MTA':"1.776820e+00"} 
param_card_settings['mass'] = massinputs
yukawainputs={"ymt":"1.725000e+02"}
param_card_settings["yukawa"]=yukawainputs
widthinputs={"WT":"1.320000e+00","WH":"6.382339e-03"}
param_card_settings["DECAY"]=widthinputs


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

modify_param_card(process_dir=process_dir,params=param_card_settings)






generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)



include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.description = 'MadGraph_wgg_SM'
evgenConfig.keywords+=['SM','triboson','AQGC','electroweak']
evgenConfig.generators = ["MadGraph","Pythia8","EvtGen"]
evgenConfig.contact = ['Raphael Hulsken <raphael.hulsken@cern.ch>']

