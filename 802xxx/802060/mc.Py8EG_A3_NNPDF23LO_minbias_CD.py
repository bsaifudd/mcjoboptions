evgenConfig.description = "Pythia8 central diffractive minimum bias events with the A3 NNPDF23LO tune and EvtGen"
evgenConfig.keywords = ["QCD", "minBias", "SM"]
evgenConfig.contact  = [ "maciej.piotr.lewicki@cern.ch"]
evgenConfig.nEventsPerJob = 10000

# lines 23-27 are specific for setting up the cenral diffraction:
# - sampleType=2 to account for MPI, to match with other samples (ND, SD, DD)
# - MinBias Rockefeller tune (PomFlux=5):
#   https://indico.cern.ch/event/181298/contributions/309559/attachments/243625/340911/MBR_ICHEP2012_RCiesielski.pdf
# - PomSet=6 is the default Pomeron PDF based on H1 2006 Fit B LO
# - zeroAXB - makes sure that central diffraction is not turned off

include("Pythia8_i/Pythia8_Base_Fragment.py")

genSeq.Pythia8.Commands += [
    "Tune:ee = 7",
    "Tune:pp = 14",
    "MultipartonInteractions:bProfile = 2",
    "MultipartonInteractions:pT0Ref = 2.45",
    "MultipartonInteractions:ecmPow = 0.21",
    "MultipartonInteractions:coreRadius = 0.55",
    "MultipartonInteractions:coreFraction = 0.9",
    "Diffraction:sampleType = 2",
    "SigmaDiffractive:PomFlux = 5",
    "PDF:PomSet = 6",
    "SoftQCD:centralDiffractive = on",
    "SigmaTotal:zeroAXB = off"
    ]

evgenConfig.tune = "A3 NNPDF23LO"