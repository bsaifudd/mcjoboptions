#--------------------------------------------------------------
#Pythia8 pp/gg -> X -> J/psi+Xi ->2mu+proton+2pion production
#--------------------------------------------------------------

evgenConfig.nEventsPerJob = 10000
evgenConfig.description = "Pythia8 pp/gg -> X -> J/psi+Xi ->2mu+proton+2pion"
evgenConfig.keywords = ["heavyFlavour","Jpsi","Muon"]
evgenConfig.contact = ["yue.xu@cern.ch"]
evgenConfig.process = "pp/gg -> X -> J/psi+Xi ->2mu+proton+2pion"
evgenConfig.generators += ['Pythia8']

include('Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py')
include("Pythia8_i/Pythia8_Photospp.py")

genSeq.Pythia8.Commands += [
'Higgs:useBSM = on',
'HiggsBSM:ffbar2H+- = on',
'37:mMin = 0',
'37:mMax = 15.0',
############# For Fixed Mass Distribution#############
'37:m0 = 4.5', # P_css
'37:mWidth = 0.017', 
'37:spinType = 2',
'37:addChannel = 1 1.00 100 443 -3312', # Jpsi + charged Xi(ssd)
'37:onMode = off',
'37:onIfMatch = 443 3312',
'443:onMode = off',
'443:onIfMatch = 13 13',
'3312:onMode = off',
'3312:onIfMatch = 3122 -211',
]

### Set lepton filters
if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   muonfilter1 = MultiMuonFilter("muonfilter1")
   muonfilter2 = MultiMuonFilter("muonfilter2")
   filtSeq += muonfilter1
   filtSeq += muonfilter2

filtSeq.muonfilter1.Ptcut = 2400.0 #MeV
filtSeq.muonfilter1.Etacut = 2.7
filtSeq.muonfilter1.NMuons = 2 #minimum

filtSeq.muonfilter2.Ptcut = 2800.0 #MeV
filtSeq.muonfilter2.Etacut = 2.7
filtSeq.muonfilter2.NMuons = 1 #minimum

