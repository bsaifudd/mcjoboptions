evgenConfig.description = "Inclusive pp->bb->J/psi(1S)(mu9mu5) production with Photos"
evgenConfig.process = "bb -> J/Psi(1S) -> 2mu"
evgenConfig.keywords = ["charmonium","2muon"]
evgenConfig.contact  = ["dominic.hirschbuehl@cern.ch","sabidi@cern.ch"] 
evgenConfig.nEventsPerJob = 200

include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include('Pythia8B_i/Pythia8B_Photospp.py')
include("Pythia8B_i/Pythia8B_inclusiveBJpsi_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']
# Close all J/psi decays apart from J/psi->mumu
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]

# Setting for production
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 0.0
genSeq.Pythia8B.QuarkEtaCut = 2.8
genSeq.Pythia8B.AntiQuarkEtaCut = 2.8
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.NHadronizationLoops = 1


# mu pT cuts
genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [9.0,5.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.8
genSeq.Pythia8B.MinimumCountPerCut = [1,2]
