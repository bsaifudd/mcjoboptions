##############################################################
# Job options fragment for pp->Upsilon1S(mu2p5mu2p5)X
##############################################################

evgenConfig.description   = "Inclusive pp->Upsilon1S(mu2p5mu2p5) production with Photos"
evgenConfig.process       = "Upsilon(1S) -> 2mu"
evgenConfig.keywords      = [ "charmonium", "Upsilon", "2muon", "inclusive" ]
evgenConfig.contact       = [ "pavel.reznicek@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include('Pythia8B_i/Pythia8B_Photospp.py')
include("Pythia8B_i/Pythia8B_Bottomonium_Common.py")

#genSeq.Pythia8B.Commands += [ 'PhaseSpace:pTHatMin = 1.' ]  # already defined in Bottomonium_Common

genSeq.Pythia8B.Commands += [ '553:onMode = off' ]
genSeq.Pythia8B.Commands += [ '553:3:onMode = on' ]

genSeq.Pythia8B.SignalPDGCodes = [ 553, -13,13 ]

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [ 2.5 ]
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [ 2 ]
