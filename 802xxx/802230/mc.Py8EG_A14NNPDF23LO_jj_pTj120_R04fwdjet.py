# JO for Pythia 8 jet jet forward jet R=0.4 jet (|eta|>2.8) pTj>120GeV

evgenConfig.description = "Pythia8 dijet sample with A14 NNPDF23 LO tune, filtered for at least one forward jet pT>120 GeV"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["agnieszka.ogrodnik@cern.ch"]
evgenConfig.nEventsPerJob = 500

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 15."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.4)
filtSeq.QCDTruthJetFilter.MinEta = 2.8
filtSeq.QCDTruthJetFilter.MaxEta = 9
filtSeq.QCDTruthJetFilter.SymEta = True
from AthenaCommon.SystemOfUnits import GeV
filtSeq.QCDTruthJetFilter.MinPt = 120*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 7000*GeV
