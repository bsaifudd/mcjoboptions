evgenConfig.description   = "B*- -> B- gamma with B- -> J/psi(mumu) mu- with the last mu- mass as kaon"
evgenConfig.keywords      = [ "bottom", "Bplus", "2muon", "3muon", "exclusive", "Jpsi" ]
evgenConfig.contact       = [ "Semen.Turchikhin@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("Pythia8B_i/Pythia8B_BPDGCodes.py") # list of B-species
include("Pythia8B_PDG2020Masses.py")

genSeq.Pythia8B.Commands                 += [ 'HardQCD:all = on' ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands                 += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 7' ]
genSeq.Pythia8B.QuarkPtCut                = 7.5
genSeq.Pythia8B.AntiQuarkPtCut            = 0.0
genSeq.Pythia8B.QuarkEtaCut               = 3.0
genSeq.Pythia8B.AntiQuarkEtaCut           = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.NHadronizationLoops = 1
genSeq.Pythia8B.NDecayLoops         = 1

# Final state selections
genSeq.EvtInclusiveDecay.whiteList+=[100541, 100543, -100541, -100543]
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [ -523 ]
genSeq.EvtInclusiveDecay.userDecayFile = "Bminus_Jpsi_mumu_muminus.dec"
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG20_artmu.pdt"
evgenConfig.auxfiles += [genSeq.EvtInclusiveDecay.pdtFile,genSeq.EvtInclusiveDecay.userDecayFile]

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter     
filtSeq += MultiMuonFilter("TwoMuonFilter")
filtSeq += MultiMuonFilter("ThreeMuonFilter")

TwoMuonFilter = filtSeq.TwoMuonFilter
TwoMuonFilter.Ptcut = 3500.
TwoMuonFilter.Etacut = 2.7
TwoMuonFilter.NMuons = 2

ThreeMuonFilter = filtSeq.ThreeMuonFilter
ThreeMuonFilter.Ptcut = 2000.
ThreeMuonFilter.Etacut = 2.7
ThreeMuonFilter.NMuons = 3

