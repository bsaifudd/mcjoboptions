#---------------------------------------------------------------------------------
#Pythia8 pp/gg -> Cascade_b0 ->  Jpsi + Lambda + Ks production
#---------------------------------------------------------------------------------

evgenConfig.description = "Pythia8B Cascade_b0 -> Jpsi + Lambda + Ks"
evgenConfig.process = "Cascade_b0 -> Jpsi + Lambda + Ks"
evgenConfig.keywords = ["muon","Jpsi","heavyFlavour"]
evgenConfig.generators = ["Pythia8B"]
evgenConfig.contact  = ["yuxuan.zhang@cern.ch"]
evgenConfig.nEventsPerJob = 100

include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")
include("Pythia8B_i/Pythia8B_Photospp.py")

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True
#genSeq.Pythia8B.UserSelection = 'BJPSIINCLUSIVE'

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.'] # 7 8 9 
genSeq.Pythia8B.QuarkPtCut = 6.0
genSeq.Pythia8B.AntiQuarkPtCut = 6.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.NHadronizationLoops = 10  # 1 (old value) # 6 8 10

# Cascade_b0
genSeq.Pythia8B.Commands += ['5232:onMode = off']
genSeq.Pythia8B.Commands += ['5232:addChannel = 1 1.00 0 443 3122 310']
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:onIfMatch = 13 13']
## Lambda and Ks have long lifetime and decay in Geant4

genSeq.Pythia8B.SignalPDGCodes = [5232,443,13,-13,3122,310] ## need the full decay chain, select Cascade_b0 decay
genSeq.Pythia8B.SignalPtCuts = [0.0,0.0,3.5,3.5,0.0,0]
genSeq.Pythia8B.SignalEtaCuts = [102.5,102.5,2.7,2.7,102.5,2.6]
genSeq.Pythia8B.NumberOfSignalsRequiredPerEvent = 1


