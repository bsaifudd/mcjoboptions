include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on",
                            "23:onMode = off",                  # turn off all decays modes
                            "23:onIfAny = 15",                  # turn on the tautau decay mode
                            "15:onMode = on",                   # decay of taus
                            "15:offIfAny = 11",                 # exclude electron decays
                            "TauDecays:externalMode = 2",       # no external assignments
                            "TauDecays:mode = 4",               # use internal spin assignments
                            "PhaseSpace:mHatMin = 60.",         # lower invariant mass
                            "PhaseSpace:mHatMax = 250."]        # upper invariant mass

evgenConfig.description = "Pythia 8 DY->tautau production with NNPDF23LO tune"
evgenConfig.contact = ["miles.cb@cern.ch"]
evgenConfig.keywords = ["SM", "drellYan", "electroweak", "2tau"]
evgenConfig.generators += ["Pythia8"]

if not hasattr( filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  tauFilterD1 = TauFilter("tauFilterD1")
  filtSeq += tauFilterD1
  filtSeq.tauFilterD1.Ntaus = 1
  filtSeq.tauFilterD1.EtaMaxhad = 100.0
  filtSeq.tauFilterD1.EtaMaxmu = 100.0
  filtSeq.tauFilterD1.Ptcuthad = 25000.0 #MeV
  filtSeq.tauFilterD1.Ptcute = 10000000.0 # turn off tau->el
  filtSeq.tauFilterD1.Ptcutmu = 10000000.0 # turn off tau->mu
  tauFilterD2 = TauFilter("tauFilterD2")
  filtSeq += tauFilterD2
  filtSeq.tauFilterD2.Ntaus = 1
  filtSeq.tauFilterD2.EtaMaxhad = 100.0
  filtSeq.tauFilterD2.EtaMaxmu = 100.0
  filtSeq.tauFilterD2.Ptcuthad = 10000000.0 # turn off tau->had
  filtSeq.tauFilterD2.Ptcute = 10000000.0 # turn off tau->el
  filtSeq.tauFilterD2.Ptcutmu = 12000.0 # MeV
  filtSeq.Expression = "tauFilterD1 and tauFilterD2"




