evgenConfig.description = "ttHbb dilep with A14 tune and NNPDF23LO PDF"
evgenConfig.process = "H -> b + bbar, tt dilep"
evgenConfig.contact = ["sadreyer@cern.ch"] 
evgenConfig.keywords    = [ 'SM','Higgs', 'resonance', 'electroweak']
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.generators += ["EvtGen"]
evgenConfig.nEventsPerJob = 10000

if runArgs.trfSubstepName == 'generate' :
  include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
  genSeq.Pythia8.Commands += ["HiggsSM:gg2Httbar = on",
                              "25:m0 = 125.0",
                              "25:mWidth = 400.0",
                              "25:doForceWidth = true ",
                              "25:mMax = 200 ",
                              "25:mMin = 50 ",
                              "25:onMode = off",
                              "25:onIfAny = 5",
                              "24:onMode = off",
                              "24:onIfAny = 11 13"]

from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
filtSeq += ParticleFilter("ParticleFilter")
filtSeq.ParticleFilter.Ptcut = 200.*GeV
filtSeq.ParticleFilter.Etacut = 10.0
filtSeq.ParticleFilter.StatusReq = -1
filtSeq.ParticleFilter.PDG = 25
filtSeq.ParticleFilter.MinParts = 1
filtSeq.ParticleFilter.Exclusive = True
