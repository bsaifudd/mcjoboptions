evgenConfig.description = "Dijet truth jets"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["matthew.henry.klein@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection = off",
                            "PhaseSpace:mHatMin = 300.",
                            "PhaseSpace:pTHatMin = 100."]

include("GeneratorFilters/FindJets.py")
include ("GeneratorFilters/VBFForwardJetsFilter.py")
CreateJets(prefiltSeq, 0.4)


if not hasattr( filtSeq, "VBFForwardJetsFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
    filtSeq += VBFForwardJetsFilter()
    pass

filtSeq.VBFForwardJetsFilter.TruthJetContainer="AntiKt4TruthJets"
filtSeq.VBFForwardJetsFilter.JetMinPt=70.*GeV
filtSeq.VBFForwardJetsFilter.JetMaxEta=4.5
filtSeq.VBFForwardJetsFilter.NJets=2
filtSeq.VBFForwardJetsFilter.Jet1MinPt=70.*GeV
filtSeq.VBFForwardJetsFilter.Jet1MaxEta=4.5
filtSeq.VBFForwardJetsFilter.Jet2MinPt=70.*GeV
filtSeq.VBFForwardJetsFilter.Jet2MaxEta=4.5
filtSeq.VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
filtSeq.VBFForwardJetsFilter.LGMinPt=15.*GeV
filtSeq.VBFForwardJetsFilter.LGMaxEta=2.5
filtSeq.VBFForwardJetsFilter.DeltaRJLG=0.05
filtSeq.VBFForwardJetsFilter.RatioPtJLG=0.3

# medium cut:
filtSeq.VBFForwardJetsFilter.MassJJ = 1150.*GeV
filtSeq.VBFForwardJetsFilter.DeltaEtaJJ = 4.0
filtSeq.VBFForwardJetsFilter.DeltaPhiJJ = 2.0
filtSeq.VBFForwardJetsFilter.RequireSamePair=True

if not hasattr( filtSeq, "MultiBjetFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiBjetFilter
  filtSeq += MultiBjetFilter()
  print ("MultiBjetFilter added ...")
  pass

filtSeq.MultiBjetFilter.JetPtMin = 15000.
filtSeq.MultiBjetFilter.NJetsMin = 1
filtSeq.MultiBjetFilter.NBJetsMin = 1
filtSeq.MultiBjetFilter.JetEtaMax = 2.5

if not hasattr( filtSeq, "MultiCjetFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiCjetFilter
  filtSeq += MultiCjetFilter()
  print ("MultiCjetFilter added ...")
  pass

filtSeq.MultiCjetFilter.JetPtMin = 15000.
filtSeq.MultiCjetFilter.NJetsMin = 1
filtSeq.MultiCjetFilter.NCJetsMin = 1
filtSeq.MultiCjetFilter.JetEtaMax = 2.5

filtSeq.Expression = "VBFForwardJetsFilter and (MultiCjetFilter or MultiBjetFilter)"  

evgenConfig.nEventsPerJob = 1000
