from MadGraphControl.MadGraphUtils import *

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

#Job bookkeping infos
evgenConfig.description = 'aMcAtNlo pp->4l  0,1j@NLO FxFx merged'
evgenConfig.contact = ["rdinardo@cern.ch"]
evgenConfig.keywords += ['ZZ','4lepton','jets']
evgenConfig.generators += ["aMcAtNlo","Pythia8"]
evgenConfig.nEventsPerJob = 500


# General settings
nevents = runArgs.maxEvents*75 if runArgs.maxEvents>0 else 75*evgenConfig.nEventsPerJob

gridpack_mode=True

if not is_gen_from_gridpack():
    process = """
    import model loop_sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    generate p p > l+ l- l+ l- [QCD] @0
    add process p p > l+ l- l+ l- j [QCD] @1    
    output -f
    """

    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

#Fetch default LO run_card.dat and set parameters
settings = {'lhe_version'   : '3.0',
            'ickkw'         : 3,
            'ptj'           : 10,
            'jetradius'     : 1.0,
            'maxjetflavor'  : 5,
            'mll'           : 4.,
            'mll_sf'        : 4.,
            'ptl'           : 1.,
            'parton_shower' :'PYTHIA8',
            'nevents'       :int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

runName='FxFx_llll'




generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("Pythia8_i/Pythia8_aMcAtNlo.py")

genSeq.Pythia8.Commands += ["JetMatching:merge          = on", 
                            "SpaceShower:alphaSuseCMW    = on",              
                            "SpaceShower:alphaSorder     = 2",               
                            "TimeShower:alphaSuseCMW     = on",              
                            "TimeShower:alphaSorder      = 2",               
                            "SpaceShower:alphaSvalue    = 0.118",
                            "TimeShower:alphaSvalue     = 0.118" 
] 

PYTHIA8_nJetMax=1
PYTHIA8_qCut=25.

print("PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax)
print("PYTHIA8_qCut = %i"%PYTHIA8_qCut)

genSeq.Pythia8.Commands += ["JetMatching:merge            = on",
                            "JetMatching:scheme           = 1",
                            "JetMatching:setMad           = off",
                            "SpaceShower:rapidityOrder    = off",
                            "SpaceShower:pTmaxFudge       = 1.0", 
                            "JetMatching:qCut             = %f"%PYTHIA8_qCut,
                            "JetMatching:coneRadius       = 1.0",
                            "JetMatching:etaJetMax        = 10.0",
                            "JetMatching:doFxFx           = on",
                            "JetMatching:qCutME           = 8.0",
                            "JetMatching:nJetMax          = %i"%PYTHIA8_nJetMax, 
                            'JetMatching:jetAlgorithm = 2', #explicit setting of kt-merging for FxFx (also imposed by Py8-FxFx inteface)
                            'JetMatching:slowJetPower = 1', #explicit setting of kt-merging for FxFx (also imposed by Py8-FxFx inteface)
                            'JetMatching:nQmatch = 5', #4 corresponds to 4-flavour scheme (no matching of b-quarks), 5 for 5-flavour scheme
                            "JetMatching:eTjetMin = %f"%PYTHIA8_qCut #This is 20 in the Pythia default, it should be <= qCut
                            ]

genSeq.Pythia8.UserHooks = ['JetMatchingMadgraph']
genSeq.Pythia8.FxFxXS = True



include("GeneratorFilters/FourLeptonInvMassFilter.py")
filtSeq.FourLeptonInvMassFilter.MinPt   = 3.*GeV
filtSeq.FourLeptonInvMassFilter.MaxEta  = 3.5
filtSeq.FourLeptonInvMassFilter.MinMass = 100.*GeV
filtSeq.FourLeptonInvMassFilter.MaxMass = 170.*GeV


from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter

filtSeq += MultiLeptonFilter("SubLeadingLeptons")
filtSeq.SubLeadingLeptons.Ptcut = 8000.
filtSeq.SubLeadingLeptons.Etacut = 3.5
filtSeq.SubLeadingLeptons.NLeptons = 3
