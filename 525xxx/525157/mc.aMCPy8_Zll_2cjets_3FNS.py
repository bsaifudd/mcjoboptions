import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

evgenConfig.description = 'Zll + 2 c-jets, MG Pythia, 3FNS'
evgenConfig.contact = ["yi.yu@cern.ch", "federico.sforza@cern.ch", "lucreziaboccardo1@gmail.com", "xinzhe.liu@cern.ch"]
evgenConfig.keywords += ['Z','jets']
evgenConfig.generators += ["aMcAtNlo"]

# General settings
evgenConfig.nEventsPerJob = 10000
nevents = 1.1 * runArgs.maxEvents if runArgs.maxEvents>0 else 1.1 * evgenConfig.nEventsPerJob

#Madgraph run card and shower settings
# Shower/merging settings  
maxjetflavor=3
parton_shower='PYTHIA8'

gridpack_mode=True

if not is_gen_from_gridpack():
    process = """
    import model loop_sm-c_mass
    define p = g u d s u~ d~ s~
    define j = p
    define l+ = e+ mu+
    define l- = e- mu-
    generate p p > l+ l- c c~ [QCD]
    output -f"""

    process_dir = str(new_process(process))
else:
    process_dir = str(MADGRAPH_GRIDPACK_LOCATION)

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':321300, # NNPDF31 (pch, 3fns, nlo)
    'pdf_variations':[321300,335300,321900], # list of pdfs ids for which all variations (error sets) will be included as weights 
    'alternative_pdfs':[335300], # NNPDF40 (pch, 3fns, nlo)
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
    }

#Fetch default run_card.dat and set parameters
settings = {
            'maxjetflavor'  : int(maxjetflavor),
            'parton_shower' : parton_shower,
            'nevents'       : int(nevents),
            'jetradius'     : 0.4,
            'ptj'           : 8,
            'etaj'          : 10,
        }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(required_accuracy=0.0001,process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

# Helper for resetting process number
check_reset_proc_number(opts)

#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO                                                 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

