from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*10. if runArgs.maxEvents>0 else 10.*evgenConfig.nEventsPerJob

process_def = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model SMEFTsim_topU3l_MwScheme_UFO
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define bq = b b~
generate p p > ta- ta+ QED<=3 NP<=0 @0
add process p p > ta- ta+ j QED<=3 NP<=0 @1
add process p p > ta- ta+ j j QED<=3 NP<=0 @2
output -f
"""

process_dir = new_process(process_def)

lhaid=260000
pdflabel='lhapdf'

settings = { 'lhe_version' : '3.0', 
             'cut_decays'  : 'F', 
             'lhaid'       : lhaid,
             'pdlabel'     : "'"+pdflabel+"'",
             'ickkw'       : 0,
             'ptj'         : 20,
             'ptb'         : 20,
             'xqcut'       : 0.,
             'drjj'        : 0.0,
             'mmll'        : 120.,
             'maxjetflavor': 5,
             'ktdurham'    : 30,
             'dparameter'  : 0.4,
             'use_syst'    : 'False',
             'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

params = \
    { "mass" : { "1" : 0.0, "2" : 0.0, "3" : 0.0, "4" : 0.0, "5" : 0.0 }
    }

modify_param_card(process_dir=process_dir,params=params)

# Reweighting setup
reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write("""change process p p > ta- ta+ QED<=3 NP<=1 NP^2==1
change process p p > ta- ta+ j QED<=3 NP<=1 NP^2==1 --add
change process p p > ta- ta+ j j QED<=3 NP<=1 NP^2==1 --add
change helicity false
launch --rwgt_name=clebQ                                                                                                                 
    set smeft 125 1.                                                                                                                     
launch --rwgt_name=cbl                                                                                                                   
    set smeft 125 0.                                                                                                                     
    set smeft 122 1.                                                                                                                     
launch --rwgt_name=cbe                                                                                                                   
    set smeft 122 0.                                                                                                                     
    set smeft 116 1.                                                                                                                     
launch --rwgt_name=cQe                                                                                                                   
    set smeft 116 0.                                                                                                                     
    set smeft 118 1.                                                                                                                     
launch --rwgt_name=cQl1                                                                                                                  
    set smeft 118 0.                                                                                                                     
    set smeft 110 1.                                                                                                                     
launch --rwgt_name=cQl3                                                                                                                  
    set smeft 110 0.                                                                                                                     
    set smeft 111 1.                                                                                                                     
launch --rwgt_name=clebQIm                                                                                                               
    set smeft 111 0.                                                                                                                     
    set smeftcpv 49 1.                                                                                                                   
launch --rwgt_name=ceWRe                                                                                                                 
    set smeftcpv 49 0.                                                                                                                   
    set smeft 101 1.                                                                                                                     
launch --rwgt_name=ceBRe                                                                                                                 
    set smeft 101 0.                                                                                                                     
    set smeft 102 1.                                                                                                                     
launch --rwgt_name=ceyRe                                                                                                                 
    set smeft 101 -0.55                                                                                                                  
    set smeft 102 1.                                                                                                                     
launch --rwgt_name=ceZRe                                                                                                                 
    set smeft 101 1.8                                                                                                                    
    set smeft 102 1.""")
reweight_card_f.close()

generate(process_dir=process_dir,runArgs=runArgs)
# saveProcDir=True only for local testing!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#### Shower 
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_SMEFTbbtautau'
evgenConfig.contact  = [ "chays@cern.ch" ]

process="pp>ta+ta-"
nJetMax=2

PYTHIA8_nJetMax=nJetMax
PYTHIA8_Process=process
PYTHIA8_Dparameter=settings['dparameter']
PYTHIA8_TMS=settings['ktdurham']
PYTHIA8_nQuarksMerge=settings['maxjetflavor']
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")

# event filter
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq,0.4)

from GeneratorFilters.GeneratorFiltersConf import MultiBjetFilter
filtSeq += MultiBjetFilter()

filtSeq.MultiBjetFilter.NBJetsMin = 1
filtSeq.MultiBjetFilter.InclusiveEfficiency = 0.15

filtSeq.Expression = "MultiBjetFilter"
