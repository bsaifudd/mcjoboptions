from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

# General settings
evgenConfig.nEventsPerJob = 10000
nevents=3.5*runArgs.maxEvents if runArgs.maxEvents>0 else 3.5*evgenConfig.nEventsPerJob


#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------

mgproc="""generate p p > t t~ t w- QED=3 \nadd process p p > t t~ t~ w+ QED=3"""
name="tttwSM"
keyword=['SM','top', 'LO']

process_string="""
import model sm-no_b_mass
define p = g u c d s u~ c~ d~ s~
define p = p b b~
define j = g u c d s u~ c~ d~ s~
define j = j b b~
"""+mgproc+"""
output -f
"""

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version'  : '3.0',
           'nevents'      : nevents,
           'dynamical_scale_choice' : 3,
 }

process_dir = new_process(process_string)
modify_run_card(runArgs=runArgs,
                process_dir=process_dir,
                settings=extras)

#---------------------------------------------------------------------------
# MadSpin Card
#---------------------------------------------------------------------------
bwcut = 15
topdecay = "decay t > w+ b, w+ > all all \ndecay t~ > w- b~, w- > all all \ndecay w+ > all all \ndecay w- > all all\n"

madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#set Nevents_for_max_weigth 75
set BW_cut %i
set seed %i
%s
launch
"""%(bwcut, runArgs.randomSeed, topdecay))
mscard.close()

print_cards()

#---------------------------------------------------------------------------
# MG5 Generation
#---------------------------------------------------------------------------

generate(process_dir=process_dir,runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,
                          runArgs=runArgs,
                          lhe_version=3,
		          saveProcDir= False
                          )

#---------------------------------------------------------------------------
# Parton Showering Generation
#---------------------------------------------------------------------------

check_reset_proc_number(opts) # not sure what this does

evgenConfig.generators += ["MadGraph"]
evgenConfig.keywords+=keyword 
evgenConfig.contact= ["yang.qin@cern.ch", "shuo.han@cern.ch", "tsaichen_lee@berkeley.edu"]

evgenConfig.description = 'Standard-Model tttw production at LO QCD with MadGraph5 and Pythia8'

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

include("GeneratorFilters/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut    = 12000.
filtSeq.MultiLeptonFilter.Etacut   = 3.
filtSeq.MultiLeptonFilter.NLeptons = 2
