# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  14:08
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.72718214E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.21028658E+03  # scale for input parameters
    1   -1.64663718E+02  # M_1
    2   -1.07451169E+03  # M_2
    3    3.22824373E+03  # M_3
   11   -9.35134736E+02  # A_t
   12   -6.02139221E+02  # A_b
   13   -3.50061906E+03  # A_tau
   23    2.23521521E+03  # mu
   25    5.50231532E+00  # tan(beta)
   26    1.23499112E+03  # m_A, pole mass
   31    9.06654547E+02  # M_L11
   32    9.06654547E+02  # M_L22
   33    1.96829477E+03  # M_L33
   34    1.39485107E+03  # M_E11
   35    1.39485107E+03  # M_E22
   36    1.89550735E+03  # M_E33
   41    9.02995391E+02  # M_Q11
   42    9.02995391E+02  # M_Q22
   43    2.10418729E+03  # M_Q33
   44    1.93342671E+03  # M_U11
   45    1.93342671E+03  # M_U22
   46    8.48484503E+02  # M_U33
   47    5.93371558E+02  # M_D11
   48    5.93371558E+02  # M_D22
   49    3.44477687E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.21028658E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.21028658E+03  # (SUSY scale)
  1  1     8.52171529E-06   # Y_u(Q)^DRbar
  2  2     4.32903137E-03   # Y_c(Q)^DRbar
  3  3     1.02949166E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.21028658E+03  # (SUSY scale)
  1  1     9.42220920E-05   # Y_d(Q)^DRbar
  2  2     1.79021975E-03   # Y_s(Q)^DRbar
  3  3     9.34387904E-02   # Y_b(Q)^DRbar
Block Ye Q=  1.21028658E+03  # (SUSY scale)
  1  1     1.64424441E-05   # Y_e(Q)^DRbar
  2  2     3.39977595E-03   # Y_mu(Q)^DRbar
  3  3     5.71786385E-02   # Y_tau(Q)^DRbar
Block Au Q=  1.21028658E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -9.35135232E+02   # A_t(Q)^DRbar
Block Ad Q=  1.21028658E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -6.02139013E+02   # A_b(Q)^DRbar
Block Ae Q=  1.21028658E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -3.50061843E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  1.21028658E+03  # soft SUSY breaking masses at Q
   1   -1.64663718E+02  # M_1
   2   -1.07451169E+03  # M_2
   3    3.22824373E+03  # M_3
  21   -3.52264449E+06  # M^2_(H,d)
  22   -4.95614458E+06  # M^2_(H,u)
  31    9.06654547E+02  # M_(L,11)
  32    9.06654547E+02  # M_(L,22)
  33    1.96829477E+03  # M_(L,33)
  34    1.39485107E+03  # M_(E,11)
  35    1.39485107E+03  # M_(E,22)
  36    1.89550735E+03  # M_(E,33)
  41    9.02995391E+02  # M_(Q,11)
  42    9.02995391E+02  # M_(Q,22)
  43    2.10418729E+03  # M_(Q,33)
  44    1.93342671E+03  # M_(U,11)
  45    1.93342671E+03  # M_(U,22)
  46    8.48484503E+02  # M_(U,33)
  47    5.93371558E+02  # M_(D,11)
  48    5.93371558E+02  # M_(D,22)
  49    3.44477687E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.11417933E+02  # h0
        35     1.23510467E+03  # H0
        36     1.23499112E+03  # A0
        37     1.23636446E+03  # H+
   1000001     7.77481255E+02  # ~d_L
   2000001     2.90353447E+02  # ~d_R
   1000002     7.73660753E+02  # ~u_L
   2000002     1.91546665E+03  # ~u_R
   1000003     7.77481469E+02  # ~s_L
   2000003     2.90352055E+02  # ~s_R
   1000004     7.73660861E+02  # ~c_L
   2000004     1.91546637E+03  # ~c_R
   1000005     2.08582532E+03  # ~b_1
   2000005     3.46104982E+03  # ~b_2
   1000006     7.00562658E+02  # ~t_1
   2000006     2.09088166E+03  # ~t_2
   1000011     9.15967424E+02  # ~e_L-
   2000011     1.40048816E+03  # ~e_R-
   1000012     9.12447827E+02  # ~nu_eL
   1000013     9.15966291E+02  # ~mu_L-
   2000013     1.40048788E+03  # ~mu_R-
   1000014     9.12447482E+02  # ~nu_muL
   1000015     1.89954148E+03  # ~tau_1-
   2000015     1.97306596E+03  # ~tau_2-
   1000016     1.97053499E+03  # ~nu_tauL
   1000021     3.11425220E+03  # ~g
   1000022     1.64602670E+02  # ~chi_10
   1000023     1.08527957E+03  # ~chi_20
   1000025     2.22492759E+03  # ~chi_30
   1000035     2.22514073E+03  # ~chi_40
   1000024     1.08549810E+03  # ~chi_1+
   1000037     2.22633839E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.74617466E-01   # alpha
Block Hmix Q=  1.21028658E+03  # Higgs mixing parameters
   1    2.23521521E+03  # mu
   2    5.50231532E+00  # tan[beta](Q)
   3    2.44216759E+02  # v(Q)
   4    1.52520307E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     4.82217798E-02   # Re[R_st(1,1)]
   1  2     9.98836653E-01   # Re[R_st(1,2)]
   2  1    -9.98836653E-01   # Re[R_st(2,1)]
   2  2     4.82217798E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99992868E-01   # Re[R_sb(1,1)]
   1  2     3.77688061E-03   # Re[R_sb(1,2)]
   2  1    -3.77688061E-03   # Re[R_sb(2,1)]
   2  2     9.99992868E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.81839849E-02   # Re[R_sta(1,1)]
   1  2     9.95168280E-01   # Re[R_sta(1,2)]
   2  1    -9.95168280E-01   # Re[R_sta(2,1)]
   2  2     9.81839849E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99798824E-01   # Re[N(1,1)]
   1  2    -5.19141425E-04   # Re[N(1,2)]
   1  3    -1.99348383E-02   # Re[N(1,3)]
   1  4     2.15514998E-03   # Re[N(1,4)]
   2  1     2.54623131E-04   # Re[N(2,1)]
   2  2     9.99107072E-01   # Re[N(2,2)]
   2  3    -4.01955758E-02   # Re[N(2,3)]
   2  4    -1.30118681E-02   # Re[N(2,4)]
   3  1     1.56253016E-02   # Re[N(3,1)]
   3  2    -1.92280260E-02   # Re[N(3,2)]
   3  3    -7.06720457E-01   # Re[N(3,3)]
   3  4     7.07058929E-01   # Re[N(3,4)]
   4  1     1.25736659E-02   # Re[N(4,1)]
   4  2    -3.76174343E-02   # Re[N(4,2)]
   4  3    -7.06068774E-01   # Re[N(4,3)]
   4  4    -7.07031625E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.98408273E-01   # Re[U(1,1)]
   1  2     5.63996425E-02   # Re[U(1,2)]
   2  1    -5.63996425E-02   # Re[U(2,1)]
   2  2    -9.98408273E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.99836514E-01   # Re[V(1,1)]
   1  2     1.80816446E-02   # Re[V(1,2)]
   2  1     1.80816446E-02   # Re[V(2,1)]
   2  2    -9.99836514E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     1.07914802E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99999999E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     6.84111639E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99999989E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
DECAY   1000013     1.07914658E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.99999999E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     6.84133647E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.99967610E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000015     9.45271964E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.87257993E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     4.25503455E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     8.48697228E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.50195279E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.67530164E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.77863396E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.54604694E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.07027769E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
DECAY   1000014     1.07027723E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
DECAY   1000016     1.50499762E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.61804849E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.79133446E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.59057514E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.46218760E-02   # ~d_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> chi^0_1 d)
DECAY   1000001     9.83900319E-02   # ~d_L
#    BR                NDA      ID1      ID2
     9.99995340E-01    2     1000022         1   # BR(~d_L -> chi^0_1 d)
DECAY   2000003     7.46206709E-02   # ~s_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> chi^0_1 s)
DECAY   1000003     9.85551295E-02   # ~s_L
#    BR                NDA      ID1      ID2
     9.98320441E-01    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.67955852E-03    2     2000003        25   # BR(~s_L -> ~s_R h^0)
DECAY   1000005     3.94924538E+01   # ~b_1
#    BR                NDA      ID1      ID2
     7.24220502E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.23411390E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.38617214E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.52277073E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
     3.78452118E-01    2     1000006       -37   # BR(~b_1 -> ~t_1 H^-)
DECAY   2000005     1.24750736E+01   # ~b_2
#    BR                NDA      ID1      ID2
     1.54126466E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.31323591E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     8.29824132E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.64942333E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     7.93416714E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     8.19642438E-03    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     4.13571055E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     6.79379722E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   1000002     9.89627012E-02   # ~u_L
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> chi^0_1 u)
DECAY   2000002     4.21292645E+00   # ~u_R
#    BR                NDA      ID1      ID2
     9.99999970E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
DECAY   1000004     9.89620228E-02   # ~c_L
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> chi^0_1 c)
DECAY   2000004     4.21293357E+00   # ~c_R
#    BR                NDA      ID1      ID2
     9.99997032E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
DECAY   1000006     1.21001957E+00   # ~t_1
#    BR                NDA      ID1      ID2
     9.99998628E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     4.27189930E+01   # ~t_2
#    BR                NDA      ID1      ID2
     6.84590153E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.12738942E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.29938881E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.17200512E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.78203936E-01    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     1.77461994E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.77609833E-01    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     1.53295181E+01   # chi^+_1
#    BR                NDA      ID1      ID2
     2.57630105E-02    2    -1000011        12   # BR(chi^+_1 -> ~e^+_L nu_e)
     2.57633257E-02    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     2.68260962E-02    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     2.68261817E-02    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
     2.26285094E-01    2     1000002        -1   # BR(chi^+_1 -> ~u_L d_bar)
     2.26284937E-01    2     1000004        -3   # BR(chi^+_1 -> ~c_L s_bar)
     1.30672706E-04    2     1000006        -5   # BR(chi^+_1 -> ~t_1 b_bar)
     2.21049772E-01    2    -1000001         2   # BR(chi^+_1 -> ~d^*_L u)
     2.21048460E-01    2    -1000003         4   # BR(chi^+_1 -> ~s^*_L c)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     6.89043404E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     3.12191575E-04    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     3.12191890E-04    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     1.07841900E-04    2     1000002        -1   # BR(chi^+_2 -> ~u_L d_bar)
     1.10219681E-04    2     1000004        -3   # BR(chi^+_2 -> ~c_L s_bar)
     8.28333075E-01    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     1.32481939E-04    2     2000006        -5   # BR(chi^+_2 -> ~t_2 b_bar)
     1.04629914E-03    2    -1000001         2   # BR(chi^+_2 -> ~d^*_L u)
     1.05991155E-03    2    -1000003         4   # BR(chi^+_2 -> ~s^*_L c)
     2.15621357E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     4.17295645E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     8.84901691E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     4.19951366E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     4.83750657E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.45459400E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.11023647E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.25514878E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.53106567E+01   # chi^0_2
#    BR                NDA      ID1      ID2
     1.28906373E-02    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     1.28906373E-02    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     1.28907993E-02    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     1.28907993E-02    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     1.33776471E-02    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     1.33776471E-02    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     1.33776959E-02    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     1.33776959E-02    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     1.13010148E-01    2     1000002        -2   # BR(chi^0_2 -> ~u_L u_bar)
     1.13010148E-01    2    -1000002         2   # BR(chi^0_2 -> ~u^*_L u)
     1.13009306E-01    2     1000004        -4   # BR(chi^0_2 -> ~c_L c_bar)
     1.13009306E-01    2    -1000004         4   # BR(chi^0_2 -> ~c^*_L c)
     1.10689654E-01    2     1000001        -1   # BR(chi^0_2 -> ~d_L d_bar)
     1.10689654E-01    2    -1000001         1   # BR(chi^0_2 -> ~d^*_L d)
     1.10689537E-01    2     1000003        -3   # BR(chi^0_2 -> ~s_L s_bar)
     1.10689537E-01    2    -1000003         3   # BR(chi^0_2 -> ~s^*_L s)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     7.40608336E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     3.89670714E-01    2     1000006        -6   # BR(chi^0_3 -> ~t_1 t_bar)
     3.89670714E-01    2    -1000006         6   # BR(chi^0_3 -> ~t^*_1 t)
     3.92294470E-02    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     3.92294470E-02    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     7.86251954E-03    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.19342550E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     4.74763074E-03    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     1.44089228E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     7.42138615E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     3.54935589E-03    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.32209188E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.42354626E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     1.18611869E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     1.18611869E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     7.04024563E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.01849672E-04    2     1000002        -2   # BR(chi^0_4 -> ~u_L u_bar)
     2.01849672E-04    2    -1000002         2   # BR(chi^0_4 -> ~u^*_L u)
     2.08774720E-04    2     1000004        -4   # BR(chi^0_4 -> ~c_L c_bar)
     2.08774720E-04    2    -1000004         4   # BR(chi^0_4 -> ~c^*_L c)
     4.13560871E-01    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     4.13560871E-01    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     2.55488563E-04    2     1000001        -1   # BR(chi^0_4 -> ~d_L d_bar)
     2.55488563E-04    2    -1000001         1   # BR(chi^0_4 -> ~d^*_L d)
     2.56643419E-04    2     1000003        -3   # BR(chi^0_4 -> ~s_L s_bar)
     2.56643419E-04    2    -1000003         3   # BR(chi^0_4 -> ~s^*_L s)
     4.27879562E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     4.27879562E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.27525183E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.74301521E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.38107469E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     7.45172912E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     3.82422837E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     7.60731811E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.83175041E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     7.08883620E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.94461975E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.94461975E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     7.15551188E+02   # ~g
#    BR                NDA      ID1      ID2
     5.67079518E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     5.67079518E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     2.48960293E-02    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     2.48960293E-02    2    -2000002         2   # BR(~g -> ~u^*_R u)
     5.67079484E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     5.67079484E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     2.48960301E-02    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     2.48960301E-02    2    -2000004         4   # BR(~g -> ~c^*_R c)
     5.73851691E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     5.73851691E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.96210128E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.96210128E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     6.32982265E-02    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     6.32982265E-02    2    -2000001         1   # BR(~g -> ~d^*_R d)
     5.66341168E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     5.66341168E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     6.32982372E-02    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     6.32982372E-02    2    -2000003         3   # BR(~g -> ~s^*_R s)
     5.66341127E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     5.66341127E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     1.95842915E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.95842915E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     2.77792357E-03   # Gamma(h0)
     2.06083106E-03   2        22        22   # BR(h0 -> photon photon)
     4.58909935E-04   2        22        23   # BR(h0 -> photon Z)
     5.56079147E-03   2        23        23   # BR(h0 -> Z Z)
     5.81007774E-02   2       -24        24   # BR(h0 -> W W)
     7.40861171E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.49961938E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.89104604E-04   2       -13        13   # BR(h0 -> Muon muon)
     8.33401388E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.85385603E-07   2        -2         2   # BR(h0 -> Up up)
     3.59648689E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.65867121E-07   2        -1         1   # BR(h0 -> Down down)
     2.76996095E-04   2        -3         3   # BR(h0 -> Strange strange)
     7.39860507E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.79239560E+00   # Gamma(HH)
     2.50240048E-06   2        22        22   # BR(HH -> photon photon)
     8.23245893E-07   2        22        23   # BR(HH -> photon Z)
     7.39187273E-04   2        23        23   # BR(HH -> Z Z)
     7.68287335E-04   2       -24        24   # BR(HH -> W W)
     5.86119803E-04   2        21        21   # BR(HH -> gluon gluon)
     3.47820845E-09   2       -11        11   # BR(HH -> Electron electron)
     1.54823773E-04   2       -13        13   # BR(HH -> Muon muon)
     4.51350488E-02   2       -15        15   # BR(HH -> Tau tau)
     5.15737175E-11   2        -2         2   # BR(HH -> Up up)
     1.00036604E-05   2        -4         4   # BR(HH -> Charm charm)
     6.93781188E-01   2        -6         6   # BR(HH -> Top top)
     2.13887402E-07   2        -1         1   # BR(HH -> Down down)
     7.73647677E-05   2        -3         3   # BR(HH -> Strange strange)
     2.55084661E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.78194820E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.13991540E-03   2        25        25   # BR(HH -> h0 h0)
     2.08410832E-05   2  -1000001   1000001   # BR(HH -> Sdown1 sdown1)
     2.08219046E-05   2  -1000003   1000003   # BR(HH -> Sstrange1 sstrange1)
DECAY        36     1.80714324E+00   # Gamma(A0)
     3.70540348E-06   2        22        22   # BR(A0 -> photon photon)
     1.31417320E-06   2        22        23   # BR(A0 -> photon Z)
     9.74575366E-04   2        21        21   # BR(A0 -> gluon gluon)
     3.39055940E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.50922376E-04   2       -13        13   # BR(A0 -> Muon muon)
     4.39971755E-02   2       -15        15   # BR(A0 -> Tau tau)
     4.80614657E-11   2        -2         2   # BR(A0 -> Up up)
     9.32090608E-06   2        -4         4   # BR(A0 -> Charm charm)
     7.04522249E-01   2        -6         6   # BR(A0 -> Top top)
     2.08542578E-07   2        -1         1   # BR(A0 -> Down down)
     7.54315214E-05   2        -3         3   # BR(A0 -> Strange strange)
     2.48706221E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.48961239E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.00991150E-03   2        23        25   # BR(A0 -> Z h0)
     1.68254601E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.13621289E+00   # Gamma(Hp)
     3.13938519E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.34218520E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     3.79645000E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.65978710E-07   2        -1         2   # BR(Hp -> Down up)
     2.77047524E-06   2        -3         2   # BR(Hp -> Strange up)
     2.22616147E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.66574901E-07   2        -1         4   # BR(Hp -> Down charm)
     6.97185945E-05   2        -3         4   # BR(Hp -> Strange charm)
     3.11756904E-04   2        -5         4   # BR(Hp -> Bottom charm)
     4.85785344E-05   2        -1         6   # BR(Hp -> Down top)
     1.05925738E-03   2        -3         6   # BR(Hp -> Strange top)
     9.59545846E-01   2        -5         6   # BR(Hp -> Bottom top)
     8.60492064E-04   2        24        25   # BR(Hp -> W h0)
     2.02027610E-12   2        24        35   # BR(Hp -> W HH)
     3.11008308E-12   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.43975461E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.03314984E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.02754739E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00185049E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.11795437E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.30300363E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.43975461E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.03314984E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.02754739E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99973354E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.66457361E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99973354E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.66457361E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.28781500E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.95866369E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.93950296E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.66457361E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99973354E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.42785737E-04   # BR(b -> s gamma)
    2    1.58831646E-06   # BR(b -> s mu+ mu-)
    3    3.52591374E-05   # BR(b -> s nu nu)
    4    2.47281482E-15   # BR(Bd -> e+ e-)
    5    1.05635845E-10   # BR(Bd -> mu+ mu-)
    6    2.21139134E-08   # BR(Bd -> tau+ tau-)
    7    8.33430461E-14   # BR(Bs -> e+ e-)
    8    3.56041239E-09   # BR(Bs -> mu+ mu-)
    9    7.55197583E-07   # BR(Bs -> tau+ tau-)
   10    9.66906214E-05   # BR(B_u -> tau nu)
   11    9.98777101E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43645805E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94244395E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16410965E-03   # epsilon_K
   17    2.28172873E-15   # Delta(M_K)
   18    2.48088921E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29103284E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.03565919E-15   # Delta(g-2)_electron/2
   21   -4.42777002E-11   # Delta(g-2)_muon/2
   22   -6.81274869E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -1.39199261E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.11132442E-01   # C7
     0305 4322   00   2    -4.17626262E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.23547724E-01   # C8
     0305 6321   00   2    -5.35235021E-04   # C8'
 03051111 4133   00   0     1.58410400E+00   # C9 e+e-
 03051111 4133   00   2     1.58430345E+00   # C9 e+e-
 03051111 4233   00   2    -3.07390059E-06   # C9' e+e-
 03051111 4137   00   0    -4.40679610E+00   # C10 e+e-
 03051111 4137   00   2    -4.40578122E+00   # C10 e+e-
 03051111 4237   00   2     2.34501212E-05   # C10' e+e-
 03051313 4133   00   0     1.58410400E+00   # C9 mu+mu-
 03051313 4133   00   2     1.58430336E+00   # C9 mu+mu-
 03051313 4233   00   2    -3.07390333E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.40679610E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.40578131E+00   # C10 mu+mu-
 03051313 4237   00   2     2.34501239E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50512784E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -5.10856388E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50512784E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -5.10856328E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50512784E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -5.10959892E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85831529E-07   # C7
     0305 4422   00   2     2.71596480E-07   # C7
     0305 4322   00   2     9.50085624E-07   # C7'
     0305 6421   00   0     3.30490221E-07   # C8
     0305 6421   00   2     4.67353011E-07   # C8
     0305 6321   00   2     6.70695384E-07   # C8'
 03051111 4133   00   2    -2.66479938E-06   # C9 e+e-
 03051111 4233   00   2    -1.77745228E-09   # C9' e+e-
 03051111 4137   00   2     2.09107467E-05   # C10 e+e-
 03051111 4237   00   2    -4.65150729E-10   # C10' e+e-
 03051313 4133   00   2    -2.66479748E-06   # C9 mu+mu-
 03051313 4233   00   2    -1.77745196E-09   # C9' mu+mu-
 03051313 4137   00   2     2.09107486E-05   # C10 mu+mu-
 03051313 4237   00   2    -4.65150662E-10   # C10' mu+mu-
 03051212 4137   00   2    -4.56187291E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     3.02109858E-10   # C11' nu_1 nu_1
 03051414 4137   00   2    -4.56187291E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     3.02109854E-10   # C11' nu_2 nu_2
 03051616 4137   00   2    -4.56188616E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     2.80617183E-10   # C11' nu_3 nu_3
