# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  10:10
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.04251177E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.43904039E+03  # scale for input parameters
    1   -1.00268111E+03  # M_1
    2    1.57151861E+03  # M_2
    3    1.69289694E+03  # M_3
   11    1.58186118E+03  # A_t
   12    2.54491149E+03  # A_b
   13    6.87727141E+02  # A_tau
   23   -8.03984970E+02  # mu
   25    3.96428024E+01  # tan(beta)
   26    1.70765911E+03  # m_A, pole mass
   31    1.50177324E+03  # M_L11
   32    1.50177324E+03  # M_L22
   33    1.30518568E+03  # M_L33
   34    1.19606317E+03  # M_E11
   35    1.19606317E+03  # M_E22
   36    1.96689618E+03  # M_E33
   41    1.44199329E+03  # M_Q11
   42    1.44199329E+03  # M_Q22
   43    3.77164357E+03  # M_Q33
   44    2.31426623E+03  # M_U11
   45    2.31426623E+03  # M_U22
   46    1.48232156E+03  # M_U33
   47    3.51751667E+03  # M_D11
   48    3.51751667E+03  # M_D22
   49    1.29810956E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.43904039E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.43904039E+03  # (SUSY scale)
  1  1     8.38703926E-06   # Y_u(Q)^DRbar
  2  2     4.26061594E-03   # Y_c(Q)^DRbar
  3  3     1.01322171E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.43904039E+03  # (SUSY scale)
  1  1     6.68118152E-04   # Y_d(Q)^DRbar
  2  2     1.26942449E-02   # Y_s(Q)^DRbar
  3  3     6.62563848E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.43904039E+03  # (SUSY scale)
  1  1     1.16591503E-04   # Y_e(Q)^DRbar
  2  2     2.41074251E-02   # Y_mu(Q)^DRbar
  3  3     4.05447231E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.43904039E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.58186115E+03   # A_t(Q)^DRbar
Block Ad Q=  2.43904039E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     2.54491285E+03   # A_b(Q)^DRbar
Block Ae Q=  2.43904039E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     6.87727165E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.43904039E+03  # soft SUSY breaking masses at Q
   1   -1.00268111E+03  # M_1
   2    1.57151861E+03  # M_2
   3    1.69289694E+03  # M_3
  21    2.30169608E+06  # M^2_(H,d)
  22   -6.38762735E+05  # M^2_(H,u)
  31    1.50177324E+03  # M_(L,11)
  32    1.50177324E+03  # M_(L,22)
  33    1.30518568E+03  # M_(L,33)
  34    1.19606317E+03  # M_(E,11)
  35    1.19606317E+03  # M_(E,22)
  36    1.96689618E+03  # M_(E,33)
  41    1.44199329E+03  # M_(Q,11)
  42    1.44199329E+03  # M_(Q,22)
  43    3.77164357E+03  # M_(Q,33)
  44    2.31426623E+03  # M_(U,11)
  45    2.31426623E+03  # M_(U,22)
  46    1.48232156E+03  # M_(U,33)
  47    3.51751667E+03  # M_(D,11)
  48    3.51751667E+03  # M_(D,22)
  49    1.29810956E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.18546030E+02  # h0
        35     1.70760264E+03  # H0
        36     1.70765911E+03  # A0
        37     1.70861564E+03  # H+
   1000001     1.55996132E+03  # ~d_L
   2000001     3.56356493E+03  # ~d_R
   1000002     1.55793773E+03  # ~u_L
   2000002     2.37308214E+03  # ~u_R
   1000003     1.55995803E+03  # ~s_L
   2000003     3.56355915E+03  # ~s_R
   1000004     1.55793464E+03  # ~c_L
   2000004     2.37308173E+03  # ~c_R
   1000005     1.39181891E+03  # ~b_1
   2000005     3.79815628E+03  # ~b_2
   1000006     1.56519781E+03  # ~t_1
   2000006     3.80074516E+03  # ~t_2
   1000011     1.51837035E+03  # ~e_L-
   2000011     1.20822668E+03  # ~e_R-
   1000012     1.51590660E+03  # ~nu_eL
   1000013     1.51835850E+03  # ~mu_L-
   2000013     1.20818873E+03  # ~mu_R-
   1000014     1.51589065E+03  # ~nu_muL
   1000015     1.31817842E+03  # ~tau_1-
   2000015     1.96590660E+03  # ~tau_2-
   1000016     1.31653103E+03  # ~nu_tauL
   1000021     1.86820139E+03  # ~g
   1000022     8.10075715E+02  # ~chi_10
   1000023     8.11838475E+02  # ~chi_20
   1000025     9.95820707E+02  # ~chi_30
   1000035     1.58731199E+03  # ~chi_40
   1000024     8.12948819E+02  # ~chi_1+
   1000037     1.58753268E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.48923776E-02   # alpha
Block Hmix Q=  2.43904039E+03  # Higgs mixing parameters
   1   -8.03984970E+02  # mu
   2    3.96428024E+01  # tan[beta](Q)
   3    2.43506048E+02  # v(Q)
   4    2.91609964E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -1.66845719E-02   # Re[R_st(1,1)]
   1  2     9.99860803E-01   # Re[R_st(1,2)]
   2  1    -9.99860803E-01   # Re[R_st(2,1)]
   2  2    -1.66845719E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -6.83830077E-03   # Re[R_sb(1,1)]
   1  2     9.99976619E-01   # Re[R_sb(1,2)]
   2  1    -9.99976619E-01   # Re[R_sb(2,1)]
   2  2    -6.83830077E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.99667682E-01   # Re[R_sta(1,1)]
   1  2     2.57783786E-02   # Re[R_sta(1,2)]
   2  1    -2.57783786E-02   # Re[R_sta(2,1)]
   2  2    -9.99667682E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     1.78201295E-01   # Re[N(1,1)]
   1  2     2.30873545E-02   # Re[N(1,2)]
   1  3    -6.97519526E-01   # Re[N(1,3)]
   1  4     6.93669795E-01   # Re[N(1,4)]
   2  1    -1.71936070E-02   # Re[N(2,1)]
   2  2    -6.79522076E-02   # Re[N(2,2)]
   2  3    -7.06744411E-01   # Re[N(2,3)]
   2  4    -7.03988079E-01   # Re[N(2,4)]
   3  1    -9.83843179E-01   # Re[N(3,1)]
   3  2     4.22371136E-03   # Re[N(3,2)]
   3  3    -1.13952895E-01   # Re[N(3,3)]
   3  4     1.38019913E-01   # Re[N(3,4)]
   4  1    -1.12999429E-03   # Re[N(4,1)]
   4  2     9.97412468E-01   # Re[N(4,2)]
   4  3    -3.15212204E-02   # Re[N(4,3)]
   4  4    -6.46026622E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.46297785E-02   # Re[U(1,1)]
   1  2    -9.99003595E-01   # Re[U(1,2)]
   2  1     9.99003595E-01   # Re[U(2,1)]
   2  2    -4.46297785E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.14441766E-02   # Re[V(1,1)]
   1  2     9.95810204E-01   # Re[V(1,2)]
   2  1     9.95810204E-01   # Re[V(2,1)]
   2  2     9.14441766E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     6.63305359E-01   # ~e^-_R
#    BR                NDA      ID1      ID2
     8.80715847E-02    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     8.14125579E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.11114142E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     6.71616698E-01   # ~e^-_L
#    BR                NDA      ID1      ID2
     7.12488142E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.01949163E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     8.78385173E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.01710674E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     6.71470695E-01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.00626013E-02    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.92942844E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     8.99790012E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     6.21795850E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     6.81346833E-01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     7.66403205E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.63288219E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     8.65925752E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     1.98826051E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     1.22249998E-03    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     2.01100385E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.21722219E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     4.21506358E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.48712169E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     8.05925468E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     1.54410100E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.52832764E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.42494664E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.45795845E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.89879225E-04    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.84312109E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.80256395E-04    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     3.64358558E-02    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     1.79669202E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.95917061E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     6.91301846E-01   # ~nu_e
#    BR                NDA      ID1      ID2
     2.56177385E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.69545303E-02    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     8.75504826E-01    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     8.19229057E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     7.00155559E-01   # ~nu_mu
#    BR                NDA      ID1      ID2
     2.52931016E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.67396748E-02    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     8.64397047E-01    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     9.35701770E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     2.00036360E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     5.81655852E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.84252075E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.48860646E-01    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     8.41480275E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
DECAY   1000001     1.07759735E-01   # ~d_L
#    BR                NDA      ID1      ID2
     2.57379143E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.42683819E-01    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     7.20062446E-01    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.34679944E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
DECAY   2000001     1.49609822E+02   # ~d_R
#    BR                NDA      ID1      ID2
     3.79864410E-04    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.09399633E-02    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.88676338E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000003     1.10692617E-01   # ~s_L
#    BR                NDA      ID1      ID2
     1.41998419E-02    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.50884299E-01    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     7.01176088E-01    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.33739771E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
DECAY   2000003     1.49629912E+02   # ~s_R
#    BR                NDA      ID1      ID2
     4.13133913E-04    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.09393035E-02    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.88539551E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000005     1.03306371E+01   # ~b_1
#    BR                NDA      ID1      ID2
     2.51790209E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.56035945E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.06747621E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.71499084E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     3.24613913E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.52546534E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     4.66459649E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.63897978E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.49995579E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.14675016E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     7.18789283E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     5.30742565E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.82494755E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     4.88846472E-03    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     1.73762342E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.29940549E-02    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     2.29949585E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     1.29952202E-02    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   1000002     1.60177213E-01   # ~u_L
#    BR                NDA      ID1      ID2
     6.84929842E-02    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.14563731E-01    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.37742741E-01    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     3.79200544E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
DECAY   2000002     3.06975578E+01   # ~u_R
#    BR                NDA      ID1      ID2
     4.27897189E-03    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.13433502E-01    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     8.82247671E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000004     1.63103800E-01   # ~c_L
#    BR                NDA      ID1      ID2
     6.81272200E-02    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.13388227E-01    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.29918362E-01    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     3.88566191E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
DECAY   2000004     3.06987153E+01   # ~c_R
#    BR                NDA      ID1      ID2
     4.28948479E-03    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.13429187E-01    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     8.82208994E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000006     3.42658867E+01   # ~t_1
#    BR                NDA      ID1      ID2
     2.39113851E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.37328485E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.13694718E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.92187164E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     3.26718539E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.03445799E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.06856794E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     5.55382797E-03    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.54391296E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     9.35851196E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     6.89960465E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.25353969E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     3.46334459E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.58744299E-02    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     9.08412537E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.43774155E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     1.74712822E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     2.43839102E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     5.58794328E-10   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     4.76249910E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     1.83198057E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.58740837E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.57678152E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     2.17649424E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
     1.43327334E-03    3     1000023        -1         2   # BR(chi^+_1 -> chi^0_2 d_bar u)
     4.77818505E-04    3     1000023       -11        12   # BR(chi^+_1 -> chi^0_2 e^+ nu_e)
     4.57010873E-04    3     1000023       -13        14   # BR(chi^+_1 -> chi^0_2 mu^+ nu_mu)
DECAY   1000037     1.32796870E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     3.81501646E-03    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     3.81629475E-03    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     5.05399906E-02    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     4.05909592E-03    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     4.06088857E-03    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     5.09381081E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     2.13564040E-03    2     1000002        -1   # BR(chi^+_2 -> ~u_L d_bar)
     2.13608478E-03    2     1000004        -3   # BR(chi^+_2 -> ~c_L s_bar)
     1.86787948E-03    2    -1000001         2   # BR(chi^+_2 -> ~d^*_L u)
     1.86627227E-03    2    -1000003         4   # BR(chi^+_2 -> ~s^*_L c)
     2.10494737E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.09619117E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.89939696E-03    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.08451362E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.27106795E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.59607287E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     5.47221612E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     4.87770875E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.33068974E-09   # chi^0_2
#    BR                NDA      ID1      ID2
     9.70615798E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     5.06621768E-03    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     6.49526260E-03    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     6.28033776E-03    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.46573582E-03    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.39018933E-03    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     8.68645871E-03    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     2.01087466E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.44967456E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.44967456E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     2.19770090E-03    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.56774945E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.24524352E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.26516925E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.59820687E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.56839083E-03    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     1.56839083E-03    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     1.56892840E-03    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     1.56892840E-03    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     2.08995652E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     2.08995652E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     1.68389549E-03    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     1.68389549E-03    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     1.68463053E-03    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     1.68463053E-03    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     2.12036645E-02    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     2.12036645E-02    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     8.76784783E-04    2     1000002        -2   # BR(chi^0_4 -> ~u_L u_bar)
     8.76784783E-04    2    -1000002         2   # BR(chi^0_4 -> ~u^*_L u)
     8.76118336E-04    2     1000004        -4   # BR(chi^0_4 -> ~c_L c_bar)
     8.76118336E-04    2    -1000004         4   # BR(chi^0_4 -> ~c^*_L c)
     7.61735497E-04    2     1000001        -1   # BR(chi^0_4 -> ~d_L d_bar)
     7.61735497E-04    2    -1000001         1   # BR(chi^0_4 -> ~d^*_L d)
     7.61917762E-04    2     1000003        -3   # BR(chi^0_4 -> ~s_L s_bar)
     7.61917762E-04    2    -1000003         3   # BR(chi^0_4 -> ~s^*_L s)
     1.74681831E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.74681831E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.56451613E-01    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.87899432E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     3.06858154E-03    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.85105539E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.68402064E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     3.90538148E-03    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.62925403E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.51219304E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.90559617E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.90559617E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.55853446E+01   # ~g
#    BR                NDA      ID1      ID2
     7.20760997E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     7.20760997E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     7.20769060E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     7.20769060E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     5.93774877E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     5.93774877E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     7.12230405E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     7.12230405E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     7.12244214E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     7.12244214E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     1.53857679E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.53857679E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.23126312E-03   # Gamma(h0)
     2.26320906E-03   2        22        22   # BR(h0 -> photon photon)
     9.55369422E-04   2        22        23   # BR(h0 -> photon Z)
     1.40833565E-02   2        23        23   # BR(h0 -> Z Z)
     1.32300541E-01   2       -24        24   # BR(h0 -> W W)
     7.49365100E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.90415585E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.62623228E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.57038491E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.67813532E-07   2        -2         2   # BR(h0 -> Up up)
     3.25603010E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.91087423E-07   2        -1         1   # BR(h0 -> Down down)
     2.49950991E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.66683425E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.61475506E+01   # Gamma(HH)
     2.29892272E-09   2        22        22   # BR(HH -> photon photon)
     8.79093001E-10   2        22        23   # BR(HH -> photon Z)
     6.71204752E-07   2        23        23   # BR(HH -> Z Z)
     4.46432870E-07   2       -24        24   # BR(HH -> W W)
     2.52635374E-05   2        21        21   # BR(HH -> gluon gluon)
     8.28756391E-09   2       -11        11   # BR(HH -> Electron electron)
     3.68937742E-04   2       -13        13   # BR(HH -> Muon muon)
     1.04118371E-01   2       -15        15   # BR(HH -> Tau tau)
     9.33407228E-14   2        -2         2   # BR(HH -> Up up)
     1.81031757E-08   2        -4         4   # BR(HH -> Charm charm)
     1.32555613E-03   2        -6         6   # BR(HH -> Top top)
     8.08445438E-07   2        -1         1   # BR(HH -> Down down)
     2.92467869E-04   2        -3         3   # BR(HH -> Strange strange)
     8.93328778E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.17051976E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.62964942E-05   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.74188795E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.94287269E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.70425404E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.40679905E+01   # Gamma(A0)
     1.07886586E-07   2        22        22   # BR(A0 -> photon photon)
     1.17665493E-07   2        22        23   # BR(A0 -> photon Z)
     4.10087020E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.28031496E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.68614259E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.04029067E-01   2       -15        15   # BR(A0 -> Tau tau)
     8.90998971E-14   2        -2         2   # BR(A0 -> Up up)
     1.72764338E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.32338951E-03   2        -6         6   # BR(A0 -> Top top)
     8.07736102E-07   2        -1         1   # BR(A0 -> Down down)
     2.92211390E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.92521577E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     9.82169500E-04   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.06288520E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     4.75791336E-05   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.86104783E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     9.31330507E-07   2        23        25   # BR(A0 -> Z h0)
     3.85897291E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.59380173E+01   # Gamma(Hp)
     1.00516036E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.29737444E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.21554009E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.43320918E-07   2        -1         2   # BR(Hp -> Down up)
     1.24649463E-05   2        -3         2   # BR(Hp -> Strange up)
     9.33278058E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.50505233E-08   2        -1         4   # BR(Hp -> Down charm)
     2.67942543E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.30692223E-03   2        -5         4   # BR(Hp -> Bottom charm)
     6.15380618E-08   2        -1         6   # BR(Hp -> Down top)
     1.73029189E-06   2        -3         6   # BR(Hp -> Strange top)
     8.75693666E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.29224137E-05   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.89526013E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     8.96679392E-07   2        24        25   # BR(Hp -> W h0)
     3.16016045E-14   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.74199749E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.57157758E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.57155178E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00001642E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.19896690E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.36313745E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.74199749E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.57157758E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.57155178E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999893E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.07278588E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999893E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.07278588E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26630434E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.19357409E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.36584626E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.07278588E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999893E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.28982474E-04   # BR(b -> s gamma)
    2    1.58739753E-06   # BR(b -> s mu+ mu-)
    3    3.52111227E-05   # BR(b -> s nu nu)
    4    2.79839874E-15   # BR(Bd -> e+ e-)
    5    1.19543373E-10   # BR(Bd -> mu+ mu-)
    6    2.49662269E-08   # BR(Bd -> tau+ tau-)
    7    9.47356665E-14   # BR(Bs -> e+ e-)
    8    4.04706930E-09   # BR(Bs -> mu+ mu-)
    9    8.56394360E-07   # BR(Bs -> tau+ tau-)
   10    9.39312624E-05   # BR(B_u -> tau nu)
   11    9.70273979E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43000919E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93882366E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16132771E-03   # epsilon_K
   17    2.28170313E-15   # Delta(M_K)
   18    2.47710113E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28269543E-11   # BR(K^+ -> pi^+ nu nu)
   20   -4.30145949E-15   # Delta(g-2)_electron/2
   21   -1.83897112E-10   # Delta(g-2)_muon/2
   22   -5.38759861E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.69934031E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.96652774E-01   # C7
     0305 4322   00   2     1.17075264E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.09965682E-01   # C8
     0305 6321   00   2    -1.06066510E-04   # C8'
 03051111 4133   00   0     1.60960784E+00   # C9 e+e-
 03051111 4133   00   2     1.61027672E+00   # C9 e+e-
 03051111 4233   00   2    -4.06464558E-05   # C9' e+e-
 03051111 4137   00   0    -4.43229994E+00   # C10 e+e-
 03051111 4137   00   2    -4.42653800E+00   # C10 e+e-
 03051111 4237   00   2     3.12231455E-04   # C10' e+e-
 03051313 4133   00   0     1.60960784E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61027274E+00   # C9 mu+mu-
 03051313 4233   00   2    -4.06489785E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43229994E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.42654198E+00   # C10 mu+mu-
 03051313 4237   00   2     3.12233240E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50410017E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -6.77766523E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50410017E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -6.77761570E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50410016E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -6.76365078E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85838980E-07   # C7
     0305 4422   00   2    -8.08987184E-06   # C7
     0305 4322   00   2     7.30766425E-06   # C7'
     0305 6421   00   0     3.30496603E-07   # C8
     0305 6421   00   2     9.15903257E-06   # C8
     0305 6321   00   2     4.80432451E-06   # C8'
 03051111 4133   00   2    -1.51594784E-06   # C9 e+e-
 03051111 4233   00   2     1.01845943E-06   # C9' e+e-
 03051111 4137   00   2     1.20186920E-05   # C10 e+e-
 03051111 4237   00   2    -7.66528757E-06   # C10' e+e-
 03051313 4133   00   2    -1.51589857E-06   # C9 mu+mu-
 03051313 4233   00   2     1.01845589E-06   # C9' mu+mu-
 03051313 4137   00   2     1.20187425E-05   # C10 mu+mu-
 03051313 4237   00   2    -7.66529719E-06   # C10' mu+mu-
 03051212 4137   00   2    -2.59447347E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     1.66390706E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.59447351E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     1.66390706E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.59475522E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     1.66390806E-06   # C11' nu_3 nu_3
