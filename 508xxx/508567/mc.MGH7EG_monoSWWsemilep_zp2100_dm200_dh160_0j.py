# Herwig7 setup
include("Herwig7_i/Herwig72_LHEF.py")

evgenConfig.contact = ["Danika MacDonell <danikam1@uvic.ca>", "Philipp Mogg <philipp.mogg@cern.ch>"]
evgenConfig.generators = ["aMcAtNlo", "Herwig7"]
evgenConfig.description = "Dark Higgs (WW) Dark Matter from 2MDM UFO"
evgenConfig.process = "generate p p > zp > n1 n1 hs, (hs > w+ w- > l vl j j)"
evgenConfig.tune = "H7.2-Default"
evgenConfig.keywords = ["exotic","BSM"]
evgenConfig.nEventsPerJob = 10000

Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

Herwig7Config.add_commands ("""
cd /Herwig/Particles
create /ThePEG/ParticleData hs
setup hs 54   hs MHs WIDTH MAX_WIDTH 0 0 0 1 0
create /ThePEG/ParticleData Zp
setup Zp 55   Zp MZP WIDTH MAX_WIDTH 0 0 0 3 0
create /ThePEG/ParticleData DM
setup DM 1000022   DM MDM WIDTH MAX_WIDTH 0 0 0 2 1
set /Herwig/Particles/DM:Stable Stable
""")

Herwig7Config.run()
