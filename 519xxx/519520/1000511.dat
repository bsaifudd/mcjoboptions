# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:05
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.94172376E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.69505731E+03  # scale for input parameters
    1   -2.25963070E+02  # M_1
    2    4.74158011E+02  # M_2
    3    3.66645006E+03  # M_3
   11   -7.20960740E+03  # A_t
   12    8.72656156E+01  # A_b
   13    8.44918698E+02  # A_tau
   23   -2.73765071E+02  # mu
   25    2.83607466E+01  # tan(beta)
   26    4.57912223E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.68295247E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.84769883E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.59716622E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.69505731E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.69505731E+03  # (SUSY scale)
  1  1     8.38958252E-06   # Y_u(Q)^DRbar
  2  2     4.26190792E-03   # Y_c(Q)^DRbar
  3  3     1.01352896E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.69505731E+03  # (SUSY scale)
  1  1     4.78121482E-04   # Y_d(Q)^DRbar
  2  2     9.08430817E-03   # Y_s(Q)^DRbar
  3  3     4.74146689E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.69505731E+03  # (SUSY scale)
  1  1     8.34356950E-05   # Y_e(Q)^DRbar
  2  2     1.72518555E-02   # Y_mu(Q)^DRbar
  3  3     2.90147829E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.69505731E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -7.20960729E+03   # A_t(Q)^DRbar
Block Ad Q=  3.69505731E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     8.72656185E+01   # A_b(Q)^DRbar
Block Ae Q=  3.69505731E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     8.44918715E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.69505731E+03  # soft SUSY breaking masses at Q
   1   -2.25963070E+02  # M_1
   2    4.74158011E+02  # M_2
   3    3.66645006E+03  # M_3
  21    8.18933367E+04  # M^2_(H,d)
  22    1.31262931E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.68295247E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.84769883E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.59716622E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28876244E+02  # h0
        35     4.58020695E+02  # H0
        36     4.57912223E+02  # A0
        37     4.69255281E+02  # H+
   1000001     1.01023735E+04  # ~d_L
   2000001     1.00774841E+04  # ~d_R
   1000002     1.01020125E+04  # ~u_L
   2000002     1.00807998E+04  # ~u_R
   1000003     1.01023758E+04  # ~s_L
   2000003     1.00774877E+04  # ~s_R
   1000004     1.01020148E+04  # ~c_L
   2000004     1.00808006E+04  # ~c_R
   1000005     2.74688357E+03  # ~b_1
   2000005     4.71291649E+03  # ~b_2
   1000006     2.89118850E+03  # ~t_1
   2000006     4.72243458E+03  # ~t_2
   1000011     1.00215002E+04  # ~e_L-
   2000011     1.00087458E+04  # ~e_R-
   1000012     1.00207355E+04  # ~nu_eL
   1000013     1.00215100E+04  # ~mu_L-
   2000013     1.00087646E+04  # ~mu_R-
   1000014     1.00207452E+04  # ~nu_muL
   1000015     1.00140718E+04  # ~tau_1-
   2000015     1.00243179E+04  # ~tau_2-
   1000016     1.00234930E+04  # ~nu_tauL
   1000021     4.13881114E+03  # ~g
   1000022     2.14407108E+02  # ~chi_10
   1000023     2.74502690E+02  # ~chi_20
   1000025     3.03435858E+02  # ~chi_30
   1000035     5.33038509E+02  # ~chi_40
   1000024     2.77089573E+02  # ~chi_1+
   1000037     5.33403258E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.78304700E-02   # alpha
Block Hmix Q=  3.69505731E+03  # Higgs mixing parameters
   1   -2.73765071E+02  # mu
   2    2.83607466E+01  # tan[beta](Q)
   3    2.43005990E+02  # v(Q)
   4    2.09683604E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     7.21149634E-02   # Re[R_st(1,1)]
   1  2     9.97396326E-01   # Re[R_st(1,2)]
   2  1    -9.97396326E-01   # Re[R_st(2,1)]
   2  2     7.21149634E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.12186128E-03   # Re[R_sb(1,1)]
   1  2     9.99999371E-01   # Re[R_sb(1,2)]
   2  1    -9.99999371E-01   # Re[R_sb(2,1)]
   2  2    -1.12186128E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -7.49128614E-02   # Re[R_sta(1,1)]
   1  2     9.97190084E-01   # Re[R_sta(1,2)]
   2  1    -9.97190084E-01   # Re[R_sta(2,1)]
   2  2    -7.49128614E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.13627719E-01   # Re[N(1,1)]
   1  2    -2.85007085E-02   # Re[N(1,2)]
   1  3     3.21647515E-01   # Re[N(1,3)]
   1  4    -2.47012097E-01   # Re[N(1,4)]
   2  1    -5.80770826E-02   # Re[N(2,1)]
   2  2    -2.15002728E-01   # Re[N(2,2)]
   2  3    -7.02839665E-01   # Re[N(2,3)]
   2  4    -6.75586623E-01   # Re[N(2,4)]
   3  1    -4.02210653E-01   # Re[N(3,1)]
   3  2     6.73377572E-02   # Re[N(3,2)]
   3  3    -6.26148115E-01   # Re[N(3,3)]
   3  4     6.64553050E-01   # Re[N(3,4)]
   4  1     1.17487742E-02   # Re[N(4,1)]
   4  2    -9.73872252E-01   # Re[N(4,2)]
   4  3     1.02458873E-01   # Re[N(4,3)]
   4  4     2.02328897E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.46291580E-01   # Re[U(1,1)]
   1  2    -9.89241514E-01   # Re[U(1,2)]
   2  1     9.89241514E-01   # Re[U(2,1)]
   2  2    -1.46291580E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -2.88568403E-01   # Re[V(1,1)]
   1  2     9.57459282E-01   # Re[V(1,2)]
   2  1     9.57459282E-01   # Re[V(2,1)]
   2  2     2.88568403E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02365328E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     8.34842106E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.37149279E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.61648975E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.37398949E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44177016E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.15673846E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.84909661E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     6.64680336E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.84817717E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.30750764E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.95402053E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03549581E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     8.33001936E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.94405042E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.61729659E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.49360605E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.14994930E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.44236400E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.15764027E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.86862868E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     6.80506780E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.84705028E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.30697061E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.95157509E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     8.41907740E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.24119157E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.06668121E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.65477334E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.03542987E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.14271919E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.60541192E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.13991940E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     6.48357397E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.16151880E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.58043264E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     7.07719020E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.37029424E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44181683E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     6.45525733E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.03387141E-02    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.43181995E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.92223672E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.08693174E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.57697523E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44241060E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     6.45260626E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.03344681E-02    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.43082124E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.92103663E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.12503546E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.57477239E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.60982202E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     5.78316384E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     9.26229484E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.17863047E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.61799292E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.47468841E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.01851630E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.55358355E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.44863416E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.63594486E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.89879787E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.86276542E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.16571764E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.70994357E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.24892801E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.13725053E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     2.76912396E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.26106432E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.04627349E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.55391657E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.45120153E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.64747292E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.89821071E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.86296957E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.16817443E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.72175154E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.25832256E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.13709448E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     2.77390928E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.26103153E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.04603745E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.54596311E+01   # ~b_1
#    BR                NDA      ID1      ID2
     9.91661623E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.33965259E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.93893410E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.59948688E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.59097525E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     9.27815738E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     2.87422067E+02   # ~b_2
#    BR                NDA      ID1      ID2
     8.82666055E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.89658002E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.99240583E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.77605313E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.07005740E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.65459991E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     6.77490624E-02    2     1000021         5   # BR(~b_2 -> ~g b)
     3.11658463E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     2.50901946E-03    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
DECAY   2000002     5.72554093E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.27903438E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.32423719E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     6.34921825E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.60722617E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.86260643E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.36634190E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.28530097E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.08465941E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.07745321E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.18132004E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.04594039E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.72561414E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.27903151E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.35321857E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     6.35193974E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.60710417E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.86281027E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.36659599E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.28762683E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.08450184E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.07977644E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.18129035E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.04570427E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.21393929E+02   # ~t_1
#    BR                NDA      ID1      ID2
     6.95340569E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.20485370E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.21483209E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.29292804E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.48848389E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.67186184E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     3.10659046E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.36879710E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.41148090E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.31974156E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.79320785E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     7.01682256E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.28377400E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.87857824E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     1.45226214E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.17168145E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     2.20206565E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.23019916E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     1.04173855E-03   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.33578224E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.33123509E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11189548E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11188822E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.10919785E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     3.50051536E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     7.84435353E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.54848205E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.29058702E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.40122894E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.96971237E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.81646450E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.90577329E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.96570814E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     4.66277475E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.18968870E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.18390388E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.52540600E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.52537083E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.46015496E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.44221729E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.44208726E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.40569762E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.03984766E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     3.34231017E-04   # chi^0_3
#    BR                NDA      ID1      ID2
     7.39039512E-04    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
     1.11991346E-02    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     6.92372164E-02    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     6.91632137E-02    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     8.87675960E-02    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     8.87673787E-02    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     8.87065909E-02    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     2.00258996E-02    3     1000022        11       -11   # BR(chi^0_3 -> chi^0_1 e^- e^+)
     2.00259823E-02    3     1000022        13       -13   # BR(chi^0_3 -> chi^0_1 mu^- mu^+)
     2.00474308E-02    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
     1.18722150E-01    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
     1.88566507E-02    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     1.83941145E-02    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     2.41779253E-02    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     2.41750334E-02    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     1.91865524E-02    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     5.45634215E-03    3     1000023        11       -11   # BR(chi^0_3 -> chi^0_2 e^- e^+)
     5.45530836E-03    3     1000023        13       -13   # BR(chi^0_3 -> chi^0_2 mu^- mu^+)
     5.17366729E-03    3     1000023        15       -15   # BR(chi^0_3 -> chi^0_2 tau^- tau^+)
     3.23331966E-02    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     4.21489598E-02    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     4.21489598E-02    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     4.16912287E-02    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     4.16912287E-02    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     1.40497725E-02    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     1.40497725E-02    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     1.40490440E-02    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     1.40490440E-02    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     1.37557836E-02    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     1.37557836E-02    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     4.30644026E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.14288458E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.14288458E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.28398018E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.27618002E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.72846766E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.67074329E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.37544942E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.38314268E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.48295088E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.48295088E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     6.98924125E+01   # ~g
#    BR                NDA      ID1      ID2
     2.22899256E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.22899256E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.74369561E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.74369561E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     6.09319902E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.64537938E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     5.88076750E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.25422476E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     3.95017290E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     3.10331408E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     7.77750977E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     7.77750977E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     7.07378146E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     7.07378146E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.78927739E-03   # Gamma(h0)
     2.15976008E-03   2        22        22   # BR(h0 -> photon photon)
     1.68634899E-03   2        22        23   # BR(h0 -> photon Z)
     3.57782044E-02   2        23        23   # BR(h0 -> Z Z)
     2.74985140E-01   2       -24        24   # BR(h0 -> W W)
     6.35248716E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.83833870E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.15219943E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.20593119E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.09618293E-07   2        -2         2   # BR(h0 -> Up up)
     2.12757304E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.57601640E-07   2        -1         1   # BR(h0 -> Down down)
     2.01671824E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.38113069E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     6.23302990E+00   # Gamma(HH)
     3.40524434E-08   2        22        22   # BR(HH -> photon photon)
     1.43395606E-08   2        22        23   # BR(HH -> photon Z)
     3.03319969E-05   2        23        23   # BR(HH -> Z Z)
     6.52174441E-05   2       -24        24   # BR(HH -> W W)
     2.01461020E-04   2        21        21   # BR(HH -> gluon gluon)
     9.61163582E-09   2       -11        11   # BR(HH -> Electron electron)
     4.27709697E-04   2       -13        13   # BR(HH -> Muon muon)
     1.23482178E-01   2       -15        15   # BR(HH -> Tau tau)
     3.13661183E-13   2        -2         2   # BR(HH -> Up up)
     6.08094860E-08   2        -4         4   # BR(HH -> Charm charm)
     1.58730099E-03   2        -6         6   # BR(HH -> Top top)
     9.13157859E-07   2        -1         1   # BR(HH -> Down down)
     3.30267509E-04   2        -3         3   # BR(HH -> Strange strange)
     8.72744330E-01   2        -5         5   # BR(HH -> Bottom bottom)
     6.83334990E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.46837244E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     6.22001604E+00   # Gamma(A0)
     5.07833839E-07   2        22        22   # BR(A0 -> photon photon)
     5.10448932E-07   2        22        23   # BR(A0 -> photon Z)
     2.24787747E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.55332745E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.25115114E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.22740650E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.56632030E-13   2        -2         2   # BR(A0 -> Up up)
     4.96448561E-08   2        -4         4   # BR(A0 -> Charm charm)
     3.10688781E-03   2        -6         6   # BR(A0 -> Top top)
     9.07648682E-07   2        -1         1   # BR(A0 -> Down down)
     3.28274972E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.67593711E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.52846576E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.01221176E-05   2        23        25   # BR(A0 -> Z h0)
     3.12727638E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     5.02081744E+00   # Gamma(Hp)
     1.33751660E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.71830063E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.61741549E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.10131140E-06   2        -1         2   # BR(Hp -> Down up)
     1.81266906E-05   2        -3         2   # BR(Hp -> Strange up)
     1.14467560E-05   2        -5         2   # BR(Hp -> Bottom up)
     5.33256411E-08   2        -1         4   # BR(Hp -> Down charm)
     3.96938812E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.60293253E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.60743712E-07   2        -1         6   # BR(Hp -> Down top)
     6.11493226E-06   2        -3         6   # BR(Hp -> Strange top)
     8.35578904E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.06308723E-05   2        24        25   # BR(Hp -> W h0)
     4.73533673E-08   2        24        35   # BR(Hp -> W HH)
     4.96781261E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.15199664E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    8.04179951E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    8.04331948E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99811027E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.43224031E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.24326779E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.15199664E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    8.04179951E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    8.04331948E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99993317E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    6.68256690E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99993317E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    6.68256690E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25067791E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.24124968E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.36359938E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    6.68256690E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99993317E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    4.31766429E-04   # BR(b -> s gamma)
    2    1.59206207E-06   # BR(b -> s mu+ mu-)
    3    3.52655544E-05   # BR(b -> s nu nu)
    4    1.61789670E-15   # BR(Bd -> e+ e-)
    5    6.91145329E-11   # BR(Bd -> mu+ mu-)
    6    1.44582059E-08   # BR(Bd -> tau+ tau-)
    7    5.49345780E-14   # BR(Bs -> e+ e-)
    8    2.34679839E-09   # BR(Bs -> mu+ mu-)
    9    4.97482619E-07   # BR(Bs -> tau+ tau-)
   10    7.38453551E-05   # BR(B_u -> tau nu)
   11    7.62794247E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42040857E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.92749176E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15856147E-03   # epsilon_K
   17    2.28169839E-15   # Delta(M_K)
   18    2.48080880E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29238712E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.95381379E-16   # Delta(g-2)_electron/2
   21   -8.35320379E-12   # Delta(g-2)_muon/2
   22   -2.36588112E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.09396159E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.97895255E-01   # C7
     0305 4322   00   2    -1.98485350E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -2.02918826E-01   # C8
     0305 6321   00   2    -2.01699191E-03   # C8'
 03051111 4133   00   0     1.62385550E+00   # C9 e+e-
 03051111 4133   00   2     1.62415345E+00   # C9 e+e-
 03051111 4233   00   2    -8.36419712E-05   # C9' e+e-
 03051111 4137   00   0    -4.44654760E+00   # C10 e+e-
 03051111 4137   00   2    -4.44612195E+00   # C10 e+e-
 03051111 4237   00   2     6.23136816E-04   # C10' e+e-
 03051313 4133   00   0     1.62385550E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62415333E+00   # C9 mu+mu-
 03051313 4233   00   2    -8.36903194E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44654760E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44612207E+00   # C10 mu+mu-
 03051313 4237   00   2     6.23185112E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50525964E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -1.34832479E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50525964E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -1.34822012E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50525964E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -1.31872562E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85808044E-07   # C7
     0305 4422   00   2    -6.53018278E-06   # C7
     0305 4322   00   2    -2.00636850E-06   # C7'
     0305 6421   00   0     3.30470105E-07   # C8
     0305 6421   00   2     9.05137137E-06   # C8
     0305 6321   00   2    -7.91356833E-07   # C8'
 03051111 4133   00   2     2.34651562E-08   # C9 e+e-
 03051111 4233   00   2     3.83429079E-06   # C9' e+e-
 03051111 4137   00   2     4.35473141E-06   # C10 e+e-
 03051111 4237   00   2    -2.84856734E-05   # C10' e+e-
 03051313 4133   00   2     2.34631694E-08   # C9 mu+mu-
 03051313 4233   00   2     3.83429035E-06   # C9' mu+mu-
 03051313 4137   00   2     4.35473321E-06   # C10 mu+mu-
 03051313 4237   00   2    -2.84856748E-05   # C10' mu+mu-
 03051212 4137   00   2    -9.13303897E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     6.16358269E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -9.13303801E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     6.16358269E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -9.13276724E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     6.16358267E-06   # C11' nu_3 nu_3
