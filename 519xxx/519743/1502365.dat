# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:01
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.37246678E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.53228770E+03  # scale for input parameters
    1   -4.43030857E+01  # M_1
    2    7.67575007E+02  # M_2
    3    4.33416236E+03  # M_3
   11    1.34785107E+03  # A_t
   12   -1.44929886E+03  # A_b
   13   -1.68465447E+03  # A_tau
   23    4.34024269E+02  # mu
   25    2.27891619E+01  # tan(beta)
   26    1.48417451E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.24587791E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.75966258E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.15166603E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.53228770E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.53228770E+03  # (SUSY scale)
  1  1     8.39244030E-06   # Y_u(Q)^DRbar
  2  2     4.26335967E-03   # Y_c(Q)^DRbar
  3  3     1.01387420E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.53228770E+03  # (SUSY scale)
  1  1     3.84323430E-04   # Y_d(Q)^DRbar
  2  2     7.30214517E-03   # Y_s(Q)^DRbar
  3  3     3.81128413E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.53228770E+03  # (SUSY scale)
  1  1     6.70672490E-05   # Y_e(Q)^DRbar
  2  2     1.38673800E-02   # Y_mu(Q)^DRbar
  3  3     2.33226518E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.53228770E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.34785104E+03   # A_t(Q)^DRbar
Block Ad Q=  2.53228770E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.44929887E+03   # A_b(Q)^DRbar
Block Ae Q=  2.53228770E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.68465445E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.53228770E+03  # soft SUSY breaking masses at Q
   1   -4.43030857E+01  # M_1
   2    7.67575007E+02  # M_2
   3    4.33416236E+03  # M_3
  21    1.89422690E+06  # M^2_(H,d)
  22   -5.69412690E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.24587791E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.75966258E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.15166603E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.20475307E+02  # h0
        35     1.48397795E+03  # H0
        36     1.48417451E+03  # A0
        37     1.48781580E+03  # H+
   1000001     1.00683266E+04  # ~d_L
   2000001     1.00454969E+04  # ~d_R
   1000002     1.00681023E+04  # ~u_L
   2000002     1.00486283E+04  # ~u_R
   1000003     1.00683311E+04  # ~s_L
   2000003     1.00455032E+04  # ~s_R
   1000004     1.00681068E+04  # ~c_L
   2000004     1.00486305E+04  # ~c_R
   1000005     2.29308334E+03  # ~b_1
   2000005     3.22291386E+03  # ~b_2
   1000006     2.29337569E+03  # ~t_1
   2000006     2.79608833E+03  # ~t_2
   1000011     1.00210232E+04  # ~e_L-
   2000011     1.00087271E+04  # ~e_R-
   1000012     1.00202559E+04  # ~nu_eL
   1000013     1.00210393E+04  # ~mu_L-
   2000013     1.00087579E+04  # ~mu_R-
   1000014     1.00202718E+04  # ~nu_muL
   1000015     1.00174915E+04  # ~tau_1-
   2000015     1.00257161E+04  # ~tau_2-
   1000016     1.00248161E+04  # ~nu_tauL
   1000021     4.70447664E+03  # ~g
   1000022     4.48289165E+01  # ~chi_10
   1000023     4.30380245E+02  # ~chi_20
   1000025     4.41274800E+02  # ~chi_30
   1000035     8.38656089E+02  # ~chi_40
   1000024     4.30359379E+02  # ~chi_1+
   1000037     8.38566720E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.25911162E-02   # alpha
Block Hmix Q=  2.53228770E+03  # Higgs mixing parameters
   1    4.34024269E+02  # mu
   2    2.27891619E+01  # tan[beta](Q)
   3    2.43443668E+02  # v(Q)
   4    2.20277398E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.97263128E-01   # Re[R_st(1,1)]
   1  2     7.39341210E-02   # Re[R_st(1,2)]
   2  1    -7.39341210E-02   # Re[R_st(2,1)]
   2  2    -9.97263128E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99987420E-01   # Re[R_sb(1,1)]
   1  2     5.01601790E-03   # Re[R_sb(1,2)]
   2  1    -5.01601790E-03   # Re[R_sb(2,1)]
   2  2     9.99987420E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.28123645E-01   # Re[R_sta(1,1)]
   1  2     9.91758202E-01   # Re[R_sta(1,2)]
   2  1    -9.91758202E-01   # Re[R_sta(2,1)]
   2  2     1.28123645E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.94813627E-01   # Re[N(1,1)]
   1  2     2.38055900E-04   # Re[N(1,2)]
   1  3     1.01538826E-01   # Re[N(1,3)]
   1  4     5.97143033E-03   # Re[N(1,4)]
   2  1    -6.75060175E-02   # Re[N(2,1)]
   2  2    -1.48752789E-01   # Re[N(2,2)]
   2  3     7.02468387E-01   # Re[N(2,3)]
   2  4    -6.92714740E-01   # Re[N(2,4)]
   3  1     7.57538157E-02   # Re[N(3,1)]
   3  2    -4.33342631E-02   # Re[N(3,2)]
   3  3    -7.00428102E-01   # Re[N(3,3)]
   3  4    -7.08367119E-01   # Re[N(3,4)]
   4  1     7.08130456E-03   # Re[N(4,1)]
   4  2    -9.87924437E-01   # Re[N(4,2)]
   4  3    -7.50233733E-02   # Re[N(4,3)]
   4  4     1.35375979E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.06095703E-01   # Re[U(1,1)]
   1  2     9.94355923E-01   # Re[U(1,2)]
   2  1     9.94355923E-01   # Re[U(2,1)]
   2  2     1.06095703E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.91724094E-01   # Re[V(1,1)]
   1  2     9.81448864E-01   # Re[V(1,2)]
   2  1     9.81448864E-01   # Re[V(2,1)]
   2  2     1.91724094E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02861997E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.89693124E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     4.54059530E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     5.71681546E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.43088041E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.71381405E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.04902798E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.94389270E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.91407414E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.01065712E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03626437E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.88201778E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     4.90750869E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     6.07970876E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     7.48974434E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.43126468E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.71176467E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.06191677E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.33423845E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.94312206E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.91222899E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.00905327E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.32870954E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     6.76924673E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     8.21047171E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     7.17156629E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     4.72445189E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.54811145E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     9.71934975E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.52605292E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.45303361E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.17823707E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.69076113E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.74697846E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.11276332E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.59969073E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43093000E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.69724761E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.89967159E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.15626390E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.98899234E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.25758130E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.85496542E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43131421E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.69492677E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.89863102E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.15568853E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.98819486E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.28336201E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.85343307E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.53966880E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.08668510E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.62591965E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.00489729E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.77919523E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     9.03989760E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.45183833E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.88863777E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.13530285E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.88528616E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.18122411E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.24379473E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.33549979E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.30634233E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.91821177E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.60813742E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.39814897E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.85584919E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.88885596E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.13527530E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.88485812E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.18137168E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.24391980E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.34393967E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.39049858E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.91805941E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.61375005E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.39811840E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.85566907E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     7.22298326E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.37644501E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     4.46384239E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     4.20997525E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.02938276E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.75522285E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.29424818E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.98523916E+01   # ~b_2
#    BR                NDA      ID1      ID2
     9.41813446E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.23285091E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.22451531E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.69011042E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.44198667E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.34815656E-03    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.41392953E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.71196643E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     2.71694170E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.06014279E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.38866216E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.01352116E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.53511931E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.55656321E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.18121766E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.25583974E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.84637033E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.88282950E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.25140550E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.36205793E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.85548685E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.06021715E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.38859849E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.04782909E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     2.57096811E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.55642661E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.18136492E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.25578713E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.84914126E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.88267947E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.26825563E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.36202803E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.85530664E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     7.24032606E+01   # ~t_1
#    BR                NDA      ID1      ID2
     4.84028105E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.85333006E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.96396719E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.16805486E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     8.54509995E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.10222633E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     9.31532325E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.20298092E+02   # ~t_2
#    BR                NDA      ID1      ID2
     5.06089731E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.17937267E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.26059710E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.98934983E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.42650457E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     8.11980274E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.28116946E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.11971289E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.66256166E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.68564912E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.97636463E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.36332790E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     7.09453135E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     3.47809614E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.54506731E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.68384614E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.47938379E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.22777893E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.92219375E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.43964951E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.23617694E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.96979562E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     5.13989898E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     4.85988826E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.60250720E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     4.96853485E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.03066500E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     8.31864131E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.18049361E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.18049361E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.70380707E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.73060064E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.14618236E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.33333983E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.73048002E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.53384540E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.21488825E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.21488825E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.59520119E+02   # ~g
#    BR                NDA      ID1      ID2
     3.89774706E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     3.89774706E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.57965823E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.57965823E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.10087496E-01    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.10087496E-01    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.55946510E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.55946510E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     7.55563210E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     7.55563210E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     2.93403835E-03   # Gamma(h0)
     2.66521859E-03   2        22        22   # BR(h0 -> photon photon)
     1.28780624E-03   2        22        23   # BR(h0 -> photon Z)
     2.02667025E-02   2        23        23   # BR(h0 -> Z Z)
     1.83387539E-01   2       -24        24   # BR(h0 -> W W)
     8.63310682E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.42115030E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.41139771E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.95226279E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.52326372E-07   2        -2         2   # BR(h0 -> Up up)
     2.95621543E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.31535846E-07   2        -1         1   # BR(h0 -> Down down)
     2.28411017E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.06417903E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     8.86406458E-05   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.76869177E+01   # Gamma(HH)
     2.57115847E-07   2        22        22   # BR(HH -> photon photon)
     6.06776383E-07   2        22        23   # BR(HH -> photon Z)
     9.58594544E-06   2        23        23   # BR(HH -> Z Z)
     7.77595646E-06   2       -24        24   # BR(HH -> W W)
     1.21586161E-05   2        21        21   # BR(HH -> gluon gluon)
     6.18506657E-09   2       -11        11   # BR(HH -> Electron electron)
     2.75328438E-04   2       -13        13   # BR(HH -> Muon muon)
     7.95091238E-02   2       -15        15   # BR(HH -> Tau tau)
     3.11967054E-13   2        -2         2   # BR(HH -> Up up)
     6.05103305E-08   2        -4         4   # BR(HH -> Charm charm)
     4.07419212E-03   2        -6         6   # BR(HH -> Top top)
     4.90144191E-07   2        -1         1   # BR(HH -> Down down)
     1.77286002E-04   2        -3         3   # BR(HH -> Strange strange)
     4.36950575E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.61206773E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.25269616E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.25269616E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.91350106E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.04390335E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.04977083E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     5.59548864E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.04203217E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.61929245E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     2.81064740E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.38250953E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     9.58580661E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     6.18410554E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.73695690E+01   # Gamma(A0)
     7.42415454E-07   2        22        22   # BR(A0 -> photon photon)
     1.79949654E-06   2        22        23   # BR(A0 -> photon Z)
     4.43183186E-05   2        21        21   # BR(A0 -> gluon gluon)
     6.14583008E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.73581626E-04   2       -13        13   # BR(A0 -> Muon muon)
     7.90051674E-02   2       -15        15   # BR(A0 -> Tau tau)
     3.05035202E-13   2        -2         2   # BR(A0 -> Up up)
     5.91600650E-08   2        -4         4   # BR(A0 -> Charm charm)
     4.21379229E-03   2        -6         6   # BR(A0 -> Top top)
     4.87026169E-07   2        -1         1   # BR(A0 -> Down down)
     1.76158340E-04   2        -3         3   # BR(A0 -> Strange strange)
     4.34168665E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.70206925E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.20265581E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.20265581E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.93595085E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     4.65949342E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     3.59786135E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.92361169E-04   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     5.46027858E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.44104751E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     9.38007616E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.82340792E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     2.84199670E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.29887135E-05   2        23        25   # BR(A0 -> Z h0)
     5.34550294E-41   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.92323230E+01   # Gamma(Hp)
     7.20076050E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.07854999E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     8.70787129E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.97860448E-07   2        -1         2   # BR(Hp -> Down up)
     8.33253351E-06   2        -3         2   # BR(Hp -> Strange up)
     5.01225808E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.65510367E-08   2        -1         4   # BR(Hp -> Down charm)
     1.79496898E-04   2        -3         4   # BR(Hp -> Strange charm)
     7.01894803E-04   2        -5         4   # BR(Hp -> Bottom charm)
     3.74856844E-07   2        -1         6   # BR(Hp -> Down top)
     8.43111422E-06   2        -3         6   # BR(Hp -> Strange top)
     4.73263008E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.93497574E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     8.80346230E-04   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.33626661E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.22552246E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     3.68978028E-03   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.13143310E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.17482793E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.18494959E-05   2        24        25   # BR(Hp -> W h0)
     5.88136128E-11   2        24        35   # BR(Hp -> W HH)
     4.52211422E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.43338504E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.19402562E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.19345900E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00010910E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.81639733E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.92549898E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.43338504E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.19402562E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.19345900E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99998409E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.59077667E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99998409E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.59077667E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26603404E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.34030674E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.06153060E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.59077667E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99998409E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.38800559E-04   # BR(b -> s gamma)
    2    1.58751379E-06   # BR(b -> s mu+ mu-)
    3    3.52335744E-05   # BR(b -> s nu nu)
    4    2.59392825E-15   # BR(Bd -> e+ e-)
    5    1.10809672E-10   # BR(Bd -> mu+ mu-)
    6    2.31969938E-08   # BR(Bd -> tau+ tau-)
    7    8.70738239E-14   # BR(Bs -> e+ e-)
    8    3.71979118E-09   # BR(Bs -> mu+ mu-)
    9    7.89012121E-07   # BR(Bs -> tau+ tau-)
   10    9.52855096E-05   # BR(B_u -> tau nu)
   11    9.84262834E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42229359E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93669576E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15846504E-03   # epsilon_K
   17    2.28167143E-15   # Delta(M_K)
   18    2.47854008E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28704021E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.35046806E-16   # Delta(g-2)_electron/2
   21    1.00490135E-11   # Delta(g-2)_muon/2
   22    2.84481121E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.69518084E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.07178189E-01   # C7
     0305 4322   00   2    -3.77591819E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.19047547E-01   # C8
     0305 6321   00   2    -4.76001192E-04   # C8'
 03051111 4133   00   0     1.61043371E+00   # C9 e+e-
 03051111 4133   00   2     1.61093835E+00   # C9 e+e-
 03051111 4233   00   2     8.51171550E-05   # C9' e+e-
 03051111 4137   00   0    -4.43312581E+00   # C10 e+e-
 03051111 4137   00   2    -4.42960745E+00   # C10 e+e-
 03051111 4237   00   2    -6.44111094E-04   # C10' e+e-
 03051313 4133   00   0     1.61043371E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61093831E+00   # C9 mu+mu-
 03051313 4233   00   2     8.51167023E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43312581E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.42960748E+00   # C10 mu+mu-
 03051313 4237   00   2    -6.44110698E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50458792E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.39791042E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50458792E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.39791137E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50458792E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.39817962E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85815499E-07   # C7
     0305 4422   00   2     8.37424054E-06   # C7
     0305 4322   00   2     2.05697179E-07   # C7'
     0305 6421   00   0     3.30476490E-07   # C8
     0305 6421   00   2    -5.75491782E-07   # C8
     0305 6321   00   2    -8.93996158E-09   # C8'
 03051111 4133   00   2     5.05794955E-07   # C9 e+e-
 03051111 4233   00   2     2.25749457E-06   # C9' e+e-
 03051111 4137   00   2     1.05492102E-07   # C10 e+e-
 03051111 4237   00   2    -1.70945240E-05   # C10' e+e-
 03051313 4133   00   2     5.05794002E-07   # C9 mu+mu-
 03051313 4233   00   2     2.25749432E-06   # C9' mu+mu-
 03051313 4137   00   2     1.05493088E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.70945247E-05   # C10' mu+mu-
 03051212 4137   00   2     1.89619494E-09   # C11 nu_1 nu_1
 03051212 4237   00   2     3.71002063E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     1.89624224E-09   # C11 nu_2 nu_2
 03051414 4237   00   2     3.71002063E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     1.90970259E-09   # C11 nu_3 nu_3
 03051616 4237   00   2     3.71002062E-06   # C11' nu_3 nu_3
