# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:23
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.91205687E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.51351232E+03  # scale for input parameters
    1    4.29577039E+01  # M_1
    2    1.05577258E+03  # M_2
    3    4.81364356E+03  # M_3
   11   -6.14128009E+03  # A_t
   12   -7.79197711E+02  # A_b
   13   -1.34690593E+03  # A_tau
   23   -2.92597117E+02  # mu
   25    4.84818535E+01  # tan(beta)
   26    1.86660041E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.95071625E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.05288290E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.22039026E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.51351232E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.51351232E+03  # (SUSY scale)
  1  1     8.38615548E-06   # Y_u(Q)^DRbar
  2  2     4.26016698E-03   # Y_c(Q)^DRbar
  3  3     1.01311494E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.51351232E+03  # (SUSY scale)
  1  1     8.17000594E-04   # Y_d(Q)^DRbar
  2  2     1.55230113E-02   # Y_s(Q)^DRbar
  3  3     8.10208579E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.51351232E+03  # (SUSY scale)
  1  1     1.42572578E-04   # Y_e(Q)^DRbar
  2  2     2.94794873E-02   # Y_mu(Q)^DRbar
  3  3     4.95796481E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.51351232E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.14127996E+03   # A_t(Q)^DRbar
Block Ad Q=  3.51351232E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -7.79197707E+02   # A_b(Q)^DRbar
Block Ae Q=  3.51351232E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.34690594E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.51351232E+03  # soft SUSY breaking masses at Q
   1    4.29577039E+01  # M_1
   2    1.05577258E+03  # M_2
   3    4.81364356E+03  # M_3
  21    3.29312614E+06  # M^2_(H,d)
  22    1.38378120E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.95071625E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.05288290E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.22039026E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.27575236E+02  # h0
        35     1.86578467E+03  # H0
        36     1.86660041E+03  # A0
        37     1.86811235E+03  # H+
   1000001     1.00865906E+04  # ~d_L
   2000001     1.00628394E+04  # ~d_R
   1000002     1.00862896E+04  # ~u_L
   2000002     1.00658378E+04  # ~u_R
   1000003     1.00865977E+04  # ~s_L
   2000003     1.00628518E+04  # ~s_R
   1000004     1.00862966E+04  # ~c_L
   2000004     1.00658388E+04  # ~c_R
   1000005     4.00579772E+03  # ~b_1
   2000005     4.31871146E+03  # ~b_2
   1000006     3.06915667E+03  # ~t_1
   2000006     4.02220224E+03  # ~t_2
   1000011     1.00208658E+04  # ~e_L-
   2000011     1.00089569E+04  # ~e_R-
   1000012     1.00201036E+04  # ~nu_eL
   1000013     1.00208984E+04  # ~mu_L-
   2000013     1.00090191E+04  # ~mu_R-
   1000014     1.00201356E+04  # ~nu_muL
   1000015     1.00265638E+04  # ~tau_1-
   2000015     1.00304485E+04  # ~tau_2-
   1000016     1.00293230E+04  # ~nu_tauL
   1000021     5.25294999E+03  # ~g
   1000022     4.26463595E+01  # ~chi_10
   1000023     3.04406787E+02  # ~chi_20
   1000025     3.09861537E+02  # ~chi_30
   1000035     1.14197432E+03  # ~chi_40
   1000024     3.03336260E+02  # ~chi_1+
   1000037     1.14096748E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.04823939E-02   # alpha
Block Hmix Q=  3.51351232E+03  # Higgs mixing parameters
   1   -2.92597117E+02  # mu
   2    4.84818535E+01  # tan[beta](Q)
   3    2.43061975E+02  # v(Q)
   4    3.48419709E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     1.29514379E-01   # Re[R_st(1,1)]
   1  2     9.91577544E-01   # Re[R_st(1,2)]
   2  1    -9.91577544E-01   # Re[R_st(2,1)]
   2  2     1.29514379E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99942159E-01   # Re[R_sb(1,1)]
   1  2     1.07553988E-02   # Re[R_sb(1,2)]
   2  1    -1.07553988E-02   # Re[R_sb(2,1)]
   2  2    -9.99942159E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -3.09594622E-01   # Re[R_sta(1,1)]
   1  2     9.50868640E-01   # Re[R_sta(1,2)]
   2  1    -9.50868640E-01   # Re[R_sta(2,1)]
   2  2    -3.09594622E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.88813277E-01   # Re[N(1,1)]
   1  2     1.03052114E-03   # Re[N(1,2)]
   1  3     1.48066572E-01   # Re[N(1,3)]
   1  4     1.79870118E-02   # Re[N(1,4)]
   2  1    -1.17739291E-01   # Re[N(2,1)]
   2  2    -6.64293175E-02   # Re[N(2,2)]
   2  3    -7.00723105E-01   # Re[N(2,3)]
   2  4    -7.00508198E-01   # Re[N(2,4)]
   3  1     9.15209509E-02   # Re[N(3,1)]
   3  2    -4.04348551E-02   # Re[N(3,2)]
   3  3     6.97652778E-01   # Re[N(3,3)]
   3  4    -7.09414928E-01   # Re[N(3,4)]
   4  1     3.11113509E-03   # Re[N(4,1)]
   4  2    -9.96970966E-01   # Re[N(4,2)]
   4  3     1.85478364E-02   # Re[N(4,3)]
   4  4     7.54664979E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.62639222E-02   # Re[U(1,1)]
   1  2    -9.99655044E-01   # Re[U(1,2)]
   2  1     9.99655044E-01   # Re[U(2,1)]
   2  2    -2.62639222E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.06824460E-01   # Re[V(1,1)]
   1  2     9.94277896E-01   # Re[V(1,2)]
   2  1     9.94277896E-01   # Re[V(2,1)]
   2  2     1.06824460E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02874663E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.77791752E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.38380162E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     8.36072160E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.41508813E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.66329247E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     5.20367123E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.00806102E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     4.29217900E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06905817E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.06332215E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.71195740E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.54185628E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.96388083E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.40900893E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.41682223E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.65539799E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     5.79661854E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     6.16256519E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00439372E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     4.28693965E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06165079E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.52353732E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.21179368E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.62779902E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.47665698E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.36623502E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.96957393E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     4.77552886E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.86517800E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.90605814E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.32461274E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.37125567E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.09290098E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.99941051E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.22068375E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.41513314E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.73021796E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.48207471E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02784805E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     7.09994280E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.00327120E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.41686702E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.71956231E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.47904525E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02415293E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     8.31085522E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.99595311E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.90605777E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.48762538E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.84449306E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.25017207E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.61948605E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.46310559E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.26003389E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.28938520E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.82634495E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     1.10404393E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.86812671E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.54078988E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.51337502E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.65429511E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.57522320E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.76994228E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.10341587E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.56070360E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.63183549E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.26100725E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.28933976E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.37925680E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.65224577E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     1.12927986E-04    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     9.86590286E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.54131591E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.51504591E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.08063323E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.99771220E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.76921331E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.16816232E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.56055734E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.63112437E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.28541382E+02   # ~b_1
#    BR                NDA      ID1      ID2
     7.66164879E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.11265201E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.10149077E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     6.48075842E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.48529531E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.34072388E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     2.23514571E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     1.13968659E+02   # ~b_2
#    BR                NDA      ID1      ID2
     3.10429949E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.40851557E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.38943639E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.63061304E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.87949420E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.27141955E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.12421557E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.13692624E-04    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     1.29765737E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.43175489E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.95917186E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     7.01849961E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     4.24046762E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.49281906E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.54068224E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.45518862E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.10883921E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.75259784E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.82563595E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.54394023E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.63141696E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.43182818E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.95909041E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     7.05856100E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     4.28158377E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.49266374E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.54120784E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.45495948E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.14044800E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     7.75187183E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.91226500E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.54379548E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.63070573E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.29292030E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.13703369E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.32153330E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.40499687E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.02134364E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.75669268E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.67177703E-04    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.41190837E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.53460025E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.65179686E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.66479163E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     6.50463447E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.12731730E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.25985240E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.08818908E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.53224328E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.93223174E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99722599E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.77315537E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     9.78737155E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     3.15388956E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.49063011E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.49553515E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.46617234E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.39684013E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.62025624E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     4.87867182E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     5.17577102E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.57680815E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.98561969E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.01432076E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.25493733E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.96313722E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.03651485E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.10951811E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.21018611E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.21018611E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.88548149E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.62324808E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.46148381E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.54628313E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.69764053E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.74629468E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.79780698E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     4.26391846E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.26391846E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.37169810E+02   # ~g
#    BR                NDA      ID1      ID2
     1.75545088E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.75545088E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.39132838E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.39132838E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.00201832E-01    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.00201832E-01    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     9.92591481E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     9.92591481E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     5.95160598E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     5.95160598E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.97813993E-03   # Gamma(h0)
     2.49168893E-03   2        22        22   # BR(h0 -> photon photon)
     1.82494617E-03   2        22        23   # BR(h0 -> photon Z)
     3.69431964E-02   2        23        23   # BR(h0 -> Z Z)
     2.90524927E-01   2       -24        24   # BR(h0 -> W W)
     7.50322842E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.53257083E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.01618106E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.81365545E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.27182165E-07   2        -2         2   # BR(h0 -> Up up)
     2.46845729E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.22797730E-07   2        -1         1   # BR(h0 -> Down down)
     1.89083614E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.02570332E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     7.40014092E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     6.49721690E+01   # Gamma(HH)
     4.87195132E-08   2        22        22   # BR(HH -> photon photon)
     6.52534962E-08   2        22        23   # BR(HH -> photon Z)
     3.89656049E-07   2        23        23   # BR(HH -> Z Z)
     2.28247073E-07   2       -24        24   # BR(HH -> W W)
     1.93235423E-05   2        21        21   # BR(HH -> gluon gluon)
     9.55848487E-09   2       -11        11   # BR(HH -> Electron electron)
     4.25527048E-04   2       -13        13   # BR(HH -> Muon muon)
     1.22888028E-01   2       -15        15   # BR(HH -> Tau tau)
     3.09528967E-14   2        -2         2   # BR(HH -> Up up)
     6.00351544E-09   2        -4         4   # BR(HH -> Charm charm)
     4.51364552E-04   2        -6         6   # BR(HH -> Top top)
     7.58996255E-07   2        -1         1   # BR(HH -> Down down)
     2.74502256E-04   2        -3         3   # BR(HH -> Strange strange)
     6.99899827E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.69396837E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     4.68609995E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     4.68609995E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.46162763E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.60832504E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.36522047E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     9.61229261E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     7.82094327E-06   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.98319988E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.77368458E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     7.87399667E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     2.85324192E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.83684742E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     6.20574487E+01   # Gamma(A0)
     1.29058901E-08   2        22        22   # BR(A0 -> photon photon)
     7.40378002E-08   2        22        23   # BR(A0 -> photon Z)
     3.00025527E-05   2        21        21   # BR(A0 -> gluon gluon)
     9.46072894E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.21174199E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.21631454E-01   2       -15        15   # BR(A0 -> Tau tau)
     3.03402879E-14   2        -2         2   # BR(A0 -> Up up)
     5.88366910E-09   2        -4         4   # BR(A0 -> Charm charm)
     4.60648931E-04   2        -6         6   # BR(A0 -> Top top)
     7.51191513E-07   2        -1         1   # BR(A0 -> Down down)
     2.71679477E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.92719165E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.06170439E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     4.89814401E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     4.89814401E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.51858213E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.57590032E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.53657989E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.10379962E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     8.37149709E-06   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.40243474E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     3.00540975E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     9.98106935E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.82918903E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     5.44150225E-07   2        23        25   # BR(A0 -> Z h0)
     4.98276416E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     7.00300398E+01   # Gamma(Hp)
     1.06440292E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.55065489E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.28718255E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.28842324E-07   2        -1         2   # BR(Hp -> Down up)
     1.22322452E-05   2        -3         2   # BR(Hp -> Strange up)
     7.51665366E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.41207074E-08   2        -1         4   # BR(Hp -> Down charm)
     2.62641489E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.05259986E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.97141094E-08   2        -1         6   # BR(Hp -> Down top)
     1.03047226E-06   2        -3         6   # BR(Hp -> Strange top)
     7.06593249E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.96271318E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.88159883E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.11209696E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     4.37164966E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     5.27799681E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     4.27240835E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     4.37819345E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     4.84174135E-07   2        24        25   # BR(Hp -> W h0)
     1.32712108E-12   2        24        35   # BR(Hp -> W HH)
     1.53507318E-13   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.86379078E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.35050374E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.35049012E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000579E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    4.19648256E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    4.25443184E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.86379078E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.35050374E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.35049012E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999980E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.98685546E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999980E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.98685546E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26013231E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.75949807E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.55282003E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.98685546E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999980E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.49065206E-04   # BR(b -> s gamma)
    2    1.58819198E-06   # BR(b -> s mu+ mu-)
    3    3.52612617E-05   # BR(b -> s nu nu)
    4    8.98375005E-16   # BR(Bd -> e+ e-)
    5    3.83585118E-11   # BR(Bd -> mu+ mu-)
    6    6.93909175E-09   # BR(Bd -> tau+ tau-)
    7    3.04375154E-14   # BR(Bs -> e+ e-)
    8    1.29967424E-09   # BR(Bs -> mu+ mu-)
    9    2.39962968E-07   # BR(Bs -> tau+ tau-)
   10    9.27357802E-05   # BR(B_u -> tau nu)
   11    9.57925106E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41866440E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93266609E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15743700E-03   # epsilon_K
   17    2.28166399E-15   # Delta(M_K)
   18    2.48046507E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29165458E-11   # BR(K^+ -> pi^+ nu nu)
   20   -4.29076846E-16   # Delta(g-2)_electron/2
   21   -1.83445152E-11   # Delta(g-2)_muon/2
   22   -5.19972872E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.81314491E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.20306515E-01   # C7
     0305 4322   00   2    -4.18352656E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.20946778E-01   # C8
     0305 6321   00   2    -4.00311180E-04   # C8'
 03051111 4133   00   0     1.62289652E+00   # C9 e+e-
 03051111 4133   00   2     1.62318918E+00   # C9 e+e-
 03051111 4233   00   2     4.32974793E-04   # C9' e+e-
 03051111 4137   00   0    -4.44558863E+00   # C10 e+e-
 03051111 4137   00   2    -4.44489390E+00   # C10 e+e-
 03051111 4237   00   2    -3.22209731E-03   # C10' e+e-
 03051313 4133   00   0     1.62289652E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62318893E+00   # C9 mu+mu-
 03051313 4233   00   2     4.32971034E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44558863E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44489414E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.22209430E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50520112E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     6.97332564E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50520112E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     6.97333337E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50520112E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     6.97551242E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85794551E-07   # C7
     0305 4422   00   2    -9.98346659E-06   # C7
     0305 4322   00   2    -2.48360544E-06   # C7'
     0305 6421   00   0     3.30458547E-07   # C8
     0305 6421   00   2     4.35023997E-06   # C8
     0305 6321   00   2    -9.29968632E-07   # C8'
 03051111 4133   00   2     1.44313769E-07   # C9 e+e-
 03051111 4233   00   2     1.00019637E-05   # C9' e+e-
 03051111 4137   00   2     1.36703822E-06   # C10 e+e-
 03051111 4237   00   2    -7.44403808E-05   # C10' e+e-
 03051313 4133   00   2     1.44309247E-07   # C9 mu+mu-
 03051313 4233   00   2     1.00019604E-05   # C9' mu+mu-
 03051313 4137   00   2     1.36704227E-06   # C10 mu+mu-
 03051313 4237   00   2    -7.44403907E-05   # C10' mu+mu-
 03051212 4137   00   2    -2.71804784E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.61105348E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.71804585E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.61105348E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.71749300E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.61105348E-05   # C11' nu_3 nu_3
