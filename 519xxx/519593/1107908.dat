# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  19:56
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.15422182E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.99591279E+03  # scale for input parameters
    1   -4.71179567E+01  # M_1
    2    1.96542930E+03  # M_2
    3    3.69827142E+03  # M_3
   11   -7.34462394E+03  # A_t
   12   -1.25911336E+03  # A_b
   13    7.43445997E+02  # A_tau
   23   -2.61020617E+02  # mu
   25    4.05528062E+01  # tan(beta)
   26    3.13885607E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.85835034E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.22818824E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.24097485E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.99591279E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.99591279E+03  # (SUSY scale)
  1  1     8.38692092E-06   # Y_u(Q)^DRbar
  2  2     4.26055583E-03   # Y_c(Q)^DRbar
  3  3     1.01320741E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.99591279E+03  # (SUSY scale)
  1  1     6.83445216E-04   # Y_d(Q)^DRbar
  2  2     1.29854591E-02   # Y_s(Q)^DRbar
  3  3     6.77763493E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.99591279E+03  # (SUSY scale)
  1  1     1.19266188E-04   # Y_e(Q)^DRbar
  2  2     2.46604650E-02   # Y_mu(Q)^DRbar
  3  3     4.14748453E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.99591279E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -7.34462382E+03   # A_t(Q)^DRbar
Block Ad Q=  3.99591279E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.25911336E+03   # A_b(Q)^DRbar
Block Ae Q=  3.99591279E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     7.43446001E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.99591279E+03  # soft SUSY breaking masses at Q
   1   -4.71179567E+01  # M_1
   2    1.96542930E+03  # M_2
   3    3.69827142E+03  # M_3
  21    9.80733136E+06  # M^2_(H,d)
  22    1.58569860E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.85835034E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.22818824E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.24097485E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.27825690E+02  # h0
        35     3.13827080E+03  # H0
        36     3.13885607E+03  # A0
        37     3.14046854E+03  # H+
   1000001     1.01045335E+04  # ~d_L
   2000001     1.00812286E+04  # ~d_R
   1000002     1.01041748E+04  # ~u_L
   2000002     1.00838438E+04  # ~u_R
   1000003     1.01045359E+04  # ~s_L
   2000003     1.00812327E+04  # ~s_R
   1000004     1.01041772E+04  # ~c_L
   2000004     1.00838444E+04  # ~c_R
   1000005     2.39502844E+03  # ~b_1
   2000005     4.88301791E+03  # ~b_2
   1000006     3.26344526E+03  # ~t_1
   2000006     4.89277980E+03  # ~t_2
   1000011     1.00198771E+04  # ~e_L-
   2000011     1.00094175E+04  # ~e_R-
   1000012     1.00191185E+04  # ~nu_eL
   1000013     1.00198884E+04  # ~mu_L-
   2000013     1.00094386E+04  # ~mu_R-
   1000014     1.00191294E+04  # ~nu_muL
   1000015     1.00153640E+04  # ~tau_1-
   2000015     1.00231236E+04  # ~tau_2-
   1000016     1.00222298E+04  # ~nu_tauL
   1000021     4.18504651E+03  # ~g
   1000022     4.56631197E+01  # ~chi_10
   1000023     2.76045901E+02  # ~chi_20
   1000025     2.80843887E+02  # ~chi_30
   1000035     2.08534923E+03  # ~chi_40
   1000024     2.74722377E+02  # ~chi_1+
   1000037     2.08531927E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.41229555E-02   # alpha
Block Hmix Q=  3.99591279E+03  # Higgs mixing parameters
   1   -2.61020617E+02  # mu
   2    4.05528062E+01  # tan[beta](Q)
   3    2.42926938E+02  # v(Q)
   4    9.85241743E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     7.67113729E-02   # Re[R_st(1,1)]
   1  2     9.97053341E-01   # Re[R_st(1,2)]
   2  1    -9.97053341E-01   # Re[R_st(2,1)]
   2  2     7.67113729E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.05423610E-03   # Re[R_sb(1,1)]
   1  2     9.99999444E-01   # Re[R_sb(1,2)]
   2  1    -9.99999444E-01   # Re[R_sb(2,1)]
   2  2    -1.05423610E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.30571912E-01   # Re[R_sta(1,1)]
   1  2     9.91438841E-01   # Re[R_sta(1,2)]
   2  1    -9.91438841E-01   # Re[R_sta(2,1)]
   2  2    -1.30571912E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.85685149E-01   # Re[N(1,1)]
   1  2     1.36611398E-03   # Re[N(1,2)]
   1  3    -1.65648328E-01   # Re[N(1,3)]
   1  4     3.13616451E-02   # Re[N(1,4)]
   2  1    -9.54744383E-02   # Re[N(2,1)]
   2  2    -3.03510268E-02   # Re[N(2,2)]
   2  3    -7.01883286E-01   # Re[N(2,3)]
   2  4    -7.05211528E-01   # Re[N(2,4)]
   3  1     1.38955841E-01   # Re[N(3,1)]
   3  2    -2.45560114E-02   # Re[N(3,2)]
   3  3     6.92750988E-01   # Re[N(3,3)]
   3  4    -7.07237121E-01   # Re[N(3,4)]
   4  1     8.32739932E-04   # Re[N(4,1)]
   4  2    -9.99236684E-01   # Re[N(4,2)]
   4  3     4.06848833E-03   # Re[N(4,3)]
   4  4     3.88433101E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -5.76016913E-03   # Re[U(1,1)]
   1  2    -9.99983410E-01   # Re[U(1,2)]
   2  1     9.99983410E-01   # Re[U(2,1)]
   2  2    -5.76016913E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -5.49569442E-02   # Re[V(1,1)]
   1  2     9.98488725E-01   # Re[V(1,2)]
   2  1     9.98488725E-01   # Re[V(2,1)]
   2  2     5.49569442E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02894100E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.71617008E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     9.10230580E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.92799949E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.33689643E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.19428641E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.18013606E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     8.12835198E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01200835E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.03841471E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.05313438E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.67032899E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.02375416E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.03359849E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.39283402E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.33810859E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.18845354E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.62378439E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.24617642E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00928398E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.03295267E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.19612563E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.18709742E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.43535661E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.39535628E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     5.42791315E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.81909360E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.08816954E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.67139918E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.76912347E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.03580566E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.03187577E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.37130945E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.02051563E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.75389162E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.33693065E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.09844494E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.40490472E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.21409739E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.01702994E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.98952899E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01968439E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.33814266E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.09021402E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.40363377E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.21118976E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01430118E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.89217618E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01424013E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.67987390E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.24326250E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.11844420E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.55874200E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.40198936E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.05438576E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.79259277E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.50736574E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.92865315E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.97101716E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.89780970E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.71135152E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.00993071E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.60886486E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.06693638E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.21484398E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.15659169E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.50804332E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.92911586E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.23205985E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.26415750E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.89659947E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.71172819E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.01120031E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.85009996E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.06659765E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.21477629E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.15613746E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     4.37350809E+01   # ~b_1
#    BR                NDA      ID1      ID2
     4.34710916E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.40291376E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.34126311E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.82110375E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     2.92844936E+02   # ~b_2
#    BR                NDA      ID1      ID2
     6.38696865E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     7.46072956E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     7.28377879E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     4.90043731E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.36512185E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     9.90484431E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     9.27380168E-02    2     1000021         5   # BR(~b_2 -> ~g b)
     2.68670472E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.28764139E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.67884822E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.85253364E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.60919963E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     7.64482318E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.60349236E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.71118445E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.07346375E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.48368646E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.06323345E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.99842930E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.21119452E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.15626536E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.67892122E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.85248484E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.64100114E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     7.67674150E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.60336942E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.71156081E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.07335323E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.51059832E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.06289590E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     4.50108531E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.21112695E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.15581107E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.38153321E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.05907621E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.34998815E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.39108245E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.75284429E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     3.13712956E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.01039065E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.57545631E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.56412016E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.66379407E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.41316059E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     9.23106973E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     8.25799849E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     1.23186583E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.26493213E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.93570881E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.85570307E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99913130E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.85029819E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.39378184E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.41605307E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.39413660E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.40722458E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.42201700E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.41033774E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.36409556E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     1.00493347E-02    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.02897146E-02    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.01784696E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     6.64690422E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.35301238E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.38706222E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     4.56022858E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.43949558E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.97393825E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.26536471E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.26536471E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.58882456E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.93782100E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.35949085E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     7.46830527E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.37952238E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.51181742E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.29154929E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     6.37249556E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     9.15418045E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     9.15418045E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     7.45235029E+01   # ~g
#    BR                NDA      ID1      ID2
     3.64328040E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     3.64328040E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.21859650E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.21859650E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.75877381E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.75877381E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     5.49022550E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.46097065E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     5.48832972E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.40107760E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     8.00366878E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     8.00366878E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.62643706E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.62643706E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.19365654E-03   # Gamma(h0)
     2.38408619E-03   2        22        22   # BR(h0 -> photon photon)
     1.76855636E-03   2        22        23   # BR(h0 -> photon Z)
     3.61057802E-02   2        23        23   # BR(h0 -> Z Z)
     2.82654391E-01   2       -24        24   # BR(h0 -> W W)
     7.16123246E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.42735545E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.96938026E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.67874644E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.25214627E-07   2        -2         2   # BR(h0 -> Up up)
     2.43020829E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.10444168E-07   2        -1         1   # BR(h0 -> Down down)
     1.84615820E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.90919772E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     3.30833487E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     8.03320024E+01   # Gamma(HH)
     1.52194122E-08   2        22        22   # BR(HH -> photon photon)
     1.83097938E-08   2        22        23   # BR(HH -> photon Z)
     2.31353291E-07   2        23        23   # BR(HH -> Z Z)
     6.58645244E-08   2       -24        24   # BR(HH -> W W)
     7.26931484E-06   2        21        21   # BR(HH -> gluon gluon)
     9.52653348E-09   2       -11        11   # BR(HH -> Electron electron)
     4.24173029E-04   2       -13        13   # BR(HH -> Muon muon)
     1.22507195E-01   2       -15        15   # BR(HH -> Tau tau)
     5.49446874E-14   2        -2         2   # BR(HH -> Up up)
     1.06585857E-08   2        -4         4   # BR(HH -> Charm charm)
     8.08720653E-04   2        -6         6   # BR(HH -> Top top)
     7.05216620E-07   2        -1         1   # BR(HH -> Down down)
     2.55063586E-04   2        -3         3   # BR(HH -> Strange strange)
     6.61700850E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.05664254E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     5.59275980E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     5.59275980E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.46377782E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.22236468E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.79576976E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.62634912E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     5.61654386E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.16510977E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     2.41071508E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.44808437E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.03342121E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.63993684E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     7.70315843E+01   # Gamma(A0)
     5.27790624E-09   2        22        22   # BR(A0 -> photon photon)
     1.22912075E-08   2        22        23   # BR(A0 -> photon Z)
     1.40887469E-05   2        21        21   # BR(A0 -> gluon gluon)
     9.41371936E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.19150565E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.21056812E-01   2       -15        15   # BR(A0 -> Tau tau)
     5.27440085E-14   2        -2         2   # BR(A0 -> Up up)
     1.02311723E-08   2        -4         4   # BR(A0 -> Charm charm)
     7.87102246E-04   2        -6         6   # BR(A0 -> Top top)
     6.96851555E-07   2        -1         1   # BR(A0 -> Down down)
     2.52037700E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.53857606E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.12160802E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     5.83552612E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     5.83552612E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.62143307E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.09179018E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.08548225E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.65033606E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     5.47029279E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.10453553E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     3.25032293E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.71652857E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     2.43564431E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     4.05762503E-07   2        23        25   # BR(A0 -> Z h0)
     1.34396357E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     8.56677215E+01   # Gamma(Hp)
     1.04620613E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.47285798E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.26517866E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.67875464E-07   2        -1         2   # BR(Hp -> Down up)
     1.12785822E-05   2        -3         2   # BR(Hp -> Strange up)
     7.09616481E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.17037312E-08   2        -1         4   # BR(Hp -> Down charm)
     2.40686139E-04   2        -3         4   # BR(Hp -> Strange charm)
     9.93717008E-04   2        -5         4   # BR(Hp -> Bottom charm)
     5.44506406E-08   2        -1         6   # BR(Hp -> Down top)
     1.54394985E-06   2        -3         6   # BR(Hp -> Strange top)
     6.70834254E-01   2        -5         6   # BR(Hp -> Bottom top)
     4.16523328E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.01738412E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     8.73795964E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.20317458E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.78205848E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     5.04683052E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     5.26233720E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     3.65630383E-07   2        24        25   # BR(Hp -> W h0)
     8.14663390E-13   2        24        35   # BR(Hp -> W HH)
     1.73217301E-13   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.57376178E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.64457271E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.64453009E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00002592E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    5.82157896E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.08076438E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.57376178E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.64457271E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.64453009E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999718E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.82230836E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999718E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.82230836E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26454468E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    7.36105628E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.22631733E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.82230836E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999718E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.40342419E-04   # BR(b -> s gamma)
    2    1.58901040E-06   # BR(b -> s mu+ mu-)
    3    3.52674174E-05   # BR(b -> s nu nu)
    4    1.27243367E-15   # BR(Bd -> e+ e-)
    5    5.43569099E-11   # BR(Bd -> mu+ mu-)
    6    1.13790698E-08   # BR(Bd -> tau+ tau-)
    7    4.34529085E-14   # BR(Bs -> e+ e-)
    8    1.85630900E-09   # BR(Bs -> mu+ mu-)
    9    3.93887153E-07   # BR(Bs -> tau+ tau-)
   10    9.57580844E-05   # BR(B_u -> tau nu)
   11    9.89144351E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41984508E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93541312E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15755746E-03   # epsilon_K
   17    2.28166170E-15   # Delta(M_K)
   18    2.48083741E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29257291E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.71227199E-16   # Delta(g-2)_electron/2
   21   -1.58711795E-11   # Delta(g-2)_muon/2
   22   -4.49357806E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.48102273E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.11610140E-01   # C7
     0305 4322   00   2    -2.69953038E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.11452758E-01   # C8
     0305 6321   00   2    -2.15656517E-04   # C8'
 03051111 4133   00   0     1.62706421E+00   # C9 e+e-
 03051111 4133   00   2     1.62738864E+00   # C9 e+e-
 03051111 4233   00   2     3.88053909E-04   # C9' e+e-
 03051111 4137   00   0    -4.44975631E+00   # C10 e+e-
 03051111 4137   00   2    -4.44965586E+00   # C10 e+e-
 03051111 4237   00   2    -2.87005477E-03   # C10' e+e-
 03051313 4133   00   0     1.62706421E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62738841E+00   # C9 mu+mu-
 03051313 4233   00   2     3.88053520E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44975631E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44965609E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.87005476E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50532945E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     6.20560409E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50532945E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     6.20560473E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50532945E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     6.20578319E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85803618E-07   # C7
     0305 4422   00   2    -1.05704436E-05   # C7
     0305 4322   00   2    -2.61312682E-06   # C7'
     0305 6421   00   0     3.30466313E-07   # C8
     0305 6421   00   2     1.22238056E-05   # C8
     0305 6321   00   2    -9.90080255E-07   # C8'
 03051111 4133   00   2     6.14950646E-08   # C9 e+e-
 03051111 4233   00   2     7.65929266E-06   # C9' e+e-
 03051111 4137   00   2     9.52656478E-07   # C10 e+e-
 03051111 4237   00   2    -5.66516598E-05   # C10' e+e-
 03051313 4133   00   2     6.14922866E-08   # C9 mu+mu-
 03051313 4233   00   2     7.65929096E-06   # C9' mu+mu-
 03051313 4137   00   2     9.52658533E-07   # C10 mu+mu-
 03051313 4237   00   2    -5.66516649E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.87359214E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.22491681E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.87359121E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.22491681E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.87333364E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.22491681E-05   # C11' nu_3 nu_3
