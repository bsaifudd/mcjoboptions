# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  17:03
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.17314319E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.51246992E+03  # scale for input parameters
    1   -2.21603756E+02  # M_1
    2    2.21283625E+02  # M_2
    3    4.18036272E+03  # M_3
   11   -7.39070631E+03  # A_t
   12    1.10263149E+03  # A_b
   13   -7.61582513E+02  # A_tau
   23    1.09046994E+03  # mu
   25    2.09121934E+01  # tan(beta)
   26    8.48465972E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.13658388E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.10290898E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.95025733E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.51246992E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.51246992E+03  # (SUSY scale)
  1  1     8.39395275E-06   # Y_u(Q)^DRbar
  2  2     4.26412800E-03   # Y_c(Q)^DRbar
  3  3     1.01405691E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.51246992E+03  # (SUSY scale)
  1  1     3.52733213E-04   # Y_d(Q)^DRbar
  2  2     6.70193105E-03   # Y_s(Q)^DRbar
  3  3     3.49800817E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.51246992E+03  # (SUSY scale)
  1  1     6.15545250E-05   # Y_e(Q)^DRbar
  2  2     1.27275235E-02   # Y_mu(Q)^DRbar
  3  3     2.14056008E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.51246992E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -7.39070570E+03   # A_t(Q)^DRbar
Block Ad Q=  2.51246992E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.10263164E+03   # A_b(Q)^DRbar
Block Ae Q=  2.51246992E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -7.61582526E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.51246992E+03  # soft SUSY breaking masses at Q
   1   -2.21603756E+02  # M_1
   2    2.21283625E+02  # M_2
   3    4.18036272E+03  # M_3
  21   -5.84765299E+05  # M^2_(H,d)
  22   -1.11516451E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.13658388E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.10290898E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.95025733E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.24015886E+02  # h0
        35     8.48478341E+02  # H0
        36     8.48465972E+02  # A0
        37     8.53851377E+02  # H+
   1000001     1.00748377E+04  # ~d_L
   2000001     1.00509961E+04  # ~d_R
   1000002     1.00745890E+04  # ~u_L
   2000002     1.00541219E+04  # ~u_R
   1000003     1.00748416E+04  # ~s_L
   2000003     1.00510009E+04  # ~s_R
   1000004     1.00745928E+04  # ~c_L
   2000004     1.00541240E+04  # ~c_R
   1000005     2.15318742E+03  # ~b_1
   2000005     4.03089992E+03  # ~b_2
   1000006     2.09913955E+03  # ~t_1
   2000006     3.00718698E+03  # ~t_2
   1000011     1.00215047E+04  # ~e_L-
   2000011     1.00087256E+04  # ~e_R-
   1000012     1.00207508E+04  # ~nu_eL
   1000013     1.00215185E+04  # ~mu_L-
   2000013     1.00087491E+04  # ~mu_R-
   1000014     1.00207634E+04  # ~nu_muL
   1000015     1.00152897E+04  # ~tau_1-
   2000015     1.00255427E+04  # ~tau_2-
   1000016     1.00243446E+04  # ~nu_tauL
   1000021     4.57065905E+03  # ~g
   1000022     2.24225374E+02  # ~chi_10
   1000023     2.43275538E+02  # ~chi_20
   1000025     1.10379962E+03  # ~chi_30
   1000035     1.10511978E+03  # ~chi_40
   1000024     2.43466977E+02  # ~chi_1+
   1000037     1.10717694E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.78923729E-02   # alpha
Block Hmix Q=  2.51246992E+03  # Higgs mixing parameters
   1    1.09046994E+03  # mu
   2    2.09121934E+01  # tan[beta](Q)
   3    2.43393936E+02  # v(Q)
   4    7.19894506E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.74914610E-01   # Re[R_st(1,1)]
   1  2     2.22579206E-01   # Re[R_st(1,2)]
   2  1    -2.22579206E-01   # Re[R_st(2,1)]
   2  2     9.74914610E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99991602E-01   # Re[R_sb(1,1)]
   1  2     4.09822692E-03   # Re[R_sb(1,2)]
   2  1    -4.09822692E-03   # Re[R_sb(2,1)]
   2  2     9.99991602E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     2.10464114E-01   # Re[R_sta(1,1)]
   1  2     9.77601584E-01   # Re[R_sta(1,2)]
   2  1    -9.77601584E-01   # Re[R_sta(2,1)]
   2  2     2.10464114E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99117116E-01   # Re[N(1,1)]
   1  2     7.02661421E-04   # Re[N(1,2)]
   1  3     4.15128331E-02   # Re[N(1,3)]
   1  4     6.41710470E-03   # Re[N(1,4)]
   2  1    -2.36483299E-03   # Re[N(2,1)]
   2  2    -9.96825443E-01   # Re[N(2,2)]
   2  3     7.69353920E-02   # Re[N(2,3)]
   2  4    -2.03565617E-02   # Re[N(2,4)]
   3  1     3.38612664E-02   # Re[N(3,1)]
   3  2    -4.00438546E-02   # Re[N(3,2)]
   3  3    -7.04945541E-01   # Re[N(3,3)]
   3  4    -7.07320075E-01   # Re[N(3,4)]
   4  1    -2.47550092E-02   # Re[N(4,1)]
   4  2     6.88115728E-02   # Re[N(4,2)]
   4  3     7.03853262E-01   # Re[N(4,3)]
   4  4    -7.06571117E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.94082228E-01   # Re[U(1,1)]
   1  2     1.08630217E-01   # Re[U(1,2)]
   2  1     1.08630217E-01   # Re[U(2,1)]
   2  2     9.94082228E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99585661E-01   # Re[V(1,1)]
   1  2     2.87837972E-02   # Re[V(1,2)]
   2  1     2.87837972E-02   # Re[V(2,1)]
   2  2     9.99585661E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02376714E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.98275761E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.12004123E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     5.98591640E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44721977E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.69728148E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.03268955E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.43368367E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     9.19011632E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.01677002E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     7.01884728E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03007476E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.97027390E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.42959570E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     9.07816827E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     6.18263296E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44753699E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.69542598E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.03204234E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.51536207E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     1.02667014E-03    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.01545981E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     7.01731930E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.19243339E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     6.77330754E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     3.65479332E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.77262113E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     5.53757303E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     7.25304175E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.00488954E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.49863779E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.40344909E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.76146365E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.19292311E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     3.34463039E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.47141650E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.73019596E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44726769E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.65066461E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.01702692E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.00507927E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.00141368E-03    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.08291431E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     4.92738434E-04    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44758470E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.64878110E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.01637002E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.00486049E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.00097803E-03    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.08161616E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     7.07732787E-04    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.53697496E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.14868605E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.84195601E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     9.46772777E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.88530808E-03    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.73694093E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.77913646E-02    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.04766746E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.10861504E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.88894602E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.35831254E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.18808132E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.91614298E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.44534890E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     3.64976142E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.37678640E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.60648570E-03    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.88855852E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.04784721E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.10857915E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.88860352E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.35844024E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.18806252E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.91601510E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.51381485E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     3.71796980E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.37675928E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.61204929E-03    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.88840631E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     5.41220809E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.56644588E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.70067737E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.63458219E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.65255310E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.33076417E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     4.38418048E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     2.06925088E+01   # ~b_2
#    BR                NDA      ID1      ID2
     1.08586043E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.40380788E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.01932813E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.01254065E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.77325397E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     3.99805470E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.32702832E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.54770713E-04    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     2.54711793E-02    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     6.57562318E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.22606253E-02    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     1.11835000E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     1.22629858E-02    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     5.21907555E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.29015732E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.57024312E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.35827141E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.22288167E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.92771329E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     2.82353794E-04    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.39204748E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.12825945E-04    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.88821295E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.21914840E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.29009820E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.57011303E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.35839867E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.22283816E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.92757721E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     2.85142954E-04    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.39202181E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.26441564E-04    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.88806066E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     5.06723605E+01   # ~t_1
#    BR                NDA      ID1      ID2
     9.37105563E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.68779507E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.07327211E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.01420979E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.44933225E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     6.75534564E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     6.08053926E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.39110121E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.86346495E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.23200381E-03    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     6.75833354E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.02625869E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.50051970E-03    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.30669471E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.08575323E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.70473991E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     9.79308618E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     2.28202661E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     7.93987073E-04    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     1.23264556E-10   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.40369837E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.32978946E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.10683935E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.10670233E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.05295526E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     8.65949323E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     8.25844686E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.87804405E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     5.91104100E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     1.30991842E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     2.85511508E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.62235015E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     2.79960870E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     1.60208722E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     5.59419478E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.69636750E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     5.58516321E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.16865054E-10   # chi^0_2
#    BR                NDA      ID1      ID2
     2.83355626E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     9.12143586E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     8.60315293E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.16891302E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.16859913E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     7.93613363E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.56017161E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.55922345E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.30584014E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.52033582E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     9.62109281E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.60356341E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.60356341E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.10669054E-02    2          37  -1000024   # BR(chi^0_3 -> H^+ chi^-_1)
     1.10669054E-02    2         -37   1000024   # BR(chi^0_3 -> H^- chi^+_1)
     2.58448623E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.94798171E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     2.72852336E-04    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     2.31311505E-04    2     1000023        36   # BR(chi^0_3 -> chi^0_2 A^0)
     4.39674285E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.57463677E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     6.42624269E-03    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
     1.49082012E-02    2     1000023        35   # BR(chi^0_3 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     6.21607828E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.99062330E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     5.29118370E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     5.29118370E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     8.90947507E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.79916061E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.79916061E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.09505362E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     1.09505362E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     5.22114728E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     7.08998352E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     3.77108585E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     3.50996593E-04    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     5.76028176E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.03671933E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     5.79657893E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     1.38741717E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     6.75939628E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.17829754E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     5.30698406E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.30698406E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.15125834E+02   # ~g
#    BR                NDA      ID1      ID2
     1.87457878E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.87457878E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.06569281E-01    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.06569281E-01    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.90362932E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.90362932E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.55311946E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.55311946E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.41788404E-03   # Gamma(h0)
     2.58738688E-03   2        22        22   # BR(h0 -> photon photon)
     1.56008666E-03   2        22        23   # BR(h0 -> photon Z)
     2.77269329E-02   2        23        23   # BR(h0 -> Z Z)
     2.33593812E-01   2       -24        24   # BR(h0 -> W W)
     7.94213004E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.03991253E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.24183787E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.46388252E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.32355539E-07   2        -2         2   # BR(h0 -> Up up)
     2.56886177E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.83519700E-07   2        -1         1   # BR(h0 -> Down down)
     2.11048519E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.64347085E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     5.53027053E+00   # Gamma(HH)
     3.10018657E-07   2        22        22   # BR(HH -> photon photon)
     9.58025237E-07   2        22        23   # BR(HH -> photon Z)
     7.76448838E-05   2        23        23   # BR(HH -> Z Z)
     1.23220966E-04   2       -24        24   # BR(HH -> W W)
     7.29709766E-05   2        21        21   # BR(HH -> gluon gluon)
     9.74972808E-09   2       -11        11   # BR(HH -> Electron electron)
     4.33935542E-04   2       -13        13   # BR(HH -> Muon muon)
     1.25298834E-01   2       -15        15   # BR(HH -> Tau tau)
     8.98032823E-13   2        -2         2   # BR(HH -> Up up)
     1.74150294E-07   2        -4         4   # BR(HH -> Charm charm)
     8.72780497E-03   2        -6         6   # BR(HH -> Top top)
     8.19280242E-07   2        -1         1   # BR(HH -> Down down)
     2.96413112E-04   2        -3         3   # BR(HH -> Strange strange)
     8.46506936E-01   2        -5         5   # BR(HH -> Bottom bottom)
     9.08831578E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     4.52181342E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.55539297E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.72424393E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     9.22022079E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     5.47303991E+00   # Gamma(A0)
     7.05157412E-07   2        22        22   # BR(A0 -> photon photon)
     1.72518914E-06   2        22        23   # BR(A0 -> photon Z)
     1.71352600E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.69988129E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.31716966E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.24660504E-01   2       -15        15   # BR(A0 -> Tau tau)
     7.96584850E-13   2        -2         2   # BR(A0 -> Up up)
     1.54480905E-07   2        -4         4   # BR(A0 -> Charm charm)
     9.27120419E-03   2        -6         6   # BR(A0 -> Top top)
     8.15096129E-07   2        -1         1   # BR(A0 -> Down down)
     2.94899363E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.42235474E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.31296867E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     6.53391753E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.64528000E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     5.02091630E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.20463859E-04   2        23        25   # BR(A0 -> Z h0)
     3.53107310E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     6.05439266E+00   # Gamma(Hp)
     1.10141009E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.70887201E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.33192620E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.07978576E-07   2        -1         2   # BR(Hp -> Down up)
     1.34252273E-05   2        -3         2   # BR(Hp -> Strange up)
     9.53217694E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.52559730E-08   2        -1         4   # BR(Hp -> Down charm)
     2.91448478E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.33484064E-03   2        -5         4   # BR(Hp -> Bottom charm)
     8.22677559E-07   2        -1         6   # BR(Hp -> Down top)
     1.83296570E-05   2        -3         6   # BR(Hp -> Strange top)
     8.58855363E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.90969162E-08   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     5.69978717E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.12048168E-04   2        24        25   # BR(Hp -> W h0)
     1.00048961E-09   2        24        35   # BR(Hp -> W HH)
     1.01205234E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.00459674E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    4.37315236E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    4.37319833E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99989489E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.29716711E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.28665596E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.00459674E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    4.37315236E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    4.37319833E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999988E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.20516324E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999988E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.20516324E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.23989145E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    7.26674479E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.09984589E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.20516324E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999988E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.48903540E-04   # BR(b -> s gamma)
    2    1.58898516E-06   # BR(b -> s mu+ mu-)
    3    3.52798129E-05   # BR(b -> s nu nu)
    4    2.68069042E-15   # BR(Bd -> e+ e-)
    5    1.14515651E-10   # BR(Bd -> mu+ mu-)
    6    2.39492016E-08   # BR(Bd -> tau+ tau-)
    7    9.01149824E-14   # BR(Bs -> e+ e-)
    8    3.84969620E-09   # BR(Bs -> mu+ mu-)
    9    8.15781626E-07   # BR(Bs -> tau+ tau-)
   10    9.37926443E-05   # BR(B_u -> tau nu)
   11    9.68842108E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41879724E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93477520E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15713195E-03   # epsilon_K
   17    2.28166057E-15   # Delta(M_K)
   18    2.48205144E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29526431E-11   # BR(K^+ -> pi^+ nu nu)
   20    7.84292253E-17   # Delta(g-2)_electron/2
   21    3.35309863E-12   # Delta(g-2)_muon/2
   22    9.48750910E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.88210052E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.15456600E-01   # C7
     0305 4322   00   2    -7.77606682E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.35835727E-01   # C8
     0305 6321   00   2    -9.58930040E-04   # C8'
 03051111 4133   00   0     1.61011601E+00   # C9 e+e-
 03051111 4133   00   2     1.61071453E+00   # C9 e+e-
 03051111 4233   00   2     6.42727866E-05   # C9' e+e-
 03051111 4137   00   0    -4.43280811E+00   # C10 e+e-
 03051111 4137   00   2    -4.43382545E+00   # C10 e+e-
 03051111 4237   00   2    -4.86894327E-04   # C10' e+e-
 03051313 4133   00   0     1.61011601E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61071448E+00   # C9 mu+mu-
 03051313 4233   00   2     6.42707086E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43280811E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43382551E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.86892286E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50557350E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.05677929E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50557350E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.05678378E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50557350E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.05804946E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85872620E-07   # C7
     0305 4422   00   2     7.69174274E-06   # C7
     0305 4322   00   2     1.48784306E-06   # C7'
     0305 6421   00   0     3.30525418E-07   # C8
     0305 6421   00   2    -3.88259808E-06   # C8
     0305 6321   00   2     5.78334456E-07   # C8'
 03051111 4133   00   2     9.98347187E-07   # C9 e+e-
 03051111 4233   00   2     2.50653869E-06   # C9' e+e-
 03051111 4137   00   2     8.86623999E-06   # C10 e+e-
 03051111 4237   00   2    -1.90029312E-05   # C10' e+e-
 03051313 4133   00   2     9.98345642E-07   # C9 mu+mu-
 03051313 4233   00   2     2.50653852E-06   # C9' mu+mu-
 03051313 4137   00   2     8.86624156E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.90029317E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.87856788E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     4.12450160E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.87856780E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     4.12450160E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.87854506E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     4.12450159E-06   # C11' nu_3 nu_3
