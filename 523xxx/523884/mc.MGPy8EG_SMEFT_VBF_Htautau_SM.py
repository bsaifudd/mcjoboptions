from MadGraphControl.MadGraphUtils import *
import math, subprocess, os, shutil

safefactor=1.1
nevents=runArgs.maxEvents*safefactor

#==================================================================================

# General settings
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#==================================================================================

process = '''
import model SMEFTsim_U35_MwScheme_UFO-SMlimit_massless
generate p p > h j j QCD=0 /a NP<=1
output -f
'''
process_dir = new_process(process)


#==================================================================================

#Fetch default LO run_card.dat and set parameters
extras = { 
            'nevents'     : nevents,
            'lhe_version' : '3.0', 
            'cut_decays'  : 'F', 
            'pdlabel'     : 'lhapdf',
            'lhaid'       : 260000,
#            'ickkw'       : ickkw,
            'ptj'         : 20.,
            'ptb'         : 20.,
            'use_syst'    : "False" 
         }

#==================================================================================

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#==================================================================================

print_cards()

#==================================================================================

generate(process_dir=process_dir,runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#==================================================================================

evgenConfig.description = 'MadGraph_SMEFT_VBF_Htautau_SM'
evgenConfig.keywords    = ['Higgs']
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.contact     = ['huanguo.li@cern.ch']
evgenConfig.nEventsPerJob = 10000 


#==================================================================================
# Pythia 8 showering setup

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Dipole option Pythia8
genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15' ]



