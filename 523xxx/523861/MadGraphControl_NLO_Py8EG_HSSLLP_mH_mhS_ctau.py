#############################################
### Production of scalar long-lived particles
### though a higgs-like scalar mediator
############################################

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short 

import re
import math, cmath
import os
import random

safefactor=1.2
nevents=runArgs.maxEvents*safefactor

phys_short = get_physics_short()
str_mH = phys_short.split('_')[2].replace("mH","")
str_mhS = phys_short.split('_')[3].replace("mhS","")
str_avgtau = phys_short.split('_')[4].replace("ctau","")
mH = float(str_mH)
mhS = float(str_mhS)
avgtau = float(str_avgtau)

if mH <= 125:
    param_card_extras = {}
    param_card_extras['HIDDEN'] = {'epsilon':'1e-10','kap':'1e-4','mhsinput':mhS,'mzdinput':1.000000e+03}
    param_card_extras['HIGGS']={'mhinput':mH}
    param_card_extras['DECAY']={ 'wzp':'Auto', 'wh':'Auto', 'wt':'Auto', 'whs':'Auto'}

elif mH > 125:
    param_card_extras = {}
    param_card_extras['HIDDEN'] = {'epsilon':'1e-10','kap':'1e-4','mhsinput':mhS,'mzdinput':1.000000e+03}
    param_card_extras['HIGGS']={'mhinput':mH}
    param_card_extras['DECAY']={ 'wzp':'5', 'wh':'5', 'wt':'Auto', 'whs':'5'}

my_process = """  
import model HAHM_variableMW_v3_UFO
define p = g u c d s b u~ c~ d~ s~
define j = g u c d b s u~ c~ d~ s~
define f = u c d s u~ c~ d~ s~ b b~ e+ e- mu+ mu- ta+ ta- t t~  
generate g g > h HIG=1 HIW=0 QED=0 QCD=0, (h > h2 h2, h2 > f f)
output -f
"""

process_dir = new_process(my_process)
rname='run_01'
settings = {'nevents':nevents} 

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=param_card_extras) 
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#---------------------------------------------------------------------------
# Arrange LHE file output
#---------------------------------------------------------------------------

# initialise random number generator/sequence
random.seed(runArgs.randomSeed)
# lifetime function
def lifetime(avgtau = 21):
    import math
    t = random.random()
    return -1.0 * avgtau * math.log(t)
    

# replacing lifetime of scalar, manually
unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
unzip1.wait()
    
oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')
init = True
for line in oldlhe:
    if init==True:
        newlhe.write(line)
        if '</init>' in line:
            init = False
    else:  
        if 'vent' in line or line.startswith("<"):
            newlhe.write(line)
            continue
        newline = line.rstrip('\n')
        columns = (' '.join(newline.split())).split()
        pdgid = int(columns[0])
        if pdgid == 35:
            part1 = line[:-22]
            part2 = "%.11E" % (lifetime(avgtau))
            part3 = line[-12:]
            newlhe.write(part1+part2+part3)
        else:
            newlhe.write(line)

oldlhe.close()
newlhe.close()
    
zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
zip1.wait()
shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',
            process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#---------------------------------------------------------------------------
# Parton Showering Generation
#---------------------------------------------------------------------------
check_reset_proc_number(opts)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#relax the cuts on displaced vertices and non G4 particles
testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

evgenConfig.description = "Displaced hadronic jets process Higgs > S S with mH={}GeV, mS={}GeV".format(mH, mhS, avgtau)
evgenConfig.keywords = ["exotic", "BSM", "BSMHiggs", "longLived"]
evgenConfig.contact  = ['simon.berlendis@cern.ch', 'hao.zhou@cern.ch',
                        'Cristiano.Alpigiani@cern.ch', 'hrussell@cern.ch','Hannah.Zuldie.van.der.Schyf@cern.ch' ]
evgenConfig.process="Higgs --> LLPs"
evgenConfig.nEventsPerJob = 2000
