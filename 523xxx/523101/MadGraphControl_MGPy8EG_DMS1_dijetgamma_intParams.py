from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
import math

# Input settings

mg_proc=[]
for quark_decay in quark_decays:
    quark_pair='%s %s~'%(quark_decay,quark_decay) if quark_decay!='j' else 'j j'

    if not mg_proc:
        mg_proc.append("generate p p > Y1 a, Y1 > %s @1"%(quark_pair))
    else:
        mg_proc.append("add process p p > Y1 a, Y1 > %s @1"%(quark_pair))

process_def='pp>({Y1,5000001}>jj)a'

process="""
import model DMsimp_s_spin1

"""+('\n'.join(mg_proc))+"""
output -f
"""

process_dir = new_process(process)

settings = {'nevents': int(runArgs.maxEvents*2./filteff),
            'iseed': int(runArgs.randomSeed),
            'lhe_version': 3.0,
            'cut_decays' :'False',
            #'pdlabel'    :"'lhapdf'",
            #'lhaid'      :263000,
            'xqcut'      :0.,
            'drjj'       :0.}
            #'pta'        :0.9 * phminpt}

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

print_cards()

params = { 'DMINPUTS': {'2':gVDM, # gVXd
                        '3':gADM, # gAXd
                        '4':gVSM, # gVd11
                        '5':gVSM, # gVu11
                        '6':gVSM, # gVd22
                        '7':gVSM, # gVu22
                        '8':gVSM, # gVd33
                        '9':gVSM, # gVu33
                        '13':gASM, # gAd11
                        '14':gASM, # gAu11
                        '15':gASM, # gAd22
                        '16':gASM, # gAu22
                        '17':gASM, # gAd33
                        '18':gASM}, # gAu33
           'MASS' : {'1000022':mDM,
                     '5000001':mR},
           'DECAY' : {'5000001':'DECAY 5000001 auto # Y1'}
}
# params = { 'DMINPUTS': {'gVXd':gVDM,
#                         'gAXd':gADM,
#                         'gVd11':gVSM,
#                         'gVu11':gVSM,
#                         'gVd22':gVSM,
#                         'gVu22':gVSM,
#                         'gVd33':gVSM,
#                         'gVu33':gVSM,
#                         'gAd11':gASM,
#                         'gAu11':gASM,
#                         'gAd22':gASM,
#                         'gAu22':gASM,
#                         'gAd33':gASM,
#                         'gAu33':gASM},
#            'MASS' : {'1000022':mDM,
#                      '5000001':mR},
#            'DECAY' : {'5000001':'DECAY 5000001 auto # Y1'}
# }

modify_param_card(process_dir=process_dir, params=params)

generate(process_dir=process_dir, runArgs=runArgs)


arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3)

#### Shower
evgenConfig.description = "Zprime with ISR - model DMsimp_s_spin1"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Chris Delitzsch <chris.malena.delitzsch@cern.ch>, Karol Krizka <kkrizka@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += ["1000022:all = xd xd~ 2 0 0 %d 0" %(mDM),
                            "1000022:isVisible = false"]

# Add filter
include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.Ptmin = [phminpt*GeV]
filtSeq.DirectPhotonFilter.Ptmax = [10000.*GeV]
