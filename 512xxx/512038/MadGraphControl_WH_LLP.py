#############################################
### Production of scalar long-lived particles
### though a higgs-like scalar mediator
############################################

#######
# PDF #
#######
pdflabel = 'lhapdf'
lhaid = 315000 # NNPDF31_lo_as_0118

# basename for madgraph LHEF file
rname = 'run_01'

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
modelcode = "HAHM_variableMW_v3_UFO"
myprocess_plus = "p p > w+ h, (w+ > l+ vl), (h > h2 h2, h2 > f f) "
myprocess_min = "p p > w- h, (w- > l- vl~), (h > h2 h2, h2 > f f) "

process= """
import model """+modelcode+"""
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define f = u c d s u~ c~ d~ s~ b b~ e+ e- mu+ mu- ta+ ta- t t~
define p = g u c d s u~ c~ d~ s~
generate """+myprocess_plus+"""
add process """+myprocess_min+"""
output -f
"""

#---------------------------------------------------------------------------
# Energy
#---------------------------------------------------------------------------
    
beamEnergy = -999.
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
   raise RuntimeError("No center of mass energy found")


#---------------------------------------------------------------------------
# Number of events
#---------------------------------------------------------------------------
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
nevents = int(nevents)

safefactor = 1.1
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents)*safefactor
else: nevents = nevents*safefactor


#---------------------------------------------------------------------------
# Run and Param cards
#---------------------------------------------------------------------------

run_card_extras = { 
    'lhe_version':'3.0',
    'cut_decays':'F',
    'event_norm':'sum',
    'pdlabel':pdflabel,
    'lhaid':lhaid,
    'ptj':'0',
    'ptb':'0',
    'pta':'0',
    'ptl':'0',
    'etaj':'-1',
    'etab':'-1',
    'etaa':'-1',
    'etal':'-1',
    'drjj':'0',
    'drbb':'0',
    'drll':'0',
    'draa':'0',
    'drbj':'0',
    'draj':'0',
    'drjl':'0',
    'drab':'0',
    'drbl':'0',
    'dral':'0' ,
    'use_syst':'T',
    'sys_scalefact': '1 0.5 2',
    'sys_pdf'      : "NNPDF31_lo_as_0118",
    'nevents' : nevents,
    'small_width_treatment'  :'1e-12'
    }

if mH <= 125: 
    param_card_extras = { 
        "HIDDEN": { 'epsilon': '1e-10', #kinetic mixing parameter
                    'kap': '1e-4', #higgs mixing parameter
                    'mhsinput':mhS, #dark higgs mass
                    'mzdinput': '1.000000e+03' # Z' mass
                    }, 
        "HIGGS": { 'mhinput':mH}, #higgs mass
        #auto-calculate decay widths and BR of Zp, H, t, hs
        "DECAY": { 'wzp':'Auto', 'wh':'Auto', 'wt':'Auto', 'whs':'Auto'} 
        }
elif mH > 125:
    param_card_extras = { 
        "HIDDEN": { 'epsilon': '1e-10', #kinetic mixing parameter
                    'kap': '1e-4', #higgs mixing parameter
                    'mhsinput':mhS, #dark higgs mass
                    'mzdinput': '1.000000e+03' # Z' mass
                    }, 
        "HIGGS": { 'mhinput':mH}, #higgs mass
        #auto-calculate decay widths and BR of Zp, H, t, hs
        "DECAY": { 'wzp':'5', 'wh':'5', 'wt':'Auto', 'whs':'5'} 
        }


#---------------------------------------------------------------------------
# Change the param cards and gerenate
#---------------------------------------------------------------------------


process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_card_extras)
modify_param_card(process_dir=process_dir,params=param_card_extras)

generate(process_dir=process_dir,grid_pack=False,runArgs=runArgs)


#---------------------------------------------------------------------------
# Hacking the lhe
#---------------------------------------------------------------------------

unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
unzip1.wait()
oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')

# lifetime function

# initialise random number generator/sequence
import random
random.seed(runArgs.randomSeed)
def lifetime(avgtau = 21):
    import math
    t = random.random()
    return -1.0 * avgtau * math.log(t)

init = True
for line in oldlhe:
    if init==True:
        newlhe.write(line)
        if '</init>' in line:
            init = False
    else:
        if 'vent' in line:
            newlhe.write(line)
            continue

        newline=line.rstrip('\n')
        columns=newline.split()
        pdgid=columns[0]

        if pdgid == '35' and avgtau>0:
            part1 = line[:-22]
            part2 = "%.5e" % (lifetime(avgtau))
            part3 = line[-12:]
            newlhe.write(part1+part2+part3)
        else:
            newlhe.write(line)
oldlhe.close()
newlhe.close()

# re-zipping hacked LHE
zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
zip1.wait()
shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')

arrange_output(runArgs=runArgs, process_dir=process_dir, saveProcDir=False) 

#---------------------------------------------------------------------------
# Pythia now
#---------------------------------------------------------------------------

from MadGraphControl.MadGraphUtils import check_reset_proc_number
check_reset_proc_number(opts)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]

# Turn off checks for displaced vertices. 
# Other checks are fine.  
genSeq.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"] # Allow long-lived particles to decay
testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #In mm 
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000

evgenConfig.inputconfcheck=""

evgenConfig.description="Process: W(lnu)H with Higgs decaying to 2 LLPs giving 4b"
evgenConfig.keywords+=['exotic','BSMHiggs','longLived']
evgenConfig.contact = ['victoria.sanchez.sebastian@cern.ch', 'paolo.sabatini@cern.ch']
evgenConfig.process = "WH -> 2 LLPs -> 4f"
