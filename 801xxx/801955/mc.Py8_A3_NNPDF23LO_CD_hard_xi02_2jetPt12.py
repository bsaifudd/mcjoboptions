#Pythia8 minimum bias CD with A3

evgenConfig.description = "Central-diffractive events producing di-jets. Using the A3 NNPDF23LO tune and Rockefeller."
evgenConfig.keywords = ["QCD", "minBias","diffraction"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact  = [ "malewick@cern.ch" ]
# how many events should be produced in one production job
evgenConfig.nEventsPerJob = 200

include("Pythia8_i/Pythia8_Base_Fragment.py")

genSeq.Pythia8.Commands += [
    "Tune:ee = 7",
    "Tune:pp = 14",
    "MultipartonInteractions:bProfile = 2",
    "MultipartonInteractions:pT0Ref = 2.45",
    "MultipartonInteractions:ecmPow = 0.21",
    "MultipartonInteractions:coreRadius = 0.55",
    "MultipartonInteractions:coreFraction = 0.9",
    "Diffraction:sampleType = 2",
    "SigmaDiffractive:PomFlux = 5",
    "PDF:PomSet = 6",
    "SoftQCD:centralDiffractive = on",
    "SigmaTotal:zeroAXB = off"
    ]

evgenConfig.tune = "A3 NNPDF23LO"

# rel >=22
include("GeneratorFilters/xAODForwardProtonFilter_Common.py")

filtSeq.xAODForwardProtonFilter.xi_min = 0.01
filtSeq.xAODForwardProtonFilter.xi_max = 0.20
filtSeq.xAODForwardProtonFilter.beam_energy = 0.5*runArgs.ecmEnergy*GeV
filtSeq.xAODForwardProtonFilter.pt_min = 0.0*GeV
filtSeq.xAODForwardProtonFilter.pt_max = 3.5*GeV

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)

from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
if "QCDTruthMultiJetFilter" not in filtSeq:
    filtSeq += QCDTruthMultiJetFilter()

filtSeq.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.QCDTruthMultiJetFilter.NjetMinPt = 12.*GeV
filtSeq.QCDTruthMultiJetFilter.Njet = 2
filtSeq.QCDTruthMultiJetFilter.DoShape = False
filtSeq.QCDTruthMultiJetFilter.MaxEta = 4.

