# Evgen parameters
evgenConfig.description = 'DPS production of associated J/psi+W using Pythia'
evgenConfig.keywords += ['Jpsi', 'W', 'QCD', 'Charmonium']
evgenConfig.contact = ['C. D. Burton <burton@utexas.edu>']
evgenConfig.nEventsPerJob = 2000

# Running Pythia
include('Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py')
include('Pythia8B_i/Pythia8B_Charmonium_Common.py')

# Choose decay modes for J/psi and W
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
genSeq.Pythia8B.Commands += ['24:onMode = off']
genSeq.Pythia8B.Commands += ['24:6:onMode = on']
genSeq.Pythia8B.Commands += ['24:7:onMode = on']
genSeq.Pythia8B.Commands += ['SecondHard:generate = on']
genSeq.Pythia8B.Commands += ['SecondHard:SingleW = on']

# Filter the leptonic W decays
include('GeneratorFilters/OneLeptonFilter.py')
filtSeq.WZtoLeptonFilter.Ptcut_electron = 2e4
filtSeq.WZtoLeptonFilter.Ptcut_muon = 2e4

# Filter the di-muon J/psi decays
include('GeneratorFilters/DiLeptonMassFilter.py')
filtSeq.DiLeptonMassFilter.MinPt = 2000
filtSeq.DiLeptonMassFilter.MaxEta = 3
filtSeq.DiLeptonMassFilter.MinMass = 2600
filtSeq.DiLeptonMassFilter.MaxMass = 3600
filtSeq.DiLeptonMassFilter.MinDilepPt = 7000
filtSeq.DiLeptonMassFilter.AllowSameCharge = False
