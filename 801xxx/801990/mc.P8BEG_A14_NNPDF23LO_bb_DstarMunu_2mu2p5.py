##############################################################################
# Job options for Pythia8B_i generation of bb->muD*
##############################################################################
evgenConfig.description = "Signal bb->(D*munu)(D*munu)->(D0pimunu)(D0pimunu)"
evgenConfig.keywords = ["bottom","exclusive","muon"]
evgenConfig.contact = [ 'shion@mail.cern.ch' ]
evgenConfig.process = "pp->bb->(D*munu)(D*munu)->(D0pimunu)(D0pimunu)"
evgenConfig.nEventsPerJob = 2000

### Create EvtGen Decay File -> Generating the File ###
### ----------------------------------------------- ###
include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")

decayfile_str = "2022inclusive_BELLE.dec"
userDecayFileName = "B_DStarMuNu_D0Pi_KPiX_USER.dec"

genSeq.EvtInclusiveDecay.decayFile = decayfile_str
genSeq.EvtInclusiveDecay.userDecayFile = userDecayFileName
evgenConfig.auxfiles += [decayfile_str,userDecayFileName]

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
#
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
#
genSeq.Pythia8B.VetoDoubleBEvents = False
genSeq.Pythia8B.VetoDoubleCEvents = False

#
# Event selection
#
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.'] 
genSeq.Pythia8B.QuarkPtCut = 8.0
genSeq.Pythia8B.AntiQuarkPtCut = 8.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False

genSeq.Pythia8B.OutputLevel = INFO
genSeq.Pythia8B.NHadronizationLoops = 5

include("GeneratorFilters/MultiMuonFilter.py")

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
MultiMuonFilter = filtSeq.MultiMuonFilter
MultiMuonFilter.Ptcut = 2500.
MultiMuonFilter.Etacut = 2.7
MultiMuonFilter.NMuons = 2
