include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Pythia8 dijet events with prompt (fragmentation) photons in 800 < pT_ylead < 1000."
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.nEventsPerJob = 500
evgenConfig.contact = ["frank.siegert@cern.ch", "ana.cueto@cern.ch", "javier.llorente.merino@cern.ch"]

## Configure Pythia
genSeq.Pythia8.Commands += ["HardQCD:gg2gg = on",
                            "HardQCD:gg2qqbar = on",
                            "HardQCD:qg2qg = on",
                            "HardQCD:qq2qq = on",
                            "HardQCD:qqbar2gg = on",
                            "HardQCD:qqbar2qqbarNew = on",
                            "PhaseSpace:Bias2Selection=on", 
                            "PhaseSpace:pTHatMin = 400",
]

genSeq.Pythia8.Commands += [ "Enhancements:doEnhance = true",
                             "Enhancements:doEnhanceTrial = false"]

genSeq.Pythia8.Commands += [ "EnhancedSplittings:List = {isr:Q2QA=5.0, fsr:Q2QA=5.0}" ]

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 800000. ]
filtSeq.DirectPhotonFilter.Ptmax = [ 1000000. ]
filtSeq.DirectPhotonFilter.OrderPhotons = True
