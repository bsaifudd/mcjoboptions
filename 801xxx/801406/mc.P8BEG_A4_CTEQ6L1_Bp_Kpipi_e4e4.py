##############################################################
# Job options for B+ -> K+pi+pi- ee
##############################################################

evgenConfig.description   = "Exclusive B+ -> K+pi+pi- ee decay production"
evgenConfig.process       = "Bd -> Kpipi ee"
evgenConfig.keywords      = [ "bottom", "2electron", "exclusive" ]
evgenConfig.contact       = [ "ann-kathrin.perrevoort@cern.ch" ]
evgenConfig.nEventsPerJob = 100

# Create EvtGen decay rules
f = open("Bp_Kpipi_EE_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")

f.write("Decay B+\n")
f.write("1.0000   K+ pi+ pi- e+ e- PHOTOS PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("GeneratorFilters/BSignalFilter.py")
include("Pythia8B_i/Pythia8B_BPDGCodes.py") # list of B-species

genSeq.Pythia8B.Commands                 += [ 'HardQCD:all = on' ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands                 += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 7' ]
genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 7.5
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.NHadronizationLoops = 4
genSeq.Pythia8B.NDecayLoops         = 1

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [ 521 ]
genSeq.EvtInclusiveDecay.userDecayFile = "Bp_Kpipi_EE_USER.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn             = False
filtSeq.BSignalFilter.LVL2MuonCutOn             = False
# filtSeq.BSignalFilter.LVL2MuonCutEta            = 2.7
# filtSeq.BSignalFilter.LVL1MuonCutEta            = 2.7
# filtSeq.BSignalFilter.LVL1MuonCutPT             = 10000.0
# filtSeq.BSignalFilter.LVL2MuonCutPT             = 10000.0
if not hasattr( filtSeq, "MultiElectronFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
  filtSeq += MultiElectronFilter()
  pass
filtSeq.MultiElectronFilter.NElectrons = 2
filtSeq.MultiElectronFilter.Ptcut      = 4000.0
filtSeq.MultiElectronFilter.Etacut     = 2.7
filtSeq.BSignalFilter.B_PDGCode                 = 521
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 400.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.7

