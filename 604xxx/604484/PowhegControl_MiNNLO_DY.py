# Integration settings
PowhegConfig.ncall1 = 1000000
PowhegConfig.itmx1    = 5
PowhegConfig.ncall2 = 2000000
PowhegConfig.itmx2    = 5
PowhegConfig.foldcsi   = 2 # reduces negative event fraction to 5%, which seems sufficient
PowhegConfig.foldy     = 5 # increasing further slows down event generation too much (1min/event for 5 5 5)
PowhegConfig.foldphi   = 2
PowhegConfig.nubound = 3000000
PowhegConfig.icsimax = 3
PowhegConfig.iymax = 3
PowhegConfig.storemintupb = 0
PowhegConfig.check_bad_st2 = 1

# MiNNLOPS settings
PowhegConfig.minlo = 1         # (default 0) if 1, activate minlo
PowhegConfig.minnlo = 1        # (default 0) if 1, activate miNNlo
PowhegConfig.largeptscales = 1 # (default 0) if 0, at large pt, use muR=muF~Q in fixed order part, if 1 , at large pt , use muR = muF = pt -> SMALL HIGH PT IMPROVEMENT FOR 1
PowhegConfig.withdamp = 1      # ( default 1) if 1 , split real amplitude into a singular # ! and remnant contribution -> LITTLE/NO EFFECT
PowhegConfig.bornsuppfact = 1  # ( default 1) if 1 , the Born suppression factor is included, weighted events are generated.  -> 1 TO AVOID (HIGHER) BORNKTMIN GENERATION CUT
PowhegConfig.bornktmin = 0.26  # ( default 0.26) minimum transverse momentum -> KEEP IT LOW(?)
# Non-perturbative setting 
PowhegConfig.Q0=0.5 # value used in profiled scales, >Lambda_QCD (see 2006.04133 for more details) -> 0.5-1.0 seems to give best results for DY

# update PDF list to most useful for W,Z precision analysis
# nominal set
PowhegConfig.PDF = list(range(14000, 14058+1)) # CT18NNLO
# latest 'nominal' global fits & PDF4LHC21  with full error sets
PowhegConfig.PDF += list(range(14100, 14158+1)) # CT18ZNNLO (CMS choice)
PowhegConfig.PDF += list(range(304400, 304500+1)) # NNPDF31_nnlo_as_0118_hessian
PowhegConfig.PDF += list(range(331600, 331652+1)) # NNPDF40_nnlo_hessian_pdfas (central as=0.118)
PowhegConfig.PDF += list(range(27400, 27464+1)) # MSHT20nnlo_as118
PowhegConfig.PDF += list(range(93300, 93342+1)) #PDF4LHC21_40_pdfas
# ATLAS-specifics & ABMP16
PowhegConfig.PDF += list(range(65700, 65752+1)) # ATLASpdf21_T3
PowhegConfig.PDF += list(range(42560, 42589+1)) # ABMP16
# add a selection of special and historic PDF central values
PowhegConfig.PDF += [14200, 14300, 11200, 10800] # CT18ANNLO, CT18XNNLO, CT10nnlo, historic CT10 (NLO)
PowhegConfig.PDF += [325100, 260000, 303200, 90400] # NNPDF31_nnlo_as_0118_luxqed, NNPDF30_nlo_as_0118, NNPDF30_nnlo_as_0118_hessian, PDF4LHC15_nlo_30_pdfas
PowhegConfig.PDF += [29100, 29250] # MSHT20an3lo_as118, MSHT20an3lo_as118_Kcorr
PowhegConfig.PDF += [13000, 25300, 61200] # CT14, MMHT14, HERAPDF2.0
PowhegConfig.PDF += [21100, 21200, 10550] # MSTW2008nlo68cl, MSTW2008nnlo68cl, CTEQ6.6

def phMass(mass, width):
    return mass / (1.+(width/mass)**2)**0.5

def phWidth(mass, width):
    return width / (1.+(width/mass)**2)**0.5

def setMassAndWidth(mass_Z=91.1876, width_Z = 2.4952, mass_W = 80.399, width_W = 2.085, sin2theta=None):
    PowhegConfig.mass_Z = phMass(mass_Z, width_Z)
    PowhegConfig.width_Z = phWidth(mass_Z, width_Z)
    PowhegConfig.width_W = phWidth(mass_W, width_W)
    if sin2theta is None:
        PowhegConfig.mass_W = phMass(mass_W, width_W)
    else:
        PowhegConfig.mass_W = (1-sin2theta)**0.5*PowhegConfig.mass_Z
