#---------------------------------------------------------------------------------
# EVGEN configuration
#---------------------------------------------------------------------------------
evgenConfig.process     = "WmH->ZH Z->inc H->inc"
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet production: W-->all"
evgenConfig.keywords    = ["SM", "Higgs", "SMHiggs", "mH125"]
evgenConfig.inputFilesPerJob = 11
evgenConfig.nEventsPerJob    = 10000
evgenConfig.generators  = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.contact     = [ "binbin.dong@cern.ch","rongqian.qian@cern.ch" ]


#---------------------------------------------------------------------------------
# Pythia8 showing with the A14 NNPDF 2.3 tune
#---------------------------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']
else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

