#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG MiNNLO Z"
evgenConfig.keywords    = [ "SM", "Z", "0jet", "1jet", "2jet"]
evgenConfig.generators  = [ "Powheg" ]
evgenConfig.contact     = [ "hannes.mildner@cern.ch" ]
evgenConfig.nEventsPerJob = 500

include("PowhegControl/PowhegControl_Zj_MiNNLO_Common.py")
# Integration settings, MiNNLO settings, PDF settings included here 
include("PowhegControl_MiNNLO_DY.py")

# EW input settings
setMassAndWidth(sin2theta = 0.23113)

# decay channel and generation cuts
PowhegConfig.decay_mode = "z > tau+ tau-"
PowhegConfig.mass_Z_low = 60.

# run powheg
PowhegConfig.generate()
