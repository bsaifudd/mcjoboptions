# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# This is an example joboption to generate events with Powheg
# using ATLAS' interface. Users should optimise and carefully
# validate the settings before making an official sample request.
#--------------------------------------------------------------
import os
import re
from MadGraphControl.MadGraphUtils import *

job_option_name = get_physics_short()
print(job_option_name)
JO_name_list = job_option_name.split('_')
leptoquark_mass = float( JO_name_list[-3] )
leptoquark_coupling = float( JO_name_list[-2].replace("p",".") )
slice_bound_low = float( JO_name_list[-1].split("SL")[0] )
slice_bound_high = float( JO_name_list[-1].split("SL")[-1] )
# use S43, S13, S23, S53 to label LQs in physics short
leptoquark_charge = int( JO_name_list[3][1] ) 
input_order = JO_name_list[1]

if "lam11_" in job_option_name:
    generation = "1e"
elif "lam22_" in job_option_name:
    generation = "2m"
elif "lam33_" in job_option_name:
    generation = "3t"
elif "lam12_" in job_option_name:
    generation = "1m"
elif "lam21_" in job_option_name:
    generation = "2e"
elif "lam31_" in job_option_name:
    generation = "3e"
elif "lam32_" in job_option_name:
    generation = "3m"
else:
    raise RuntimeError("Cannot determine LQ generational couplings from job option name: {:s}.".format(job_option_name))
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'Drell-Yan NLO (i.e. including single production diagrams) production of scalar LQ model S43 with, generations: {0:s}, mLQ={1:d}'.format(
    generation, int(leptoquark_mass))
evgenConfig.keywords    += ['BSM', 'exotic', 'NLO', 'leptoquark', 'scalar']
evgenConfig.generators 	= ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.contact = ["daniel.buchin@cern.ch"]
evgenConfig.tune       	= "H7.2-Default"

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg Drell-Yan Scalar LeptoQuark process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_DY_SLQ_Common.py")

# --------------------------------------------------------------
# Relevant parameters for this process
# --------------------------------------------------------------
if slice_bound_low < 100:
    if (leptoquark_coupling > 2) and (leptoquark_mass < 3000):
        PowhegConfig.nEvents     *= 170. # more powheg events needed for filters
    elif leptoquark_coupling > 2:
        PowhegConfig.nEvents     *= 190.
    elif leptoquark_coupling > 0.5:
        PowhegConfig.nEvents     *= 200.
    else:
        PowhegConfig.nEvents     *= 230.
elif slice_bound_low < 150:
    PowhegConfig.nEvents     *= 50.
elif slice_bound_low < 200:
    PowhegConfig.nEvents     *= 30.
elif slice_bound_low < 400:
    PowhegConfig.nEvents     *= 20.
else:
    PowhegConfig.nEvents     *= 8.
# Yukawa couplings to down-type quarks (S1t RR)
PowhegConfig.YSD1x1 = leptoquark_coupling if "1e" in generation else 0
PowhegConfig.YSD2x1 = leptoquark_coupling if "2e" in generation else 0
PowhegConfig.YSD3x1 = leptoquark_coupling if "3e" in generation else 0
PowhegConfig.YSD1x2 = leptoquark_coupling if "1m" in generation else 0
PowhegConfig.YSD2x2 = leptoquark_coupling if "2m" in generation else 0
PowhegConfig.YSD3x2 = leptoquark_coupling if "3m" in generation else 0
PowhegConfig.YSD1x3 = leptoquark_coupling if "1t" in generation else 0
PowhegConfig.YSD2x3 = leptoquark_coupling if "2t" in generation else 0
PowhegConfig.YSD3x3 = leptoquark_coupling if "3t" in generation else 0

PowhegConfig.MSD = leptoquark_mass # mass

# Yukawa couplings to up-type quarks (S1 RR)
PowhegConfig.YSU1x1 = 0.
PowhegConfig.YSU1x2 = 0.
PowhegConfig.YSU1x3 = 0.
PowhegConfig.YSU2x1 = 0.
PowhegConfig.YSU2x2 = 0.
PowhegConfig.YSU2x3 = 0.
PowhegConfig.YSU3x1 = 0.
PowhegConfig.YSU3x2 = 0.
PowhegConfig.YSU3x3 = 0.
PowhegConfig.MSU = 2e3 # mass

# General Leptoquark (LQ) Parameters
PowhegConfig.bornonly = 0 # NOT Include NLO 
PowhegConfig.smartsig = 0
PowhegConfig.SM = 0 # Include SM contribution
PowhegConfig.LQ = 1 # Include basic LQ contributions
PowhegConfig.LQ_EW = 0 # Include LQ corrections to photon/Z couplings
PowhegConfig.LQ_Int = 1 # Include the interference between the SM and the LQ contributions
PowhegConfig.mass_t = 172.5 # top-quark (running) mass

# cut away the Zpeak as we cut that part anyway for SRs - what about VRs though?
PowhegConfig.mass_low = slice_bound_low # lower limit for dilepton mass
PowhegConfig.mass_high = slice_bound_high # upper limit for dilepton mass
#PowhegConfig.runningscale = 1
#PowhegConfig.new_damp = 1
#PowhegConfig.hnew_damp = 0.5
#PowhegConfig.hdamp = 1.0
if "e" in generation:
    PowhegConfig.decay_mode = "e+ e-"
elif "m" in generation:
    PowhegConfig.decay_mode = "mu+ mu-"
elif "t" in generation:
    PowhegConfig.decay_mode = "ta+ ta-"
else:
    raise RuntimeError("Cannot determine final state lepton from job option name: {:s}.".format(job_option_name))



# --------------------------------------------------------------
# Integration settings
# --------------------------------------------------------------
PowhegConfig.ncall1 		 = 80000
PowhegConfig.ncall2 		 = 100000
PowhegConfig.nubound 		 = 100000

PowhegConfig.xupbound        = 2

PowhegConfig.PDF	         = list(range(82400, 82501)) # use LUXlep for now, like in res. prod. samples

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------
# Filter
# --------------------------------------------------------------
from AthenaCommon.SystemOfUnits import GeV
include ( 'GeneratorFilters/xAODLeptonFilter_Common.py' )
filtSeq.xAODLeptonFilter.Ptcut  = 100*GeV
filtSeq.xAODLeptonFilter.Etacut = 2.8

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4, "WZ")
from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
filtSeq += QCDTruthJetFilter("QCDTruthJetFilter")
filtSeq.QCDTruthJetFilter.MinPt = 100*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 7000*GeV
filtSeq.QCDTruthJetFilter.MaxEta = 2.8
filtSeq.QCDTruthJetFilter.TruthJetContainer = "AntiKt4TruthWZJets"


# --------------------------------------------------------------
# Herwig PS
# --------------------------------------------------------------
include("Herwig7_i/Herwig72_LHEF.py")

pdf_order = "NLO"
me_order  = input_order

# configure Herwig7
Herwig7Config.add_commands("""
### We are dealing with lepton PDFs!
set /Herwig/Partons/RemnantDecayer:AllowLeptons Yes
set /Herwig/Partons/RemnantDecayer:AllowTop Yes

### "Not enough energy in both remnants in HwRemDecayer::setRemMasses()" can happen quite often in b-initiated processes (probably triggered by the "Ratio > GtobbbarSudakov:PDFmax" Warning)
set /Herwig/Generators/EventGenerator:MaxErrors 100000
""")

Herwig7Config.me_pdf_commands(order=pdf_order, name="LUXlep-NNPDF31_nlo_as_0118_luxqed")   #LUXlep-NNPDF31_nlo_as_0118_luxqed  #NNPDF23_lo_as_0130_qed (247000) #NNPDF30_nlo_as_0118 (260000)
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order=me_order, usepwhglhereader=True)

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.run()

testSeq.TestHepMC.MaxVtxDisp = 1500.0
