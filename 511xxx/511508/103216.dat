# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  13:51
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.68366583E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.05692984E+03  # scale for input parameters
    1   -1.35592260E+03  # M_1
    2   -1.07871782E+03  # M_2
    3    4.27483380E+03  # M_3
   11   -2.57465232E+03  # A_t
   12   -1.84388912E+03  # A_b
   13    7.44608254E+02  # A_tau
   23   -2.70536515E+02  # mu
   25    1.60577968E+01  # tan(beta)
   26    3.49385892E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.37161732E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.56816333E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.57735012E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.05692984E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.05692984E+03  # (SUSY scale)
  1  1     8.40061446E-06   # Y_u(Q)^DRbar
  2  2     4.26751214E-03   # Y_c(Q)^DRbar
  3  3     1.01486170E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.05692984E+03  # (SUSY scale)
  1  1     2.71067380E-04   # Y_d(Q)^DRbar
  2  2     5.15028023E-03   # Y_s(Q)^DRbar
  3  3     2.68813901E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.05692984E+03  # (SUSY scale)
  1  1     4.73032401E-05   # Y_e(Q)^DRbar
  2  2     9.78080979E-03   # Y_mu(Q)^DRbar
  3  3     1.64497130E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.05692984E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -2.57465302E+03   # A_t(Q)^DRbar
Block Ad Q=  4.05692984E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.84388889E+03   # A_b(Q)^DRbar
Block Ae Q=  4.05692984E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     7.44608189E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.05692984E+03  # soft SUSY breaking masses at Q
   1   -1.35592260E+03  # M_1
   2   -1.07871782E+03  # M_2
   3    4.27483380E+03  # M_3
  21    1.20725396E+07  # M^2_(H,d)
  22    3.11036148E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.37161732E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.56816333E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.57735012E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22891783E+02  # h0
        35     3.49346464E+03  # H0
        36     3.49385892E+03  # A0
        37     3.49727650E+03  # H+
   1000001     1.01011354E+04  # ~d_L
   2000001     1.00769049E+04  # ~d_R
   1000002     1.01007713E+04  # ~u_L
   2000002     1.00795536E+04  # ~u_R
   1000003     1.01011360E+04  # ~s_L
   2000003     1.00769056E+04  # ~s_R
   1000004     1.01007720E+04  # ~c_L
   2000004     1.00795542E+04  # ~c_R
   1000005     4.48675452E+03  # ~b_1
   2000005     4.70245497E+03  # ~b_2
   1000006     3.66493571E+03  # ~t_1
   2000006     4.49085087E+03  # ~t_2
   1000011     1.00207576E+04  # ~e_L-
   2000011     1.00090149E+04  # ~e_R-
   1000012     1.00199924E+04  # ~nu_eL
   1000013     1.00207594E+04  # ~mu_L-
   2000013     1.00090183E+04  # ~mu_R-
   1000014     1.00199941E+04  # ~nu_muL
   1000015     1.00099731E+04  # ~tau_1-
   2000015     1.00212762E+04  # ~tau_2-
   1000016     1.00204933E+04  # ~nu_tauL
   1000021     4.76922059E+03  # ~g
   1000022     2.82117822E+02  # ~chi_10
   1000023     2.89530414E+02  # ~chi_20
   1000025     1.16679698E+03  # ~chi_30
   1000035     1.36562151E+03  # ~chi_40
   1000024     2.85157797E+02  # ~chi_1+
   1000037     1.16588702E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -5.94402229E-02   # alpha
Block Hmix Q=  4.05692984E+03  # Higgs mixing parameters
   1   -2.70536515E+02  # mu
   2    1.60577968E+01  # tan[beta](Q)
   3    2.42944767E+02  # v(Q)
   4    1.22070502E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     5.63572769E-02   # Re[R_st(1,1)]
   1  2     9.98410666E-01   # Re[R_st(1,2)]
   2  1    -9.98410666E-01   # Re[R_st(2,1)]
   2  2     5.63572769E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99998309E-01   # Re[R_sb(1,1)]
   1  2     1.83879636E-03   # Re[R_sb(1,2)]
   2  1    -1.83879636E-03   # Re[R_sb(2,1)]
   2  2    -9.99998309E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -3.98453250E-02   # Re[R_sta(1,1)]
   1  2     9.99205860E-01   # Re[R_sta(1,2)]
   2  1    -9.99205860E-01   # Re[R_sta(2,1)]
   2  2    -3.98453250E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -3.06546938E-02   # Re[N(1,1)]
   1  2     6.76698164E-02   # Re[N(1,2)]
   1  3     7.10600327E-01   # Re[N(1,3)]
   1  4    -6.99662963E-01   # Re[N(1,4)]
   2  1     1.79847838E-02   # Re[N(2,1)]
   2  2    -3.69716756E-02   # Re[N(2,2)]
   2  3     7.03186795E-01   # Re[N(2,3)]
   2  4     7.09815451E-01   # Re[N(2,4)]
   3  1     1.55988816E-02   # Re[N(3,1)]
   3  2     9.96940069E-01   # Re[N(3,2)]
   3  3    -2.20339792E-02   # Re[N(3,3)]
   3  4     7.33599211E-02   # Re[N(3,4)]
   4  1     9.99246472E-01   # Re[N(4,1)]
   4  2    -1.28214864E-02   # Re[N(4,2)]
   4  3     9.48742742E-03   # Re[N(4,3)]
   4  4    -3.53848275E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -3.15019819E-02   # Re[U(1,1)]
   1  2    -9.99503689E-01   # Re[U(1,2)]
   2  1    -9.99503689E-01   # Re[U(2,1)]
   2  2     3.15019819E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.04928474E-01   # Re[V(1,1)]
   1  2     9.94479771E-01   # Re[V(1,2)]
   2  1     9.94479771E-01   # Re[V(2,1)]
   2  2    -1.04928474E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.84390122E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.74106051E-04    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.35266194E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.45811804E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     9.98444807E-01    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.40896416E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.22317702E-04    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.34010376E-04    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.07833129E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     8.18446744E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.20305834E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.08645563E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.84770642E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.17144264E-03    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     5.28974849E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.45804597E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     9.97661478E-01    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     3.91920914E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.40915483E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.90436594E-04    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.00786460E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.07791600E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     8.18336283E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.20222017E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.08563327E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.93437206E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.78089070E-02    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     4.44229372E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     9.64048195E-04    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     8.13976332E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     9.13443837E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.48339236E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.46149629E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.89004462E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.87218636E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.96508886E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     7.93538971E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.81908157E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.86232999E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.40902006E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     2.20749930E-03    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     6.77981504E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.97674779E-01    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     9.00886083E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.88123711E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02469895E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.40921069E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     2.20720105E-03    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     6.77889904E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.97634564E-01    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     9.00764379E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     7.01527281E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02388634E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.46294891E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.12623063E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     6.53021749E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.86716681E-01    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     8.67723279E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.34042078E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.80327531E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.84399206E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.11745608E-02    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.88807981E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.12674552E+02   # ~d_L
#    BR                NDA      ID1      ID2
     3.87484135E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.16941915E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     6.98217292E-02    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     2.54506795E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.43799321E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.41153976E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.85831002E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.84409866E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.11743166E-02    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.88786367E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.12683581E+02   # ~s_L
#    BR                NDA      ID1      ID2
     3.91852706E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.21223147E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     6.98207085E-02    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     2.54503134E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.49694835E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.41151969E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.85819521E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.56424665E+02   # ~b_1
#    BR                NDA      ID1      ID2
     2.13435812E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.04127527E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.08483730E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.78423305E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.74966873E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.25687614E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     4.53212159E-02    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     1.56040672E+01   # ~b_2
#    BR                NDA      ID1      ID2
     2.17182272E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.12743228E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.78757863E-04    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.40894707E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.28372132E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.94997606E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
DECAY   2000002     5.00940685E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.32344808E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.56698207E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.12656234E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.80412737E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     7.06036820E-02    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.90680044E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.59540719E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.39737415E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.85793667E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.00948006E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.32338573E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.56684287E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.12665241E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.83328575E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     7.06026746E-02    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.90677996E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.60403710E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.39735379E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.85782181E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.53446557E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.34097149E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.41414781E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.88282624E-03    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.96501340E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.73033261E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     7.91558720E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.58085480E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.84361240E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.91326717E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.08022435E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.10241389E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.93167779E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.10581722E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.25688922E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.07198012E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     7.83921898E-10   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     4.64890723E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     1.98931716E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.54964994E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.54036164E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     2.71764021E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.07188156E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.46183201E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.49965102E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.46883157E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.41914651E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.26617990E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     4.53987128E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     5.24674630E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.25261257E-07   # chi^0_2
#    BR                NDA      ID1      ID2
     5.00078016E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     7.08233676E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     4.76345130E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     9.08096842E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     9.06362404E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     2.04931695E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.04315741E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     8.46041517E-03    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.21438725E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     5.87969131E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     5.87969131E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     3.91789220E-03    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     3.91789220E-03    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     1.95989737E-03    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     1.95989737E-03    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     1.95419494E-03    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     1.95419494E-03    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     8.85471631E-04    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     8.85471631E-04    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     1.18368754E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.33391294E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.33391294E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.89586857E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.73983958E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.62603114E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.33578571E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.04285679E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.92275412E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     5.04060893E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     5.04060893E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     4.14864816E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.17446794E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.17446794E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.08176055E-03    2     1000037       -24   # BR(chi^0_4 -> chi^+_2 W^-)
     4.08176055E-03    2    -1000037        24   # BR(chi^0_4 -> chi^-_2 W^+)
     5.98225513E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.84959956E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.60638097E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.31817385E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     3.06105874E-03    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.05628946E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.92592488E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     5.31239223E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.31239223E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.65150719E+01   # ~g
#    BR                NDA      ID1      ID2
     4.33351339E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     4.33351339E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.90029876E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.90029876E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     3.51708822E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.51708822E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     2.05326267E-03    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     2.05326267E-03    2    -2000005         5   # BR(~g -> ~b^*_2 b)
     2.37421454E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     2.61570733E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.36078890E-03   # Gamma(h0)
     2.52637862E-03   2        22        22   # BR(h0 -> photon photon)
     1.42495381E-03   2        22        23   # BR(h0 -> photon Z)
     2.44010755E-02   2        23        23   # BR(h0 -> Z Z)
     2.10299515E-01   2       -24        24   # BR(h0 -> W W)
     7.97385680E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.24055853E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.33108222E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.72106855E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.49360814E-07   2        -2         2   # BR(h0 -> Up up)
     2.89856625E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.08448834E-07   2        -1         1   # BR(h0 -> Down down)
     2.20061361E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.84959229E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.97839683E+01   # Gamma(HH)
     5.17129145E-08   2        22        22   # BR(HH -> photon photon)
     8.27715983E-08   2        22        23   # BR(HH -> photon Z)
     2.03195267E-06   2        23        23   # BR(HH -> Z Z)
     5.04985448E-07   2       -24        24   # BR(HH -> W W)
     5.78265447E-07   2        21        21   # BR(HH -> gluon gluon)
     2.83685658E-09   2       -11        11   # BR(HH -> Electron electron)
     1.26316792E-04   2       -13        13   # BR(HH -> Muon muon)
     3.64827094E-02   2       -15        15   # BR(HH -> Tau tau)
     5.83345456E-13   2        -2         2   # BR(HH -> Up up)
     1.13167965E-07   2        -4         4   # BR(HH -> Charm charm)
     8.61423185E-03   2        -6         6   # BR(HH -> Top top)
     2.06094348E-07   2        -1         1   # BR(HH -> Down down)
     7.45450417E-05   2        -3         3   # BR(HH -> Strange strange)
     1.96998629E-01   2        -5         5   # BR(HH -> Bottom bottom)
     6.79982088E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.29562515E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.29562515E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.06873818E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.94972011E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.11317654E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     9.53101480E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.50696162E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     7.38425647E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.32140953E-01   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     3.59021249E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.01219140E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.06729374E-05   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.88111166E-06   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.21933556E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.93223398E+01   # Gamma(A0)
     9.55890700E-08   2        22        22   # BR(A0 -> photon photon)
     1.20742488E-07   2        22        23   # BR(A0 -> photon Z)
     9.95060833E-06   2        21        21   # BR(A0 -> gluon gluon)
     2.75589806E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.22712063E-04   2       -13        13   # BR(A0 -> Muon muon)
     3.54416357E-02   2       -15        15   # BR(A0 -> Tau tau)
     5.42104999E-13   2        -2         2   # BR(A0 -> Up up)
     1.05163658E-07   2        -4         4   # BR(A0 -> Charm charm)
     8.08817130E-03   2        -6         6   # BR(A0 -> Top top)
     2.00210244E-07   2        -1         1   # BR(A0 -> Down down)
     7.24166414E-05   2        -3         3   # BR(A0 -> Strange strange)
     1.91374498E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.55377012E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.31128538E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.31128538E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     7.55383744E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     2.54558100E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.30119318E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.34830060E-01   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.67384103E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     6.00667030E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     9.42605282E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.46935005E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     3.67483773E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.41363852E-04   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.23697975E-05   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     3.77726498E-06   2        23        25   # BR(A0 -> Z h0)
     2.10756598E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     5.09264384E+01   # Gamma(Hp)
     3.21928278E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.37634393E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     3.89308401E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     2.03276970E-07   2        -1         2   # BR(Hp -> Down up)
     3.43716977E-06   2        -3         2   # BR(Hp -> Strange up)
     2.22512399E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.51123162E-08   2        -1         4   # BR(Hp -> Down charm)
     7.33774972E-05   2        -3         4   # BR(Hp -> Strange charm)
     3.11597184E-04   2        -5         4   # BR(Hp -> Bottom charm)
     6.14051018E-07   2        -1         6   # BR(Hp -> Down top)
     1.34971896E-05   2        -3         6   # BR(Hp -> Strange top)
     2.19495719E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.45323327E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.30872149E-01   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.14099715E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.23935600E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.29447133E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     4.03155174E-08   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     5.53229681E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.62954939E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     3.67075012E-06   2        24        25   # BR(Hp -> W h0)
     2.15166296E-11   2        24        35   # BR(Hp -> W HH)
     1.24636646E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.13486756E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.57939351E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.57852838E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00033551E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.54266706E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.87818109E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.13486756E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.57939351E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.57852838E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99992413E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    7.58726834E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99992413E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    7.58726834E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27124252E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.12619222E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    9.79606996E-02        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    7.58726834E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99992413E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.27735485E-04   # BR(b -> s gamma)
    2    1.58921946E-06   # BR(b -> s mu+ mu-)
    3    3.52496364E-05   # BR(b -> s nu nu)
    4    2.42068917E-15   # BR(Bd -> e+ e-)
    5    1.03409132E-10   # BR(Bd -> mu+ mu-)
    6    2.16499802E-08   # BR(Bd -> tau+ tau-)
    7    8.17172332E-14   # BR(Bs -> e+ e-)
    8    3.49095889E-09   # BR(Bs -> mu+ mu-)
    9    7.40539449E-07   # BR(Bs -> tau+ tau-)
   10    9.66650830E-05   # BR(B_u -> tau nu)
   11    9.98513299E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42023917E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93661719E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15742916E-03   # epsilon_K
   17    2.28165936E-15   # Delta(M_K)
   18    2.47980695E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28998583E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.47310193E-16   # Delta(g-2)_electron/2
   21    6.29797686E-12   # Delta(g-2)_muon/2
   22    1.78208958E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    3.07031032E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.97001000E-01   # C7
     0305 4322   00   2    -1.26260915E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.03337438E-01   # C8
     0305 6321   00   2    -1.43001608E-04   # C8'
 03051111 4133   00   0     1.62680352E+00   # C9 e+e-
 03051111 4133   00   2     1.62702313E+00   # C9 e+e-
 03051111 4233   00   2     5.80787939E-05   # C9' e+e-
 03051111 4137   00   0    -4.44949562E+00   # C10 e+e-
 03051111 4137   00   2    -4.44754270E+00   # C10 e+e-
 03051111 4237   00   2    -4.29490312E-04   # C10' e+e-
 03051313 4133   00   0     1.62680352E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62702310E+00   # C9 mu+mu-
 03051313 4233   00   2     5.80787855E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44949562E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44754273E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.29490315E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50492895E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     9.28692234E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50492895E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     9.28692246E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50492895E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     9.28695630E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85819896E-07   # C7
     0305 4422   00   2     2.40277885E-06   # C7
     0305 4322   00   2    -3.83630648E-07   # C7'
     0305 6421   00   0     3.30480256E-07   # C8
     0305 6421   00   2     2.55084916E-06   # C8
     0305 6321   00   2    -1.16354718E-07   # C8'
 03051111 4133   00   2     4.31072274E-08   # C9 e+e-
 03051111 4233   00   2     1.13980294E-06   # C9' e+e-
 03051111 4137   00   2     1.56526651E-06   # C10 e+e-
 03051111 4237   00   2    -8.42965642E-06   # C10' e+e-
 03051313 4133   00   2     4.31067945E-08   # C9 mu+mu-
 03051313 4233   00   2     1.13980289E-06   # C9' mu+mu-
 03051313 4137   00   2     1.56526699E-06   # C10 mu+mu-
 03051313 4237   00   2    -8.42965657E-06   # C10' mu+mu-
 03051212 4137   00   2    -3.18460672E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.82275571E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -3.18460652E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.82275571E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -3.18455116E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.82275571E-06   # C11' nu_3 nu_3
