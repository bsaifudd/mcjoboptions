# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  14:57
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.73904826E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.73897314E+03  # scale for input parameters
    1   -1.94957367E+02  # M_1
    2    2.00237584E+02  # M_2
    3    4.11157224E+03  # M_3
   11    6.88765450E+02  # A_t
   12   -1.30668938E+03  # A_b
   13   -1.24335591E+03  # A_tau
   23   -3.75971346E+02  # mu
   25    5.76569490E+01  # tan(beta)
   26    2.93139712E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.65362665E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.98493578E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.10626546E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.73897314E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.73897314E+03  # (SUSY scale)
  1  1     8.38563310E-06   # Y_u(Q)^DRbar
  2  2     4.25990162E-03   # Y_c(Q)^DRbar
  3  3     1.01305183E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.73897314E+03  # (SUSY scale)
  1  1     9.71555829E-04   # Y_d(Q)^DRbar
  2  2     1.84595607E-02   # Y_s(Q)^DRbar
  3  3     9.63478941E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.73897314E+03  # (SUSY scale)
  1  1     1.69543597E-04   # Y_e(Q)^DRbar
  2  2     3.50562386E-02   # Y_mu(Q)^DRbar
  3  3     5.89588263E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.73897314E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     6.88765459E+02   # A_t(Q)^DRbar
Block Ad Q=  3.73897314E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.30668952E+03   # A_b(Q)^DRbar
Block Ae Q=  3.73897314E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.24335585E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.73897314E+03  # soft SUSY breaking masses at Q
   1   -1.94957367E+02  # M_1
   2    2.00237584E+02  # M_2
   3    4.11157224E+03  # M_3
  21    8.58828048E+06  # M^2_(H,d)
  22    1.15202194E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.65362665E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.98493578E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.10626546E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23398578E+02  # h0
        35     2.92700357E+03  # H0
        36     2.93139712E+03  # A0
        37     2.92563870E+03  # H+
   1000001     1.00982270E+04  # ~d_L
   2000001     1.00732409E+04  # ~d_R
   1000002     1.00978520E+04  # ~u_L
   2000002     1.00763984E+04  # ~u_R
   1000003     1.00982347E+04  # ~s_L
   2000003     1.00732548E+04  # ~s_R
   1000004     1.00978595E+04  # ~c_L
   2000004     1.00763993E+04  # ~c_R
   1000005     2.76656458E+03  # ~b_1
   2000005     3.19579802E+03  # ~b_2
   1000006     2.76920454E+03  # ~t_1
   2000006     5.04835231E+03  # ~t_2
   1000011     1.00216114E+04  # ~e_L-
   2000011     1.00088531E+04  # ~e_R-
   1000012     1.00208335E+04  # ~nu_eL
   1000013     1.00216490E+04  # ~mu_L-
   2000013     1.00089234E+04  # ~mu_R-
   1000014     1.00208700E+04  # ~nu_muL
   1000015     1.00284614E+04  # ~tau_1-
   2000015     1.00330204E+04  # ~tau_2-
   1000016     1.00313608E+04  # ~nu_tauL
   1000021     4.56701444E+03  # ~g
   1000022     1.92828554E+02  # ~chi_10
   1000023     2.09992135E+02  # ~chi_20
   1000025     3.93504712E+02  # ~chi_30
   1000035     4.02605424E+02  # ~chi_40
   1000024     2.10465474E+02  # ~chi_1+
   1000037     4.06828071E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.74656710E-02   # alpha
Block Hmix Q=  3.73897314E+03  # Higgs mixing parameters
   1   -3.75971346E+02  # mu
   2    5.76569490E+01  # tan[beta](Q)
   3    2.43027613E+02  # v(Q)
   4    8.59308908E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99991060E-01   # Re[R_st(1,1)]
   1  2     4.22840705E-03   # Re[R_st(1,2)]
   2  1    -4.22840705E-03   # Re[R_st(2,1)]
   2  2    -9.99991060E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99817230E-01   # Re[R_sb(1,1)]
   1  2     1.91182082E-02   # Re[R_sb(1,2)]
   2  1    -1.91182082E-02   # Re[R_sb(2,1)]
   2  2    -9.99817230E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -4.45730301E-01   # Re[R_sta(1,1)]
   1  2     8.95167302E-01   # Re[R_sta(1,2)]
   2  1    -8.95167302E-01   # Re[R_sta(2,1)]
   2  2    -4.45730301E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.85872581E-01   # Re[N(1,1)]
   1  2     1.57223964E-02   # Re[N(1,2)]
   1  3    -1.48116082E-01   # Re[N(1,3)]
   1  4     7.66138742E-02   # Re[N(1,4)]
   2  1     1.55214776E-02   # Re[N(2,1)]
   2  2     9.47805376E-01   # Re[N(2,2)]
   2  3     2.81225991E-01   # Re[N(2,3)]
   2  4     1.49452316E-01   # Re[N(2,4)]
   3  1     1.59223614E-01   # Re[N(3,1)]
   3  2    -9.50187146E-02   # Re[N(3,2)]
   3  3     6.85574401E-01   # Re[N(3,3)]
   3  4    -7.03993626E-01   # Re[N(3,4)]
   4  1    -4.96203359E-02   # Re[N(4,1)]
   4  2     3.03955950E-01   # Re[N(4,2)]
   4  3    -6.54951378E-01   # Re[N(4,3)]
   4  4    -6.90063254E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.20806765E-01   # Re[U(1,1)]
   1  2    -3.90019103E-01   # Re[U(1,2)]
   2  1     3.90019103E-01   # Re[U(2,1)]
   2  2    -9.20806765E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.78313762E-01   # Re[V(1,1)]
   1  2     2.07128422E-01   # Re[V(1,2)]
   2  1     2.07128422E-01   # Re[V(2,1)]
   2  2     9.78313762E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02502138E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.72008403E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.40907778E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.52943272E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     2.45624879E-03    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44753722E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.95822831E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.78313125E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.33814107E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     5.16291296E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     9.24017408E-02    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.07385377E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.62765987E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.19681471E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.73108281E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.49473147E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     7.32989590E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     4.07578231E-03    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44998564E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.94683935E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.77977742E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     8.21855501E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.40645161E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     5.15421432E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     9.22460610E-02    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.93775672E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.40761692E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.29815056E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.44928547E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.07858910E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.35887493E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.40748301E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     2.08971849E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.76244631E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.25107665E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.82818926E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.00679574E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.89668140E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.14101232E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44759014E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.94874154E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.68711289E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     9.85027489E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.31670562E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.82726117E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     2.60578473E-02    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.45003819E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.93535086E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.68258611E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     9.83368101E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.31111827E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.82001233E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     2.74417837E-02    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     2.14081334E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     5.38049050E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.81890297E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     6.66766825E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.24508450E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.43697626E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.91488659E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.07451561E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.07636650E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.80282368E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.88925625E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.38956202E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.77585765E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.20730889E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.05572130E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.78445395E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.17860103E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     2.10944422E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.89356333E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.07588781E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.07637115E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     3.43071480E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     1.13697193E-04    2    -1000037         4   # BR(~s_R -> chi^-_2 c)
     9.88661058E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.39028789E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.77799936E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.20745284E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.10566041E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.82936686E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.17847043E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     2.10975116E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.89267890E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.39348581E+02   # ~b_1
#    BR                NDA      ID1      ID2
     1.06944443E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.07593686E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.65882455E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.57426740E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     1.64767932E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     3.93634742E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.16383056E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.51249274E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.73240734E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.32676943E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.12700693E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     7.13474064E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.18548440E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.02141775E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     5.11615142E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     7.44484979E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.24636326E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.16574211E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.08407228E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     1.05269039E-04    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.57142913E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.38938465E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.54329354E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.28007773E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     3.08213376E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     6.03932235E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.33039985E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     5.94962791E-03    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.89318781E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.24643640E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.16568835E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.08748183E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     1.08559226E-04    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.57129685E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.39010996E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.54304002E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.27938196E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     3.10996780E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     6.04135196E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.33041222E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     6.03923928E-03    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.89230330E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.44965558E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.37680249E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     5.28479887E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.39399977E-02    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     8.23559600E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.83960425E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.85128141E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.11742287E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.93628342E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.58324493E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.28251349E+02   # ~t_2
#    BR                NDA      ID1      ID2
     5.03794169E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.02903979E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.21832588E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.11568268E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.98166968E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.26102904E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.35095258E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     2.34325461E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.16847357E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.98793741E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.84499855E-08   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.38089282E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.29696752E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.12518636E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.12505065E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.07186774E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.92563834E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.61415897E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.34747808E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.99424963E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.04389608E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.50260742E-08   # chi^0_2
#    BR                NDA      ID1      ID2
     1.93173446E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.05217496E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     9.78600542E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.34893521E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.34846111E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     6.71177301E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.03869348E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.03700839E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.60224230E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.80112199E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     3.21579528E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.67488805E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.67488805E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     7.77777552E-03    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.62285131E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     4.13597760E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.53486471E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     1.12232080E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     2.20991486E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     3.02094303E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     3.02094303E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.23665992E-01    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     3.14080546E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     7.11362785E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.69567723E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000021     1.68060772E+02   # ~g
#    BR                NDA      ID1      ID2
     7.26759047E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     7.26759047E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.60550037E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.60550037E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.61190544E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.61190544E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.04733950E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.04733950E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     3.69841924E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     3.57223917E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     3.60143152E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     3.60143152E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.24850873E-03   # Gamma(h0)
     2.64747811E-03   2        22        22   # BR(h0 -> photon photon)
     1.54534872E-03   2        22        23   # BR(h0 -> photon Z)
     2.69554592E-02   2        23        23   # BR(h0 -> Z Z)
     2.29941426E-01   2       -24        24   # BR(h0 -> W W)
     8.35678673E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.04467568E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.24395325E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.46990944E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.43498619E-07   2        -2         2   # BR(h0 -> Up up)
     2.78501552E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.85281109E-07   2        -1         1   # BR(h0 -> Down down)
     2.11681877E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.62356360E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.56462703E+02   # Gamma(HH)
     4.84547244E-08   2        22        22   # BR(HH -> photon photon)
     1.12318257E-07   2        22        23   # BR(HH -> photon Z)
     6.01956096E-08   2        23        23   # BR(HH -> Z Z)
     1.87769481E-08   2       -24        24   # BR(HH -> W W)
     1.01748204E-05   2        21        21   # BR(HH -> gluon gluon)
     7.95307209E-09   2       -11        11   # BR(HH -> Electron electron)
     3.54107613E-04   2       -13        13   # BR(HH -> Muon muon)
     1.02270115E-01   2       -15        15   # BR(HH -> Tau tau)
     1.31817253E-14   2        -2         2   # BR(HH -> Up up)
     2.55696258E-09   2        -4         4   # BR(HH -> Charm charm)
     2.00240644E-04   2        -6         6   # BR(HH -> Top top)
     6.02491046E-07   2        -1         1   # BR(HH -> Down down)
     2.17919582E-04   2        -3         3   # BR(HH -> Strange strange)
     6.56199211E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.03780149E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     6.09859292E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     6.09859292E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     4.23506542E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     9.82579800E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.87691158E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.56449069E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.19504773E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     8.49219191E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     7.22006000E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     6.62853907E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     2.10375167E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     8.13139261E-03   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     5.93107054E-03   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     3.12565076E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.43335000E+02   # Gamma(A0)
     6.32817475E-10   2        22        22   # BR(A0 -> photon photon)
     2.97264520E-08   2        22        23   # BR(A0 -> photon Z)
     1.53634209E-05   2        21        21   # BR(A0 -> gluon gluon)
     7.71770191E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.43627144E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.92434274E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.25799568E-14   2        -2         2   # BR(A0 -> Up up)
     2.44010271E-09   2        -4         4   # BR(A0 -> Charm charm)
     1.94374538E-04   2        -6         6   # BR(A0 -> Top top)
     5.84558358E-07   2        -1         1   # BR(A0 -> Down down)
     2.11433493E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.36763557E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.19385100E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     6.70167872E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     6.70167872E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     4.30059123E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.05573379E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     6.32606378E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.93466368E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.29752638E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     9.79623087E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     8.81238019E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     6.38584774E-03   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.65679207E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     8.41720854E-03   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     6.49765656E-03   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.05474018E-07   2        23        25   # BR(A0 -> Z h0)
     1.25424381E-11   2        23        35   # BR(A0 -> Z HH)
     5.74051672E-42   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.77696063E+02   # Gamma(Hp)
     8.96774611E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.83399155E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.08447079E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.86760425E-07   2        -1         2   # BR(Hp -> Down up)
     9.90152263E-06   2        -3         2   # BR(Hp -> Strange up)
     7.18335220E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.75123650E-08   2        -1         4   # BR(Hp -> Down charm)
     2.11463159E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.00592630E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.29296735E-08   2        -1         6   # BR(Hp -> Down top)
     5.94808991E-07   2        -3         6   # BR(Hp -> Strange top)
     6.78348477E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.22241739E-06   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.87546743E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.05085281E-02   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.17189000E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     5.25653590E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     7.65920041E-03   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     5.96103754E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     7.65995910E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     8.46306712E-08   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.01428575E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.32430948E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.32432377E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99995703E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.05110400E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.00813058E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.01428575E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.32430948E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.32432377E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999985E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.52390660E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999985E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.52390660E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26475017E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.91135631E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.60809935E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.52390660E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999985E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.29367587E-04   # BR(b -> s gamma)
    2    1.58906981E-06   # BR(b -> s mu+ mu-)
    3    3.52475877E-05   # BR(b -> s nu nu)
    4    2.23833117E-15   # BR(Bd -> e+ e-)
    5    9.56194578E-11   # BR(Bd -> mu+ mu-)
    6    2.00452238E-08   # BR(Bd -> tau+ tau-)
    7    7.82910499E-14   # BR(Bs -> e+ e-)
    8    3.34460456E-09   # BR(Bs -> mu+ mu-)
    9    7.10197979E-07   # BR(Bs -> tau+ tau-)
   10    9.48333128E-05   # BR(B_u -> tau nu)
   11    9.79591815E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41787616E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93459909E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15675086E-03   # epsilon_K
   17    2.28165492E-15   # Delta(M_K)
   18    2.47967626E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28974436E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.98406804E-16   # Delta(g-2)_electron/2
   21   -1.27581237E-11   # Delta(g-2)_muon/2
   22   -3.63178802E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.89338663E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.98897001E-01   # C7
     0305 4322   00   2    -1.70040964E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.04435021E-01   # C8
     0305 6321   00   2    -1.73645817E-04   # C8'
 03051111 4133   00   0     1.62556805E+00   # C9 e+e-
 03051111 4133   00   2     1.62583674E+00   # C9 e+e-
 03051111 4233   00   2     7.90583489E-04   # C9' e+e-
 03051111 4137   00   0    -4.44826016E+00   # C10 e+e-
 03051111 4137   00   2    -4.44631618E+00   # C10 e+e-
 03051111 4237   00   2    -5.85937859E-03   # C10' e+e-
 03051313 4133   00   0     1.62556805E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62583656E+00   # C9 mu+mu-
 03051313 4233   00   2     7.90581663E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44826016E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44631636E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.85937867E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50493167E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.26733671E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50493167E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.26733700E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50493168E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.26741902E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85820152E-07   # C7
     0305 4422   00   2    -1.45169049E-05   # C7
     0305 4322   00   2    -5.74668299E-07   # C7'
     0305 6421   00   0     3.30480476E-07   # C8
     0305 6421   00   2     8.48762794E-06   # C8
     0305 6321   00   2     6.70910681E-08   # C8'
 03051111 4133   00   2     1.52379993E-06   # C9 e+e-
 03051111 4233   00   2     1.56590612E-05   # C9' e+e-
 03051111 4137   00   2     9.54275550E-09   # C10 e+e-
 03051111 4237   00   2    -1.16064214E-04   # C10' e+e-
 03051313 4133   00   2     1.52379014E-06   # C9 mu+mu-
 03051313 4233   00   2     1.56590527E-05   # C9' mu+mu-
 03051313 4137   00   2     9.55316397E-09   # C10 mu+mu-
 03051313 4237   00   2    -1.16064239E-04   # C10' mu+mu-
 03051212 4137   00   2     3.39916222E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.51037623E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     3.39921487E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.51037623E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     3.41376021E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.51037622E-05   # C11' nu_3 nu_3
