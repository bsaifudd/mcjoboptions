# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  17:31
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.12765803E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.40312063E+03  # scale for input parameters
    1   -1.11321434E+02  # M_1
    2    1.08264432E+02  # M_2
    3    3.17684940E+03  # M_3
   11   -7.61270610E+03  # A_t
   12    1.62527499E+03  # A_b
   13   -8.90814898E+02  # A_tau
   23    1.50709717E+03  # mu
   25    5.05599617E+01  # tan(beta)
   26    1.75651591E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.39071127E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.72905470E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.18869657E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.40312063E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.40312063E+03  # (SUSY scale)
  1  1     8.38601191E-06   # Y_u(Q)^DRbar
  2  2     4.26009405E-03   # Y_c(Q)^DRbar
  3  3     1.01309760E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.40312063E+03  # (SUSY scale)
  1  1     8.52005618E-04   # Y_d(Q)^DRbar
  2  2     1.61881068E-02   # Y_s(Q)^DRbar
  3  3     8.44922593E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.40312063E+03  # (SUSY scale)
  1  1     1.48681211E-04   # Y_e(Q)^DRbar
  2  2     3.07425588E-02   # Y_mu(Q)^DRbar
  3  3     5.17039266E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.40312063E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -7.61270632E+03   # A_t(Q)^DRbar
Block Ad Q=  3.40312063E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.62527467E+03   # A_b(Q)^DRbar
Block Ae Q=  3.40312063E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -8.90814901E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.40312063E+03  # soft SUSY breaking masses at Q
   1   -1.11321434E+02  # M_1
   2    1.08264432E+02  # M_2
   3    3.17684940E+03  # M_3
  21    5.84151230E+05  # M^2_(H,d)
  22   -2.25224675E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.39071127E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.72905470E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.18869657E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28806874E+02  # h0
        35     1.75648037E+03  # H0
        36     1.75651591E+03  # A0
        37     1.75804338E+03  # H+
   1000001     1.01044325E+04  # ~d_L
   2000001     1.00792096E+04  # ~d_R
   1000002     1.01040702E+04  # ~u_L
   2000002     1.00827116E+04  # ~u_R
   1000003     1.01044410E+04  # ~s_L
   2000003     1.00792224E+04  # ~s_R
   1000004     1.01040778E+04  # ~c_L
   2000004     1.00827127E+04  # ~c_R
   1000005     2.49139118E+03  # ~b_1
   2000005     3.27758596E+03  # ~b_2
   1000006     2.48064471E+03  # ~t_1
   2000006     4.66863714E+03  # ~t_2
   1000011     1.00217484E+04  # ~e_L-
   2000011     1.00085526E+04  # ~e_R-
   1000012     1.00209823E+04  # ~nu_eL
   1000013     1.00217994E+04  # ~mu_L-
   2000013     1.00086158E+04  # ~mu_R-
   1000014     1.00210210E+04  # ~nu_muL
   1000015     1.00244562E+04  # ~tau_1-
   2000015     1.00384653E+04  # ~tau_2-
   1000016     1.00320749E+04  # ~nu_tauL
   1000021     3.61671140E+03  # ~g
   1000022     1.12371085E+02  # ~chi_10
   1000023     1.21096048E+02  # ~chi_20
   1000025     1.52280373E+03  # ~chi_30
   1000035     1.52301032E+03  # ~chi_40
   1000024     1.21282745E+02  # ~chi_1+
   1000037     1.52488444E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.96551859E-02   # alpha
Block Hmix Q=  3.40312063E+03  # Higgs mixing parameters
   1    1.50709717E+03  # mu
   2    5.05599617E+01  # tan[beta](Q)
   3    2.43091884E+02  # v(Q)
   4    3.08534814E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.97696288E-01   # Re[R_st(1,1)]
   1  2     6.78389018E-02   # Re[R_st(1,2)]
   2  1    -6.78389018E-02   # Re[R_st(2,1)]
   2  2     9.97696288E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99458369E-01   # Re[R_sb(1,1)]
   1  2     3.29084973E-02   # Re[R_sb(1,2)]
   2  1    -3.29084973E-02   # Re[R_sb(2,1)]
   2  2     9.99458369E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     6.39942157E-01   # Re[R_sta(1,1)]
   1  2     7.68423084E-01   # Re[R_sta(1,2)]
   2  1    -7.68423084E-01   # Re[R_sta(2,1)]
   2  2     6.39942157E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99575083E-01   # Re[N(1,1)]
   1  2     2.31758571E-04   # Re[N(1,2)]
   1  3     2.91094525E-02   # Re[N(1,3)]
   1  4     1.49681281E-03   # Re[N(1,4)]
   2  1    -1.30485584E-03   # Re[N(2,1)]
   2  2    -9.98578122E-01   # Re[N(2,2)]
   2  3     5.30289877E-02   # Re[N(2,3)]
   2  4    -5.28748707E-03   # Re[N(2,4)]
   3  1    -2.16237593E-02   # Re[N(3,1)]
   3  2     3.37699448E-02   # Re[N(3,2)]
   3  3     7.05895389E-01   # Re[N(3,3)]
   3  4     7.07180107E-01   # Re[N(3,4)]
   4  1     1.95029426E-02   # Re[N(4,1)]
   4  2    -4.12464643E-02   # Re[N(4,2)]
   4  3    -7.05728182E-01   # Re[N(4,3)]
   4  4     7.07012092E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.97187560E-01   # Re[U(1,1)]
   1  2     7.49464437E-02   # Re[U(1,2)]
   2  1     7.49464437E-02   # Re[U(2,1)]
   2  2     9.97187560E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99972045E-01   # Re[V(1,1)]
   1  2     7.47730026E-03   # Re[V(1,2)]
   2  1     7.47730026E-03   # Re[V(2,1)]
   2  2     9.99972045E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02747105E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99188753E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     4.46359854E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     3.63100372E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44854526E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.68869310E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.03989077E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.43476864E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.76198360E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.05439937E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.26437996E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.06342209E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.92103782E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.21032292E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.12679367E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     3.52609299E-03    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.45035122E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.67802829E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.03615751E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     7.61123332E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     8.93380484E-04    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.04689129E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.26033335E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.70383806E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.09238626E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.21557015E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.40375293E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.39140643E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.41468293E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.48220130E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.77791896E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.54004944E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.33119773E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.51154393E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.52285287E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.64056278E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.45379324E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44859192E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.67267954E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.03108483E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.96882474E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     7.75652960E-04    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.08759668E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.45039589E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.66192612E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.02732654E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.96142605E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     7.74691495E-04    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.08012149E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     1.26510181E-03    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.95950438E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.41850423E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.24325510E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     4.41788591E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     5.74107389E-04    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.52061540E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.58412012E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.09524895E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.22188916E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.90770164E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.41070192E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.89603505E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.97988650E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.14720812E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.19320288E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     6.43839086E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.18145033E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.09626210E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.22044105E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.90607506E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.41124782E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.89595702E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.97947098E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.14946578E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.48430627E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.19311596E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     6.48497567E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.18085863E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.63529128E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.96988232E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.66280050E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.04106438E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.04030937E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.25624545E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.93988147E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     6.39122543E+01   # ~b_2
#    BR                NDA      ID1      ID2
     2.87174637E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.78045079E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.23709450E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.23641763E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.73930627E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.44731806E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.10086726E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.49464196E-02    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.06270732E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     6.26742895E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.58866445E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.64084205E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.41055141E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.90590576E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.98536173E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.19985717E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.18115072E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.26749922E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.58862447E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.64073513E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.41109597E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.90576709E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.98492622E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.19977389E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.18055894E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     6.85297863E+01   # ~t_1
#    BR                NDA      ID1      ID2
     1.41103091E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     5.31578607E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     4.42847990E-02    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     1.56159422E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.37160557E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.37595476E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.18992040E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.97978686E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.10220290E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     4.90509366E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.10719680E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.32801253E-04    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     7.72825234E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.79870780E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.70360634E-04    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.54977038E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.15742023E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     2.36881610E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     6.68376121E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     1.19168090E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.40165894E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     1.82965547E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     3.34425886E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     5.66918374E-13   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.57699903E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.23450654E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.13357185E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.13251594E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     9.19818736E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
     2.27276161E-04    3     1000023       -11        12   # BR(chi^+_1 -> chi^0_2 e^+ nu_e)
DECAY   1000037     1.12898010E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     8.89643509E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.96871297E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.95582985E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.93081328E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.16651727E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.25284209E-03    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     1.02312700E-04    3     1000024        15       -15   # BR(chi^+_2 -> chi^+_1 tau^- tau^+)
     2.78421871E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     9.62846161E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     7.92268316E-13   # chi^0_2
#    BR                NDA      ID1      ID2
     6.14680430E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     5.82206692E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     4.38389675E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     7.45761570E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     7.44587818E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     3.19003893E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     1.57649482E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.57365795E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     8.56656073E-03    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     9.38379024E-02    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.18287335E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.83208619E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.83208619E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     3.80105736E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.69201401E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     4.38682896E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.54999529E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.60498844E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.44906440E-04    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     5.10672986E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     5.98171016E-04    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     9.92805479E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     9.92805479E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.14258644E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.93140335E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.93140335E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.83608626E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.17305215E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     4.91296885E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.73205605E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.57792697E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.46626095E-04    3     1000022         5        -5   # BR(chi^0_4 -> chi^0_1 b b_bar)
     5.04394643E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     6.06434095E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     9.08362472E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     9.08362472E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     7.04793629E+01   # ~g
#    BR                NDA      ID1      ID2
     5.96240608E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     5.96240608E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.06170159E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.06170159E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.09650253E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.09650253E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     2.42827801E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     2.42827801E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     4.00562430E-03   # Gamma(h0)
     2.58653894E-03   2        22        22   # BR(h0 -> photon photon)
     2.01417823E-03   2        22        23   # BR(h0 -> photon Z)
     4.24333634E-02   2        23        23   # BR(h0 -> Z Z)
     3.26520838E-01   2       -24        24   # BR(h0 -> W W)
     7.65037642E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.26244864E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.89603063E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.46731987E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.18984068E-07   2        -2         2   # BR(h0 -> Up up)
     2.30954789E-02   2        -4         4   # BR(h0 -> Charm charm)
     4.90419869E-07   2        -1         1   # BR(h0 -> Down down)
     1.77374677E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.71805048E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.41995657E+01   # Gamma(HH)
     1.11519897E-08   2        22        22   # BR(HH -> photon photon)
     2.15125156E-08   2        22        23   # BR(HH -> photon Z)
     7.18081734E-07   2        23        23   # BR(HH -> Z Z)
     4.58701363E-07   2       -24        24   # BR(HH -> W W)
     3.00940969E-05   2        21        21   # BR(HH -> gluon gluon)
     1.34180739E-08   2       -11        11   # BR(HH -> Electron electron)
     5.97337377E-04   2       -13        13   # BR(HH -> Muon muon)
     1.72503421E-01   2       -15        15   # BR(HH -> Tau tau)
     3.97350412E-14   2        -2         2   # BR(HH -> Up up)
     7.70625444E-09   2        -4         4   # BR(HH -> Charm charm)
     3.56908517E-04   2        -6         6   # BR(HH -> Top top)
     9.75254510E-07   2        -1         1   # BR(HH -> Down down)
     3.52902665E-04   2        -3         3   # BR(HH -> Strange strange)
     7.90692161E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.81428604E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     9.79802478E-03   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     9.79802478E-03   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     9.04866483E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.28112638E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     7.10503751E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.73187023E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     7.46777755E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     8.28110960E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.87960424E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     2.35331112E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.24719373E+01   # Gamma(A0)
     9.30866956E-08   2        22        22   # BR(A0 -> photon photon)
     1.68899734E-07   2        22        23   # BR(A0 -> photon Z)
     4.54629280E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.33978979E-08   2       -11        11   # BR(A0 -> Electron electron)
     5.96439304E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.72244834E-01   2       -15        15   # BR(A0 -> Tau tau)
     3.33436306E-14   2        -2         2   # BR(A0 -> Up up)
     6.46739938E-09   2        -4         4   # BR(A0 -> Charm charm)
     2.98569863E-04   2        -6         6   # BR(A0 -> Top top)
     9.73789062E-07   2        -1         1   # BR(A0 -> Down down)
     3.52372050E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.89522645E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.93067751E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.01887256E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.01887256E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     9.62931479E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.41840070E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.86351462E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     7.35985806E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     7.86496941E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.96625205E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     8.56257931E-04   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     9.89081530E-07   2        23        25   # BR(A0 -> Z h0)
     3.14190999E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     5.82188471E+01   # Gamma(Hp)
     1.31300010E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.61348549E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.58781078E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.39481274E-07   2        -1         2   # BR(Hp -> Down up)
     1.40872698E-05   2        -3         2   # BR(Hp -> Strange up)
     8.66632815E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.92417996E-08   2        -1         4   # BR(Hp -> Down charm)
     3.02699403E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.21359518E-03   2        -5         4   # BR(Hp -> Bottom charm)
     3.10180072E-08   2        -1         6   # BR(Hp -> Down top)
     1.11593768E-06   2        -3         6   # BR(Hp -> Strange top)
     8.13408409E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.08627216E-09   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     7.60212710E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     7.96772018E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.04897081E-03   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     7.64109091E-03   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     7.61839056E-03   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     7.24799010E-07   2        24        25   # BR(Hp -> W h0)
     2.17991032E-13   2        24        35   # BR(Hp -> W HH)
     1.94316241E-13   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.87828871E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.55632190E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.55630973E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000476E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.86427693E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.91188904E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.87828871E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.55632190E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.55630973E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999985E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.45761331E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999985E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.45761331E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25774208E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.07152589E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.62859766E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.45761331E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999985E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.12630106E-04   # BR(b -> s gamma)
    2    1.59225697E-06   # BR(b -> s mu+ mu-)
    3    3.52689364E-05   # BR(b -> s nu nu)
    4    2.43379344E-15   # BR(Bd -> e+ e-)
    5    1.03965593E-10   # BR(Bd -> mu+ mu-)
    6    2.15747478E-08   # BR(Bd -> tau+ tau-)
    7    8.07400707E-14   # BR(Bs -> e+ e-)
    8    3.44911099E-09   # BR(Bs -> mu+ mu-)
    9    7.25638760E-07   # BR(Bs -> tau+ tau-)
   10    9.38335887E-05   # BR(B_u -> tau nu)
   11    9.69265048E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41569508E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93319071E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15617357E-03   # epsilon_K
   17    2.28165208E-15   # Delta(M_K)
   18    2.48129661E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29350979E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.05140954E-16   # Delta(g-2)_electron/2
   21    4.49515064E-12   # Delta(g-2)_muon/2
   22    1.27473746E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.13228464E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.77006719E-01   # C7
     0305 4322   00   2    -1.55699881E-04   # C7'
     0305 6421   00   0    -9.52278349E-02   # C8
     0305 6421   00   2    -1.00016097E-01   # C8
     0305 6321   00   2    -3.47587970E-04   # C8'
 03051111 4133   00   0     1.62186698E+00   # C9 e+e-
 03051111 4133   00   2     1.62240950E+00   # C9 e+e-
 03051111 4233   00   2     7.63407886E-04   # C9' e+e-
 03051111 4137   00   0    -4.44455908E+00   # C10 e+e-
 03051111 4137   00   2    -4.44470871E+00   # C10 e+e-
 03051111 4237   00   2    -5.68998460E-03   # C10' e+e-
 03051313 4133   00   0     1.62186698E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62240927E+00   # C9 mu+mu-
 03051313 4233   00   2     7.63402450E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44455908E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44470894E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.68998035E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50538594E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.23172289E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50538594E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.23172401E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50538595E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.23203762E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85901800E-07   # C7
     0305 4422   00   2     2.44879694E-05   # C7
     0305 4322   00   2     2.68051595E-06   # C7'
     0305 6421   00   0     3.30550412E-07   # C8
     0305 6421   00   2    -3.46353144E-05   # C8
     0305 6321   00   2     5.63304698E-07   # C8'
 03051111 4133   00   2     4.11025616E-06   # C9 e+e-
 03051111 4233   00   2     1.63235447E-05   # C9' e+e-
 03051111 4137   00   2     3.90594873E-06   # C10 e+e-
 03051111 4237   00   2    -1.21674167E-04   # C10' e+e-
 03051313 4133   00   2     4.11024330E-06   # C9 mu+mu-
 03051313 4233   00   2     1.63235395E-05   # C9' mu+mu-
 03051313 4137   00   2     3.90596219E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.21674182E-04   # C10' mu+mu-
 03051212 4137   00   2    -7.84092540E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.63390649E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -7.84091852E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.63390649E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -7.83901006E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.63390649E-05   # C11' nu_3 nu_3
