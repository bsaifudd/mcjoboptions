# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  22:18
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.19651872E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.24075093E+03  # scale for input parameters
    1   -4.14173625E+02  # M_1
    2   -4.77974974E+02  # M_2
    3    2.53169147E+03  # M_3
   11   -4.47008749E+03  # A_t
   12    1.85993686E+03  # A_b
   13   -4.29790467E+02  # A_tau
   23    2.08369613E+02  # mu
   25    3.08951111E+01  # tan(beta)
   26    8.15771007E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.12478625E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.71802354E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.80038851E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.24075093E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.24075093E+03  # (SUSY scale)
  1  1     8.38876297E-06   # Y_u(Q)^DRbar
  2  2     4.26149159E-03   # Y_c(Q)^DRbar
  3  3     1.01342995E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.24075093E+03  # (SUSY scale)
  1  1     5.20796350E-04   # Y_d(Q)^DRbar
  2  2     9.89513066E-03   # Y_s(Q)^DRbar
  3  3     5.16466786E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.24075093E+03  # (SUSY scale)
  1  1     9.08827715E-05   # Y_e(Q)^DRbar
  2  2     1.87916747E-02   # Y_mu(Q)^DRbar
  3  3     3.16045055E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.24075093E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -4.47008745E+03   # A_t(Q)^DRbar
Block Ad Q=  3.24075093E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.85993688E+03   # A_b(Q)^DRbar
Block Ae Q=  3.24075093E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -4.29790467E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.24075093E+03  # soft SUSY breaking masses at Q
   1   -4.14173625E+02  # M_1
   2   -4.77974974E+02  # M_2
   3    2.53169147E+03  # M_3
  21    5.18156258E+05  # M^2_(H,d)
  22    5.11511111E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.12478625E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.71802354E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.80038851E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.26274660E+02  # h0
        35     8.15641738E+02  # H0
        36     8.15771007E+02  # A0
        37     8.22827242E+02  # H+
   1000001     1.01099371E+04  # ~d_L
   2000001     1.00847255E+04  # ~d_R
   1000002     1.01095705E+04  # ~u_L
   2000002     1.00885429E+04  # ~u_R
   1000003     1.01099412E+04  # ~s_L
   2000003     1.00847323E+04  # ~s_R
   1000004     1.01095746E+04  # ~c_L
   2000004     1.00885442E+04  # ~c_R
   1000005     2.22916127E+03  # ~b_1
   2000005     3.85896455E+03  # ~b_2
   1000006     2.22721597E+03  # ~t_1
   2000006     4.71551333E+03  # ~t_2
   1000011     1.00217065E+04  # ~e_L-
   2000011     1.00082773E+04  # ~e_R-
   1000012     1.00209356E+04  # ~nu_eL
   1000013     1.00217244E+04  # ~mu_L-
   2000013     1.00083122E+04  # ~mu_R-
   1000014     1.00209534E+04  # ~nu_muL
   1000015     1.00181532E+04  # ~tau_1-
   2000015     1.00268367E+04  # ~tau_2-
   1000016     1.00260212E+04  # ~nu_tauL
   1000021     2.95052410E+03  # ~g
   1000022     2.02233207E+02  # ~chi_10
   1000023     2.21827018E+02  # ~chi_20
   1000025     4.22437481E+02  # ~chi_30
   1000035     5.33804021E+02  # ~chi_40
   1000024     2.10747544E+02  # ~chi_1+
   1000037     5.33622236E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.22699546E-02   # alpha
Block Hmix Q=  3.24075093E+03  # Higgs mixing parameters
   1    2.08369613E+02  # mu
   2    3.08951111E+01  # tan[beta](Q)
   3    2.43163527E+02  # v(Q)
   4    6.65482336E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99331019E-01   # Re[R_st(1,1)]
   1  2     3.65720463E-02   # Re[R_st(1,2)]
   2  1    -3.65720463E-02   # Re[R_st(2,1)]
   2  2     9.99331019E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99999464E-01   # Re[R_sb(1,1)]
   1  2     1.03577990E-03   # Re[R_sb(1,2)]
   2  1    -1.03577990E-03   # Re[R_sb(2,1)]
   2  2     9.99999464E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     7.04876465E-02   # Re[R_sta(1,1)]
   1  2     9.97512652E-01   # Re[R_sta(1,2)]
   2  1    -9.97512652E-01   # Re[R_sta(2,1)]
   2  2     7.04876465E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -1.30505504E-01   # Re[N(1,1)]
   1  2     1.65665476E-01   # Re[N(1,2)]
   1  3    -7.14212309E-01   # Re[N(1,3)]
   1  4    -6.67400960E-01   # Re[N(1,4)]
   2  1     5.01640030E-02   # Re[N(2,1)]
   2  2    -8.13188988E-02   # Re[N(2,2)]
   2  3    -6.94452451E-01   # Re[N(2,3)]
   2  4     7.13166603E-01   # Re[N(2,4)]
   3  1    -9.87566705E-01   # Re[N(3,1)]
   3  2    -9.71876534E-02   # Re[N(3,2)]
   3  3     5.41141394E-02   # Re[N(3,3)]
   3  4     1.11077556E-01   # Re[N(3,4)]
   4  1    -7.18602090E-02   # Re[N(4,1)]
   4  2     9.78006517E-01   # Re[N(4,2)]
   4  3     6.86165566E-02   # Re[N(4,3)]
   4  4     1.83387927E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.54095403E-02   # Re[U(1,1)]
   1  2     9.95438104E-01   # Re[U(1,2)]
   2  1    -9.95438104E-01   # Re[U(2,1)]
   2  2    -9.54095403E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     2.54158999E-01   # Re[V(1,1)]
   1  2     9.67162449E-01   # Re[V(1,2)]
   2  1     9.67162449E-01   # Re[V(2,1)]
   2  2    -2.54158999E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.01110413E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.70778721E-02    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.52284566E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.75246572E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     5.15267698E-03    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44143261E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     2.81486827E-03    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     9.08747399E-04    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.19025724E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.68648677E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     5.56647040E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.03035513E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02517051E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.77432417E-02    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.18988585E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.72524180E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     5.14482227E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.38520630E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.44213859E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     3.06231576E-03    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.14350917E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.18969093E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.68519935E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     5.56375534E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.02741392E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     9.03111831E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.29559940E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.04143219E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.39499646E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.81005403E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.23382776E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     6.04364830E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.63751541E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     6.02315837E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     6.13333197E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.06737354E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.37770386E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.05921483E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.31868142E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44147996E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.69396195E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.57329972E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.64786101E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.14311516E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.94965963E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.69200359E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44218587E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.69313582E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.57155704E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.64510662E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.14158232E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.99606002E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.68927186E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.64126552E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.48851810E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.13993015E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     4.96290176E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.76193154E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.54884727E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.01267990E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.71186799E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.42796197E-04    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     8.15248714E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.91640433E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.02033665E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.97836380E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.51527117E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     3.42200179E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.41637482E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.00924152E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.09343841E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.32711079E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.71226563E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.57663164E-04    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     8.15209498E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.91582605E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.02057361E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.99078599E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.63311292E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     3.42261680E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.41622852E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.01346745E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.09340948E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.32686940E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.22260435E+01   # ~b_1
#    BR                NDA      ID1      ID2
     7.64013315E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     6.89508923E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.07292473E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.04691602E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.03836389E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.45046861E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.00793438E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.03363233E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     9.74161792E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.09409948E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.05258205E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.99260538E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.82188459E-03    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     5.21468080E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.22792528E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     2.72455481E-02    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     1.35733366E-02    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     1.35739150E-02    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     6.88373679E+02   # ~u_R
#    BR                NDA      ID1      ID2
     5.56973752E-04    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.18078833E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     1.68061388E-04    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.67384802E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.02021162E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.12455402E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.90429758E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.12199478E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     5.14006637E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     7.16172360E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.03218015E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.32682619E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.88381050E+02   # ~c_R
#    BR                NDA      ID1      ID2
     5.59324791E-04    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.18076097E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     1.68236518E-04    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.67374571E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.02044824E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.12654859E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     2.92735541E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.12193105E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     5.13993161E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     7.18575718E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.03215233E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.32658479E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     8.27550441E+01   # ~t_1
#    BR                NDA      ID1      ID2
     2.37869094E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.70081768E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.56722734E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.12625774E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.48151874E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.03565098E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.19578688E-02    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     4.38222575E+02   # ~t_2
#    BR                NDA      ID1      ID2
     9.89419694E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.11704795E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.51774209E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     6.00233758E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.07232161E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.14116762E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.10464326E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.01778865E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     5.08724149E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.63531275E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.76423359E-07   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.52810564E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.16610520E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.17603636E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.17513775E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     9.54615050E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     4.02645263E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.55470836E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.57450919E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.61618374E-02    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.49998764E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.09747439E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.75426599E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     4.70977902E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     9.25732960E-06   # chi^0_2
#    BR                NDA      ID1      ID2
     8.84265960E-02    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.10875624E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.04897604E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.42164165E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.42125992E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     8.34045439E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.20824427E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.20688377E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.84915146E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.90115393E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     7.82250035E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     7.82250035E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     7.33504789E-03    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     7.33504789E-03    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     2.60750233E-03    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     2.60750233E-03    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     2.60633055E-03    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     2.60633055E-03    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     2.30226260E-03    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     2.30226260E-03    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     9.66755250E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     3.12447024E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     3.12447024E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.81058812E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.67887644E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     7.88683426E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.10135378E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     4.99065314E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.23838154E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.23838154E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.79417096E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.04766927E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.99066067E-04    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.26906489E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.81091102E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.72814323E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.72814323E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.20411149E+01   # ~g
#    BR                NDA      ID1      ID2
     6.47118425E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     6.47118425E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.41825784E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.41825784E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.51049602E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.51049602E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     1.41761967E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.20064756E-04    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.57259008E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.12652780E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     2.60411829E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.60411829E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.68382275E-03   # Gamma(h0)
     2.56877710E-03   2        22        22   # BR(h0 -> photon photon)
     1.75959794E-03   2        22        23   # BR(h0 -> photon Z)
     3.40961806E-02   2        23        23   # BR(h0 -> Z Z)
     2.74742100E-01   2       -24        24   # BR(h0 -> W W)
     7.85622297E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.70601343E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.09332535E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.03592379E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.25504243E-07   2        -2         2   # BR(h0 -> Up up)
     2.43599547E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.43622023E-07   2        -1         1   # BR(h0 -> Down down)
     1.96616046E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.23145299E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.36855131E+01   # Gamma(HH)
     2.69599448E-07   2        22        22   # BR(HH -> photon photon)
     5.55411497E-07   2        22        23   # BR(HH -> photon Z)
     7.42903184E-06   2        23        23   # BR(HH -> Z Z)
     1.22126426E-05   2       -24        24   # BR(HH -> W W)
     6.35117207E-05   2        21        21   # BR(HH -> gluon gluon)
     8.05361234E-09   2       -11        11   # BR(HH -> Electron electron)
     3.58441522E-04   2       -13        13   # BR(HH -> Muon muon)
     1.03499106E-01   2       -15        15   # BR(HH -> Tau tau)
     1.68836442E-13   2        -2         2   # BR(HH -> Up up)
     3.27385194E-08   2        -4         4   # BR(HH -> Charm charm)
     1.82266209E-03   2        -6         6   # BR(HH -> Top top)
     6.95730778E-07   2        -1         1   # BR(HH -> Down down)
     2.51650163E-04   2        -3         3   # BR(HH -> Strange strange)
     6.48869904E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.43644448E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     6.17416628E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     6.17416628E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.11139614E-02   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.49082223E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     5.22722402E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.88376024E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.69168038E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.02114919E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     4.86365591E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     5.64099149E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.35070660E+01   # Gamma(A0)
     2.03098028E-07   2        22        22   # BR(A0 -> photon photon)
     1.17355732E-06   2        22        23   # BR(A0 -> photon Z)
     1.03272750E-04   2        21        21   # BR(A0 -> gluon gluon)
     8.01496023E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.56721324E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.03004445E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.56704948E-13   2        -2         2   # BR(A0 -> Up up)
     3.03591316E-08   2        -4         4   # BR(A0 -> Charm charm)
     2.09011392E-03   2        -6         6   # BR(A0 -> Top top)
     6.92376364E-07   2        -1         1   # BR(A0 -> Down down)
     2.50436879E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.45757217E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.12162142E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     5.90352190E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     5.90352190E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.29501971E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.60324934E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.00593718E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     5.14096748E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.70666232E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     4.95902430E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.54491681E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.16849455E-05   2        23        25   # BR(A0 -> Z h0)
     1.64747215E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.46423641E+01   # Gamma(Hp)
     9.49538610E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     4.05957400E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.14826850E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.12086930E-07   2        -1         2   # BR(Hp -> Down up)
     1.18223550E-05   2        -3         2   # BR(Hp -> Strange up)
     7.42951049E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.42108184E-08   2        -1         4   # BR(Hp -> Down charm)
     2.56665492E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.04039274E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.51239199E-07   2        -1         6   # BR(Hp -> Down top)
     3.64104112E-06   2        -3         6   # BR(Hp -> Strange top)
     6.57920125E-01   2        -5         6   # BR(Hp -> Bottom top)
     6.75017547E-06   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.90286494E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     4.63894472E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.68508902E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     4.08033644E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     5.41864245E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.11827155E-05   2        24        25   # BR(Hp -> W h0)
     1.76661231E-09   2        24        35   # BR(Hp -> W HH)
     1.61343889E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.94672773E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    9.54513217E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    9.54507890E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000558E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.04207915E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.04766028E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.94672773E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    9.54513217E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    9.54507890E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999993E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    7.45282284E-09        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999993E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    7.45282284E-09        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26142781E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.57333975E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.34675149E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    7.45282284E-09        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999993E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.53087960E-04   # BR(b -> s gamma)
    2    1.58755147E-06   # BR(b -> s mu+ mu-)
    3    3.52542265E-05   # BR(b -> s nu nu)
    4    3.12497798E-15   # BR(Bd -> e+ e-)
    5    1.33494243E-10   # BR(Bd -> mu+ mu-)
    6    2.78716098E-08   # BR(Bd -> tau+ tau-)
    7    1.04259321E-13   # BR(Bs -> e+ e-)
    8    4.45391628E-09   # BR(Bs -> mu+ mu-)
    9    9.42431148E-07   # BR(Bs -> tau+ tau-)
   10    8.84881673E-05   # BR(B_u -> tau nu)
   11    9.14048891E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41859038E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93207341E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15720496E-03   # epsilon_K
   17    2.28166732E-15   # Delta(M_K)
   18    2.48040383E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29137473E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.66087978E-16   # Delta(g-2)_electron/2
   21   -1.13761477E-11   # Delta(g-2)_muon/2
   22   -3.22221024E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.89809972E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.19409992E-01   # C7
     0305 4322   00   2    -8.74024276E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.40891306E-01   # C8
     0305 6321   00   2    -1.05192541E-03   # C8'
 03051111 4133   00   0     1.61933451E+00   # C9 e+e-
 03051111 4133   00   2     1.61969451E+00   # C9 e+e-
 03051111 4233   00   2     1.96241320E-04   # C9' e+e-
 03051111 4137   00   0    -4.44202661E+00   # C10 e+e-
 03051111 4137   00   2    -4.44056301E+00   # C10 e+e-
 03051111 4237   00   2    -1.46665246E-03   # C10' e+e-
 03051313 4133   00   0     1.61933451E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61969444E+00   # C9 mu+mu-
 03051313 4233   00   2     1.96230364E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44202661E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44056309E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.46664174E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50503590E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.17669858E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50503590E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.17672219E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50503591E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.18337548E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85828650E-07   # C7
     0305 4422   00   2    -1.34824387E-05   # C7
     0305 4322   00   2     7.96194544E-07   # C7'
     0305 6421   00   0     3.30487755E-07   # C8
     0305 6421   00   2    -2.34394966E-05   # C8
     0305 6321   00   2     1.13103586E-08   # C8'
 03051111 4133   00   2     1.59604750E-06   # C9 e+e-
 03051111 4233   00   2     6.62663706E-06   # C9' e+e-
 03051111 4137   00   2     3.12861646E-06   # C10 e+e-
 03051111 4237   00   2    -4.95577999E-05   # C10' e+e-
 03051313 4133   00   2     1.59604351E-06   # C9 mu+mu-
 03051313 4233   00   2     6.62663599E-06   # C9' mu+mu-
 03051313 4137   00   2     3.12862075E-06   # C10 mu+mu-
 03051313 4237   00   2    -4.95578031E-05   # C10' mu+mu-
 03051212 4137   00   2    -6.27556769E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.07339919E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -6.27556571E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.07339919E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -6.27501090E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.07339919E-05   # C11' nu_3 nu_3
