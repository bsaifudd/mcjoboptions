include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Dijet truth jet slice JZ1W, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["simone.amoroso@cern.ch"]

genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)    
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)  
JZSlice(1,filtSeq) 
include("GeneratorFilters/JetFilter_JZXW_Fragment.py")
evgenConfig.nEventsPerJob = 200
