#based on 361224
evgenConfig.description = "Low-pT inelastic minimum bias events using EPOS"
evgenConfig.keywords = ["QCD", "minBias" , "SM"]
evgenConfig.contact  = [ "deepak.kar@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

include("Epos_i/Epos_Base_Fragment.py")

if hasattr(testSeq, "TestHepMC"):
      testSeq.TestHepMC.EnergyDifference = 5000.0

