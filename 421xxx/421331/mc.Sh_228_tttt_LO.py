include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF31LO.py")

evgenConfig.description = "Sherpa 2.2.8 tttt production at LO with all decay modes."
evgenConfig.keywords = ["SM", "top", "tttt" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

genSeq.Sherpa_i.RunCard="""
(run){
  SCALES VAR{H_T2/16}
  EXCLUSIVE_CLUSTER_MODE 1;

  %me generator settings
  CSS_REWEIGHT=1
  REWEIGHT_SPLITTING_ALPHAS_SCALES 1
  REWEIGHT_SPLITTING_PDF_SCALES 1
  CSS_REWEIGHT_SCALE_CUTOFF=5.0
  HEPMC_INCLUDE_ME_ONLY_VARIATIONS=1

  INTEGRATION_ERROR=0.05;

  %decay settings
  HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
  STABLE[24] 0; STABLE[6] 0; WIDTH[6] 0;
}(run)

(processes){
  Process : 93 93 ->  6 -6 6 -6;
  Order (*,0);
  End process
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]

genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.CleanupGeneratedFiles = 1
