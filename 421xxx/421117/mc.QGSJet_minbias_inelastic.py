#based on 361235
evgenConfig.description = "Low-pT inelastic minimum bias events using EPOS"
evgenConfig.keywords = ["QCD", "minBias"]

## Base config for QGSJet
from QGSJet_i.QGSJet_iConf import QGSJet
genSeq += QGSJet("QGSJet")
evgenConfig.generators += ["QGSJet"]

genSeq.QGSJet.BeamMomentum     = -runArgs.ecmEnergy/2.0
genSeq.QGSJet.TargetMomentum   = runArgs.ecmEnergy/2.0
genSeq.QGSJet.PrimaryParticle  = 1
genSeq.QGSJet.TargetParticle   = 1
genSeq.QGSJet.Model            = 7
genSeq.QGSJet.ParamFile        = "qgsjet_crmc.param"
genSeq.QGSJet.TabCreate        = 0

## Get files from the InstallArea
import os
os.system("get_files %s" % genSeq.QGSJet.ParamFile)
inputFiles = "sectnu-II-04 \
              qgsdat-II-04.lzma "
#              epos.initl \
#              epos.iniev \
#              epos.inirj \
#              epos.inics \
#              epos.inirj.lhc \
#              epos.inics.lhc"
if not os.path.exists("tabs"):
    os.mkdir("tabs")
os.system("get_files %s" % inputFiles)
os.system("mv %s tabs/" % inputFiles)

testSeq.TestHepMC.CmeDifference = 5
testSeq.TestHepMC.AccuracyMargin = 0.5

