from enum import Enum


##---


class lptOptions:
    splitToken = "_"

    jobType = Enum("jobType", ["N1N1", "N1N2", "RESHOWER"])
    jobTypeMap = {
        "N1N1": jobType.N1N1,
        "N1N2": jobType.N1N2,
        "Reshower": jobType.RESHOWER,
    }

    npOptionsMap = {"n": True, "p": False}
    ynOptionMap = {"Y": True, "N": False}

    ##---

    @staticmethod
    def getJobName():
        from MCJobOptionUtils.JOsupport import get_physics_short

        return get_physics_short()

    ##---

    @staticmethod
    def parseOptions(optionsStr, firstTokenPosition=0):
        tokens = optionsStr.split(lptOptions.splitToken)

        ## ToDo: Check the number of tokens.

        type = lptOptions.jobTypeMap[tokens[firstTokenPosition]]

        mass, lifetime = (
            float(tokens[firstTokenPosition + 1]),
            float(tokens[firstTokenPosition + 2]),
        )

        mcOptionsToken = tokens[firstTokenPosition + 3]
        ns, doTaus, doUpType, doDownType, doBottom, doTop = (
            lptOptions.npOptionsMap[mcOptionsToken[0]],
            lptOptions.ynOptionMap[mcOptionsToken[1]],
            lptOptions.ynOptionMap[mcOptionsToken[2]],
            lptOptions.ynOptionMap[mcOptionsToken[3]],
            lptOptions.ynOptionMap[mcOptionsToken[4]],
            lptOptions.ynOptionMap[mcOptionsToken[5]],
        )

        ## ToDo: Filter token.

        return (
            type,
            mass,
            lifetime,
            ns,
            doTaus,
            doUpType,
            doDownType,
            doBottom,
            doTop,
        )

    @staticmethod
    def getJobOptions(firstTokenPosition=0):
        return lptOptions.parseOptions(lptOptions.getJobName(), firstTokenPosition)
