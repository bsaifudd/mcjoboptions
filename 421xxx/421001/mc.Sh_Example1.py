include("Sherpa_i/Base_Fragment.py")
if os.environ["SHERPAVER"].startswith('2.'):
  include("Sherpa_i/NNPDF30NNLO.py")
else:
  include("Sherpa_i/PDF4LHC21.py")

evgenConfig.description = "Sherpa 2.2.x example JO, Z+0,1-jet production."
evgenConfig.keywords = [ "2lepton" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch"]

if os.environ["SHERPAVER"].startswith('2.'):
  genSeq.Sherpa_i.RunCard="""
(processes){
  Process 93 93 -> 11 -11 93{0} 
  Order (*,2)
  CKKW sqr(20/E_CMS)
  End process;
}(processes)

(selector){
  Mass 11 -11 40 E_CMS
}(selector)
"""
  genSeq.Sherpa_i.Parameters += [ "LOG_FILE=", "MI_HANDLER=None" ]

else:
  genSeq.Sherpa_i.RunCard="""
PROCESSES:
- 93 93 -> 11 -11 93{0}:
    Order: {QCD: 0, EW: 2}
    CKKW: 20
    
SELECTORS:
- [Mass, 11, -11, 40, E_CMS]
"""

genSeq.Sherpa_i.OpenLoopsLibs = []
genSeq.Sherpa_i.ExtraFiles = []
genSeq.Sherpa_i.NCores = 1

genSeq.Sherpa_i.CleanupGeneratedFiles = 1
