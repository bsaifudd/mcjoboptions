#based on 410066
#include('MadGraphControl/MadGraphControl_ttV_LO_Pythia8_A14_CKKWLkTMerge.py')
from MadGraphControl.MadGraphUtils import *

# include base fragment for PDF setting
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

# Set number of events from MG5_aMC with a generous boost for the filter
nevents = runArgs.maxEvents*6. if runArgs.maxEvents>0 else 6.*evgenConfig.nEventsPerJob

# MG merging settings
maxjetflavor=5

# Pythia8 merging settings
nJetMax=2
ktdurham=30

process = """
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define w = w+ w-
define q = u c d s b
define q~ = u~ c~ d~ s~ b~
define zonshell = u c d s b vl
define zonshell~ = u~ c~ d~ s~ b~ vl~
generate p p > t t~ w
output -f"""

process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version'  : '3.0',
             'cut_decays'   : 'F', 
             'maxjetflavor' : maxjetflavor,
             'asrwgtflavor' : maxjetflavor,
             'ickkw'        : 0,
             'ptj'          : 20,
             'ptb'          : 20,
             'mmll'         : -1,
             'mmjj'         : 0,
             'drjj'         : 0,
             'drll'         : 0,
             'drjl'         : 0.4,
             'ptl'          : 0,
             'etal'         : 10,
             'etab'         : 6,
             'etaj'         : 6,
             'nevents'      :int(nevents)  }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

#### Shower 
evgenConfig.description = 'MadGraph ttW Np0'
evgenConfig.keywords+=['SM','ttW']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#PYTHIA8_TMS=ktdurham
#PYTHIA8_nJetMax=2
#PYTHIA8_Dparameter=0.4
#PYTHIA8_Process=process
#PYTHIA8_nQuarksMerge=maxjetflavor
#include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
#genSeq.Pythia8.Commands+=["Merging:mayRemoveDecayProducts=on"]
