# JOs based on 420000, 860107
###############################################################
#
# Job options file for Hijing generation of
# Pb + Pb collisions with variable ECM/(colliding nucleon pair)
#
# Flow with new LHC physics flow full v2-v6
# using function jjia_minbias_new
# =========================================
# Version for > FlowAfterburner-00-02-00
# =========================================
#
# Andrzej Olszewski April 2015
# Update to variable ECM JK March 2023
#==============================================================

evgenConfig.nEventsPerJob = 10000

# use common fragment
include("Hijing_i/Hijing_Common.py")

from FlowAfterburner.FlowAfterburnerConf import AddFlowByShifting
genSeq += AddFlowByShifting()

evgenConfig.description = "Hijing"
evgenConfig.keywords = ["minBias"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch", "Andrzej.Olszewski@ifj.edu.pl"]

ecmEnergy=5020.
if hasattr(runArgs,'ecmEnergy'):
    ecmEnergy = runArgs.ecmEnergy
else: 
    print("No center of mass energy found, default to 5020 GeV")

#----------------------
# Hijing Parameters
#----------------------
Hijing = genSeq.Hijing
Hijing.McEventKey = "HIJING_EVENT"
#Hijing.McEventsRW = "HIJING_EVENT"
Hijing.Initialize = ["efrm %f" % ecmEnergy,
                     "frame CMS", "proj A", "targ A",
                     "iap 208", "izp 82", "iat 208", "izt 82",
# simulation of minimum-bias events
                    "bmin 0", "bmax 20",
# turns OFF jet quenching:
                    "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ... 
                    "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                    "ihpr2 21 1"]

AddFlowByShifting = genSeq.AddFlowByShifting
AddFlowByShifting.McTruthKey    = "HIJING_EVENT"
AddFlowByShifting.McFlowKey     = "GEN_EVENT"

#----------------------
# Flow Parameters
#----------------------
AddFlowByShifting.FlowFunctionName = "jjia_minbias_new"
AddFlowByShifting.FlowImplementation = "exact"

AddFlowByShifting.RandomizePhi  = 0

include("EvtGen_i/EvtGen_Fragment.py")

