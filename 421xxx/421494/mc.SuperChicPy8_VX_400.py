evgenConfig.description = "SuperChic4 MC gamma + gamma pp collisions at 13000 GeV to 2 muons with dissociation on both side"
#evgenConfig.keywords = ["2photon","2muon","dissociation"]
evgenConfig.contact = ["maura.barros@cern.ch"]
evgenConfig.nEventsPerJob = 1000
evgenConfig.generators = ["SuperChic"]

from Superchic_i.SuperChicUtils import SuperChicConfig, SuperChicRun

#class with the superchic initialization parameters.  Please see SuperChicUtils for a complete list of tunable parameters.
scConfig = SuperChicConfig(runArgs)

scConfig.isurv = 4                  # Model of soft survival (from 1 -> 4, corresponding to arXiv:1306.2149)
scConfig.PDFname = 'MSHT20qed_nnlo' # PDF set name
scConfig.PDFmember = 0              # PDF member
scConfig.proc = 83                  # Process number (59 = gg->gg, 56 = gg->ee, 68 = gg->a->gg ); Please consult Superchic Manual https://superchic.hepforge.org/
scConfig.beam = 'prot'              # Beam type ('prot', 'ion')
scConfig.sfaci = True             # Include soft survival effects
scConfig.diff = 'el'                # interaction: elastic ('el'), single ('sd','sda','sdb') and double ('dd') dissociation.
scConfig.genunw  = True
scConfig.ymin  = -5.0              # Minimum object rapidity Y_X
scConfig.ymax  = 5.0               # Maximum object rapidity Y_X
scConfig.mmin  = 10                 # Minimum object mass M_X
scConfig.mmax  = 5000               # Maximum object mass M_X
scConfig.gencuts  = True           # Generate cuts below
scConfig.scorr  = True           # Include spin correlations
scConfig.fwidth  = True          # Include finite width
scConfig.ptxmax  = 10000000000          #cut on proton pt 
scConfig.ptamin  = 20.0             # Minimum pT of outgoing object a 
scConfig.ptbmin  = 14.0             # Minimum pT of outgoing object b 
scConfig.ptcmin  = 14.0             # Minimum pT of outgoing object c 
scConfig.etaamin  = -2.5           # Minimum eta of outgoing object a
scConfig.etaamax   = 2.5           # Maximum eta of outgoing object a
scConfig.etabmin  = -2.5           # Minimum eta of outgoing object b
scConfig.etabmax   = 2.5           # Maximum eta of outgoing object b
scConfig.etacmin  = -2.5           # Minimum eta of outgoing object c
scConfig.etacmax   = 2.5           # Maximum eta of outgoing object c
scConfig.acoabmax  = 100000000000
scConfig.tau = 0.04                # Mass distribution decay constant (GeV^-1)
scConfig.mxs = 400                 # Mass of MX

SuperChicRun(scConfig, genSeq)
include('Pythia8_VX_Common.py')
