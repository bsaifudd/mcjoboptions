#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
# based on 361108

include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.decay_mode = "z > tau+ tau-"

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 100
PowhegConfig.running_width = 1
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->tautau production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["oldrich.kepka@cern.ch"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2tau' ]
evgenConfig.nEventsPerJob= 1000

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include('GeneratorFilters/xAODTauFilter_Common.py')

filtSeq.xAODTauFilter.Ntaus = 2 
filtSeq.xAODTauFilter.EtaMaxe = 2.7 
filtSeq.xAODTauFilter.EtaMaxmu = 2.7 
filtSeq.xAODTauFilter.EtaMaxhad = 2.7 # no hadronic tau decays
filtSeq.xAODTauFilter.Ptcute = 12000.0
filtSeq.xAODTauFilter.Ptcutmu = 12000.0



