import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

#############################################
### Production of scalar long-lived particles
### though a higgs-like scalar mediator
############################################
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
joparts = get_physics_short().split('_')

# Generate A->ZH, H->4l, Z->inc inc

#: parse the job arguments to get mA,mH and Z decay mode
mH = float(joparts[3][2:])
mS = float(joparts[4][2:])
ctau=float(joparts[5][2:])
width = 0.0
# so far only support LW A not for H

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------

try :    
    modelcode='HAHM_variableMW_v3_UFO'
    process1 = 'generate g g > h HIG=1 HIW=0 QED=0 QCD=0, (h > h2 h2, h2 > f f)'
    avgtau = ctau
except KeyError:
    raise RuntimeError('Bad runNumber')

# basename for madgraph LHEF file
rname = 'run_01'

# writing proc card for MG
process ="""
import model """+modelcode+"""
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define f = u c d s u~ c~ d~ s~ b b~ e+ e- mu+ mu- ta+ ta- t t~
"""+process1+""""""
process += """
output -f
"""
# Energy
#---------------------------------------------------------------------------
    
beamEnergy = -999.
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
   raise RuntimeError("No center of mass energy found")
safefactor=1.1 #generate extra 10% events in case any fail showering
if runArgs.maxEvents > 0:
    nevents = runArgs.maxEvents*safefactor
else: nevents = 5000*safefactor
#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
run_card_extras = { 
    'lhe_version':'3.0',
    'cut_decays':'F',
    'event_norm':'sum',
    'nevents':int(nevents),
    'ptj':'0',
    'ptb':'0',
    'pta':'0',
    'ptl':'0',
    'etaj':'-1',
    'etab':'-1',
    'etaa':'-1',
    'etal':'-1',
    'drjj':'0',
    'drbb':'0',
    'drll':'0',
    'draa':'0',
    'drbj':'0',
    'draj':'0',
    'drjl':'0',
    'drab':'0',
    'drbl':'0',
    'dral':'0' ,
    }

process_dir = new_process(process)
modify_run_card(process_dir=process_dir,
               runArgs=runArgs,
               settings=run_card_extras)

#---------------------------------------------------------------------------
# MG5 param Card
#---------------------------------------------------------------------------

if mH <= 125: 
   param_card_extras = { 
        "HIDDEN": { 'epsilon': '1e-10', #kinetic mixing parameter
                    'kap': '1e-4', #higgs mixing parameter
                    'mhsinput':mS, #dark higgs mass
                    'mzdinput': '1.000000e+03' # Z' mass
                    }, 
        "HIGGS": { 'mhinput':mH}, #higgs mass
        #auto-calculate decay widths and BR of Zp, H, t, hs
        "DECAY": { 'wzp':'Auto', 'wh':'Auto', 'wt':'Auto', 'whs':'Auto'} 
        }
elif mH > 125:
   param_card_extras = { 
        "HIDDEN": { 'epsilon': '1e-10', #kinetic mixing parameter
                    'kap': '1e-4', #higgs mixing parameter
                    'mhsinput':mS, #dark higgs mass
                    'mzdinput': '1.000000e+03' # Z' mass
                    }, 
        "HIGGS": { 'mhinput':mH}, #higgs mass
        #auto-calculate decay widths and BR of Zp, H, t, hs
        "DECAY": { 'wzp':'5', 'wh':'5', 'wt':'Auto', 'whs':'5'} 
   }


modify_param_card(process_dir=process_dir,params=param_card_extras)

print_cards()

#---------------------------------------------------------------------------
# MG5 Generation
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)

import random
random.seed(runArgs.randomSeed)
# lifetime function
def lifetime(avgtau = 21):
    import math
    t = random.random()
    return -1.0 * avgtau * math.log(t)
print (process_dir)
# replacing lifetime of scalar, manually

unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
unzip1.wait()
    
oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')
init = True
for line in oldlhe:
    if init==True:
        newlhe.write(line)
        if '</init>' in line:
            init = False
    else:  
        if 'vent' in line or line.startswith("<"):
            newlhe.write(line)
            continue
        newline = line.rstrip('\n')
        columns = (' '.join(newline.split())).split()
        pdgid = int(columns[0])
        if pdgid == 35:
            part1 = line[:-22]
            part2 = "%.11E" % (lifetime(avgtau))
            part3 = line[-12:]
            newlhe.write(part1+part2+part3)
        else:
            newlhe.write(line)

oldlhe.close()
newlhe.close()
zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
zip1.wait()
shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',
            process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')

arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3)

#---------------------------------------------------------------------------
# Parton Showering Generation
#---------------------------------------------------------------------------

if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print (opts)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]
genSeq.Pythia8.Commands += ["35:tau0 = " + str(lifetime(avgtau))] # pi_d lifetime -- variable

#relax the cuts on displaced vertices and non G4 particles
testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

evgenConfig.description = "Displaced hadronic jets process Higgs > S S with mH={}GeV, mS={}GeV".format(mH, mS)
evgenConfig.keywords = ["exotic", "BSM", "BSMHiggs", "longLived"]
evgenConfig.contact  = ['simon.berlendis@cern.ch', 'hao.zhou@cern.ch',
                        'Cristiano.Alpigiani@cern.ch', 'hrussell@cern.ch' ]
evgenConfig.process="Higgs --> LLPs"
#evgenConfig.inputfilecheck = rname

