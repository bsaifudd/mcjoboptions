#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG gg->4l  'no higgs' production."
evgenConfig.keywords = ["SM", "Higgs"]
evgenConfig.contact = ["andrej.saibel@cern.ch", "guglielmo.frattari@cern.ch"]
evgenConfig.generators     = [ 'Powheg' ]
evgenConfig.nEventsPerJob    = 50

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg bblvlv process
# -----------------------------------------------------------
include("PowhegControl/PowhegControl_gg4l_Common.py")
PowhegConfig.mass_b = 0 
PowhegConfig.proc = "ZZ"
PowhegConfig.contr = "no_h"
PowhegConfig.ubexcess_correct = 0
PowhegConfig.vdecaymodeV1 = 'll'
PowhegConfig.vdecaymodeV2 = 'll'
PowhegConfig.mllmin = 10
PowhegConfig.mllmax = 200
PowhegConfig.m4lmin = 70
PowhegConfig.m4lmax = 3000

#Integration settings
PowhegConfig.ncall1 = 4000
PowhegConfig.itmx1 = 2
PowhegConfig.ncall2 = 3000
PowhegConfig.itmx2 = 2
PowhegConfig.foldcsi = 2
PowhegConfig.foldy = 2
PowhegConfig.foldphi = 5
PowhegConfig.nubound = 75000
PowhegConfig.icsimax = 2
PowhegConfig.iymax = 2
PowhegConfig.xupbound = 2
PowhegConfig.storemintupb = 1
PowhegConfig.fastbtlbound = 1
PowhegConfig.ncall1btlbrn = 50000
PowhegConfig.ncall2btlbrn = 100000
PowhegConfig.manyseeds = 1
PowhegConfig.parallelstage = 4
PowhegConfig.allrad = 1
PowhegConfig.withdamp = 1
PowhegConfig.m4l_sampling = 2
PowhegConfig.massiveloops = 1

PowhegConfig.mu_F = [1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0, 0.5, 2.0]
PowhegConfig.PDF = 260000


# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = -1' ]
