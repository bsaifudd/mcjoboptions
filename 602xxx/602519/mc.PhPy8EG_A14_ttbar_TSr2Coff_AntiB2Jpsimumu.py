#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410450 LHE files with Shower Weights added, recoil to coloured off, at least one lepton filter, Anti-B->Jpsi->mumu filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton', 'Jpsi']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch','mdshapiro@lbl.gov', 'asada@hepl.phys.nagoya-u.ac.jp', 'derue@lpnhe.in2p3.fr']
##evgenConfig.inputfilecheck="410450.Powheg_ttbar_hdamp258p75_LHE"
#evgenConfig.inputfilecheck="TXT" # D.P.: maybe not needed? 
evgenConfig.nEventsPerJob = 10000  # H.A. optional? to be adjusted.
evgenConfig.inputFilesPerJob = 4 # H.A. optional? to be adjusted.

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py') # H.A. updated for JO in 21.6, MC15 Job Options -> Pythia8_i, ref https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GeneratorFilters
include("Pythia8_i/Pythia8_Powheg_Main31.py") # H.A. updated for JO in 21.6, MC15 Job Options -> Pythia8_i, ref https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GeneratorFilters

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

genSeq.Pythia8.Commands += [ 'TimeShower:recoilToColoured = off' ]

#--------------------------------------------------------------
# Special decay of anti-B->Jpsi->mumu
#--------------------------------------------------------------
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles += ['AntiB2Jpsimumu.DEC']
genSeq.EvtInclusiveDecay.userDecayFile = 'AntiB2Jpsimumu.DEC'

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py') # H.A. updated for JO in 21.6, MC15 Job Options -> GeneratorFilters, ref https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GeneratorFilters
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

# apply a J/psi to muons filter
include('GeneratorFilters/TTbarWithJpsimumuFilter.py')
filtSeq.TTbarWithJpsimumuFilter.JpsipTMinCut = 5000.
