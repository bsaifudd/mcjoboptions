# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 tt+jet production with MiNNLO"
evgenConfig.keywords = ["SM", "top", "1jet"]
evgenConfig.contact = ["dominic.hirschbuehl@cern.ch","tpelzer@cern.ch"]
evgenConfig.nEventsPerJob = 1000
evgenConfig.generators = ["Powheg"]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg ttj_MiNNLO process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_ttj_MiNNLO_Common.py")

# --------------------------------------------------------------
# Settings
# --------------------------------------------------------------
PowhegConfig.PDF = 261000  #list(range(261000, 261101)) 
PowhegConfig.ncall1 = 200000
PowhegConfig.ncall2 = 400000
PowhegConfig.nubound = 100000
PowhegConfig.use_OLP_interface = 1
PowhegConfig.whichscale=5

# define the decay mode
PowhegConfig.decay_mode = "t t~ > all [MadSpin]"

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

