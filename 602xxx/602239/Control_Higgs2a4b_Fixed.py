#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)

    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

###Run Number Encoding and decoding
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
print(get_physics_short())
tokens = get_physics_short().replace(".py","").replace("p",".").split('_')

ma=float(tokens[-2].split('ma')[-1])
ctau=float(tokens[-1].split('ctau')[-1])

print('#############################################################')
print('ma='+str(ma))
print('This file is edited in /FilesCondorNeeds.')
print('ctau='+str(ctau))
print('#############################################################')

#################################################
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
#Assumption taken from https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/blob/master/common/Powheg/PowhegPythia8EvtGenControl_BSM_VBFH_aa_2mu2tau.py
# and https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/600xxx/600070/mc.PhPy8_VBF_H125_yyv_myv0_MET75.py

prodMode = tokens[-3].replace("125", "").replace(".","p") # p replacement is for WpH, all others should be fine 

nFinalPerProdMode = {
    'ggH': 2,
    'VBF': 3, #was VBFH
    'WpH': 3,
    'WmH': 3,
    'ttH': 3,
    'ttHsemilep':3,
    'ttHdilep':3,
    'ttHallhad':3,
    'ZH': 3, 
}

genSeq.Pythia8.Commands += [ 'Powheg:NFinal = %d' % nFinalPerProdMode[prodMode] ]


#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',

                            '35:m0 = 125',
                            '35:doForceWidth = on',
                            '35:mWidth = 0.00407',
                            '35:oneChannel = 1 1.0 100 70 70',


                            '70:new = S S 1 0 0 %.1f' % ma,#scalar mass
                            '70:tau0 = %.1f' % ctau,#nominal proper lifetime (in mm/c)
                            '70:isResonance = 0',
                            '70:oneChannel = 1 1.0 0 5 -5',
    
                            #We want Pythia to treat SM LLPs as stable and to allow GEANT4 to take over and decay them
                            #We want Pythia to decay the signal LLPs though as they don't interact with GEANT4 and GEANT4 doesn't know how to decay them
                            #Usually any particle with a tau0>1000mm is stable for Pythia, and this would include the signal LLP (from Marjorie Shapiro), so need to artificially augment that limit to 100000.0. But then the SM LLPs would all be decayed by Pythia, so need to manually tell Pythia to keep those stable
                            # The list of those are in https://gitlab.cern.ch/atlas/athena/-/blob/21.6.46-patches/Generators/EvtGen_i/share/file/2014Inclusive_17.dec
                            #Search "# Decay"
    
                            # K_S0, Lambda0, anti-Lambda0,  Sigma+, Sigma-, anti-Sigma-. , anti-Sigma+, Xi0, anti-Xi0, Xi-, anti-Xi+, Omega-, anti-Omega+
                            # ignore vpho (doesn't exist) and B_c (recayed further down)
                            # Pythia already makes pi+/-, K+/-, and n/nbar stable so don't need to include (not certain so am including in case)
    
                            # K_S0
                            '310:mayDecay = false',
                            '-310:mayDecay = false',
    
                            # Lambda0
                            '3122:mayDecay = false',
                            '-3122:mayDecay = false',
                                
                            # Sigma+
                            '3222:mayDecay = false',
                            '-3222:mayDecay = false',
    
                            # Sigma-
                            '3112:mayDecay = false',
                            '-3112:mayDecay = false',
                                    
                            # Xi0
                            '3322:mayDecay = false',
                            '-3322:mayDecay = false',
    
                            # Xi-
                            '3312:mayDecay = false',
                            '-3312:mayDecay = false',
    
                            # Omega-
                            '3334:mayDecay = false',
                            '-3334:mayDecay = false',
    
    #################################################################
                            # mu
                            '13:mayDecay = false',
                            '-13:mayDecay = false',
                            
                            # pi
                            '211:mayDecay = false',
                            '-211:mayDecay = false',
            
                            #K
                            '321:mayDecay = false',
                            '-321:mayDecay = false',
    
                            #K0L
                            '130:mayDecay = false',
                            '-130:mayDecay = false',
    
                            #n
                            '2112:mayDecay = false',
                            '-2112:mayDecay = false',
    
                            #mu^+-, pi^+-, K^+-, K^0_L and n/nbar
                            
]

genSeq.Pythia8.Commands = [i for i in genSeq.Pythia8.Commands if ("tau0Max" not in i)]
genSeq.Pythia8.Commands += [
    'ParticleDecays:tau0Max = 100000.0',
]

print(genSeq.Pythia8.Commands)

testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.contact  = ["david.rousso@cern.ch"]
evgenConfig.keywords += [ 'SUSY','Higgs','simplifiedModel']
evgenConfig.description = "POWHEG+Pythia8 H->2a->4b production"
