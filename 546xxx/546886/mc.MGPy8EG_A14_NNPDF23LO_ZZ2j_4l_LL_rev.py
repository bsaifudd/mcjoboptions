evgenConfig.nEventsPerJob = 10000

# ZZ inclusive or ZZjj_EW
isZZjj = False
# polarization states: TT, TL, LL
polStat = "LL"
# number of jets
njets = 2

include("MadGraphControl_Pythia8EvtGen_ZZ2jets_pol_rev.py")
