params={}
params['mZDinput']  = 6.000000e-00
params['MHSinput']  = 1.500000e+01
params['epsilon']   = 1.000000e-03
params['gXmu']      = 9.000000e-04
params['gXe']       = 9.000000e-04
params['gXpi']      = 1.000000e-11
params['MChi1']     = 4.000000e+00
params['MChi2']     = 2.500000e+01
params['WZp']       = "DECAY 3000001 1.000000e-03 # WZp"
params['mH']        = 125
params['nGamma']    = 2
params['decayMode'] = 'normal'
params['avgtau'] = 3




                

include ("MadGraphControl_A14N23LO_FRVZdisplaced_ggF.py")
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]

                    

