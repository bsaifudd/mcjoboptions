import re
import os
import math
import subprocess

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

nevents=runArgs.maxEvents
LHE_EventMultiplier = 4.0
if LHE_EventMultiplier > 0 :
    nevents=runArgs.maxEvents*LHE_EventMultiplier



if el_or_mu == 1 and full_or_int_or_np == 1 :
    my_process = """
    import model bbll_UFO --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e+ e- NP^2>0 @0
    add process p p > e+ e- j NP^2>0 @1
    add process p p > e+ e- j j NP^2>0 @2
    output -f"""
elif el_or_mu == 1 and full_or_int_or_np == 2 :
    my_process = """
    import model bbll_UFO --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e+ e- NP^2==2 @0
    add process p p > e+ e- j NP^2==2 @1
    add process p p > e+ e- j j NP^2==2 @2
    output -f"""
elif el_or_mu == 1 and full_or_int_or_np == 3 :
    my_process = """
    import model bbll_UFO --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e+ e- NP^2==4 @0
    add process p p > e+ e- j NP^2==4 @1
    add process p p > e+ e- j j NP^2==4 @2
    output -f"""
elif el_or_mu == 2 and full_or_int_or_np == 1 :
    my_process = """
    import model bbll_UFO --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu+ mu- NP^2>0 @0
    add process p p > mu+ mu- j NP^2>0 @1
    add process p p > mu+ mu- j j NP^2>0 @2
    output -f"""
elif el_or_mu == 2 and full_or_int_or_np == 2 :
    my_process = """
    import model bbll_UFO --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu+ mu- NP^2==2 @0
    add process p p > mu+ mu- j NP^2==2 @1
    add process p p > mu+ mu- j j NP^2==2 @2
    output -f"""
elif el_or_mu == 2 and full_or_int_or_np == 3 :
    my_process = """
    import model bbll_UFO --modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu+ mu- NP^2==4 @0
    add process p p > mu+ mu- j NP^2==4 @1
    add process p p > mu+ mu- j j NP^2==4 @2
    output -f"""



mmll = 110.0


beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(my_process)
extras = {'ickkw'      : '0',
          'ptj'   : '20.0',
          'mmll'   : mmll,
          'xqcut'   : '0.0',
          'drjj'   : '0.4',
          'ktdurham'   : '300.0',
          'pdgs_for_merging_cut': '1, 2, 3, 4, 5, 21',
          'nevents'   : nevents,
          'maxjetflavor': '5',
          'asrwgtflavor': '5'}



couplings = {'lambdas': str(Lambda),
          'cmubbll'      : str(cmubbll),
          'cmubbrl'      : str(cmubbrl),
          'cmubbrr'   : str(cmubbrr),
          'cmubblr'   : str(cmubblr),
          'celbbll'   : str(celbbll),
          'celbbrl'   : str(celbbrl),
          'celbbrr'   : str(celbbrr),
          'celbblr': str(celbblr)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

params={}
params['DIM6']=couplings
modify_param_card(process_dir=process_dir,params=params)



print_cards()


generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir = process_dir, runArgs = runArgs, lhe_version=3, saveProcDir=False)


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

PYTHIA8_nJetMax=2
PYTHIA8_Dparameter=0.4
PYTHIA8_TMS=300
PYTHIA8_nQuarksMerge=5
PYTHIA8_Process="guess"                                                                                                                                                       

include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']


evgenConfig.description = 'Production of bbll EFT'
evgenConfig.keywords += ['BSM', 'exotic']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> llb'
evgenConfig.tune = 'A14 NNPDF23LO'
evgenConfig.contact = ["Yoav Afik <yafik@cern.ch>"]

