from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

nameModel=None
nameZp=None 
nameMassParamZp=None
nameWidthParamZp=None
nameMassParamhD=None
nameWidthParamhD=None
nameMassParamN2=None
nameWidthParamN2=None
codeZp=None
nameZpH=None 
nameMassParamZpH=None
nameWidthParamZpH=None
codeZpH=None
mg_proc=None
process=None

if model=='LightVector':
    nameZp='vx'
    nameMassParamZp='MVdark'
    nameWidthParamZp='WVdark'
    nameMassParamN2='MX2'
    nameWidthParamN2='Wchip'
    nameModel=model
    codeZp=56
elif model=='InelasticVectorEFT':
    nameZp='vx'
    nameMassParamZp='MVdark'
    nameWidthParamZp='WVdark'
    nameMassParamN2='MX2'
    nameWidthParamN2='Wchip'
    nameModel=model
    codeZp=56
    nameZpH='Zp' 
    nameMassParamZpH='MZp'
    nameWidthParamZpH='WZp'
    codeZpH=57
elif model=='darkHiggs':
    nameZp='zp'
    nameMassParamZp='MZp'
    nameWidthParamZp='MZp'
    nameMassParamhD='MHD'
    nameWidthParamhD='WhD'
    nameModel=model
    codeZp=56
else:
    raise RuntimeError("Unknown model.")

if model=='LightVector':
    if fs == "ee": 
        mg_proc=[
            "define p = p b b~",
            "generate p p > %s n1 n1, %s > e+ e-"%(nameZp,nameZp)] 
        process='pp>(%s>e+e-)n1n1'%nameZp
    elif fs == "mumu": 
        mg_proc=[
            "define p = p b b~",
            "generate p p > %s n1 n1, %s > mu+ mu-"%(nameZp,nameZp)] 
        process='pp>(%s>mu+mu-)n1n1'%nameZp
elif model=='InelasticVectorEFT':
    if fs == "ee": 
        mg_proc=[
            "define p = p b b~",
            "generate p p > %s n1 n1, %s > e+ e-"%(nameZp,nameZp)] 
        process='pp>(%s>e+e-)n1n1'%nameZp
    elif fs == "mumu": 
        mg_proc=[
            "define p = p b b~",
            "generate p p > %s n1 n1, %s > mu+ mu-"%(nameZp,nameZp)] 
        process='pp>(%s>mu+mu-)n1n1'%nameZp
elif model=='darkHiggs':
    if fs == "ee": 
        mg_proc=[
            "define p = p b b~",
            "generate p p > hd %s, hd > n1 n1, %s > e+ e-"%(nameZp,nameZp)]
        process='pp>(hd>n1n1)(%s>e+e-)'%nameZp
    elif fs == "mumu": 
        mg_proc=[
            "define p = p b b~",
            "generate p p > hd %s, hd > n1 n1, %s > mu+ mu-"%(nameZp,nameZp)]
        process='pp>(hd>n1n1)(%s>mu+mu-)'%nameZp

mgproc = """
import model %s

%s

output -f
"""%(nameModel,'\n'.join(mg_proc))

#---------------------------------------------------------------------------
# Number of events to generate
#---------------------------------------------------------------------------
LHE_EventMultiplier = 2.5
evgenConfig.nEventsPerJob = int(runArgs.maxEvents)
nevents=evgenConfig.nEventsPerJob
if LHE_EventMultiplier>0:
  nevents=runArgs.maxEvents*LHE_EventMultiplier

process_dir = new_process(process=mgproc)
bwcut = 15.0
if model=='darkHiggs': bwcut = 250.0

settings = {#'pdlabel'    : "'lhapdf'", 
          #'lhaid'      : 247000,
          #'sys_pdf'    : 'NNPDF23_lo_as_0130_qed'
          'xqcut' : 0,
          #'nevents' : int(runArgs.maxEvents*2./filteff),
          'nevents' : nevents,
          'bwcutoff': bwcut, 
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

if model=='LightVector' or model=='InelasticVectorEFT':
    masses = {'1000022':mDM1,
            '1000023':mDM2,
            str(codeZp):mZp}
    decays={str(codeZp): '%s # W%s'%('Auto', nameWidthParamZp), '1000023': '%s # W%s'%('Auto', nameWidthParamN2)} 
    Parameters={'mass': masses, 'DECAY': decays}
    modify_param_card(param_card_input='MadGraph_param_card_%sModified_lep.dat'%nameModel, process_dir=process_dir, params=Parameters)
elif model=='darkHiggs':
    masses={'1000022':mDM1,
            str(codeZp):mZp,
            '26':mHD}
    decays={str(codeZp): '%s # W%s'%('Auto', nameWidthParamZp), '26':'%s # W%s'%('Auto', nameWidthParamhD)}
    Parameters={'mass' : masses, 'DECAY' : decays}
    modify_param_card(param_card_input='MadGraph_param_card_%sModified_lep.dat'%nameModel, process_dir=process_dir, params=Parameters)



generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs)

evgenConfig.description = "Mono Zprime sample - model %s"%nameModel
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.process = process
evgenConfig.contact = ["Jared Little <jared.little@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += ["1000022:all = chid chid~ 2 0 0 %d 0" %(mDM1),
                            "1000022:isVisible = false"]

#include("GeneratorFilters/MissingEtFilter.py")
#filtSeq.MissingEtFilter.METCut = 50*GeV

