import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import math
import os, sys
from subprocess import call
from shutil import copy

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


nevents=int(3.0*runArgs.maxEvents)

model_pars_str = str(jofile) 
proc=int(0)
mass_vlq=int(0)
mass_vll=int(0)

for s in model_pars_str.split("_"):
    print('jobConfig fragment used to extract the model configuration '+s)
    if 'mvlq' in s:
        ss=s.replace("mvlq","")  
        if ss.isdigit(): mass_vlq = ss # GeV 
    if 'mvll' in s:
        ss=s.replace("mvll","")  
        if ss.isdigit(): mass_vll = ss # GeV
    if 'proc' in s:
        ss=s.replace("proc","")
        ss = ss.split(".")
        proc = ss[0]

mass_umu = 3500   # GeV
mass_zp  = 100000 # GeV

pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""
-42
42
-43
43
-44
44
-61
61
-62
62
-71
71
-72
72
""")
pdgfile.close()


process_string=str(0)

# Need to set all masses and couplings via MadGraph since widths need to be calcualted here
# It was not possible to get the correct orser to moddify proc_cards twice for masses and width 
if proc == "EE":
    process_string = """
import model vector_LQ_UFO_VLL --modelname
generate  p p > le+ le- QED=2 NP=0, le- > vt b t~, le+ > vt~ b~ t @1
add process p p > le+ le- QED=2 NP=0, le- > ta- b b~, le+ > vt~ b~ t @2
add process p p > le+ le- QED=2 NP=0, le- > vt b t~, le+ > ta+ b~ b @2
add process p p > le+ le- QED=2 NP=0, le- > ta- b b~, le+ > ta+ b~ b @3
output -f

launch 

set MVLQ {}
set MLe  {}
set MLn  {}
set MQu  {}
set MQd  {}
set MGp  5000
set MZp  {}

set gU          3.0
set betaL33     1.0
set betaRd33    0.0
set betaL23     0.0
set betaL32     0.0
set kappaU      0.0
set kappaUtilde 0.0
set betaLQL     1.0
set betaRQL     1.0
set betaL3L    -0.5
set betaLQ3     0.5
set betaL2L     0.0
set betaLQ2     0.0

set gZp         0.0

set wvlq Auto
compute_widths le- ln --body_decay=3.01
""".format(mass_umu, mass_vll, mass_vll, mass_vlq, mass_vlq, mass_zp)
elif proc == "EN" or proc == "NE":
    process_string = """
import model vector_LQ_UFO_VLL --modelname
generate  p p > le+ ln QED=2 NP=0, le+ > vt~ b~ t, ln > t t~ vt @1
add process p p > le+ ln QED=2 NP=0, le+ > ta+ b~ b, ln > t t~ vt @2
add process p p > le+ ln QED=2 NP=0, le+ > vt~ b~ t, ln > t b~ ta- @2
add process p p > le+ ln QED=2 NP=0, le+ > ta+ b~ b, ln > t b~ ta- @3
add process p p > le- ln~ QED=2 NP=0, le- > vt b t~, ln~ > t t~ vt~ @1
add process p p > le- ln~ QED=2 NP=0, le- > ta- b b~, ln~ > t t~ vt~ @2
add process p p > le- ln~ QED=2 NP=0, le- > vt b t~, ln~ > t~ b ta+ @2
add process p p > le- ln~ QED=2 NP=0, le- > ta- b b~, ln~ > t~ b ta+ @3
output -f

launch 

set MVLQ {}
set MLe  {}
set MLn  {}
set MQu  {}
set MQd  {}
set MGp  5000
set MZp  {}

set gU          3.0
set betaL33     1.0
set betaRd33    0.0
set betaL23     0.0
set betaL32     0.0
set kappaU      0.0
set kappaUtilde 0.0
set betaLQL     1.0
set betaRQL     1.0
set betaL3L    -0.5
set betaLQ3     0.5
set betaL2L     0.0
set betaLQ2     0.0

set gZp         0.0

set wvlq Auto
compute_widths le- ln --body_decay=3.01
""".format(mass_umu, mass_vll, mass_vll, mass_vlq, mass_vlq, mass_zp)
elif proc == "NN":
    process_string = """
import model vector_LQ_UFO_VLL --modelname
generate  p p > ln ln~ QED=2 NP=0, ln > t t~ vt @1
add process p p > ln ln~ QED=2 NP=0, ln > t t~ vt , ln~ > t~ b ta+ @2
add process p p > ln ln~ QED=2 NP=0, ln > t b~ ta- @3
output -f

launch 

set MVLQ {}
set MLe  {}
set MLn  {}
set MQu  {}
set MQd  {}
set MGp  5000
set MZp  {}

set gU          3.0
set betaL33     1.0
set betaRd33    0.0
set betaL23     0.0
set betaL32     0.0
set kappaU      0.0
set kappaUtilde 0.0
set betaLQL     1.0
set betaRQL     1.0
set betaL3L    -0.5
set betaLQ3     0.5
set betaL2L     0.0
set betaLQ2     0.0

set gZp         0.0

set wvlq Auto
compute_widths le- ln --body_decay=3.01
""".format(mass_umu, mass_vll, mass_vll, mass_vlq, mass_vlq, mass_zp)
else:
    print "No such process", proc, ", exiting."
    sys.exit()

process_dir = new_process(process_string)
print_cards() # Prints them somewhe

copy(process_dir+'/Cards/param_card.dat', 'param_card.dat') # Must be used here

# Set up the run card with mostly default values
run_card_extras = {
            'lhe_version':'3.0',
            # 'pdlabel':"'lhapdf'",
            # 'lhaid':' 260000',
            'nevents' :int(nevents),
}

# Adds definitions and process to generate, computes widths at process_dir/Cards
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_card_extras) 

#

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_MadGraph.py")

# Shower
evgenConfig.description = 'MadGraph5+Pythia8 for Vectorlike leptons of mass {} GeV, inc lepton final state'.format(mass_vll)
evgenConfig.contact = ['Judita Mamuzic <judita.mamuzic@cern.ch>','Merve Nazlim Agaras <merve.nazlim.agaras@cern.ch>']
evgenConfig.keywords += ['exotic', 'multilepton']
evgenConfig.generators += ['MadGraph','Pythia8']
# runArgs.inputGeneratorFile=output_DS

# include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
# include("Pythia8_i/Pythia8_MadGraph.py")
