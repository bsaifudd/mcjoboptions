# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  19:30
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.16432301E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.02624103E+03  # scale for input parameters
    1    4.35537716E+01  # M_1
    2    1.53332519E+03  # M_2
    3    4.59501706E+03  # M_3
   11   -1.05070476E+03  # A_t
   12    7.99187211E+02  # A_b
   13   -5.71614427E+02  # A_tau
   23   -3.78531694E+02  # mu
   25    5.12737701E+01  # tan(beta)
   26    1.09389407E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.07892027E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.95981259E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.23577350E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.02624103E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.02624103E+03  # (SUSY scale)
  1  1     8.38596658E-06   # Y_u(Q)^DRbar
  2  2     4.26007102E-03   # Y_c(Q)^DRbar
  3  3     1.01309212E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.02624103E+03  # (SUSY scale)
  1  1     8.64029611E-04   # Y_d(Q)^DRbar
  2  2     1.64165626E-02   # Y_s(Q)^DRbar
  3  3     8.56846626E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.02624103E+03  # (SUSY scale)
  1  1     1.50779485E-04   # Y_e(Q)^DRbar
  2  2     3.11764155E-02   # Y_mu(Q)^DRbar
  3  3     5.24336021E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.02624103E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.05070480E+03   # A_t(Q)^DRbar
Block Ad Q=  4.02624103E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     7.99187198E+02   # A_b(Q)^DRbar
Block Ae Q=  4.02624103E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -5.71614403E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.02624103E+03  # soft SUSY breaking masses at Q
   1    4.35537716E+01  # M_1
   2    1.53332519E+03  # M_2
   3    4.59501706E+03  # M_3
  21    1.01454795E+06  # M^2_(H,d)
  22    1.37495860E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.07892027E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.95981259E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.23577350E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22293596E+02  # h0
        35     1.09353856E+03  # H0
        36     1.09389407E+03  # A0
        37     1.09072893E+03  # H+
   1000001     1.00977199E+04  # ~d_L
   2000001     1.00739431E+04  # ~d_R
   1000002     1.00973520E+04  # ~u_L
   2000002     1.00773169E+04  # ~u_R
   1000003     1.00977246E+04  # ~s_L
   2000003     1.00739515E+04  # ~s_R
   1000004     1.00973566E+04  # ~c_L
   2000004     1.00773175E+04  # ~c_R
   1000005     3.21316978E+03  # ~b_1
   2000005     3.35808202E+03  # ~b_2
   1000006     3.21538079E+03  # ~t_1
   2000006     5.04158540E+03  # ~t_2
   1000011     1.00207061E+04  # ~e_L-
   2000011     1.00086791E+04  # ~e_R-
   1000012     1.00199384E+04  # ~nu_eL
   1000013     1.00207291E+04  # ~mu_L-
   2000013     1.00087214E+04  # ~mu_R-
   1000014     1.00199605E+04  # ~nu_muL
   1000015     1.00205997E+04  # ~tau_1-
   2000015     1.00274719E+04  # ~tau_2-
   1000016     1.00262745E+04  # ~nu_tauL
   1000021     5.05826342E+03  # ~g
   1000022     4.34503210E+01  # ~chi_10
   1000023     3.88456126E+02  # ~chi_20
   1000025     3.92216607E+02  # ~chi_30
   1000035     1.62848064E+03  # ~chi_40
   1000024     3.87777026E+02  # ~chi_1+
   1000037     1.62844521E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.97076954E-02   # alpha
Block Hmix Q=  4.02624103E+03  # Higgs mixing parameters
   1   -3.78531694E+02  # mu
   2    5.12737701E+01  # tan[beta](Q)
   3    2.42958439E+02  # v(Q)
   4    1.19660424E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99939357E-01   # Re[R_st(1,1)]
   1  2     1.10128273E-02   # Re[R_st(1,2)]
   2  1    -1.10128273E-02   # Re[R_st(2,1)]
   2  2     9.99939357E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.98743433E-01   # Re[R_sb(1,1)]
   1  2     5.01154171E-02   # Re[R_sb(1,2)]
   2  1    -5.01154171E-02   # Re[R_sb(2,1)]
   2  2    -9.98743433E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -2.52830540E-01   # Re[R_sta(1,1)]
   1  2     9.67510578E-01   # Re[R_sta(1,2)]
   2  1    -9.67510578E-01   # Re[R_sta(2,1)]
   2  2    -2.52830540E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.93402979E-01   # Re[N(1,1)]
   1  2     3.86929137E-04   # Re[N(1,2)]
   1  3     1.14175193E-01   # Re[N(1,3)]
   1  4     1.06956858E-02   # Re[N(1,4)]
   2  1    -8.84394056E-02   # Re[N(2,1)]
   2  2    -4.49195093E-02   # Re[N(2,2)]
   2  3    -7.03397273E-01   # Re[N(2,3)]
   2  4    -7.03841591E-01   # Re[N(2,4)]
   3  1     7.29849482E-02   # Re[N(3,1)]
   3  2    -2.89122915E-02   # Re[N(3,2)]
   3  3     7.01474804E-01   # Re[N(3,3)]
   3  4    -7.08357520E-01   # Re[N(3,4)]
   4  1     1.48022972E-03   # Re[N(4,1)]
   4  2    -9.98572064E-01   # Re[N(4,2)]
   4  3     1.13754375E-02   # Re[N(4,3)]
   4  4     5.21750991E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.61021778E-02   # Re[U(1,1)]
   1  2    -9.99870352E-01   # Re[U(1,2)]
   2  1     9.99870352E-01   # Re[U(2,1)]
   2  2    -1.61021778E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -7.38417888E-02   # Re[V(1,1)]
   1  2     9.97269969E-01   # Re[V(1,2)]
   2  1     9.97269969E-01   # Re[V(2,1)]
   2  2     7.38417888E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02860829E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.86888298E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.79862714E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     5.31090802E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.38019859E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.98674294E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.70641618E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01566310E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.65218033E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.05662254E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.06721923E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.79422383E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     9.62319691E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     7.14403002E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.80692204E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.38213389E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.97600749E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.39417404E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     7.20051053E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01144985E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.64987071E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.04815728E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.61788922E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.05803206E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.69282327E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.60672640E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.48849170E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.19460082E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.98968273E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.90645021E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.37085962E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.44082328E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.47233096E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.05915367E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.55758601E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.13484753E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.38024004E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.01202878E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.46876193E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02488290E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.47416311E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02446773E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.38217505E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.99943205E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.46670896E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02065554E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     4.86651786E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01605177E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.92794171E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.45591574E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.05217591E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.16707331E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.86008599E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.31671501E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.50384434E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.23229026E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.87512714E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.75131287E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.42879882E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.35447993E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.31404652E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.46584494E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.77605145E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.50492715E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.23215096E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.56502745E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.25119624E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     1.19510508E-04    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     9.87277287E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.75189304E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.42977856E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.11990750E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.81482602E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.31331365E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.46569817E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.77527558E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.32594470E+02   # ~b_1
#    BR                NDA      ID1      ID2
     9.16800707E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.70107213E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.68300402E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     5.85186010E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.74497109E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.19408668E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     9.72838883E+01   # ~b_2
#    BR                NDA      ID1      ID2
     2.38672698E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.43000309E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.42741323E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     4.45095715E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.88580804E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.98353408E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.15724330E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.57365550E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.67612806E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.74914320E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.75301702E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.55582064E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.51877584E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.75112580E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.40762873E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.83591981E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.30627095E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     8.40513233E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.45821898E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.77564143E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.67620091E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.74906939E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.79139699E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     2.59469663E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.51862821E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.75170552E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.40738780E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     2.86693539E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     7.30553988E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     9.33993349E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.45807296E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.77486546E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.33381225E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.49458265E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     3.37755392E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.35415206E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.38378888E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     5.93823742E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.43513226E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.16407941E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.91230219E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.19506152E+02   # ~t_2
#    BR                NDA      ID1      ID2
     5.05080089E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.29558601E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.31905575E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.23214042E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.60403263E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.47135308E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.00308623E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.43278973E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     1.09722284E-04    2     2000005        37   # BR(~t_2 -> ~b_2 H^+)
     5.00885075E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     6.91359788E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     5.93484042E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     6.91873170E-04    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     2.48749921E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.98574645E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.42299394E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.76301669E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.43591506E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.99121042E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.99220601E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.44046202E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     4.77157176E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     4.40923946E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.98287478E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     4.72449485E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.95298603E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     4.71616004E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.64979352E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     6.14463892E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     6.44792808E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.19963448E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.74710240E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.25260205E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.84434680E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.48295505E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.51654872E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.92876421E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.82485503E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.82485503E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.38477940E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     4.38477940E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     5.29730652E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.29230971E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     5.92131370E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     4.23476560E-03    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     4.38981160E-03    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.26482763E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.37515835E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     7.11397924E-04    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     3.86238777E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     3.53604997E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.09686439E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.18279144E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     5.39720084E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.39720084E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.62720551E+02   # ~g
#    BR                NDA      ID1      ID2
     2.71649415E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     2.71649415E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.62531476E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.62531476E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.63731828E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.63731828E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.43854441E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.43854441E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.78410876E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.25228733E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.25424957E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.25106236E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.25106236E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.25080730E-03   # Gamma(h0)
     2.55341744E-03   2        22        22   # BR(h0 -> photon photon)
     1.38974674E-03   2        22        23   # BR(h0 -> photon Z)
     2.33288135E-02   2        23        23   # BR(h0 -> Z Z)
     2.03511996E-01   2       -24        24   # BR(h0 -> W W)
     8.11662959E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.26638753E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.34256779E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.75402949E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.45305992E-07   2        -2         2   # BR(h0 -> Up up)
     2.81997537E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.12258074E-07   2        -1         1   # BR(h0 -> Down down)
     2.21438947E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.89564263E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     2.28896029E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     3.94959034E+01   # Gamma(HH)
     2.84710412E-08   2        22        22   # BR(HH -> photon photon)
     1.28761915E-08   2        22        23   # BR(HH -> photon Z)
     8.68104486E-07   2        23        23   # BR(HH -> Z Z)
     1.05067844E-06   2       -24        24   # BR(HH -> W W)
     5.62181805E-05   2        21        21   # BR(HH -> gluon gluon)
     9.81103354E-09   2       -11        11   # BR(HH -> Electron electron)
     4.36697946E-04   2       -13        13   # BR(HH -> Muon muon)
     1.26102554E-01   2       -15        15   # BR(HH -> Tau tau)
     3.08703959E-14   2        -2         2   # BR(HH -> Up up)
     5.98661893E-09   2        -4         4   # BR(HH -> Charm charm)
     4.13502865E-04   2        -6         6   # BR(HH -> Top top)
     8.39907596E-07   2        -1         1   # BR(HH -> Down down)
     3.03776534E-04   2        -3         3   # BR(HH -> Strange strange)
     8.47077064E-01   2        -5         5   # BR(HH -> Bottom bottom)
     5.29840088E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     8.34348775E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.23609246E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.14899615E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.23834127E-08   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.98377153E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.87719496E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     6.18694211E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     3.76654666E+01   # Gamma(A0)
     7.31114002E-08   2        22        22   # BR(A0 -> photon photon)
     6.23861503E-08   2        22        23   # BR(A0 -> photon Z)
     7.74420623E-05   2        21        21   # BR(A0 -> gluon gluon)
     9.79169800E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.35837602E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.25855527E-01   2       -15        15   # BR(A0 -> Tau tau)
     3.01968745E-14   2        -2         2   # BR(A0 -> Up up)
     5.85331606E-09   2        -4         4   # BR(A0 -> Charm charm)
     4.53394103E-04   2        -6         6   # BR(A0 -> Top top)
     8.38217256E-07   2        -1         1   # BR(A0 -> Down down)
     3.03165130E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.45402679E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.04414753E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     8.74178075E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.27636802E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.22708593E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.30743985E-08   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     9.92046449E-05   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     4.17582969E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.29012744E-06   2        23        25   # BR(A0 -> Z h0)
     1.15086668E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.31749170E+01   # Gamma(Hp)
     1.11422470E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.76365853E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.34742733E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.20028425E-07   2        -1         2   # BR(Hp -> Down up)
     1.36674035E-05   2        -3         2   # BR(Hp -> Strange up)
     9.18129961E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.80654519E-08   2        -1         4   # BR(Hp -> Down charm)
     2.95517184E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.28570742E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.62247442E-08   2        -1         6   # BR(Hp -> Down top)
     9.85052431E-07   2        -3         6   # BR(Hp -> Strange top)
     8.40080347E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.27485148E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.40874929E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     4.08955859E-06   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.12082672E-06   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.02134184E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.62897816E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.62899950E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99991882E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.88490693E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.80372838E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.02134184E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.62897816E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.62899950E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999957E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.28565471E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999957E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.28565471E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26477378E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.25401458E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.86103450E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.28565471E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999957E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.56544719E-04   # BR(b -> s gamma)
    2    1.58762488E-06   # BR(b -> s mu+ mu-)
    3    3.52568292E-05   # BR(b -> s nu nu)
    4    1.15001077E-15   # BR(Bd -> e+ e-)
    5    4.90972273E-11   # BR(Bd -> mu+ mu-)
    6    8.57053075E-09   # BR(Bd -> tau+ tau-)
    7    3.94925817E-14   # BR(Bs -> e+ e-)
    8    1.68624881E-09   # BR(Bs -> mu+ mu-)
    9    3.07206328E-07   # BR(Bs -> tau+ tau-)
   10    8.49122443E-05   # BR(B_u -> tau nu)
   11    8.77110976E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41682909E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.92753408E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15675002E-03   # epsilon_K
   17    2.28166786E-15   # Delta(M_K)
   18    2.48015547E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29092148E-11   # BR(K^+ -> pi^+ nu nu)
   20   -5.20764175E-16   # Delta(g-2)_electron/2
   21   -2.22644387E-11   # Delta(g-2)_muon/2
   22   -6.30776231E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.36853445E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.26138196E-01   # C7
     0305 4322   00   2    -6.78779051E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.33882934E-01   # C8
     0305 6321   00   2    -7.50185248E-04   # C8'
 03051111 4133   00   0     1.62784551E+00   # C9 e+e-
 03051111 4133   00   2     1.62809087E+00   # C9 e+e-
 03051111 4233   00   2     3.61309281E-04   # C9' e+e-
 03051111 4137   00   0    -4.45053761E+00   # C10 e+e-
 03051111 4137   00   2    -4.44938321E+00   # C10 e+e-
 03051111 4237   00   2    -2.66832210E-03   # C10' e+e-
 03051313 4133   00   0     1.62784551E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62809072E+00   # C9 mu+mu-
 03051313 4233   00   2     3.61280773E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45053761E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44938337E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.66829470E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50510176E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     5.76841010E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50510176E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     5.76847113E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50510176E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     5.78567135E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85813829E-07   # C7
     0305 4422   00   2    -1.31947316E-05   # C7
     0305 4322   00   2    -9.48485816E-07   # C7'
     0305 6421   00   0     3.30475060E-07   # C8
     0305 6421   00   2     3.83658457E-06   # C8
     0305 6321   00   2    -1.64146246E-07   # C8'
 03051111 4133   00   2     3.36288336E-07   # C9 e+e-
 03051111 4233   00   2     1.20260651E-05   # C9' e+e-
 03051111 4137   00   2    -5.19479578E-07   # C10 e+e-
 03051111 4237   00   2    -8.88544631E-05   # C10' e+e-
 03051313 4133   00   2     3.36284007E-07   # C9 mu+mu-
 03051313 4233   00   2     1.20260601E-05   # C9' mu+mu-
 03051313 4137   00   2    -5.19475133E-07   # C10 mu+mu-
 03051313 4237   00   2    -8.88544781E-05   # C10' mu+mu-
 03051212 4137   00   2     1.33701907E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.92086738E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     1.33702088E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.92086738E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     1.33752027E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.92086738E-05   # C11' nu_3 nu_3
