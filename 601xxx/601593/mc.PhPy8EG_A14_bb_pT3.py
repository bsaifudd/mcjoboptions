#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 dib production with A14 NNPDF2.3 tune, b-or-c hadron filter pT>3 GeV"
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet", "bbbar", "bottom"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg", "Pythia8"]

# Born suppression factor 7 GeV
# Filter Efficiency = 0.000800
# Weighted Filter Efficiency = 0.000237
# Born suppression factor 10 GeV
# Filter Efficiency = 0.001500
# Weighted Filter Efficiency = 0.000444
# Born suppression factor 14 GeV
# Filter Efficiency = 0.002763
# Weighted Filter Efficiency = 0.000497
# Born suppression factor 19 GeV
# Filter Efficiency = 0.006700
# Weighted Filter Efficiency = 0.000352
# Born suppression factor 25 GeV
# Filter Efficiency = 0.006100
# Weighted Filter Efficiency = 0.000155

evgenConfig.nEventsPerJob = 10000
filterMulti = 1.5 # put more into the LHE file

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg jj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_bb_Common.py")

# Set Born suppression factor
PowhegConfig.bornsuppfact = 14

# PDF sets as requested by HI group
# 3211600 TUJU21_nlo_208_82
# 904400 EPPS21nlo_CT18Anlo_Pb208
# 30025400 nNNPDF30_nlo_as_0118_A208_Z82
# 103100-103132 nCTEQ15FullNuc_208_82
# 119150--119189 nCTEQ15WZSIH_FullNuc_208_82 - crashes
PowhegConfig.PDF          = [13000, 3211600, 904400, 30025400] + range(103100, 103132+1)
PowhegConfig.rwl_group_events = 100000

PowhegConfig.foldcsi      = 5
PowhegConfig.foldphi      = 5
PowhegConfig.foldy        = 5
PowhegConfig.ncall1       = 100000
PowhegConfig.ncall2       = 100000
PowhegConfig.nubound      = 1000000
PowhegConfig.itmx1        = 5
PowhegConfig.itmx2        = 5

PowhegConfig.nEvents = runArgs.maxEvents*filterMulti if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMulti

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

# faster?
#genSeq.Pythia8.Commands += ["UncertaintyBands:doVariations=off"]

# b/c hadron filter
from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
filtSeq += HeavyFlavorHadronFilter()
HeavyFlavorHadronFilter = filtSeq.HeavyFlavorHadronFilter
HeavyFlavorHadronFilter.CharmPtMin = 3000.
HeavyFlavorHadronFilter.BottomPtMin = 3000.
HeavyFlavorHadronFilter.CharmEtaMax = 5.0
HeavyFlavorHadronFilter.BottomEtaMax = 5.0
HeavyFlavorHadronFilter.RequestCharm = True
HeavyFlavorHadronFilter.RequestBottom = True
HeavyFlavorHadronFilter.Request_cQuark = False
HeavyFlavorHadronFilter.Request_bQuark = False
HeavyFlavorHadronFilter.RequestSpecificPDGID = False
HeavyFlavorHadronFilter.RequireTruthJet = False
