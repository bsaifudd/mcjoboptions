#--------------------------------------------------------------
# Pythia A14 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# H->mumu decay in Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13']

# uses LHE: mc23_13p6TeV.603220.Ph_PDF4LHC21_inclusive_ttH125_HZZ_batch2_LHE.evgen.TXT.e8557
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2muon", "mH125", "ttHiggs" ]
evgenConfig.process     = "ttH H->mumu"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ttH production with A14 NNPDF2.3 tune, H->mumu"
evgenConfig.inputFilesPerJob = 5
evgenConfig.nEventsPerJob    = 100000
evgenConfig.generators  = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.contact        = [ 'Yanlin Liu <yanlin@cern.ch>', 'michiel.jan.veen@cern.ch' ]
