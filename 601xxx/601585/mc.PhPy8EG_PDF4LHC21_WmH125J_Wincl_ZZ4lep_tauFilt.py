#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process = "qq->WmH, H->4l, W->all"
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Wm+jet->4l w. tau filter + all production with A14 NNPDF2.3 tune"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"  ]
evgenConfig.contact     = [ 'guglielmo.frattari@cern.ch' ]
evgenConfig.generators       = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.inputFilesPerJob = 22
evgenConfig.nEventsPerJob    = 10000

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
  genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']
else:
  genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# H->ZZ->4l decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 13 15']

#--------------------------------------------------------------
# HVV Tau filter
#--------------------------------------------------------------
include('GeneratorFilters/xAODXtoVVDecayFilterExtended_Common.py')
filtSeq.xAODXtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.xAODXtoVVDecayFilterExtended.PDGParent = 23
filtSeq.xAODXtoVVDecayFilterExtended.StatusParent = 22
filtSeq.xAODXtoVVDecayFilterExtended.PDGChild1 = [15]
filtSeq.xAODXtoVVDecayFilterExtended.PDGChild2 = [11,13,15]
filtSeq.xAODXtoVVDecayFilterExtended.UseStatusParent = True
