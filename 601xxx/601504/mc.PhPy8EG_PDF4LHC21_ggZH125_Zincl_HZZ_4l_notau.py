#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# uses LHE: mc23_13p6TeV.603206.Ph_PDF4LHC21_ggZH125_Zincl_HZZ_batch1_LHE.evgen.TXT.e8557
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZZ", "4lepton", "mH125", "ZHiggs" ]
evgenConfig.process     = "gg->ZH, Z->inc, H->ZZ->4l (no tau)"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, gg->H+Z, Z->inc, H->ZZ->4l (no tau)"
evgenConfig.inputFilesPerJob = 50
evgenConfig.nEventsPerJob    = 100000
evgenConfig.generators  = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.contact     = [ 'guglielmo.frattari@cern.ch', 'michiel.jan.veen@cern.ch' ]

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
	genSeq.Pythia8.Commands  += ['Powheg:NFinal = 2']
else:
	genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# H->ZZ->4l decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',    # decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 11 11',
                             '23:onIfMatch = 13 13']
