# EVGEN configuration
# Note:  These JO are designed to run Powheg and make an LHE file and to not run a showering
# generator afterwards.  Because the current Generate_tf.py requires an output file, we
# need to fake it a bit.  We therefore will run Pythia8 on the first event in the LHE file.
# Note, sence we do not intend to keep the EVNT file, the JO name doesn't include Pythia8

#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 125.
PowhegConfig.width_H = 0.00407

PowhegConfig.complexpolescheme = 1 # use CPS

#PDF4LHC21 PDF variations (keeping also variation for PDF4LHC15 correlation studies and NNPDF3.0 for correlation with bkg samples if needed)
PowhegConfig.PDF = list(range(93300,93343)) + list(range(90400,90433)) + list(range(260000, 260101)) + [27100] +  [14400] + [331700]
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0, 0.5, 2.0]
PowhegConfig.nEvents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob
PowhegConfig.generate()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG, VBF H->all"
evgenConfig.keywords    = [ "Higgs", "SMHiggs" ]
evgenConfig.contact     = [ 'antonio.de.maria@cern.ch']
evgenConfig.inputconfcheck = "VBFH125_PDF4LHC21"
evgenConfig.generators  = [ 'Powheg' ]
evgenConfig.nEventsPerJob = 11000


