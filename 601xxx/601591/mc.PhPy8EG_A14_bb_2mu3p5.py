#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 dib production with A14 NNPDF2.3 tune, 2mu filter"
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet", "bbbar", "bottom"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg", "Pythia8"]

# Born suppression factor 7 GeV
# Filter Efficiency = 0.000800
# Weighted Filter Efficiency = 0.000237
# Born suppression factor 10 GeV
# Filter Efficiency = 0.001500
# Weighted Filter Efficiency = 0.000444
# Born suppression factor 14 GeV
# Filter Efficiency = 0.002763
# Weighted Filter Efficiency = 0.000497
# Born suppression factor 19 GeV
# Filter Efficiency = 0.006700
# Weighted Filter Efficiency = 0.000352
# Born suppression factor 25 GeV
# Filter Efficiency = 0.006100
# Weighted Filter Efficiency = 0.000155

evgenConfig.nEventsPerJob = 500
filterMulti = 20 # put more into the LHE file
lheMulti = 20    # reshower+filter each LHE multiple times

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg jj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_bb_Common.py")

# Set Born suppression factor to about ~2*minMuonPt*2
PowhegConfig.bornsuppfact = 14

#PowhegConfig.mu_F         = 1.0
#PowhegConfig.mu_R         = 1.0
#PowhegConfig.PDF          = 13000 # CT14nlo central only for testing

# use default factorisation+renormalisation scales
# PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
# PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
# PDF sets as requested by HI group
# 3211600 TUJU21_nlo_208_82
# 904400 EPPS21nlo_CT18Anlo_Pb208
# 30025400 nNNPDF30_nlo_as_0118_A208_Z82
# 103100-103132 nCTEQ15FullNuc_208_82
# 119150--119189 nCTEQ15WZSIH_FullNuc_208_82 - crashes
PowhegConfig.PDF          = [13000, 3211600, 904400, 30025400] + range(103100, 103132+1)
PowhegConfig.rwl_group_events = 100000

PowhegConfig.foldcsi      = 5
PowhegConfig.foldphi      = 5
PowhegConfig.foldy        = 5
PowhegConfig.ncall1       = 100000
PowhegConfig.ncall2       = 100000
PowhegConfig.nubound      = 1000000
PowhegConfig.itmx1        = 5
PowhegConfig.itmx2        = 5

PowhegConfig.nEvents = runArgs.maxEvents*filterMulti if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMulti

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

############################################################
# probably want to duplicate LHE file here for 'reshowering'
############################################################

include("merge_LHE.py")
import glob, os
inputLHE = glob.glob('*.events')
copies = lheMulti * inputLHE
outputlhe = "replicatedLHE.lhe.events"
merge_lhe_files(copies,outputlhe)
# change LHE file under foot to satisfy Gen_tf&logParser
# this is not totally clean code, as it depents on exactly one file being replicated
os.remove(inputLHE[0])
os.rename(outputlhe, inputLHE[0])


#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

# faster?
#genSeq.Pythia8.Commands += ["UncertaintyBands:doVariations=off"]

# muon filter
include("GeneratorFilters/MultiMuonFilter.py")
MultiMuonFilter = filtSeq.MultiMuonFilter
MultiMuonFilter.Ptcut = 3500.
MultiMuonFilter.Etacut = 2.7
MultiMuonFilter.NMuons = 2
