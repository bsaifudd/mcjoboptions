#--------------------------------------------------------------
# Pythia8 showering setup
#--------------------------------------------------------------
# initialize Pythia8 generator configuration for showering


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#--------------------------------------------------------------
# Edit merged LHE file to remove problematic lines
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#disable Dalitz decays (H->yy*->yll)
genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]

# configure Pythia8
genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.5 100 5 -5 ",  # bb decay
                             "25:addChannel = on 0.5 100 22 22 "] # yy decay

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]


#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["Powheg", "Pythia8"]
evgenConfig.description    = "SM diHiggs production, decay to bbbb, with Powheg-Box-V2, at NLO + full top mass."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "bbbar","bottom"]
evgenConfig.contact        = ['Giulia Di Gregorio <giulia.di.gregorio@cern.ch>, Giovanni Marchiori <giovanni.marchiori@cern.ch>, Keerthi Nakkalill <keerthi.nakkalil@cern.ch>']
evgenConfig.nEventsPerJob  = 10000
evgenConfig.maxeventsfactor = 1.0
evgenConfig.inputFilesPerJob = 55


#---------------------------------------------------------------------------------------------------
# Filter for bbyy
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq.Expression = "HbbFilter and HyyFilter"
