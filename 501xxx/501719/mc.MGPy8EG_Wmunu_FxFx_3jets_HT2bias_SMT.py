evgenConfig.description = 'aMcAtNlo Wmunu+0,1,2,3j NLO FxFx HT2-biased SMT Filter'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch"]
evgenConfig.keywords += ['SM', 'W', 'muon', 'jets', 'NLO']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# FxFx match+merge eff: 30.2%
# SMT+lepton filter eff: ~1.4%
# one LHE file contains 55000 events, taken up to 3 times

evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob = 500

include("merge_LHE.py")
import glob
copies = 3 * glob.glob('*.events')
outputlhe = "replicatedLHE.lhe.events"
merge_lhe_files(copies,outputlhe)

#### Shower: Py8 with A14 Tune, with modifications to make it simil-NLO                                                 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)
PYTHIA8_nJetMax=3
PYTHIA8_qCut=20.
print "PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax
print "PYTHIA8_qCut = %i"%PYTHIA8_qCut

include("Pythia8_i/Pythia8_FxFx_A14mod.py")

genSeq.Pythia8.LHEFile = outputlhe

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
filtSeq += MuonFilter("MFilter")
filtSeq.MFilter.Ptcut = 15000.
filtSeq.MFilter.Etacut = 2.7
 
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("SoftMuonFilterMuchan")
filtSeq.SoftMuonFilterMuchan.Ptcut = 3250.
filtSeq.SoftMuonFilterMuchan.Etacut = 2.7
filtSeq.SoftMuonFilterMuchan.NMuons = 2
 
filtSeq.Expression="MFilter and SoftMuonFilterMuchan"
genSeq.EvtInclusiveDecay.whiteList.remove(-20433)
genSeq.EvtInclusiveDecay.whiteList.remove(20433)


