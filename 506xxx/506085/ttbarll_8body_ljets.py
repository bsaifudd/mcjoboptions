from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

# PDF base fragment
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

evgenConfig.nEventsPerJob=30000

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

doProc=True
gridpack=True

# we will assume diagonal CKM

shortname = get_physics_short()
if 'ljets_ee' in shortname:
    dilstring = ['e+ e-']
    dilshort = 'ee'
elif 'ljets_mm' in shortname:
    dilstring = ['mu+ mu-']
    dilshort = 'mm'
elif 'ljets_tt' in shortname:
    dilstring = ['ta+ ta-']
    dilshort = 'tt'
else:
    raise ValueError("Can't figure out what to generate")

coreprocess = ""
for dil in dilstring:
    coreprocess += """
add process p p > t t~ > e+ ve b %(dil)s d u~ b~ / h QCD=2 QED=6
add process p p > t t~ > e+ ve b %(dil)s s c~ b~ / h QCD=2 QED=6
add process p p > t t~ > mu+ vm b %(dil)s d u~ b~ / h QCD=2 QED=6
add process p p > t t~ > mu+ vm b %(dil)s s c~ b~ / h QCD=2 QED=6
add process p p > t t~ > ta+ vt b %(dil)s d u~ b~ / h QCD=2 QED=6
add process p p > t t~ > ta+ vt b %(dil)s s c~ b~ / h QCD=2 QED=6
add process p p > t t~ > e- ve~ b~ %(dil)s d~ u b / h QCD=2 QED=6
add process p p > t t~ > e- ve~ b~ %(dil)s s~ c b / h QCD=2 QED=6
add process p p > t t~ > mu- vm~ b~ %(dil)s d~ u b / h QCD=2 QED=6
add process p p > t t~ > mu- vm~ b~ %(dil)s s~ c b / h QCD=2 QED=6
add process p p > t t~ > ta- vt~ b~ %(dil)s d~ u b / h QCD=2 QED=6
add process p p > t t~ > ta- vt~ b~ %(dil)s s~ c b / h QCD=2 QED=6
""" % { 'dil': dil }

coreprocess.replace('add process', 'generate', 1)

process="""
import model sm-lepton_masses
# in the SM model CKM matrix is diagonal
define qu = u c
define qd = d s
define qub = u~ c~
define qdb = d~ s~
%s
output -f""" % (coreprocess,)

import os
process_dir = new_process(process) if doProc else os.path.join(os.getcwd(), 'PROC_sm-lepton_masses_0')

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0',
             'cut_decays':'T',
             'ptj':'0.0',
             'ptl':'0.0',
             'etaj':'-1.0',
             'etab':'-1.0',
             'etal':'-1.0',
             'drjj':'0.0',
             'drll':'0.0',
             'drjl':'0.1',
             'mmll':'1.0',
             'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_config_card(process_dir=process_dir,settings={'cluster_type':'condor'})

generate(process_dir=process_dir,runArgs=runArgs, grid_pack=gridpack)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  
runArgs.inputGeneratorFile=outputDS

evgenConfig.generators = ["MadGraph"]

############################
evgenConfig.description = 'MadGraph ttbar+%s 8 body LO production, lepton+jets' % dilshort
evgenConfig.keywords+=['top','ttbar','ttZ']
evgenConfig.contact=['peter.onyisi@cern.ch']
check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

