include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

splitConfig = jofile.rstrip('.py').split('_')

masses['2000006'] = float(splitConfig[4])
masses['1000006'] = float(splitConfig[5])
masses['1000022'] = float(splitConfig[6])
if masses['1000022']<0.5: masses['1000022']=0.5
gentype = str(splitConfig[2])
decaytype = str(splitConfig[3])

#--------------------------------------------------------------
# MadGraph options
# 
bwcutoff = 15
run_settings['bwcutoff']=bwcutoff # to allow very low-mass W* and Z*
run_settings['event_norm']='sum'
#run_settings['use_syst']='F'

#MadGraph 2.6.X uses 5-flav for merging
#force 4-flav merging as use a 4-flav scheme
run_settings['pdgs_for_merging_cut']='1, 2, 3, 4, 21, 1000001, 1000002, 1000003, 1000004, 1000021, 2000001, 2000002, 2000003, 2000004'

process = '''
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~ g

generate p p > t2 t2~ $ go susylq susylq~ b1 b2 t1 b1~ b2~ t1~ @1
add process p p > t2 t2~ j $ go susylq susylq~ b1 b2 t1 b1~ b2~ t1~ @2
add process p p > t2 t2~ j j $ go susylq susylq~ b1 b2 t1 b1~ b2~ t1~ @3
'''
njets = 2
evt_multiplier = 700

#set up decays
stop2_decay = {'2000006': '''DECAY   2000006     2.94242966E-02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000006        25   # BR(~t_2 -> ~t_1    h )
#
'''}
stop1_decay = {'1000006': '''DECAY   1000006     1.34259598E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.208525E-01      4     1000022         5         2        -1 # BR(~t_1 -> chi_10 b u dbar)
     1.74404E-02       4     1000022         5         4        -1 # BR(~t_1 -> chi_10 b c dbar)
     1.74639E-02       4     1000022         5         2        -3 # BR(~t_1 -> chi_10 b u sbar)
     3.201099E-01      4     1000022         5         4        -3 # BR(~t_1 -> chi_10 b c sbar)
     5.3E-06           4     1000022         5         2        -5 # BR(~t_1 -> chi_10 b u bbar)
     5.993E-04         4     1000022         5         4        -5 # BR(~t_1 -> chi_10 b c bbar)
     1.078E-01         4     1000022         5       -11        12 # BR(~t_1 -> chi_10 b e+ nu)
     1.078E-01         4     1000022         5       -13        14 # BR(~t_1 -> chi_10 b mu+ nu)
     1.078E-01         4     1000022         5       -15        16 # BR(~t_1 -> chi_10 b tau+ nu)
#
'''}
decays.update(stop1_decay)
decays.update(stop2_decay)

# Debug the SM higgs mass/branching ratio in the default param_card (which uses the values of 110.8GeV higgs)
masses['25'] = 125.00


evgenLog.info('Registered generation of stop2 pair production, stop2 to Z+stop1, stop1->t+LSP;  decoded into mass point ' + str(masses['2000006']))

#--------------------------------------------------------------
# Madspin configuration
#
if 'MS' in jofile:
    evgenLog.info('Running w/ MadSpin option')
    madspin_card='madspin_card_test.dat'

    mscard = open(madspin_card,'w')

    mscard.write("""#************************************************************
    #*                        MadSpin                           *
    #*                                                          *
    #*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
    #*                                                          *
    #*    Part of the MadGraph5_aMC@NLO Framework:              *
    #*    The MadGraph5_aMC@NLO Development Team - Find us at   *
    #*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
    #*                                                          *
    #************************************************************
    #Some options (uncomment to apply)
    set max_weight_ps_point 400  # number of PS to estimate the maximum for each event   
    #  
    set seed %i 
    set spinmode none 
    # specify the decay for the final state particles

    decay t2 > t1 h01, t1 > n1 fu fd~ b
    decay t2~ > t1~ h01, t1~ > n1 fu~ fd b~
    
    #
    #
    # running the actual code
    launch"""%runArgs.randomSeed)

    mscard.close()

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
filtSeq += DirectPhotonFilter()

filtSeq.DirectPhotonFilter.NPhotons = 2

filtSeq.Expression = "DirectPhotonFilter"

evgenConfig.contact  = [ "elodie.deborah.resseguie@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel', 'stop']
evgenConfig.description = 'stop2 direct pair production, st2->h+st1, st1->t+LSP in simplified model, m_stop2 = %s GeV, m_stop1 = %s GeV, m_N1 = %s GeV'%(masses['2000006'],masses['1000006'],masses['1000022'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{t2,2000006}{t2~,-2000006}"]
