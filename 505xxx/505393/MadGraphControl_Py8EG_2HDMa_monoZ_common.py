import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import re
import shutil

# This control file makes use of UFO v2, registered in https://its.cern.ch/jira/browse/AGENE-1652
# Z's don't contribute because sinbma (sin(beta-alpha)) is 1.0, so excluding them with '/z' reduces runtime for the LHE generation,
# without changing the physics/cross-sections/kinematic ditributions

#----------------------------------------------------------------------------
# Process definition
#----------------------------------------------------------------------------
if initialGluons:
  # For the gluon-gluon fusion production use the 4FS to take into account the b-quark loops
  process="""
  import model Pseudoscalar_2HDM -modelname
  define p = g d u s c d~ u~ s~ c~
  define j = g d u s c d~ u~ s~ c~
  generate g g > xd xd~ l+ l- / h1  [QCD]
  output -f
  """
else:
  # For b-initiated production use 5FS
  process="""
  import model Pseudoscalar_2HDM-bbMET_5FS -modelname
  define p = g d u s c b d~ u~ s~ c~ b~
  define j = g d u s c b d~ u~ s~ c~ b~
  generate p p > xd xd~ l+ l- / h1
  output -f
  """

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else:
  raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of events to generate
#---------------------------------------------------------------------------
nevents=evgenConfig.nEventsPerJob

if runArgs.maxEvents>0:
    nevents = runArgs.maxEvents * multiplier
else:
    nevents *= multiplier
nevents = int(nevents)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents


#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {}
if initialGluons:
  extras = {
        'maxjetflavor'  : 4,
        'asrwgtflavor'  : 4,
        'lhe_version' : '3.0',
        'cut_decays'  : 'F',
        'nevents'       : nevents,
    }
else:
  extras = {
        'maxjetflavor'  : 5,
        'asrwgtflavor'  : 5,
        'lhe_version' : '3.0',
        'cut_decays'  : 'F',
        'nevents'       : nevents,
    }

# Build run_card
process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------

# Build param_card.dat

params={}
## blocks might be modified
dict_blocks={
"MASS": ["MB", "MXD", "MH2", "MH3", "MHC", "MH4"],
"DMINPUTS" : ["GPXD", ],
"FRBLOCK": ["TANBETA", "SINBMA", ],
"HIGGS": ["LAM3", "LAP1", "LAP2", "SINP"],
}

for bl in dict_blocks.keys():
  for pa in dict_blocks[bl]:
    if pa in THDMparams.keys():
      if bl not in params: params[bl]={}
      if pa=="MB":
        params[bl]["5"]=THDMparams[pa]
      else:
        params[bl][pa]=THDMparams[pa]

## auto calculation of decay width
THDMparams_decay={
"25": "Auto",
"35": "Auto",
"36": "Auto",
"37": "Auto",
"55": "Auto",
}

params["decay"]=THDMparams_decay

print("Updating parameters:")
print(params)

modify_param_card(process_dir=process_dir,params=params)

if reweight:
  print(params)
  # Create reweighting card
  reweight_card_loc=process_dir+'/Cards/reweight_card.dat'
  rwcard = open(reweight_card_loc,'w')

  for rw_name in reweights:
    params_rwt = params.copy()
    for param in rw_name.split('-'):
      param_name, value = param.split('_')
      if param_name == "SINP":
        params_rwt['HIGGS']['SINP'] = value
      elif param_name == "TANB":
        params_rwt['FRBLOCK']['TANBETA'] = value

    param_card_reweight = process_dir+'/Cards/param_card_reweight.dat'
    #shutil.copy(process_dir+'/Cards/param_card_default.dat', param_card_reweight)
    shutil.copy(process_dir+'/Cards/param_card.dat', param_card_reweight)
    param_card_rwt_new=process_dir+'/Cards/param_card_rwt_%s.dat' % rw_name
    modify_param_card(param_card_input=param_card_reweight, process_dir=process_dir,params=params_rwt, output_location=param_card_rwt_new)

    rwcard.write("launch --rwgt_info=%s\n" % rw_name)
    rwcard.write("%s\n" % param_card_rwt_new)
  rwcard.close()


print_cards()

#---------------------------------------------------------------------------
# Generate the events
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#---------------------------------------------------------------------------
# Metadata
#---------------------------------------------------------------------------
if initialGluons:
  evgenConfig.process = "g g > xd xd~ l+ l-"
  initialStateString = "gluon fusion"
else:
  evgenConfig.process = "p p > xd xd~ l+ l-"
  initialStateString = "b quark annihilation"

evgenConfig.description = "Pseudoscalar Mediator simplified Model for mono-Z(-> l+ l-) " +initialStateString + " initiated process  \
with tan(beta) = " + str(THDMparams['TANBETA']) + ", sin(theta) = " + str(THDMparams['SINP']) + ", mA = " + str(THDMparams['MH3']) + ", ma"\
+ str(THDMparams['MH4'])
evgenConfig.keywords = ["exotic","BSMHiggs","Higgs","WIMP", "simplifiedModel"]
evgenConfig.contact = ["Kristian Bjoerke <kristian.bjoerke@cern.ch>, Lailin Xu <lailin.xu@cern.ch>, Jing Li <j.li@cern.ch>"]

#---------------------------------------------------------------------------
# Shower
#---------------------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Teach pythia about the dark matter particle
genSeq.Pythia8.Commands += ["SLHA:allowUserOverride = on",
                            "1000022:all = xd xd~ 2 0 0 %d 0.0 0.0 0.0 0.0" % (int(THDMparams['MXD'])),
                            "1000022:isVisible = false",
                            "1000022:mayDecay = off"
                            ]
