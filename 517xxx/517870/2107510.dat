# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.05.2022,  10:42
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.93070429E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.67227996E+03  # scale for input parameters
    1   -3.06193160E+02  # M_1
    2    1.51445492E+03  # M_2
    3    1.52187422E+03  # M_3
   11   -5.35787731E+03  # A_t
   12   -1.72777986E+03  # A_b
   13    9.44966840E+02  # A_tau
   23   -1.20579011E+03  # mu
   25    1.85103567E+01  # tan(beta)
   26    9.06777929E+02  # m_A, pole mass
   31    1.07028213E+03  # M_L11
   32    1.07028213E+03  # M_L22
   33    2.53559085E+02  # M_L33
   34    4.91334647E+02  # M_E11
   35    4.91334647E+02  # M_E22
   36    9.37798976E+02  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.88679174E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.55041855E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.80616200E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.67227996E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.67227996E+03  # (SUSY scale)
  1  1     8.39659842E-06   # Y_u(Q)^DRbar
  2  2     4.26547200E-03   # Y_c(Q)^DRbar
  3  3     1.01437653E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.67227996E+03  # (SUSY scale)
  1  1     3.12319009E-04   # Y_d(Q)^DRbar
  2  2     5.93406117E-03   # Y_s(Q)^DRbar
  3  3     3.09722590E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.67227996E+03  # (SUSY scale)
  1  1     5.45019509E-05   # Y_e(Q)^DRbar
  2  2     1.12692749E-02   # Y_mu(Q)^DRbar
  3  3     1.89530665E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.67227996E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -5.35787722E+03   # A_t(Q)^DRbar
Block Ad Q=  2.67227996E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.72777986E+03   # A_b(Q)^DRbar
Block Ae Q=  2.67227996E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     9.44966841E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.67227996E+03  # soft SUSY breaking masses at Q
   1   -3.06193160E+02  # M_1
   2    1.51445492E+03  # M_2
   3    1.52187422E+03  # M_3
  21   -6.87922925E+05  # M^2_(H,d)
  22   -1.34426890E+06  # M^2_(H,u)
  31    1.07028213E+03  # M_(L,11)
  32    1.07028213E+03  # M_(L,22)
  33    2.53559085E+02  # M_(L,33)
  34    4.91334647E+02  # M_(E,11)
  35    4.91334647E+02  # M_(E,22)
  36    9.37798976E+02  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.88679174E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.55041855E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.80616200E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.26032808E+02  # h0
        35     9.06823977E+02  # H0
        36     9.06777929E+02  # A0
        37     9.09637712E+02  # H+
   1000001     1.01179963E+04  # ~d_L
   2000001     1.00944095E+04  # ~d_R
   1000002     1.01176469E+04  # ~u_L
   2000002     1.00973142E+04  # ~u_R
   1000003     1.01179990E+04  # ~s_L
   2000003     1.00944129E+04  # ~s_R
   1000004     1.01176496E+04  # ~c_L
   2000004     1.00973160E+04  # ~c_R
   1000005     2.87021616E+03  # ~b_1
   2000005     4.85261811E+03  # ~b_2
   1000006     2.45118012E+03  # ~t_1
   2000006     2.91332331E+03  # ~t_2
   1000011     1.09223798E+03  # ~e_L-
   2000011     5.07656387E+02  # ~e_R-
   1000012     1.08899817E+03  # ~nu_eL
   1000013     1.09223878E+03  # ~mu_L-
   2000013     5.07646910E+02  # ~mu_R-
   1000014     1.08899546E+03  # ~nu_muL
   1000015     3.13008686E+02  # ~tau_1-
   2000015     9.46253291E+02  # ~tau_2-
   1000016     3.05609641E+02  # ~nu_tauL
   1000021     1.87001748E+03  # ~g
   1000022     3.02423621E+02  # ~chi_10
   1000023     1.21177966E+03  # ~chi_20
   1000025     1.22196427E+03  # ~chi_30
   1000035     1.57917157E+03  # ~chi_40
   1000024     1.21293300E+03  # ~chi_1+
   1000037     1.57918226E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -5.32045185E-02   # alpha
Block Hmix Q=  2.67227996E+03  # Higgs mixing parameters
   1   -1.20579011E+03  # mu
   2    1.85103567E+01  # tan[beta](Q)
   3    2.43330742E+02  # v(Q)
   4    8.22246213E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     3.10573805E-01   # Re[R_st(1,1)]
   1  2     9.50549268E-01   # Re[R_st(1,2)]
   2  1    -9.50549268E-01   # Re[R_st(2,1)]
   2  2     3.10573805E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99996183E-01   # Re[R_sb(1,1)]
   1  2     2.76299895E-03   # Re[R_sb(1,2)]
   2  1    -2.76299895E-03   # Re[R_sb(2,1)]
   2  2    -9.99996183E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.98849250E-01   # Re[R_sta(1,1)]
   1  2     4.79601527E-02   # Re[R_sta(1,2)]
   2  1    -4.79601527E-02   # Re[R_sta(2,1)]
   2  2    -9.98849250E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99158504E-01   # Re[N(1,1)]
   1  2    -5.60203243E-04   # Re[N(1,2)]
   1  3     3.93304949E-02   # Re[N(1,3)]
   1  4    -1.16224651E-02   # Re[N(1,4)]
   2  1     1.94270496E-02   # Re[N(2,1)]
   2  2     1.48256916E-01   # Re[N(2,2)]
   2  3     7.01539791E-01   # Re[N(2,3)]
   2  4     6.96767104E-01   # Re[N(2,4)]
   3  1    -3.60212568E-02   # Re[N(3,1)]
   3  2     2.11220084E-02   # Re[N(3,2)]
   3  3    -7.05817358E-01   # Re[N(3,3)]
   3  4     7.07162066E-01   # Re[N(3,4)]
   4  1     2.70964127E-03   # Re[N(4,1)]
   4  2    -9.88723133E-01   # Re[N(4,2)]
   4  3     9.00937884E-02   # Re[N(4,3)]
   4  4     1.19592363E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.25807888E-01   # Re[U(1,1)]
   1  2    -9.92054623E-01   # Re[U(1,2)]
   2  1     9.92054623E-01   # Re[U(2,1)]
   2  2    -1.25807888E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.67580585E-01   # Re[V(1,1)]
   1  2     9.85858381E-01   # Re[V(1,2)]
   2  1     9.85858381E-01   # Re[V(2,1)]
   2  2     1.67580585E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     1.05977321E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     1.17020064E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99999974E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     1.05971121E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     1.17148483E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.98907626E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.09237343E-03    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.66090858E-03   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.99977262E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     5.70451351E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     6.69350084E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.62092557E-01    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     7.87489270E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     8.98084325E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     1.16095847E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     9.99770799E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
     2.29201287E-04    3     1000016        11       -15   # BR(~nu_e -> ~nu_tau e^- tau^+)
DECAY   1000014     1.16095465E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.99770768E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
     2.29231837E-04    3     1000016        13       -15   # BR(~nu_mu -> ~nu_tau mu^- tau^+)
DECAY   1000016     1.64532509E-04   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
DECAY   2000001     7.48660009E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.50148210E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92486172E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.73647214E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.60071401E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.03833892E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     4.74803963E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.56759670E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     9.55085015E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.52766909E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.48673992E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.50135911E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92468017E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.73657898E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.60070737E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.04219567E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     4.74798913E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.57153053E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     9.55074790E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.52756739E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.65647619E+02   # ~b_1
#    BR                NDA      ID1      ID2
     2.40217986E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.20984894E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.10744686E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.63927809E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.29305321E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     8.01889996E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     4.52990778E-01    2     1000021         5   # BR(~b_1 -> ~g b)
     1.75546982E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     2.98486104E+02   # ~b_2
#    BR                NDA      ID1      ID2
     9.03316884E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.34053806E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.35748886E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.20363584E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.67317756E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.29102585E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     9.30418457E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     4.68757012E-04    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     1.47068841E-04    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     2.26710615E-03    2     2000006       -37   # BR(~b_2 -> ~t_2 H^-)
     2.72318653E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.21235544E-03    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     5.28950962E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     1.21119145E-03    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     7.65786465E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.93433313E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.70608572E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.73636511E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.62096498E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.13990051E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.73865819E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     2.78137027E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.43167338E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.52743741E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.65793685E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.93430594E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.70599578E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.73647164E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.62094613E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.14186079E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.73860778E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     2.78907392E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.43157317E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.52733567E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     9.57987707E+01   # ~t_1
#    BR                NDA      ID1      ID2
     4.92210231E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.42644818E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.39062513E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.28675869E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.84504647E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.33330929E-04    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     3.83773929E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.73274172E+02   # ~t_2
#    BR                NDA      ID1      ID2
     6.12297481E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.10793299E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.17880589E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.16833827E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.90284151E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     7.64707465E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.14747476E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     9.93062477E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.03966869E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.53338562E+00   # chi^+_1
#    BR                NDA      ID1      ID2
     1.97079781E-03    2    -1000011        12   # BR(chi^+_1 -> ~e^+_L nu_e)
     6.69085741E-04    2    -2000013        14   # BR(chi^+_1 -> ~mu^+_R nu_mu)
     1.97077305E-03    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     5.87935068E-02    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     4.07159616E-02    2    -2000015        16   # BR(chi^+_1 -> ~tau^+_2 nu_tau)
     3.67671749E-03    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     3.71318702E-03    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
     3.28826996E-01    2     1000016       -15   # BR(chi^+_1 -> ~nu_tau tau^+)
     5.40656292E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
     9.09698899E-03    2          37   1000022   # BR(chi^+_1 -> H^+ chi^0_1)
#    BR                NDA      ID1      ID2       ID3
     9.90967713E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     2.75980076E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     6.74439180E-02    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     6.74437368E-02    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     2.27461496E-01    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     7.19400640E-04    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     6.73296779E-02    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     6.73306735E-02    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     2.27132579E-01    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     1.48780277E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     7.06055482E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     7.36026574E-02    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     6.55304882E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     6.31062021E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.21532776E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     4.15697013E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.81497500E+00   # chi^0_2
#    BR                NDA      ID1      ID2
     4.30350876E-04    2     2000011       -11   # BR(chi^0_2 -> ~e^-_R e^+)
     4.30350876E-04    2    -2000011        11   # BR(chi^0_2 -> ~e^+_R e^-)
     1.30089230E-03    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     1.30089230E-03    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     7.12681908E-04    2     2000013       -13   # BR(chi^0_2 -> ~mu^-_R mu^+)
     7.12681908E-04    2    -2000013        13   # BR(chi^0_2 -> ~mu^+_R mu^-)
     1.31522230E-03    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     1.31522230E-03    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     1.38791743E-01    2     1000015       -15   # BR(chi^0_2 -> ~tau^-_1 tau^+)
     1.38791743E-01    2    -1000015        15   # BR(chi^0_2 -> ~tau^+_1 tau^-)
     1.71844093E-02    2     2000015       -15   # BR(chi^0_2 -> ~tau^-_2 tau^+)
     1.71844093E-02    2    -2000015        15   # BR(chi^0_2 -> ~tau^+_2 tau^-)
     1.03380908E-03    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     1.03380908E-03    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     1.03385228E-03    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     1.03385228E-03    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     2.44928087E-02    2     1000016       -16   # BR(chi^0_2 -> ~nu_tau nu_bar_tau)
     2.44928087E-02    2    -1000016        16   # BR(chi^0_2 -> ~nu^*_tau nu_tau)
     3.53614788E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     2.57138818E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     1.30939431E-02    2     1000022        35   # BR(chi^0_2 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.52252354E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
DECAY   1000025     1.32037514E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.06535132E-03    2     2000011       -11   # BR(chi^0_3 -> ~e^-_R e^+)
     2.06535132E-03    2    -2000011        11   # BR(chi^0_3 -> ~e^+_R e^-)
     2.46448749E-03    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     2.46448749E-03    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     1.41852372E-01    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     1.41852372E-01    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     2.73161724E-02    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     2.73161724E-02    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     1.40534992E-04    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     1.40534992E-04    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     1.40540391E-04    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     1.40540391E-04    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     2.91640919E-03    2     1000016       -16   # BR(chi^0_3 -> ~nu_tau nu_bar_tau)
     2.91640919E-03    2    -1000016        16   # BR(chi^0_3 -> ~nu^*_tau nu_tau)
     1.49879617E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     6.20024011E-04    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     4.57455469E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     3.21325912E-02    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     6.06352711E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     2.96928157E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     3.10406669E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     3.10406669E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     3.10407662E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     3.10407662E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     1.04842818E-01    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     1.04842818E-01    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     3.37303436E-04    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     3.37303436E-04    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     3.15631503E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     3.15631503E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     3.15634352E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     3.15634352E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     1.06320247E-01    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     1.06320247E-01    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     6.10500945E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     6.10500945E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     9.85709478E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.37979442E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     6.98090996E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     8.82551701E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     5.69750865E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     7.38081000E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.94379682E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.94379682E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     4.02180451E-03   # ~g
#    BR                NDA      ID1      ID2
     5.73696031E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     2.41064529E-02    2     1000023        21   # BR(~g -> chi^0_2 g)
     2.26380836E-02    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.07616887E-03    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     1.07616812E-03    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     3.93702221E-01    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.15932756E-04    3     1000022         1        -1   # BR(~g -> chi^0_1 d d_bar)
     3.15933515E-04    3     1000022         3        -3   # BR(~g -> chi^0_1 s s_bar)
     1.97497550E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.08502823E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     7.20487217E-03    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.05328646E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     6.10515892E-03    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     9.46872995E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     1.53689643E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.53689643E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     4.29311976E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     4.29311976E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.68365909E-03   # Gamma(h0)
     2.55051291E-03   2        22        22   # BR(h0 -> photon photon)
     1.72760803E-03   2        22        23   # BR(h0 -> photon Z)
     3.31027500E-02   2        23        23   # BR(h0 -> Z Z)
     2.67981026E-01   2       -24        24   # BR(h0 -> W W)
     7.79326392E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.76823543E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.12100204E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.10491758E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.28836627E-07   2        -2         2   # BR(h0 -> Up up)
     2.50059969E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.52989972E-07   2        -1         1   # BR(h0 -> Down down)
     2.00002284E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.30237502E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.05130219E+00   # Gamma(HH)
     7.23028845E-08   2        22        22   # BR(HH -> photon photon)
     1.89017270E-08   2        22        23   # BR(HH -> photon Z)
     7.80452993E-05   2        23        23   # BR(HH -> Z Z)
     1.16188860E-04   2       -24        24   # BR(HH -> W W)
     5.79482697E-05   2        21        21   # BR(HH -> gluon gluon)
     1.03233907E-08   2       -11        11   # BR(HH -> Electron electron)
     4.59476962E-04   2       -13        13   # BR(HH -> Muon muon)
     1.21993800E-01   2       -15        15   # BR(HH -> Tau tau)
     1.44181340E-12   2        -2         2   # BR(HH -> Up up)
     2.79587294E-07   2        -4         4   # BR(HH -> Charm charm)
     1.91100629E-02   2        -6         6   # BR(HH -> Top top)
     9.88273625E-07   2        -1         1   # BR(HH -> Down down)
     3.57385142E-04   2        -3         3   # BR(HH -> Strange strange)
     8.55696194E-01   2        -5         5   # BR(HH -> Bottom bottom)
     3.02423274E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     8.78806067E-04   2        25        25   # BR(HH -> h0 h0)
     8.93290134E-04   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
DECAY        36     3.99054099E+00   # Gamma(A0)
     3.38364214E-07   2        22        22   # BR(A0 -> photon photon)
     1.54467263E-07   2        22        23   # BR(A0 -> photon Z)
     1.86841850E-04   2        21        21   # BR(A0 -> gluon gluon)
     1.03016103E-08   2       -11        11   # BR(A0 -> Electron electron)
     4.58507703E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.21742166E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.44269668E-12   2        -2         2   # BR(A0 -> Up up)
     2.79562575E-07   2        -4         4   # BR(A0 -> Charm charm)
     2.25930240E-02   2        -6         6   # BR(A0 -> Top top)
     9.86192398E-07   2        -1         1   # BR(A0 -> Down down)
     3.56632412E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.53942892E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.99445439E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.18721652E-04   2        23        25   # BR(A0 -> Z h0)
     2.20782362E-36   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.36977257E+00   # Gamma(Hp)
     1.28322204E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.48617488E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.55179151E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     9.57685488E-07   2        -1         2   # BR(Hp -> Down up)
     1.59188715E-05   2        -3         2   # BR(Hp -> Strange up)
     9.12999606E-06   2        -5         2   # BR(Hp -> Bottom up)
     5.91253209E-08   2        -1         4   # BR(Hp -> Down charm)
     3.45389807E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.27852206E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.54201778E-06   2        -1         6   # BR(Hp -> Down top)
     3.40913053E-05   2        -3         6   # BR(Hp -> Strange top)
     8.41832135E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.10302194E-04   2        24        25   # BR(Hp -> W h0)
     5.47553831E-11   2        24        35   # BR(Hp -> W HH)
     5.93819475E-11   2        24        36   # BR(Hp -> W A0)
     6.44171119E-04   2  -1000015   1000016   # BR(Hp -> Stau1 snu_tau1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.71812445E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.42661493E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.42633305E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00008227E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.83630467E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.91857209E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.71812445E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.42661493E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.42633305E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999412E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.88022366E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999412E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.88022366E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25514480E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    5.25469213E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.06659434E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.88022366E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999412E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.80968615E-04   # BR(b -> s gamma)
    2    1.58947746E-06   # BR(b -> s mu+ mu-)
    3    3.52939904E-05   # BR(b -> s nu nu)
    4    2.22279909E-15   # BR(Bd -> e+ e-)
    5    9.49554251E-11   # BR(Bd -> mu+ mu-)
    6    1.98775291E-08   # BR(Bd -> tau+ tau-)
    7    7.47804860E-14   # BR(Bs -> e+ e-)
    8    3.19461892E-09   # BR(Bs -> mu+ mu-)
    9    6.77584619E-07   # BR(Bs -> tau+ tau-)
   10    9.36797827E-05   # BR(B_u -> tau nu)
   11    9.67676290E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42771887E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93798042E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16141199E-03   # epsilon_K
   17    2.28170295E-15   # Delta(M_K)
   18    2.48206282E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29557179E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.23777400E-15   # Delta(g-2)_electron/2
   21    9.56882427E-11   # Delta(g-2)_muon/2
   22    6.63161161E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.83520205E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.51046952E-01   # C7
     0305 4322   00   2    -8.84453108E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.54723712E-01   # C8
     0305 6321   00   2    -9.52729196E-04   # C8'
 03051111 4133   00   0     1.61223903E+00   # C9 e+e-
 03051111 4133   00   2     1.61316567E+00   # C9 e+e-
 03051111 4233   00   2     4.87908941E-05   # C9' e+e-
 03051111 4137   00   0    -4.43493113E+00   # C10 e+e-
 03051111 4137   00   2    -4.43731200E+00   # C10 e+e-
 03051111 4237   00   2    -3.68387218E-04   # C10' e+e-
 03051313 4133   00   0     1.61223903E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61316482E+00   # C9 mu+mu-
 03051313 4233   00   2     4.87898098E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43493113E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43731285E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.68386291E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50587509E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     7.99135123E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50587509E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     7.99137385E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50587468E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     7.99772520E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85779150E-07   # C7
     0305 4422   00   2    -8.56276356E-06   # C7
     0305 4322   00   2    -1.62696179E-06   # C7'
     0305 6421   00   0     3.30445355E-07   # C8
     0305 6421   00   2     1.01440141E-05   # C8
     0305 6321   00   2    -5.64917378E-07   # C8'
 03051111 4133   00   2    -3.43264506E-07   # C9 e+e-
 03051111 4233   00   2     1.85799526E-06   # C9' e+e-
 03051111 4137   00   2     3.40512685E-06   # C10 e+e-
 03051111 4237   00   2    -1.40404615E-05   # C10' e+e-
 03051313 4133   00   2    -3.43267110E-07   # C9 mu+mu-
 03051313 4233   00   2     1.85799451E-06   # C9' mu+mu-
 03051313 4137   00   2     3.40512156E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.40404635E-05   # C10' mu+mu-
 03051212 4137   00   2    -5.94985846E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     3.04587543E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.94985845E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     3.04587543E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -6.02456785E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     3.04587154E-06   # C11' nu_3 nu_3
